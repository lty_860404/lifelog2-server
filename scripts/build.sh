#!/bin/bash
BASE_DIR=`dirname $0`/../
ENV_NAME=prod
if [ "$1"=="qaci" ]
then
	ENV_NAME="qaci"
fi

LOG_FILE="/var/log/webapp/${ENV_NAME}/lifelog.log"
cat ${BASE_DIR}/src/main/resources/logback.xml.tmplate | sed "s|log/lifelog.log|${LOG_FILE}|g" > /tmp/logback.xml
mv /tmp/logback.xml ${BASE_DIR}/src/main/resources/logback.xml

cat << '__EOF__'
.______    __    __   __   __       _______      __       __   _______  _______  __        ______     _______ 
|   _  \  |  |  |  | |  | |  |     |       \    |  |     |  | |   ____||   ____||  |      /  __  \   /  _____|
|  |_)  | |  |  |  | |  | |  |     |  .--.  |   |  |     |  | |  |__   |  |__   |  |     |  |  |  | |  |  __  
|   _  <  |  |  |  | |  | |  |     |  |  |  |   |  |     |  | |   __|  |   __|  |  |     |  |  |  | |  | |_ | 
|  |_)  | |  `--'  | |  | |  `----.|  '--'  |   |  `----.|  | |  |     |  |____ |  `----.|  `--'  | |  |__| | 
|______/   \______/  |__| |_______||_______/    |_______||__| |__|     |_______||_______| \______/   \______| 

==============================================================================================================
__EOF__

(cd ${BASE_DIR} && ./gradlew build)


package com.dodopipe.lifelog.persistence.repositories;

import com.dodopipe.lifelog.config.DBUnitDataSourceConfiguration;
import com.dodopipe.lifelog.config.DataSourceConfiguration;
import com.dodopipe.lifelog.config.JPAConfiguration;
import com.dodopipe.lifelog.config.LifelogActiveProfilesResolver;
import com.dodopipe.lifelog.persistence.domain.activity.ActivityAttendeeEntity;
import com.dodopipe.lifelog.persistence.domain.activity.ActivityEntity;
import com.dodopipe.lifelog.persistence.domain.activity.WeddingActivityDetailEntity;
import com.dodopipe.lifelog.persistence.domain.activity.WeddingActivityEntity;
import com.dodopipe.lifelog.persistence.repositories.activity.ActivityRepository;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import static com.dodopipe.lifelog.persistence.repositories.fixture.RepositoryDataFixture.generateActivityAttendees;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by henryhome on 7/11/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPAConfiguration.class,
                                 DataSourceConfiguration.class,
                                 DBUnitDataSourceConfiguration.class})
@ActiveProfiles(resolver = LifelogActiveProfilesResolver.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
@DatabaseSetup("DB_SETUP.xml")
public class ActivityRepositoryTest {

    @Autowired
    ActivityRepository activityRepo;

    @Test
    public void testCreateActivity()
            throws
            Exception {

        ActivityEntity activity = new ActivityEntity();
        activity.setName("Test activity");
        activityRepo.saveAndFlush(activity);
        assertNotNull(activity.getId());
    }

    @Test
    public void testFindActivityById()
            throws
            Exception {

        ActivityEntity activity = activityRepo.findById(1000l);
        assertNotNull(activity.getId());
    }

    @Test
    public void testCreateWeddingActivity()
            throws
            Exception {

        WeddingActivityEntity activity = new WeddingActivityEntity();
        activity.setName("Test wedding activity");
        activityRepo.saveAndFlush(activity);
        assertNotNull(activity.getId());
    }

    @Test
    public void testFindWeddingActivityById()
            throws
            Exception {

        ActivityEntity activity = activityRepo.findById(3333l);
        assertNotNull(activity.getId());
    }

    @Test
    public void testViewAllAttendingWeddingActivities() {

        Long userId = 3000l;

        Calendar cal = GregorianCalendar.getInstance();
        cal.set(2008,
                Calendar.MARCH,
                5,
                16,
                48,
                40);

        PageRequest pageRequest = new PageRequest(0,
                                                  20,
                                                  new Sort(Sort.Direction.ASC,
                                                           "endTime"));
        List<WeddingActivityDetailEntity> weddingActivities =
                activityRepo.findWeddingActivitiesEndAfterDateForUser(userId,
                                                                      cal.getTime(),
                                                                      pageRequest);

        for (WeddingActivityDetailEntity wed : weddingActivities) {
            System.out.println("id => " + wed.getWeddingActivityEntity()
                                             .getId());
            System.out.println("endTime => " + wed.getWeddingActivityEntity()
                                                  .getEndTime());
            System.out.println("isCurrUserHost => " + wed.isCurrUserHost());
        }

        assertEquals(2,
                     weddingActivities.size());
        assertEquals(5555,
                     weddingActivities.get(0)
                                      .getWeddingActivityEntity()
                                      .getId()
                                      .intValue());
        assertEquals(6666,
                     weddingActivities.get(1)
                                      .getWeddingActivityEntity()
                                      .getId()
                                      .intValue());
    }

    @Test
    public void testViewAllAttendedWeddingActivities() {

        Long userId = 3000l;

        Calendar cal = GregorianCalendar.getInstance();
        cal.set(2008,
                Calendar.MARCH,
                5,
                16,
                48,
                40);

        PageRequest pageRequest = new PageRequest(0,
                                                  20,
                                                  new Sort(Sort.Direction.DESC,
                                                           "endTime"));
        List<WeddingActivityDetailEntity> weddingActivities =
                activityRepo.findWeddingActivitiesEndBeforeDateForUser(userId,
                                                                       cal.getTime(),
                                                                       pageRequest);

        for (WeddingActivityDetailEntity wed : weddingActivities) {
            System.out.println("id => " + wed.getWeddingActivityEntity()
                                             .getId());
            System.out.println("endTime => " + wed.getWeddingActivityEntity()
                                                  .getEndTime());
            System.out.println("isCurrUserHost => " + wed.isCurrUserHost());
        }

        assertEquals(2,
                     weddingActivities.size());
        assertEquals(3333,
                     weddingActivities.get(0)
                                      .getWeddingActivityEntity()
                                      .getId()
                                      .intValue());
        assertEquals(4444,
                     weddingActivities.get(1)
                                      .getWeddingActivityEntity()
                                      .getId()
                                      .intValue());
    }

    @Test
    public void testSaveActivity() {
        ActivityEntity activity = new WeddingActivityEntity();
        activity.setId(80l);

        List<ActivityAttendeeEntity> attendees = generateActivityAttendees(5);

        activity.setAttendees(attendees);
        activityRepo.saveAndFlush(activity);

        ActivityEntity selected = activityRepo.findWeddingActivityById(80l);
        assertEquals(5, selected.getAttendees().size());
        assertEquals(80, selected.getAttendees().get(0).getActivityId().intValue());
    }
}




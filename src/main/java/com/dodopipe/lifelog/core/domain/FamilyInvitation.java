package com.dodopipe.lifelog.core.domain;

/**
 * @author tianyin.luo
 */
public class FamilyInvitation {

    private Long id;

    private Long inviterId;

    private Long inviteeId;

    private String inviteePhone;

    private String relationship;

    private FamilyInvitationStatus status;

    public FamilyInvitation() {

    }

    public FamilyInvitation(Long id,
                            Long inviterId,
                            Long inviteeId,
                            String inviteePhone,
                            String relationship,
                            FamilyInvitationStatus status) {
        this.id = id;
        this.inviterId = inviterId;
        this.inviteeId = inviteeId;
        this.inviteePhone = inviteePhone;
        this.relationship = relationship;
        this.status = status;
    }

    public Long getId() {

        return id;
    }

    public String getRelationship() {

        return relationship;
    }

    public Long getInviterId() {

        return inviterId;
    }

    public Long getInviteeId() {

        return inviteeId;
    }

    public String getInviteePhone() {

        return inviteePhone;
    }

    public FamilyInvitationStatus getStatus() {

        return status;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FamilyInvitation that = (FamilyInvitation) o;

        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {

        return id != null ? id.hashCode() : 0;
    }
}

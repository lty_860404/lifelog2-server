package com.dodopipe.lifelog.core.events.auth;

import com.dodopipe.lifelog.core.events.CreateEvent;

public class UserRegisterEvent
        extends CreateEvent {

    private final String verificationCode;
    private final String countryCode;
    private final String phone;
    private final String password;
    private final String langCode;

    public UserRegisterEvent(final String verificationCode,
                             final String countryCode,
                             final String phone,
                             final String password,
                             final String langCode) {

        this.verificationCode = verificationCode;
        this.countryCode = countryCode;
        this.phone = phone;
        this.password = password;
        this.langCode = langCode;
    }

    public String getVerificationCode() {

        return verificationCode;
    }

    public String getCountryCode() {

        return countryCode;
    }

    public String getPhone() {

        return phone;
    }

    public String getPassword() {

        return password;
    }

    public String getLangCode() {

        return langCode;
    }
}




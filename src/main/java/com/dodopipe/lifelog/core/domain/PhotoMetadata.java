package com.dodopipe.lifelog.core.domain;

public class PhotoMetadata {

    private String originalFileKey;
    private String thumbnailKey;
    private String contentType;

    public PhotoMetadata(String originalFileKey,
                         String thumbnailKey,
                         String contentType) {

        this.originalFileKey = originalFileKey;
        this.thumbnailKey = thumbnailKey;
        this.contentType = contentType;
    }


    public String getThumbnailKey() {

        return thumbnailKey;
    }

    public String getOriginalFileKey() {

        return originalFileKey;
    }

    public String getContentType() {

        return contentType;
    }

}

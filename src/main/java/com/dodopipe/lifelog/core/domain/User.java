package com.dodopipe.lifelog.core.domain;

import java.util.Date;

/**
 * @author tianyin.luo
 */
public class User {

    private Long id;

    private String username;

    private String nickname;

    private Long familyId;

    private String countryCode;

    private String phone;

    private String gender;

    private String signature;

    private String avatarFilePath;

    private String langCode;

    private Date registerTime;


    public User() {

    }

    public User(Long id,
                String username,
                String nickname,
                Long familyId,
                String countryCode,
                String phone,
                String gender,
                String signature,
                String avatarFilePath,
                String langCode,
                Date registerTime) {

        this.id = id;
        this.username = username;
        this.nickname = nickname;
        this.familyId = familyId;
        this.countryCode = countryCode;
        this.phone = phone;
        this.gender = gender;
        this.signature = signature;
        this.avatarFilePath = avatarFilePath;
        this.langCode = langCode;
        this.registerTime = registerTime;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {

        this.username = username;
    }

    public String getNickname() {

        return nickname;
    }

    public void setNickname(String nickname) {

        this.nickname = nickname;
    }

    public Long getFamilyId() {

        return familyId;
    }

    public void setFamilyId(Long familyId) {

        this.familyId = familyId;
    }

    public String getCountryCode() {

        return countryCode;
    }

    public void setCountryCode(String countryCode) {

        this.countryCode = countryCode;
    }

    public String getPhone() {

        return phone;
    }

    public void setPhone(String phone) {

        this.phone = phone;
    }

    public String getGender() {

        return gender;
    }

    public void setGender(String gender) {

        this.gender = gender;
    }

    public String getSignature() {

        return signature;
    }

    public void setSignature(String signature) {

        this.signature = signature;
    }

    public String getAvatarFilePath() {

        return avatarFilePath;
    }

    public void setAvatarFilePath(String avatarFilePath) {

        this.avatarFilePath = avatarFilePath;
    }

    public String getLangCode() {

        return langCode;
    }

    public void setLangCode(String langCode) {

        this.langCode = langCode;
    }

    public Date getRegisterTime() {

        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {

        this.registerTime = registerTime;
    }
}

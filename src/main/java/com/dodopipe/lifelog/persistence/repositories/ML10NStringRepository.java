package com.dodopipe.lifelog.persistence.repositories;

import com.dodopipe.lifelog.persistence.domain.MLocalizationStringEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ML10NStringRepository
        extends JpaRepository<MLocalizationStringEntity, Integer> {

    @Query("select s.propValue from M_LOCALIZATION_STRING s where s.typeCode = ?1 and s.languageCode = ?2")
    public List<String> findByType(int typeCode,
                                   String langCode);

    @Query("select s.propValue from M_LOCALIZATION_STRING s where s.typeCode = ?1 and s.propName = ?2 and s.languageCode = ?3")
    public String findByTypeAndKey(int typeCode,
                                   String key,
                                   String langCode);

}

package com.dodopipe.lifelog.rest.controller.modules.photo;

import com.dodopipe.lifelog.core.domain.BlobObject;
import com.dodopipe.lifelog.core.events.photo.PhotoCreateEvent;
import com.dodopipe.lifelog.core.services.PhotoCommandService;
import com.dodopipe.lifelog.rest.controller.PhotoCommandController;
import com.dodopipe.lifelog.rest.controller.fixture.UserAsPrincipal;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.dodopipe.lifelog.rest.controller.fixture.RestDataFixture.imageData;
import static com.dodopipe.lifelog.rest.controller.fixture.RestEventFixture.createdSinglePhotoEvent;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class UploadPhotosTest {

    MockMvc mockMvc;

    @InjectMocks
    PhotoCommandController controller;

    @Mock
    PhotoCommandService photoCommandService;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                                  .build();

    }

    @Test
    public void uploadSinglePhoto()
            throws
            Exception {

        when(photoCommandService.uploadPhotos(any(PhotoCreateEvent.class)))
                         .thenReturn(createdSinglePhotoEvent());

        BlobObject bo = imageData("test.jpg").get(0);
        byte[] data = new byte[(int) bo.getLength()];
        bo.getInputStream()
          .read(data);

        MockMultipartFile onePicture = new MockMultipartFile("images",
                                                             "test.jpg",
                                                             "image/jpeg",
                                                             data);
        this.mockMvc
                .perform(
                        MockMvcRequestBuilders.fileUpload("/photos")
                                              .file(onePicture)
                                              .param("comment",
                                                     "test comment")
                                              .principal(new UserAsPrincipal("1000"))
                        )
                .andExpect(status().isOk());


    }

    @Test
    public void uploadMultiplePhotos()
            throws
            Exception {

        when(photoCommandService.uploadPhotos(any(PhotoCreateEvent.class)))
                .thenReturn(createdSinglePhotoEvent());

        BlobObject bo = imageData("test.jpg").get(0);
        byte[] data = new byte[(int) bo.getLength()];
        bo.getInputStream()
          .read(data);

        MockMultipartFile onePicture = new MockMultipartFile("test.jpg",
                                                             "test.jpg",
                                                             "image/jpeg",
                                                             data);
        MockMultipartFile twoPicture = new MockMultipartFile("test1.jpg",
                                                             "test1.jpg",
                                                             "image/jpeg",
                                                             data);

        this.mockMvc.perform(
                MockMvcRequestBuilders.fileUpload("/photos")
                                      .file(onePicture)
                                      .file(twoPicture)
                                      .param("comment",
                                             "test comment")
                                      .principal(new UserAsPrincipal("1000"))
                            )
                    .andExpect(status().isOk());

    }

}




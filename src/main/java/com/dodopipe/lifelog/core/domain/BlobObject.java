package com.dodopipe.lifelog.core.domain;

import java.io.InputStream;

public class BlobObject {

    private InputStream in = null;

    private String contentType = null;

    private long length = -1;

    /**
     * @param in
     * @param contentType
     * @param length
     */
    public BlobObject(InputStream in,
                      String contentType,
                      long length) {

        this.in = in;
        this.contentType = contentType;
        this.length = length;
    }

    /**
     * @return
     */
    public InputStream getInputStream() {

        return in;
    }

    /**
     * @param in
     */
    public void setInputStream(InputStream in) {

        this.in = in;
    }

    /**
     * @return
     */
    public String getContentType() {

        return contentType;
    }

    /**
     * @param contentType
     */
    public void setContentType(String contentType) {

        this.contentType = contentType;
    }

    /**
     * @return
     */
    public long getLength() {

        return length;
    }

    /**
     * @param length
     */
    public void setLength(long length) {

        this.length = length;
    }


}

package com.dodopipe.lifelog.core.events.auth;

import com.dodopipe.lifelog.core.events.NotifyEvent;

public class VerificationCodeNotifyEvent extends NotifyEvent {

    private final String phone;

    public VerificationCodeNotifyEvent (String phone) {

        this.phone = phone;
    }

    public String getPhone() {

        return phone;
    }
}

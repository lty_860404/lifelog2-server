package com.dodopipe.lifelog.core.events.user;

/**
 * Created by henryhome on 8/12/14.
 */
public class PasswordUpdateEvent {

    private Long userId;
    private String password;

    public PasswordUpdateEvent(Long userId, String password) {
        this.userId = userId;
        this.password = password;
    }

    public Long getUserId() {

        return userId;
    }

    public String getPassword() {

        return password;
    }
}

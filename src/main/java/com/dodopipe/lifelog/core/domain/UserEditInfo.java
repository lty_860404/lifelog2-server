package com.dodopipe.lifelog.core.domain;

/**
 * Created by henryhome on 8/6/14.
 */
public class UserEditInfo {

    private String username;

    private String nickname;

    private String phone;

    private String gender;

    private String signature;

    private String avatarFilePath;

    private String langCode;

    public UserEditInfo() {

    }

    public UserEditInfo(String username,
                        String nickname,
                        String phone,
                        String gender,
                        String signature,
                        String avatarFilePath,
                        String langCode) {
        this.username = username;
        this.nickname = nickname;
        this.phone = phone;
        this.gender = gender;
        this.signature = signature;
        this.avatarFilePath = avatarFilePath;
        this.langCode = langCode;
    }

    public String getUsername() {

        return username;
    }

    public String getNickname() {

        return nickname;
    }

    public String getPhone() {

        return phone;
    }

    public String getGender() {

        return gender;
    }

    public String getSignature() {

        return signature;
    }

    public String getAvatarFilePath() {

        return avatarFilePath;
    }

    public String getLangCode() {

        return langCode;
    }
}

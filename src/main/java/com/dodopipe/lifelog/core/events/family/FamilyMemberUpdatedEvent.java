package com.dodopipe.lifelog.core.events.family;

import java.util.HashMap;
import java.util.Map;

/**
 * @author tianyin.luo
 */
public class FamilyMemberUpdatedEvent {

    private final boolean isSuccess;

    public FamilyMemberUpdatedEvent(boolean isSuccess) {

        this.isSuccess = isSuccess;
    }

    public boolean isSuccess() {

        return this.isSuccess;
    }

    public Map<String, Boolean> toMap() {

        Map<String, Boolean> map = new HashMap<>();
        map.put("success",
                isSuccess());
        return map;
    }
}




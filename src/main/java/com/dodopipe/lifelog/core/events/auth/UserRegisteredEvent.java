package com.dodopipe.lifelog.core.events.auth;

import com.dodopipe.lifelog.core.events.CreatedEvent;

import java.util.HashMap;
import java.util.Map;

public class UserRegisteredEvent
        extends CreatedEvent {

    private final String defaultUsername;
    private final String accessId;
    private final String secretKey;

    public static UserRegisteredEvent FAILED = new UserRegisteredEvent(null,
                                                               null,
                                                               null);

    public UserRegisteredEvent(final String username,
                               final String keyId,
                               final String key) {

        this.defaultUsername = username;
        this.accessId = keyId;
        this.secretKey = key;

    }

    public String getDefaultUsername() {

        return this.defaultUsername;
    }

    public String getAccessId() {

        return accessId;
    }

    public String getSecretKey() {

        return secretKey;
    }

    public Map<String, String> toMap() {

        Map<String, String> map = new HashMap<>();
        map.put("defaultUsername",
                this.getDefaultUsername());
        map.put("accessId",
                this.getAccessId());
        map.put("secretKey",
                this.getSecretKey());
        return map;
    }
}

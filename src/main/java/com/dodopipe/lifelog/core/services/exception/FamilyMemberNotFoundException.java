package com.dodopipe.lifelog.core.services.exception;

/**
 * Created by henryhome on 7/31/14.
 */
public class FamilyMemberNotFoundException extends ServiceException {

    private Long familyId;
    private Long memberId;

    public FamilyMemberNotFoundException(Long familyId,
                                         Long memberId) {

        this.familyId = familyId;
        this.memberId = memberId;
    }

    public FamilyMemberNotFoundException(Long familyId,
                                         Long memberId,
                                         Throwable cause) {
        super(cause);
        this.familyId = familyId;
        this.memberId = memberId;
    }

    public String getMessage() {

        return "The family member with family id = " + familyId + " and member id = " + memberId + " is not found";
    }
}




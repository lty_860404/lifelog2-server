package com.dodopipe.lifelog.rest.controller.fixture;

import com.dodopipe.lifelog.core.domain.*;
import com.dodopipe.lifelog.core.events.photo.PhotoViewedEvent;
import com.dodopipe.lifelog.core.events.photo.PhotosViewedEvent;

import java.io.IOException;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class RestDataFixture {

    public static List<BlobObject> imageData(String... names)
            throws
            IOException {

        ClassLoader cl = RestDataFixture.class.getClassLoader();

        List<BlobObject> rst = new ArrayList<>();
        for (String name : names) {
            URLConnection connect = cl.getResource(name)
                                      .openConnection();

            rst.add(new BlobObject(connect.getInputStream(),
                                   "image/jpeg",
                                   connect.getContentLengthLong()));
        }

        return rst;
    }

    public static PhotosViewedEvent generatePhotos() {

        List<PhotoDetail> photos = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            Photo photo = new Photo(1000l + i,
                                    "owner comment on this photo",
                                    new Date(),
                                    new Date(),
                                    10,
                                    generateLikes(2),
                                    generateComments(2),
                                    generatePhotoMetas(2));

            photos.add(new PhotoDetail(photo,
                                        "test_owner_username" + i,
                                        "test_owner_nickname" + i,
                                        "test_owner_avatar_file_path" + i,
                                        false));
        }
        return new PhotosViewedEvent(photos);
    }

    public static PhotosViewedEvent generateEmptyPhotos() {

        List<PhotoDetail> photos = Collections.emptyList();
        return new PhotosViewedEvent(photos);
    }

    public static PhotoViewedEvent generatePhoto() {

        Photo photo = new Photo(1000l,
                                "owner comment on this photo",
                                new Date(),
                                new Date(),
                                10,
                                generateLikes(2),
                                generateComments(2),
                                generatePhotoMetas(2));

        PhotoDetail photoDetail = new PhotoDetail(photo,
                                                     "test_owner_username",
                                                     "test_owner_nickname",
                                                     "test_owner_avatar_file_path",
                                                     true);

        return new PhotoViewedEvent(photoDetail);
    }

    private static List<PhotoLike> generateLikes(int num) {

        List<PhotoLike> likes = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            likes.add(new PhotoLike("abcde" + i,
                                    "abcde and edcba" + i));
        }

        return likes;
    }

    private static List<PhotoComment> generateComments(int num) {

        List<PhotoComment> comments = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            comments.add(new PhotoComment("abcde" + i,
                                          "abcde and edcba" + i,
                                          "abcde wants to play with edcba" + i));
        }

        return comments;
    }

    private static List<PhotoMetadata> generatePhotoMetas(int num) {

        List<PhotoMetadata> metadata = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            metadata.add(new PhotoMetadata("originalFileKey" + i,
                                           "thumbnailFileKey" + i,
                                           "photoContentType" + i));
        }

        return metadata;
    }

    public static Long[] generateActivityIds(int num) {

        Long[] activityIds = new Long[num];

        for (int i = 0; i < num; i++) {
            activityIds[i] = 450l + i;
        }

        return activityIds;
    }

    public static WeddingActivity generateWeddingActivity() {

        return new WeddingActivity(1000l,
                                   4000l,
                                   "test",
                                   null,
                                   null,
                                   null,
                                   null,
                                   null);
    }
}




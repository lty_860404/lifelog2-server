package com.dodopipe.lifelog.persistence.repositories.fixture;

import com.dodopipe.lifelog.persistence.domain.activity.ActivityAttendeeEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Henry Yan
 */
public class RepositoryDataFixture {

    public static List<ActivityAttendeeEntity> generateActivityAttendees(int num) {

        List<ActivityAttendeeEntity> attendees = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            attendees.add(new ActivityAttendeeEntity(1000l + 1000 * i,
                                                     80l,
                                                     null,
                                                     null,
                                                     null));
        }

        return attendees;
    }
}

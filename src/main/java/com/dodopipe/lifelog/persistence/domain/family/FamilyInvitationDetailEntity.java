package com.dodopipe.lifelog.persistence.domain.family;

import com.dodopipe.lifelog.core.domain.FamilyInvitationDetail;

/**
 * Created by henryhome on 8/13/14.
 */
public class FamilyInvitationDetailEntity {

    private String inviterUsername;
    private String inviterNickname;
    private String inviterPhone;
    private String inviterAvatarFilePath;
    private String inviteeUsername;
    private String inviteeNickname;
    private String inviteeAvatarFilePath;
    private FamilyInvitationEntity entity;

    public FamilyInvitationDetailEntity(String inviterUsername,
                                        String inviterNickname,
                                        String inviterPhone,
                                        String inviterAvatarFilePath,
                                        String inviteeUsername,
                                        String inviteeNickname,
                                        String inviteeAvatarFilePath,
                                        FamilyInvitationEntity entity) {
        this.inviterUsername = inviterUsername;
        this.inviterNickname = inviterNickname;
        this.inviterPhone = inviterPhone;
        this.inviterAvatarFilePath = inviterAvatarFilePath;
        this.inviteeUsername = inviteeUsername;
        this.inviteeNickname = inviteeNickname;
        this.inviteeAvatarFilePath = inviteeAvatarFilePath;
        this.entity = entity;
    }

    public FamilyInvitationDetail toFamilyInvitationDetail() {

        return new FamilyInvitationDetail(getInviterUsername(),
                                           getInviterNickname(),
                                           getInviterPhone(),
                                           getInviterAvatarFilePath(),
                                           getInviteeUsername(),
                                           getInviteeNickname(),
                                           getInviteeAvatarFilePath(),
                                           entity.toFamilyInvitation());
    }

    public String getInviterUsername() {

        return inviterUsername;
    }

    public void setInviterUsername(String inviterUsername) {

        this.inviterUsername = inviterUsername;
    }

    public String getInviterNickname() {

        return inviterNickname;
    }

    public void setInviterNickname(String inviterNickname) {

        this.inviterNickname = inviterNickname;
    }

    public String getInviterPhone() {

        return inviterPhone;
    }

    public void setInviterPhone(String inviterPhone) {

        this.inviterPhone = inviterPhone;
    }

    public String getInviterAvatarFilePath() {

        return inviterAvatarFilePath;
    }

    public void setInviterAvatarFilePath(String inviterAvatarFilePath) {

        this.inviterAvatarFilePath = inviterAvatarFilePath;
    }

    public String getInviteeUsername() {

        return inviteeUsername;
    }

    public void setInviteeUsername(String inviteeUsername) {

        this.inviteeUsername = inviteeUsername;
    }

    public String getInviteeNickname() {

        return inviteeNickname;
    }

    public void setInviteeNickname(String inviteeNickname) {

        this.inviteeNickname = inviteeNickname;
    }

    public String getInviteeAvatarFilePath() {

        return inviteeAvatarFilePath;
    }

    public void setInviteeAvatarFilePath(String inviteeAvatarFilePath) {

        this.inviteeAvatarFilePath = inviteeAvatarFilePath;
    }

    public FamilyInvitationEntity getEntity() {

        return entity;
    }

    public void setEntity(FamilyInvitationEntity entity) {

        this.entity = entity;
    }
}

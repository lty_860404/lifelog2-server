package com.dodopipe.lifelog.core.services.exception;

/**
 * @author Henry Yan
 */
public class UserNotFoundByPhoneException extends UserNotFoundException {

    private String phone;

    public UserNotFoundByPhoneException(String phone) {

        this.phone = phone;
    }

    public UserNotFoundByPhoneException(String phone,
                                        Throwable cause) {

        super(cause);
        this.phone = phone;
    }

    public String getMessage() {

        return "The user with phone = " + phone + " cannot be found";
    }
}

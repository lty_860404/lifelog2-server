package com.dodopipe.lifelog.config;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.util.StringUtils;
import org.springframework.web.context.ConfigurableWebApplicationContext;

import java.util.Arrays;

public class ContextProfileInitializer
        implements ApplicationContextInitializer<ConfigurableWebApplicationContext> {

    @Override
    public void initialize(ConfigurableWebApplicationContext applicationContext) {

        System.out.println("======== initialization");

        ConfigurableEnvironment env = applicationContext.getEnvironment();

        String profilesStr = env.getProperty("spring.profiles.active");
        String[] profiles = null;
        if (StringUtils.isEmpty(profilesStr)) {
            profiles = new String[]{LifelogProfile.QACI};
        } else {
            profiles = profilesStr.split(",");
        }

        System.out.println("============= active profile:" + Arrays.asList(profiles)
                                                                   .toString());
        env.setActiveProfiles(profiles);

    }

}

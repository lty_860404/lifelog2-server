package com.dodopipe.lifelog.persistence.domain.photo;

import com.dodopipe.lifelog.core.domain.PhotoComment;

/**
 * @author Henry Yan
 */
public class PhotoCommentDetailEntity {

    private PhotoCommentEntity photoCommentEntity;
    private String commentingUsername;
    private String commentingNickname;

    public PhotoCommentDetailEntity(PhotoCommentEntity photoCommentEntity,
                                    String commentingUsername,
                                    String commentingNickname) {

        this.photoCommentEntity = photoCommentEntity;
        this.commentingUsername = commentingUsername;
        this.commentingNickname = commentingNickname;
    }

    public PhotoComment toPhotoComment() {

        return new PhotoComment(getCommentingUsername(),
                                getCommentingNickname(),
                                photoCommentEntity.getComment());
    }

    public PhotoCommentEntity getPhotoCommentEntity() {

        return photoCommentEntity;
    }

    public void setPhotoCommentEntity(PhotoCommentEntity photoCommentEntity) {

        this.photoCommentEntity = photoCommentEntity;
    }

    public String getCommentingUsername() {

        return commentingUsername;
    }

    public void setCommentingUsername(String commentingUsername) {

        this.commentingUsername = commentingUsername;
    }

    public String getCommentingNickname() {

        return commentingNickname;
    }

    public void setCommentingNickname(String commentingNickname) {

        this.commentingNickname = commentingNickname;
    }
}




package com.dodopipe.lifelog.core.events.file;

import com.dodopipe.lifelog.core.domain.BlobObject;

/**
 * @author Henry Yan
 */
public class FileViewedEvent {

    public BlobObject file;

    public FileViewedEvent(BlobObject file) {

        this.file = file;
    }

    public BlobObject getFile() {

        return file;
    }
}

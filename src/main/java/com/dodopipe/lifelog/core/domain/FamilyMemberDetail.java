package com.dodopipe.lifelog.core.domain;

import java.io.Serializable;

import static com.dodopipe.lifelog.core.domain.FamilyRelationshipStatus.FamilyMemberStatusType.*;

/**
 * @author tianyin.luo
 */
public class FamilyMemberDetail implements Serializable{

    private String username;
    private String phone;
    private String nickname;
    private String gender;
    private String signature;
    private String avatarFilePath;
    private String langCode;
    private String relation;
    private boolean isRemoved;
    private boolean isBlocked;

    public FamilyMemberDetail() {

    }

    public FamilyMemberDetail(String username,
                              String phone,
                              String nickname,
                              String gender,
                              String signature,
                              String avatarFilePath,
                              String langCode,
                              String relation,
                              int status) {

        this.username = username;
        this.phone = phone;
        this.nickname = nickname;
        this.gender = gender;
        this.signature = signature;
        this.avatarFilePath = avatarFilePath;
        this.langCode = langCode;
        this.relation = relation;
        setStatus(status);
    }




    public boolean isRemoved() {

        return isRemoved;
    }

    public void setRemoved(boolean isRemoved) {

        this.isRemoved = isRemoved;
    }

    public boolean isBlocked() {

        return isBlocked;
    }

    public void setBlocked(boolean isBlocked) {

        this.isBlocked = isBlocked;
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {

        this.username = username;
    }



    public void setStatus(int status) {

        if(VALID.ordinal()==status){
            this.isBlocked = false;
            this.isRemoved = false;
        }else if(REMOVED.ordinal()==status){
            this.isBlocked = true;
            this.isRemoved = true;
        }else if(BE_BLOCKED.ordinal()==status){
            this.isBlocked = true;
            this.isRemoved = false;
        }
    }

    public String getPhone() {

        return phone;
    }

    public void setPhone(String phone) {

        this.phone = phone;
    }

    public String getNickname() {

        return nickname;
    }

    public void setNickname(String nickname) {

        this.nickname = nickname;
    }

    public String getGender() {

        return gender;
    }

    public void setGender(String gender) {

        this.gender = gender;
    }

    public String getSignature() {

        return signature;
    }

    public void setSignature(String signature) {

        this.signature = signature;
    }

    public String getAvatarFilePath() {

        return avatarFilePath;
    }

    public void setAvatarFilePath(String avatarFilePath) {

        this.avatarFilePath = avatarFilePath;
    }

    public String getLangCode() {

        return langCode;
    }

    public void setLangCode(String langCode) {

        this.langCode = langCode;
    }

    public String getRelation() {

        return relation;
    }

    public void setRelation(String relation) {

        this.relation = relation;
    }

}

package com.dodopipe.lifelog.core.services.exception;

/**
 * @author yongwei
 */
public class InvalidFamilyInvitationException
        extends ServiceException {

    public InvalidFamilyInvitationException(String message) {

        super(message);
    }

    public InvalidFamilyInvitationException(String message,
                                            Throwable cause) {

        super(message,
              cause);
    }

    public InvalidFamilyInvitationException(Throwable cause) {

        super(cause);
    }

    public InvalidFamilyInvitationException(String message,
                                            Throwable cause,
                                            boolean enableSuppression,
                                            boolean writableStackTrace) {

        super(message,
              cause,
              enableSuppression,
              writableStackTrace);
    }
}

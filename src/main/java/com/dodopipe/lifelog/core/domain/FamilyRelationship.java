package com.dodopipe.lifelog.core.domain;

import java.io.Serializable;

/**
 * @author tianyin.luo
 */
public class FamilyRelationship
        implements Serializable {

    private String displayName;

    private String langCode;

    private String relationshipScope;

    public FamilyRelationship() {

    }

    public FamilyRelationship(String displayName,
                              String langCode,
                              String relationshipScope) {

        this.displayName = displayName;
        this.langCode = langCode;
        this.relationshipScope = relationshipScope;
    }

    public String getDisplayName() {

        return displayName;
    }

    public String getLangCode() {

        return langCode;
    }

    public String getRelationshipScope() {

        return relationshipScope;
    }
}

package com.dodopipe.lifelog.core.events.photo;

/**
 * Created by henryhome on 8/5/14.
 */
public class PhotoLikeRemoveEvent {
    private Long photoId;
    private Long userId;

    public PhotoLikeRemoveEvent(Long photoId, Long userId) {
        this.photoId = photoId;
        this.userId = userId;
    }

    public Long getPhotoId() {

        return photoId;
    }

    public Long getUserId() {

        return userId;
    }
}

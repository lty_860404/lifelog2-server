package com.dodopipe.lifelog.persistence.adaptors;

import com.dodopipe.lifelog.core.domain.PhotoComment;
import com.dodopipe.lifelog.core.domain.PhotoDetail;
import com.dodopipe.lifelog.core.events.photo.*;
import com.dodopipe.lifelog.core.ports.persistence.PhotoQueryEventHandler;
import com.dodopipe.lifelog.core.services.exception.PhotoNotFoundException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByIdException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundException;
import com.dodopipe.lifelog.persistence.domain.photo.PhotoCommentDetailEntity;
import com.dodopipe.lifelog.persistence.domain.photo.PhotoDetailEntity;
import com.dodopipe.lifelog.persistence.domain.user.UserEntity;
import com.dodopipe.lifelog.persistence.repositories.photo.PhotoCommentRepository;
import com.dodopipe.lifelog.persistence.repositories.photo.PhotoRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Henry Yan
 */
public class PhotoQueryPersistenceEventHandler
        implements PhotoQueryEventHandler {

    private PhotoRepository photoRepo;
    private PhotoCommentRepository photoCommentRepo;
    private UserRepository userRepo;

    public PhotoQueryPersistenceEventHandler(PhotoRepository photoRepo,
                                             PhotoCommentRepository photoCommentRepo,
                                             UserRepository userRepo) {

        this.photoRepo = photoRepo;
        this.photoCommentRepo = photoCommentRepo;
        this.userRepo = userRepo;
    }

    @Override
    public PhotosViewedEvent viewAllPhotos(PhotosViewEvent event)
            throws
            UserNotFoundException {

        Long userId = event.getUserId();
        UserEntity user = userRepo.findById(userId);

        if (user == null) {
            throw new UserNotFoundByIdException(userId);
        }

        Integer pageNum = event.getPageNum();
        Integer numPerPage = event.getNumPerPage();

        PageRequest pageRequest = new PageRequest(pageNum,
                                                  numPerPage,
                                                  new Sort(Sort.Direction.DESC,
                                                           "lastModifiedTime"));

        List<PhotoDetailEntity> photoDetailEntities = photoRepo.findAllPhotosForUser(userId,
                                                                                     user.getFamily()
                                                                                         .getId(),
                                                                                     pageRequest);

        List<PhotoDetail> photoDetails = new ArrayList<>();

        for (PhotoDetailEntity photoDetailEntity : photoDetailEntities) {
            PhotoDetail photo = photoDetailEntity.toPhotoDetail();
            photoDetails.add(photo);
        }

        return new PhotosViewedEvent(Collections.unmodifiableList(photoDetails));
    }

    @Override
    public PhotosViewedEvent viewPhotosByActivity(PhotosViewEvent event) {

        Long activityId = event.getActivityId();
        Long userId = event.getUserId();
        Integer pageNum = event.getPageNum();
        Integer numPerPage = event.getNumPerPage();

        PageRequest pageRequest = new PageRequest(pageNum,
                                                  numPerPage,
                                                  new Sort(Sort.Direction.DESC,
                                                           "lastModifiedTime"));

        List<PhotoDetailEntity> photoDetailEntities = photoRepo.findPhotosByActivity(userId,
                                                                                     activityId,
                                                                                     pageRequest);

        List<PhotoDetail> photoDetails = new ArrayList<>();

        for (PhotoDetailEntity photoDetailEntity : photoDetailEntities) {
            PhotoDetail photo = photoDetailEntity.toPhotoDetail();
            photoDetails.add(photo);
        }

        return new PhotosViewedEvent(Collections.unmodifiableList(photoDetails));
    }

    @Override
    public PhotoViewedEvent viewPhoto(PhotoViewEvent event)
            throws
            PhotoNotFoundException {

        Long photoId = event.getPhotoId();
        Long userId = event.getUserId();

        PhotoDetailEntity photoDetailEntity = photoRepo.findPhotoById(userId,
                                                                      photoId);

        if (photoDetailEntity == null) {
            throw new PhotoNotFoundException(photoId);
        }

        PhotoDetail photoDetail = photoDetailEntity.toPhotoDetail();
        return new PhotoViewedEvent(photoDetail);
    }

    @Override
    public PhotoCommentsViewedEvent viewAllPhotoComments(PhotoCommentsViewEvent event) {

        Long photoId = event.getPhotoId();
        Integer pageNum = event.getPageNum();
        Integer numPerPage = event.getNumPerPage();

        PageRequest pageRequest = new PageRequest(pageNum,
                                                  numPerPage,
                                                  new Sort(Sort.Direction.DESC,
                                                           "commentingTime"));

        List<PhotoCommentDetailEntity> detailEntities =
                photoCommentRepo.findPhotoCommentDetailsByPhotoId(photoId,
                                                                  pageRequest);

        List<PhotoComment> photoComments = new ArrayList<>();

        for (PhotoCommentDetailEntity detailEntity : detailEntities) {
            photoComments.add(detailEntity.toPhotoComment());
        }

        return new PhotoCommentsViewedEvent(photoComments);
    }
}




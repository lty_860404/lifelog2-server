package com.dodopipe.lifelog.core.services.exception;

/**
 * Created by henryhome on 8/5/14.
 */
public class InvalidPhotoLikeException extends ServiceException {

    public InvalidPhotoLikeException(String message) {

        super(message);
    }

    public InvalidPhotoLikeException(String message,
                                     Throwable cause) {

        super(message,
              cause);
    }

    public InvalidPhotoLikeException(Throwable cause) {

        super(cause);
    }

    public InvalidPhotoLikeException(String message,
                                     Throwable cause,
                                     boolean enableSuppression,
                                     boolean writableStackTrace) {

        super(message,
              cause,
              enableSuppression,
              writableStackTrace);
    }
}

package com.dodopipe.lifelog.core.services.exception;

/**
 * Created by henryhome on 8/21/14.
 */
public class MessageNotDeliveredException extends ServiceException {
    public MessageNotDeliveredException(String message) {

        super(message);
    }

    public MessageNotDeliveredException(String message,
                                     Throwable cause) {

        super(message,
              cause);
    }

    public MessageNotDeliveredException(Throwable cause) {

        super(cause);
    }

    public MessageNotDeliveredException(String message,
                                     Throwable cause,
                                     boolean enableSuppression,
                                     boolean writableStackTrace) {

        super(message,
              cause,
              enableSuppression,
              writableStackTrace);
    }
}

package com.dodopipe.lifelog.persistence.repositories.photo;

import com.dodopipe.lifelog.persistence.domain.photo.PhotoLikeEntity;
import com.dodopipe.lifelog.persistence.domain.photo.PhotoLikePrimaryKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Henry Yan
 */

@Repository
public interface PhotoLikeRepository extends JpaRepository<PhotoLikeEntity, PhotoLikePrimaryKey> {

}




package com.dodopipe.lifelog.persistence.repositories;

import com.dodopipe.lifelog.config.LifelogProfile;
import com.dodopipe.lifelog.config.RepositoryConfiguration;
import com.dodopipe.lifelog.persistence.domain.MediaObject;
import com.dodopipe.lifelog.persistence.repositories.impl.MediaFileBlobRepositoryImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.net.URLConnection;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {RepositoryConfiguration.class})
@ActiveProfiles(profiles = {LifelogProfile.DEVE})
public class MediaFileBlobRepositoryTest {

    @Autowired
    private BlobRepository<MediaObject, String> repo;

    @Test
    public void testMediaFileReository() {

        assertTrue(repo instanceof MediaFileBlobRepositoryImpl);
    }

    @Test
    public void tetSaveMediaObject()
            throws
            Exception {

        MediaObject obj = prepare("test.jpg");
        MediaObject cObj = repo.save(obj);
        assertNotNull(cObj.getKey());
        assertEquals("image/jpeg",
                     cObj.getContentType());

        File f = new File(System.getProperty("java.io.tmpdir"),
                          cObj.getKey());
        assertTrue(f.exists());
        assertEquals(cObj.getLength(),
                     f.length());
        f.delete();
    }

    @Test
    public void testFindMediaObject()
            throws
            Exception {

        MediaObject obj = prepare("test.jpg");
        repo.save(obj);

        MediaObject readedObj = repo.findById(obj.getKey());

        assertNotNull(readedObj.getInputStream());
        assertEquals(readedObj.getContentType(),
                     obj.getContentType());

        File f = new File(System.getProperty("java.io.tmpdir"),
                          obj.getKey());
        f.delete();

    }


    private MediaObject prepare(String name)
            throws
            Exception {

        ClassLoader cl = this.getClass()
                             .getClassLoader();
        URLConnection connection = cl.getResource(name)
                                     .openConnection();

        MediaObject object = new MediaObject(connection.getInputStream(),
                                             connection.getContentType(),
                                             connection.getContentLength());
        return object;
    }


}

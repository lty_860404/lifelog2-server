package com.dodopipe.lifelog.rest.controller;

import com.dodopipe.lifelog.core.domain.Activity;
import com.dodopipe.lifelog.core.domain.ActivityAttendee;
import com.dodopipe.lifelog.core.domain.ActivityDetail;
import com.dodopipe.lifelog.core.events.activity.*;
import com.dodopipe.lifelog.core.services.ActivityQueryService;
import com.dodopipe.lifelog.core.services.exception.ActivityNotFoundException;
import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;
import com.dodopipe.lifelog.rest.error.IllegalPrincipalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;

/**
 * @author Henry Yan
 */
@Controller
@RequestMapping(value = "/activities/wedding", method = RequestMethod.GET)
public class ActivityQueryController {

    @Autowired
    private ActivityQueryService activityQueryService;

    @RequestMapping(value = "/attending")
    public ResponseEntity<List<ActivityDetail>> viewAllAttendingWeddingActivities(Principal principal,
                                                                                  @RequestParam("p") Integer pageNum,
                                                                                  @RequestParam("n")
                                                                                  Integer numPerPage)
            throws
            IllegalPrincipalException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);

        ActivitiesViewedEvent event =
                activityQueryService.viewAllAttendingWeddingActivities(new ActivitiesViewEvent(userId,
                                                                                               pageNum,
                                                                                               numPerPage));

        if (event.getActivities()
                 .isEmpty()) {

            return new ResponseEntity<>(
                    event.getActivities(),
                    HttpStatus.NOT_FOUND);
        } else {

            return new ResponseEntity<>(
                    event.getActivities(),
                    HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/attended")
    public ResponseEntity<List<ActivityDetail>> viewAllAttendedWeddingActivities(Principal principal,
                                                                                 @RequestParam("p") Integer pageNum,
                                                                                 @RequestParam("n")
                                                                                 Integer numPerPage)
            throws
            IllegalPrincipalException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);

        ActivitiesViewedEvent event =
                activityQueryService.viewAllAttendedWeddingActivities(new ActivitiesViewEvent(userId,
                                                                                              pageNum,
                                                                                              numPerPage));

        if (event.getActivities()
                 .isEmpty()) {

            return new ResponseEntity<>(
                    event.getActivities(),
                    HttpStatus.NOT_FOUND);
        } else {

            return new ResponseEntity<>(
                    event.getActivities(),
                    HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{activity_id}")
    public ResponseEntity<Activity> viewWeddingActivity(@PathVariable("activity_id") Long activityId,
                                                        Principal principal)
            throws
            IllegalPrincipalException,
            ActivityNotFoundException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);

        ActivityViewedEvent event = activityQueryService.viewWeddingActivity(new ActivityViewEvent(activityId,
                                                                                                   userId));

        return new ResponseEntity<>(event.getActivity(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/{activity_id}/attendees")
    public ResponseEntity<List<ActivityAttendee>> viewWeddingActivityAttendees(
            @PathVariable("activity_id") Long activityId,
            @RequestParam("p") Integer pageNum,
            @RequestParam("n") Integer numPerPage)
            throws
            InternalServerErrorException {

        ActivityAttendeesViewedEvent event =
                activityQueryService.viewWeddingActivityAttendees(new ActivityAttendeesViewEvent(activityId,
                                                                                                 pageNum,
                                                                                                 numPerPage));

        return new ResponseEntity<>(event.getAttendees(),
                                    HttpStatus.OK);
    }
}




package com.dodopipe.lifelog.persistence.repositories.user;

import com.dodopipe.lifelog.persistence.domain.user.PhoneVerificationCodeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PhoneVerificationCodeRepository
        extends JpaRepository<PhoneVerificationCodeEntity, Long> {

    @Query(value = "SELECT * FROM PHONE_VERIFICATION_CODE WHERE PHONE = ? ORDER BY ID DESC LIMIT 1",
           nativeQuery = true)
    public PhoneVerificationCodeEntity findByPhone(String phone);
}

package com.dodopipe.lifelog.core.events.user;

import com.dodopipe.lifelog.core.domain.User;
import com.dodopipe.lifelog.core.events.ViewedEvent;

/**
 * Created by henryhome on 8/6/14.
 */
public class UserViewedEvent extends ViewedEvent {
    public User user;

    public UserViewedEvent(User user) {
        this.user = user;
    }

    public User getUser() {

        return user;
    }
}

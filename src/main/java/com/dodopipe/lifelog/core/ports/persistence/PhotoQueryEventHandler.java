package com.dodopipe.lifelog.core.ports.persistence;

import com.dodopipe.lifelog.core.events.photo.*;

/**
 * @author Henry Yan
 */
public interface PhotoQueryEventHandler {

    public PhotosViewedEvent viewAllPhotos(PhotosViewEvent event);

    public PhotosViewedEvent viewPhotosByActivity(PhotosViewEvent event);

    public PhotoViewedEvent viewPhoto(PhotoViewEvent event);

    public PhotoCommentsViewedEvent viewAllPhotoComments(PhotoCommentsViewEvent event);
}




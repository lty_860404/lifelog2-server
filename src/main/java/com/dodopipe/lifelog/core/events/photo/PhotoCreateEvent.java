package com.dodopipe.lifelog.core.events.photo;

import com.dodopipe.lifelog.core.domain.PhotoFileData;
import com.dodopipe.lifelog.core.events.CreateEvent;

public class PhotoCreateEvent
        extends CreateEvent {

    private final PhotoFileData photoFileData;

    public PhotoCreateEvent(PhotoFileData photoFileData) {

        this.photoFileData = photoFileData;
    }

    public PhotoFileData getPhotoFileData() {

        return photoFileData;
    }
}

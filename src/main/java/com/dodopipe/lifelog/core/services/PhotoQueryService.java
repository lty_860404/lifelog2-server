package com.dodopipe.lifelog.core.services;

import com.dodopipe.lifelog.core.events.photo.*;
import com.dodopipe.lifelog.core.services.exception.PhotoNotFoundException;

public interface PhotoQueryService {

    public PhotosViewedEvent viewAllPhotos(PhotosViewEvent event);

    public PhotosViewedEvent viewPhotosByActivity(PhotosViewEvent event);

    public PhotoViewedEvent viewPhoto(PhotoViewEvent event)
            throws
            PhotoNotFoundException;

    public PhotoCommentsViewedEvent viewAllPhotoComments(PhotoCommentsViewEvent event);
}




package com.dodopipe.lifelog.core.services.exception;

/**
 * Created by henryhome on 8/28/14.
 */
public class VerificationCodeNotFoundException
        extends ServiceException {

    private String phone;

    public VerificationCodeNotFoundException(String phone) {

        this.phone = phone;
    }

    public VerificationCodeNotFoundException(String phone,
                                             Throwable cause) {

        super(cause);
        this.phone = phone;
    }

    public String getMessage() {

        return "The verification code with phone = " + phone + " cannot be found";
    }
}

package com.dodopipe.lifelog.persistence.domain.family;

import com.dodopipe.lifelog.core.domain.FamilyInvitation;
import com.dodopipe.lifelog.core.domain.FamilyInvitationStatus;
import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author yongwei
 */
@Entity(name = "FAMILY_INVITATION")
public class FamilyInvitationEntity
        implements Serializable {

    private final static long serialVersionUID = 1L;

    @Id
    @Column(name = "ID",
            columnDefinition = "BIGINT(25) NOT NULL AUTO_INCREMENT")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "INVITER_ID",
            columnDefinition = "BIGINT(25)",
            nullable = false)
    private Long inviterId;

    @Column(name = "INVITEE_ID",
            columnDefinition = "BIGINT(25)")
    private Long inviteeId;

    @Column(name = "INVITEE_PHONE",
            columnDefinition = "VARCHAR(80)")
    private String inviteePhone;

    @Column(name = "RELATIONSHIP",
            columnDefinition = "VARCHAR(80)")
    private String relationship;

    @Column(name = "STATUS",
            columnDefinition = "TINYINT DEFAULT 0")
    private int status = 0;

    public FamilyInvitationEntity() {

    }

    public FamilyInvitationEntity(Long inviterId,
                                  Long inviteeId,
                                  String inviteePhone,
                                  String relationship) {

        this.inviterId = inviterId;
        this.inviteeId = inviteeId;
        this.inviteePhone = inviteePhone;
        this.relationship = relationship;
    }

    public FamilyInvitationEntity(Long inviterId,
                                  Long inviteeId,
                                  String inviteePhone,
                                  String relationship,
                                  int status) {

        this.inviterId = inviterId;
        this.inviteeId = inviteeId;
        this.inviteePhone = inviteePhone;
        this.relationship = relationship;
        this.status = status;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public Long getInviterId() {

        return inviterId;
    }

    public void setInviterId(Long inviterId) {

        this.inviterId = inviterId;
    }

    public Long getInviteeId() {

        return inviteeId;
    }

    public void setInviteeId(Long inviteeId) {

        this.inviteeId = inviteeId;
    }

    public String getInviteePhone() {

        return inviteePhone;
    }

    public void setInviteePhone(String inviteePhone) {

        this.inviteePhone = inviteePhone;
    }

    public String getRelationship() {

        return relationship;
    }

    public void setRelationship(String relationship) {

        this.relationship = relationship;
    }

    public int getStatus() {

        return status;
    }

    public void setStatus(int status) {

        this.status = status;
    }

    public FamilyInvitation toFamilyInvitation()
            throws
            InternalServerErrorException {

        return new FamilyInvitation(getId(),
                                    getInviterId(),
                                    getInviteeId(),
                                    getInviteePhone(),
                                    getRelationship(),
                                    FamilyInvitationStatus.fromInt(getStatus()));
    }
}




package com.dodopipe.lifelog.persistence.repositories;

import com.dodopipe.lifelog.config.DBUnitDataSourceConfiguration;
import com.dodopipe.lifelog.config.DataSourceConfiguration;
import com.dodopipe.lifelog.config.JPAConfiguration;
import com.dodopipe.lifelog.config.LifelogActiveProfilesResolver;
import com.dodopipe.lifelog.persistence.domain.user.UserAccountEntity;
import com.dodopipe.lifelog.persistence.repositories.user.UserAccountRepository;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPAConfiguration.class,
                                 DataSourceConfiguration.class,
                                 DBUnitDataSourceConfiguration.class})
@ActiveProfiles(resolver = LifelogActiveProfilesResolver.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class
                        })
@DatabaseSetup("DB_SETUP.xml")
public class UserAccountRepositoryTest {

    @Autowired
    UserAccountRepository repo;

    @Test
    public void testSaveAccountEntity()
            throws
            Exception {

        String accessId = UserAccountEntity.generateBase64EncodedRandomProperty(120);
        String secretKey = UserAccountEntity.generateBase64EncodedRandomProperty(240);

        UserAccountEntity auth = new UserAccountEntity();

        auth.setAccessId(accessId);
        auth.setSecretKey(secretKey);
        auth.setUserId(3000l);
        auth.setUsername("abcde");
        auth.setPassword("default");
        auth.setAccountExpired(false);
        auth.setCredentialsExpired(false);

        repo.save(auth);

        System.out.println("accessId => " + auth.getAccessId());
        System.out.println("secretKey => " + auth.getSecretKey());
        System.out.println("password => " + auth.getPassword());
        System.out.println("accountExpired => " + auth.isAccountExpired());
        System.out.println("credentialsExpired => " + auth.isCredentialsExpired());

        assertNotNull(auth.getAccessId());
        assertNotNull(auth.getSecretKey());
        assertNotNull(auth.getPassword());
        assertFalse(auth.isAccountExpired());
        assertFalse(auth.isCredentialsExpired());
    }

    @Test
    public void testReadAuthentication() {

        UserAccountEntity auth = repo.findOne(1000l);

        assertEquals("abcd",
                     auth.getAccessId());
        assertEquals("efgh",
                     auth.getSecretKey());
    }
}




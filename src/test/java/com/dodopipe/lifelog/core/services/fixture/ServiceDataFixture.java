package com.dodopipe.lifelog.core.services.fixture;

import com.dodopipe.lifelog.core.events.auth.UserRegisterEvent;
import com.dodopipe.lifelog.core.events.auth.UserSignInEvent;
import com.dodopipe.lifelog.core.events.auth.UserSignOutEvent;
import com.dodopipe.lifelog.core.events.user.PasswordUpdateEvent;

/**
 * Created by henryhome on 8/4/14.
 */
public class ServiceDataFixture {

    public static UserRegisterEvent generateUserRegisterEvent() {

        return new UserRegisterEvent("testcode",
                                     "+55555",
                                     "77777777",
                                     "passwordaaaaaaaa",
                                     "EN");
    }

    public static UserSignInEvent generateUserSignInEvent() {

        return new UserSignInEvent("test_username",
                                   "test_phone",
                                   "test_password");
    }

    public static UserSignOutEvent generateUserSignOutEvent() {

        return new UserSignOutEvent(1000l);
    }

    public static PasswordUpdateEvent generatePasswordUpdateEvent() {

        return new PasswordUpdateEvent(1000l, "uuuuuuuu");
    }
}




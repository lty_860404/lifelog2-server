package com.dodopipe.lifelog.rest.controller;

import com.dodopipe.lifelog.core.domain.PhotoComment;
import com.dodopipe.lifelog.core.domain.PhotoDetail;
import com.dodopipe.lifelog.core.events.photo.*;
import com.dodopipe.lifelog.core.services.PhotoQueryService;
import com.dodopipe.lifelog.core.services.exception.PhotoNotFoundException;
import com.dodopipe.lifelog.rest.error.IllegalPrincipalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping(value = "/photos", method = RequestMethod.GET)
public class PhotoQueryController {

    @Autowired
    private PhotoQueryService photoQueryService;

    @RequestMapping
    public ResponseEntity<List<PhotoDetail>> viewAllPhotos(Principal principal,
                                                           @RequestParam("p") Integer pageNum,
                                                           @RequestParam("n") Integer numPerPage)
            throws
            IllegalPrincipalException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);
        PhotosViewedEvent event = photoQueryService.viewAllPhotos(new PhotosViewEvent(userId,
                                                                                      pageNum,
                                                                                      numPerPage));

        return new ResponseEntity<>(event.getPhotoDetails(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/activities/{activity_id}")
    public ResponseEntity<List<PhotoDetail>> viewPhotosByActivity(@PathVariable("activity_id") Long activityId,
                                                                  @RequestParam("p") Integer pageNum,
                                                                  @RequestParam("n") Integer numPerPage,
                                                                  Principal principal)
            throws
            IllegalPrincipalException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);
        PhotosViewedEvent event = photoQueryService.viewPhotosByActivity(new PhotosViewEvent(activityId,
                                                                                             userId,
                                                                                             pageNum,
                                                                                             numPerPage));

        return new ResponseEntity<>(event.getPhotoDetails(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/{photo_id}")
    public ResponseEntity<PhotoDetail> viewPhoto(@PathVariable("photo_id") Long photoId,
                                                 Principal principal)
            throws
            IllegalPrincipalException,
            PhotoNotFoundException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);
        PhotoViewedEvent event = photoQueryService.viewPhoto(new PhotoViewEvent(photoId,
                                                                                userId));
        return new ResponseEntity<>(
                event.getPhotoDetail(),
                HttpStatus.OK);
    }

    @RequestMapping(value = "/{photo_id}/all_comments")
    public ResponseEntity<List<PhotoComment>> viewAllPhotoComments(@PathVariable("photo_id") Long photoId,
                                                                   @RequestParam("p") Integer pageNum,
                                                                   @RequestParam("n") Integer numPerPage) {

        PhotoCommentsViewedEvent event = photoQueryService.viewAllPhotoComments(
                new PhotoCommentsViewEvent(photoId,
                                           pageNum,
                                           numPerPage));
        return new ResponseEntity<>(event.getPhotoComments(),
                                    HttpStatus.OK);
    }
}

package com.dodopipe.lifelog.core.services;

import com.dodopipe.lifelog.config.*;
import com.dodopipe.lifelog.core.events.family.*;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * @author tianyin.luo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CoreConfiguration.class,
                                 JPAConfiguration.class,
                                 DataSourceConfiguration.class,
                                 RepositoryConfiguration.class,
                                 DBUnitDataSourceConfiguration.class})
@ActiveProfiles(resolver = LifelogActiveProfilesResolver.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
@DatabaseSetup("DB_SETUP.xml")
public class FamilyMemberServiceTest {

    private static Logger logger = LoggerFactory.getLogger(FamilyMemberServiceTest.class);
    @Autowired
    FamilyCommandService familyCommandService;

    @Autowired
    FamilyQueryService familyQueryService;

    @Test
    public void shouldReadFamilyMemberDetail() {

        FamilyMemberDetailViewEvent event = new FamilyMemberDetailViewEvent(1000l,
                                                                            "John55555");
        FamilyMemberDetailViewedEvent success = familyQueryService.viewFamilyMemberDetail(event);

        assertEquals("John55555",
                     success.getFamilyMemberDetail()
                            .getUsername());
        assertEquals("HUSBAND",
                     success.getFamilyMemberDetail()
                            .getRelation());
    }

    @Test
    public void shouldUpdateFamilyMemberInfo() {

        FamilyMemberUpdateEvent event = new FamilyMemberUpdateEvent(1000l,
                                                                    "John55555",
                                                                    "BOY FRIEND",
                                                                    "en");

        FamilyMemberUpdatedEvent success = familyCommandService.updateFamilyMember(event);

        assertEquals(true,
                     success.isSuccess());

        FamilyMemberDetailViewEvent testEvent = new FamilyMemberDetailViewEvent(1000l,
                                                                                "John55555");
        FamilyMemberDetailViewedEvent testResult = familyQueryService.viewFamilyMemberDetail(testEvent);

        assertEquals("BOY FRIEND",
                     testResult.getFamilyMemberDetail()
                               .getRelation());
    }

    @Test
    public void shouldRemoveFamilyMemberWhenValid()
            throws
            Exception {
        //remove from valid
        FamilyMemberRemoveEvent event = new FamilyMemberRemoveEvent(1000l,
                                                                    "John55555");
        FamilyMemberRemovedEvent result = familyCommandService.deleteFamilyMember(event);
        assertEquals(true,
                     result.isSuccess());

        FamilyMemberDetailViewEvent recipientEvent = new FamilyMemberDetailViewEvent(1000l,
                                                                                     "John55555");
        FamilyMemberDetailViewedEvent recipientInfo = familyQueryService.viewFamilyMemberDetail(recipientEvent);
        assertTrue(recipientInfo.getFamilyMemberDetail()
                                .isRemoved());
        assertTrue(recipientInfo.getFamilyMemberDetail()
                                .isBlocked());

        FamilyMemberDetailViewEvent excutorEvent = new FamilyMemberDetailViewEvent(2000l,
                                                                                   "Laura55555");
        FamilyMemberDetailViewedEvent excutorInfo = familyQueryService.viewFamilyMemberDetail(excutorEvent);
        System.out.println("Excutor is removed is " + excutorInfo.getFamilyMemberDetail()
                                                                 .isRemoved());
        System.out.println("Excutor is blocked is " + excutorInfo.getFamilyMemberDetail()
                                                                 .isBlocked());

        assertFalse(excutorInfo.getFamilyMemberDetail()
                               .isRemoved());
        assertTrue(excutorInfo.getFamilyMemberDetail()
                              .isBlocked());
    }

    @Test
    public void shouldRemoveFamilyMemberWhenBlocked()
            throws
            Exception {
        //remove from blocked
        FamilyMemberRemoveEvent event = new FamilyMemberRemoveEvent(3000l,
                                                                    "Laura55555");
        FamilyMemberRemovedEvent success = familyCommandService.deleteFamilyMember(event);
        assertEquals(true,
                     success.isSuccess());

        FamilyMemberDetailViewEvent recipientEvent = new FamilyMemberDetailViewEvent(3000l,
                                                                                     "Laura55555");
        FamilyMemberDetailViewedEvent recipientInfo = familyQueryService.viewFamilyMemberDetail(recipientEvent);
        assertTrue(recipientInfo.getFamilyMemberDetail()
                                .isRemoved());
        assertTrue(recipientInfo.getFamilyMemberDetail()
                                .isBlocked());

        FamilyMemberDetailViewEvent excutorEvent = new FamilyMemberDetailViewEvent(1000l,
                                                                                   "Jack55555");
        FamilyMemberDetailViewedEvent excutorInfo = familyQueryService.viewFamilyMemberDetail(excutorEvent);
        assertTrue(excutorInfo.getFamilyMemberDetail()
                              .isRemoved());
        assertTrue(excutorInfo.getFamilyMemberDetail()
                              .isBlocked());
    }
}




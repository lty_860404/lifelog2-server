package com.dodopipe.lifelog.persistence.domain.activity;

import java.io.Serializable;

/**
 * @author Henry Yan
 */
public class ActivityAttendeePrimaryKey implements Serializable {

    private Long attendeeId;
    private Long activityId;

    public ActivityAttendeePrimaryKey(Long attendeeId, Long activityId) {

        this.attendeeId = attendeeId;
        this.activityId = activityId;
    }

    public Long getAttendeeId() {

        return attendeeId;
    }

    public void setAttendeeId(Long attendeeId) {

        this.attendeeId = attendeeId;
    }

    public Long getActivityId() {

        return activityId;
    }

    public void setActivityId(Long activityId) {

        this.activityId = activityId;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof ActivityAttendeePrimaryKey)) {
            return false;
        }

        ActivityAttendeePrimaryKey that = (ActivityAttendeePrimaryKey) o;

        if (!activityId.equals(that.activityId)) {
            return false;
        }
        if (!attendeeId.equals(that.attendeeId)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {

        int result = attendeeId.hashCode();
        result = 31 * result + activityId.hashCode();
        return result;
    }
}




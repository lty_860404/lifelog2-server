package com.dodopipe.lifelog.persistence.repositories.family;

import com.dodopipe.lifelog.persistence.domain.family.FamilyMemberEntity;
import com.dodopipe.lifelog.persistence.domain.family.FamilyMemberPrimaryKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author tianyin.luo
 */
@Repository
public interface FamilyMemberRepository
        extends JpaRepository<FamilyMemberEntity, FamilyMemberPrimaryKey> {

}

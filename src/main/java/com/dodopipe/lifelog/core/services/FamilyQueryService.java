package com.dodopipe.lifelog.core.services;

import com.dodopipe.lifelog.core.events.family.*;
import com.dodopipe.lifelog.core.services.exception.FamilyMemberNotFoundException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByIdException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByUsernameException;

/**
 * Created by henryhome on 8/18/14.
 */
public interface FamilyQueryService {

    FamilyMembersViewedEvent viewAllFamilyMembers(FamilyMembersViewEvent event);

    FamilyInvitationsViewedEvent viewAllSentInvitations(FamilyInvitationsViewEvent event);

    FamilyInvitationsViewedEvent viewAllReceivedInvitations(FamilyInvitationsViewEvent event);

    FamilyRelationshipsViewedEvent viewAllFamilyRelationships(FamilyRelationshipsViewEvent event);

    FamilyMemberDetailViewedEvent viewFamilyMemberDetail(FamilyMemberDetailViewEvent event)
            throws
            UserNotFoundByIdException,
            UserNotFoundByUsernameException,
            FamilyMemberNotFoundException;
}

package com.dodopipe.lifelog.config;

import com.dodopipe.lifelog.persistence.domain.user.UserAccountEntity;
import com.dodopipe.lifelog.persistence.repositories.user.UserAccountRepository;
import com.dodopipe.lifelog.security.SignatureAuthenticationEntryPoint;
import com.dodopipe.lifelog.security.SignatureAuthenticationFilter;
import com.dodopipe.lifelog.security.UserKeyDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.cache.NullUserCache;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import java.util.Collections;

@Configuration
@EnableWebSecurity
public class RestSecurityConfiguration {

    @Autowired
    private UserAccountRepository userRepo;

    @Bean
    public AuthenticationEntryPoint signatureAuthenticationEntryPoint() {

        return new SignatureAuthenticationEntryPoint("DOPRealm");
    }

    @Bean
    public UserDetailsService userDetailsService() {

        return new UserDetailsService() {

            @Override
            public UserDetails loadUserByUsername(String username)
                    throws
                    UsernameNotFoundException {

                UserAccountEntity user = userRepo.findByAccessId(username);
                if (user == null) {
                    throw new UsernameNotFoundException("User with username = " + username + " is not found");
                }
                return new UserKeyDetails(user.getUserId(),
                                          user.getSecretKey(),
                                          user.isAccountExpired(),
                                          user.isCredentialsExpired(),
                                          Collections.<GrantedAuthority>emptyList());
            }
        };
    }

    @Bean
    public UserCache userCache() {

        return new NullUserCache();
    }

    @Configuration
    public static class RestSecurityConfigurerAdapter
            extends
            WebSecurityConfigurerAdapter {

        @Autowired
        private UserDetailsService userService;

        @Autowired
        private UserCache userCache;

        @Autowired
        private AuthenticationEntryPoint entryPoint;

        @Override
        protected void configure(HttpSecurity http)
                throws
                Exception {

            http.csrf()
                .disable();
            http.addFilterBefore(
                    new SignatureAuthenticationFilter(userService,
                                                      userCache,
                                                      entryPoint),
                    BasicAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers("/account")
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/users",
                             "/family",
                             "/photos",
                             "/activities")
                .authenticated();
        }

    }

}

package com.dodopipe.lifelog.persistence.repositories;

import com.dodopipe.lifelog.config.DBUnitDataSourceConfiguration;
import com.dodopipe.lifelog.config.DataSourceConfiguration;
import com.dodopipe.lifelog.config.JPAConfiguration;
import com.dodopipe.lifelog.config.LifelogActiveProfilesResolver;
import com.dodopipe.lifelog.persistence.domain.MLocalizationStringEntity.StringKey;
import com.dodopipe.lifelog.persistence.domain.MLocalizationStringEntity.TypeCode;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPAConfiguration.class,
                                 DataSourceConfiguration.class,
                                 DBUnitDataSourceConfiguration.class})
@ActiveProfiles(resolver = LifelogActiveProfilesResolver.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class
                        })
@DatabaseSetup("DB_MASTER.xml")
public class ML10NStringRepositoryTest {

    @Autowired
    ML10NStringRepository repo;

    @Test
    public void testGetDefaultFamilyName() {

        String n = repo.findByTypeAndKey(TypeCode.FAMILY_NAME.ordinal(),
                                         StringKey.DEFAULT_FAMILY_NAME.toString(),
                                         "jp");

        assertEquals("のご家族",
                     n);


    }

    @Test
    public void testGetRelationshipName() {

        List<String> rs = repo.findByType(TypeCode.RELATIONSHIP.ordinal(),
                                          "jp");

        assertEquals(5,
                     rs.size());
    }
}




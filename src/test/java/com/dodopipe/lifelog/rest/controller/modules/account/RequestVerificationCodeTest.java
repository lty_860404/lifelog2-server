package com.dodopipe.lifelog.rest.controller.modules.account;

import com.dodopipe.lifelog.core.services.UserAccountService;
import com.dodopipe.lifelog.rest.controller.UserAccountController;
import com.dodopipe.lifelog.rest.controller.utils.TestUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class RequestVerificationCodeTest {

    MockMvc mockMvc;

    @InjectMocks
    private UserAccountController controller;

    @Mock
    private UserAccountService service;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        mockMvc = standaloneSetup(controller).setMessageConverters(new MappingJackson2HttpMessageConverter())
                                             .setHandlerExceptionResolvers(TestUtil.createExceptionResolver())
                                             .build();
    }

    @Test
    public void testRequestVerificationCodeWithHttpOK()
            throws
            Exception {

        /*when(service.requestVerificationCode(any(VerificationCodeCreateEvent.class))).thenReturn(null);

        mockMvc.perform(post("/account/verify").param("phone",
                                                      "+8612345678")
                                               .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpect(status().isOk());*/

    }

    @Test
    public void testRequestVerificationCodeWithoutPhone()
            throws
            Exception {

        /*when(service.requestVerificationCode(any(VerificationCodeCreateEvent.class))).thenReturn(null);

        mockMvc.perform(post("/account/verify").accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpect(status().is4xxClientError());*/

    }
}

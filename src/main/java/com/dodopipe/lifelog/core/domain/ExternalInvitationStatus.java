package com.dodopipe.lifelog.core.domain;

import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;

/**
 * @author Henry Yan
 */
public enum ExternalInvitationStatus {
    UNREGISTERED,
    REGISTERED;

    public static ExternalInvitationStatus[] all = ExternalInvitationStatus.values();
    public static ExternalInvitationStatus fromInt(int n) throws InternalServerErrorException {
        if (n < all.length) {
            return all[n];
        }
        else {
            throw new InternalServerErrorException(
                    "Convert from int to Status failed: the value of the integer is beyond the range of the enumeration");
        }
    }
}




package com.dodopipe.lifelog.persistence.domain.activity;

import com.dodopipe.lifelog.core.domain.ActivityDetail;

/**
 * Created by henryhome on 8/14/14.
 */
public class WeddingActivityDetailEntity {

    private WeddingActivityEntity weddingActivityEntity;
    private Boolean isCurrUserHost;

    public WeddingActivityDetailEntity(WeddingActivityEntity weddingActivityEntity,
                                       Boolean isCurrUserHost) {

        this.weddingActivityEntity = weddingActivityEntity;
        this.isCurrUserHost = isCurrUserHost;
    }

    public ActivityDetail toActivityDetails() {

        return new ActivityDetail(weddingActivityEntity.toWeddingActivity(),
                                   isCurrUserHost());
    }

    public WeddingActivityEntity getWeddingActivityEntity() {

        return weddingActivityEntity;
    }

    public void setWeddingActivityEntity(WeddingActivityEntity weddingActivityEntity) {

        this.weddingActivityEntity = weddingActivityEntity;
    }

    public Boolean isCurrUserHost() {

        if (isCurrUserHost == null) {
            return false;
        }

        return isCurrUserHost;
    }

    public void setCurrUserHost(Boolean isCurrUserHost) {

        this.isCurrUserHost = isCurrUserHost;
    }
}

package com.dodopipe.lifelog.persistence.adaptors;

import com.dodopipe.lifelog.core.events.auth.*;
import com.dodopipe.lifelog.core.ports.persistence.UserAccountEventHandler;
import com.dodopipe.lifelog.core.services.exception.InvalidVerificationCodeException;
import com.dodopipe.lifelog.core.services.exception.VerificationCodeExpiredException;
import com.dodopipe.lifelog.persistence.PersistenceException;
import com.dodopipe.lifelog.persistence.domain.user.PhoneVerificationCodeEntity;
import com.dodopipe.lifelog.persistence.domain.user.UserAccountEntity;
import com.dodopipe.lifelog.persistence.repositories.user.PhoneVerificationCodeRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserAccountRepository;
import com.dodopipe.lifelog.security.AccountNotFoundException;

import java.security.NoSuchAlgorithmException;

public class UserAccountPersistenceEventHandler
        implements
        UserAccountEventHandler {

    private UserAccountRepository authRepo;
    private PhoneVerificationCodeRepository verifyRepo;
    private String verificationCodeLength;
    private String verificationCodeValidInterval;

    public UserAccountPersistenceEventHandler(UserAccountRepository authRepo,
                                              PhoneVerificationCodeRepository verifyRepo) {

        this.authRepo = authRepo;
        this.verifyRepo = verifyRepo;
    }

    @Override
    public VerificationCodeCreatedEvent createVerificationCode(VerificationCodeCreateEvent event) {

        String verificationCodeLength = getVerificationCodeLength();

                PhoneVerificationCodeEntity verificationCodeEntity =
                PhoneVerificationCodeEntity.getInstance(event.getPhone(),
                                                        Integer.parseInt(verificationCodeLength));

        verifyRepo.save(verificationCodeEntity);

        return new VerificationCodeCreatedEvent(verificationCodeEntity.getVerificationCode());
    }

    @Override
    public UserRegisteredEvent register(Long userId,
                                        String username,
                                        String phone,
                                        String password)
            throws
            PersistenceException {

        UserAccountEntity ua = new UserAccountEntity();

        String[] authInfo = generateUniqueRandomAccessIdAndSecretKey();

        String encodedPassword;

        try {
            encodedPassword = UserAccountEntity.MD5Encode(password.getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new PersistenceException(e);
        }

        ua.setUserId(userId);
        ua.setAccessId(authInfo[0]);
        ua.setSecretKey(authInfo[1]);
        ua.setUsername(username);
        ua.setPhone(phone);
        ua.setPassword(encodedPassword);
        ua.setAccountExpired(false);
        ua.setCredentialsExpired(false);

        authRepo.save(ua);

        return new UserRegisteredEvent(username,
                                       ua.getAccessId(),
                                       ua.getSecretKey());
    }

    @Override
    public UserSignedInEvent signIn(UserSignInEvent event)
            throws
            PersistenceException {

        String username = event.getUsername();
        String phone = event.getPhone();
        String password;

        try {
            password = UserAccountEntity.MD5Encode(event.getPassword()
                                                        .getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new PersistenceException(e);
        }

        UserAccountEntity ua = authRepo.findByUsernameAndPassword(username,
                                                                  password);

        if (ua == null) {
            ua = authRepo.findByPhoneAndPassword(phone,
                                                 password);
        }

        if (ua == null) {
            throw new AccountNotFoundException("The account with username = " + username
                                                       + " and password = " + password
                                                       + " or phone = " + phone
                                                       + " and password = " + password
                                                       + " is not found");
        }

        String[] authInfo = generateUniqueRandomAccessIdAndSecretKey();

        ua.setAccessId(authInfo[0]);
        ua.setSecretKey(authInfo[1]);
        ua.setAccountExpired(false);

        authRepo.save(ua);

        return new UserSignedInEvent(ua.getAccessId(),
                                     ua.getSecretKey());
    }

    @Override
    public void verifyCode(String phone,
                           String verificationCode)
            throws
            InvalidVerificationCodeException,
            VerificationCodeExpiredException {

        PhoneVerificationCodeEntity entity = verifyRepo.findByPhone(phone);

        String storedVerificationCode = entity.getVerificationCode();

        if (storedVerificationCode == null || !storedVerificationCode.equalsIgnoreCase(verificationCode)) {
            throw new InvalidVerificationCodeException("The verification code is not valid");
        }

        String validInterval = getVerificationCodeValidInterval();

        long interval = System.currentTimeMillis() - entity.getCreatedTime()
                                                           .getTime();

        if (interval > Long.parseLong(validInterval)) {
            throw new VerificationCodeExpiredException("The verification code is expired");
        }
    }

    private String[] generateUniqueRandomAccessIdAndSecretKey()
            throws
            PersistenceException {

        Boolean isUnique = false;
        String accessId = null;
        String secretKey = null;

        while (!isUnique) {
            try {
                accessId = UserAccountEntity.generateBase64EncodedRandomProperty(120);
                secretKey = UserAccountEntity.generateBase64EncodedRandomProperty(240);
            } catch (NoSuchAlgorithmException e) {
                throw new PersistenceException(e);
            }

            UserAccountEntity dupeAccessId = authRepo.findByAccessId(accessId);

            if (dupeAccessId != null) {
                continue;
            }

            UserAccountEntity dupeSecretKey = authRepo.findBySecretKey(secretKey);

            if (dupeSecretKey != null) {
                continue;
            }

            isUnique = true;
        }

        return new String[]{accessId,
                            secretKey};
    }

    public String getVerificationCodeLength() {

        return verificationCodeLength;
    }

    public void setVerificationCodeLength(String verificationCodeLength) {

        this.verificationCodeLength = verificationCodeLength;
    }

    public String getVerificationCodeValidInterval() {

        return verificationCodeValidInterval;
    }

    public void setVerificationCodeValidInterval(String verificationCodeValidInterval) {

        this.verificationCodeValidInterval = verificationCodeValidInterval;
    }
}




package com.dodopipe.lifelog.core.services;

import com.dodopipe.lifelog.core.events.activity.*;
import com.dodopipe.lifelog.core.services.exception.ActivityNotFoundException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByIdException;

/**
 * @author Henry Yan
 */
public interface ActivityCommandService {

    public ActivityCreatedEvent createWeddingActivity(ActivityCreateEvent event)
            throws
            UserNotFoundByIdException;

    public ActivityUpdatedEvent updateWeddingActivity(ActivityUpdateEvent event)
            throws
            ActivityNotFoundException;

    public ActivityDeletedEvent deleteActivity(ActivityDeleteEvent event)
            throws
            ActivityNotFoundException;

    public ActivityAttendeesInvitedEvent inviteAttendees(ActivityAttendeesInviteEvent event)
            throws
            ActivityNotFoundException;

    public ActivityInvitationAcceptedEvent acceptInvitation(ActivityInvitationAcceptEvent event)
            throws
            ActivityNotFoundException;

    public ActivityInvitationRejectedEvent rejectInvitation(ActivityInvitationRejectEvent event)
            throws
            ActivityNotFoundException;
}

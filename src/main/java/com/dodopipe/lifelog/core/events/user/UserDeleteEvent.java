package com.dodopipe.lifelog.core.events.user;

import com.dodopipe.lifelog.core.events.DeleteEvent;

/**
 * Created by henryhome on 8/6/14.
 */
public class UserDeleteEvent extends DeleteEvent {

    private Long userId;

    public UserDeleteEvent(Long userId) {
        this.userId = userId;
    }

    public Long getUserId() {

        return userId;
    }
}

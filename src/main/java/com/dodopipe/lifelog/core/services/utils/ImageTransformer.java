package com.dodopipe.lifelog.core.services.utils;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.drew.metadata.exif.ExifIFD0Directory;

import javax.imageio.ImageIO;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Created by henryhome on 8/4/14.
 */
public class ImageTransformer {

    private BufferedImage image;
    private int width;
    private int height;
    private int orientation;

    public ImageTransformer(byte[] imageData)
            throws
            IOException {

        this.image = ImageIO.read(new ByteArrayInputStream(imageData));
        this.width = image.getWidth();
        this.height = image.getHeight();
        this.orientation = this.getImageOrientation(imageData);

    }

    public void rotate() {

        AffineTransform tx = new AffineTransform();

        switch (orientation) {
            case 1:
                break;
            case 2: // Flip X
                tx.scale(-1.0,
                         1.0);
                tx.translate(-this.width,
                             0);
                break;
            case 3: // PI rotation
                tx.translate(this.width,
                             this.height);
                tx.rotate(Math.PI);
                break;
            case 4: // Flip Y
                tx.scale(1.0,
                         -1.0);
                tx.translate(0,
                             -this.height);
                break;
            case 5: // - PI/2 and Flip X
                tx.rotate(-Math.PI / 2);
                tx.scale(-1.0,
                         1.0);
                switchWH();
                break;
            case 6: // -PI/2 and -width
                tx.translate(this.height,
                             0);
                tx.rotate(Math.PI / 2);
                switchWH();
                break;
            case 7: // PI/2 and Flip
                tx.scale(-1.0,
                         1.0);
                tx.translate(-this.height,
                             0);
                tx.translate(0,
                             this.width);
                tx.rotate(3 * Math.PI / 2);
                switchWH();
                break;
            case 8: // PI / 2
                tx.translate(0,
                             this.width);
                tx.rotate(3 * Math.PI / 2);
                switchWH();
                break;
        }
        if (orientation != 1)
            this.transform(tx);
    }

    public void scale(int targetWidth) {

        AffineTransform tx = new AffineTransform();

        double scale = 1d * targetWidth / this.width;

        this.width = targetWidth;
        this.height = (int) (this.height * scale);
        tx.scale(scale,
                 scale);

        this.transform(tx);

    }

    public BufferedImage getImage() {

        return this.image;
    }


    private void transform(AffineTransform t) {

        BufferedImage newImage = new BufferedImage(this.width,
                                                   this.height,
                                                   this.image.getType());
        AffineTransformOp atOp = new AffineTransformOp(t,
                                                       null);

        atOp.filter(this.image,
                    newImage);
        this.image = newImage;

    }

    private void switchWH() {

        int tmp = this.width;
        this.width = this.height;
        this.height = tmp;
    }


    private int getImageOrientation(byte[] imageData)
            throws
            IOException {

        int orientation = 1;
        try {
            Metadata metadata =
                    ImageMetadataReader.readMetadata(new BufferedInputStream(new ByteArrayInputStream(imageData)),
                                                     false);

            Directory directory = metadata.getDirectory(ExifIFD0Directory.class);

            if (directory != null
                    && directory.containsTag(ExifIFD0Directory.TAG_ORIENTATION)) {

                orientation = directory.getInt(ExifIFD0Directory.TAG_ORIENTATION);
            }
        } catch (MetadataException me) {
            //just ignore if orientation info is not available
            me.printStackTrace();
        } catch (ImageProcessingException ipe) {
            //just ignore
            ipe.printStackTrace();
        }

        return orientation;
    }
}




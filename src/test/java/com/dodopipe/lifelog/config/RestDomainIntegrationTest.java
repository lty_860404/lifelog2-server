package com.dodopipe.lifelog.config;

import com.dodopipe.lifelog.core.domain.BlobObject;
import com.dodopipe.lifelog.core.domain.UserEditInfo;
import com.dodopipe.lifelog.rest.controller.fixture.UserAsPrincipal;
import com.dodopipe.lifelog.rest.controller.utils.TestUtil;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static com.dodopipe.lifelog.rest.controller.fixture.RestDataFixture.imageData;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {MVCConfiguration.class,
                                 CoreConfiguration.class,
                                 JPAConfiguration.class,
                                 DataSourceConfiguration.class,
                                 RepositoryConfiguration.class,
                                 DBUnitDataSourceConfiguration.class})
@ActiveProfiles(resolver = LifelogActiveProfilesResolver.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
@DatabaseSetup("DB_SETUP.xml")
public class RestDomainIntegrationTest {

    @Autowired
    WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setUp() {

        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext)
                                      .build();
    }

    @Test
    public void testPostPhotosCorrectly()
            throws
            Exception {

        BlobObject bo = imageData("test.jpg").get(0);
        byte[] data = new byte[(int) bo.getLength()];
        bo.getInputStream()
          .read(data);


        MockMultipartFile onePicture = new MockMultipartFile("images",
                                                             "test.jpg",
                                                             "image/jpeg",
                                                             data);

        this.mockMvc.perform(
                MockMvcRequestBuilders.fileUpload("/photos")
                                      .file(onePicture)
                                      .param("comment",
                                             "test comment")
                                      .principal(new UserAsPrincipal("1000"))
                            )
                    .andExpect(status().isOk());
    }

    @Test
    public void testRequestAllPhotosCorrectly()
            throws
            Exception {

        this.mockMvc.perform(
                get("/photos").param("p",
                                     "0")
                              .param("n",
                                     "20")
                              .accept(MediaType.APPLICATION_JSON)
                              .principal(new UserAsPrincipal("1000")))
                    .andExpect(jsonPath("$[0].photo.id").value(2000))
                    .andExpect(jsonPath("$[1].photo.id").value(1000))
                    .andDo(print());
    }

    @Test
    public void testUpdateUserCorrectly()
            throws
            Exception {

        UserEditInfo userInfo = new UserEditInfo(null,
                                                 "test_nickname",
                                                 null,
                                                 null,
                                                 "test_signature",
                                                 "test_avatar_file_path",
                                                 null);

        mockMvc.perform(post("/user").principal(new UserAsPrincipal("3000"))
                                     .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                     .content(TestUtil.convertObjectToJsonBytes(userInfo)))
               .andExpect(status().isOk());
    }
}




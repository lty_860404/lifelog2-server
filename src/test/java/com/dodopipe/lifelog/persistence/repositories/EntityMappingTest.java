package com.dodopipe.lifelog.persistence.repositories;

import com.dodopipe.lifelog.config.DataSourceConfiguration;
import com.dodopipe.lifelog.config.JPAConfiguration;
import com.dodopipe.lifelog.config.LifelogActiveProfilesResolver;
import com.dodopipe.lifelog.persistence.domain.photo.PhotoEntity;
import com.dodopipe.lifelog.persistence.domain.user.UserEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static com.dodopipe.lifelog.persistence.domain.fixture.JPAAssertions.assertTableExist;
import static com.dodopipe.lifelog.persistence.domain.fixture.JPAAssertions.assertTableHasColumn;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPAConfiguration.class,
                                 DataSourceConfiguration.class})
@ActiveProfiles(resolver = LifelogActiveProfilesResolver.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
public class EntityMappingTest {

    @PersistenceContext
    private EntityManager em;

    @Test
    public void testPhotosMapping() {

        assertTableExist(em,
                         PhotoEntity.class);
        assertTableExist(em,
                         UserEntity.class);

    }

    @Test
    public void testPhotosFieldMapping() {

        assertTableHasColumn(em,
                             PhotoEntity.class,
                             "uploadedTime",
                             "UPLOADED_TIME");
        assertTableHasColumn(em,
                             UserEntity.class,
                             "phone",
                             "PHONE");
    }
}

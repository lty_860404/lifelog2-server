package com.dodopipe.lifelog.config;

import org.springframework.test.context.ActiveProfilesResolver;
import org.springframework.util.StringUtils;

public class LifelogActiveProfilesResolver
        implements ActiveProfilesResolver {

    @Override
    public String[] resolve(Class<?> testClass) {

        String profilesStr = System.getProperty("spring.profiles.active");

        String[] profiles = null;
        if (StringUtils.isEmpty(profilesStr)) {
            profiles = new String[]{LifelogProfile.DEVE};
        } else {
            profiles = profilesStr.split(",");
        }

        System.out.println("active spring profile : ====================>"
                                   + StringUtils.arrayToCommaDelimitedString(profiles));
        return profiles;
    }

}

package com.dodopipe.lifelog.core.services.exception;

/**
 * @author yongwei
 */
public class UserNotFoundByIdException
        extends UserNotFoundException {

    private Long userId;

    public UserNotFoundByIdException(Long userId) {

        this.userId = userId;
    }

    public UserNotFoundByIdException(Long userId,
                                     Throwable cause) {

        super(cause);
        this.userId = userId;
    }

    public String getMessage() {

        return "The user with id = " + userId + " cannot be found";
    }
}

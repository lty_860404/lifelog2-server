package com.dodopipe.lifelog.rest.functional;

import com.dodopipe.lifelog.LifelogApplication;
import com.dodopipe.lifelog.config.LifelogProfile;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.Map;

import static com.dodopipe.lifelog.rest.fixture.RestData.userRegisterRequest;
import static com.dodopipe.lifelog.rest.fixture.RestData.userVerifyPhoneRequest;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {LifelogApplication.class
})
@WebAppConfiguration
@IntegrationTest
@ActiveProfiles(LifelogProfile.QACI)
@Transactional
@TransactionConfiguration(defaultRollback = true)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class
                        })
@DatabaseSetup("DB_SETUP.xml")
@DatabaseTearDown(value = {"DB_SETUP.xml",
                           "DB_CLEAR.xml"},
                  type = DatabaseOperation.DELETE_ALL)
public class UserAccountTest {

    private final static String BASE_URL = "http://localhost:8080/account";

    @Autowired
    private EntityManagerFactory emf;

    @After
    public void tearDown() {

        emf.getCache()
           .evictAll();
    }

    @Test
    public void shouldHandleUserRegistrationCorrectly() {

        RestTemplate template = new RestTemplate();

        template.setErrorHandler(QaciTestFactory.defaultErrorHandlerInstance());

        template.postForEntity(BASE_URL + "/verify",
                               userVerifyPhoneRequest(),
                               Map.class);

        @SuppressWarnings("rawtypes")
        ResponseEntity<Map> result = template.postForEntity(BASE_URL + "/register",
                                                         userRegisterRequest(),
                                                         Map.class);

        System.out.println("==============> default username " + result.getBody().get("defaultUsername"));
        System.out.println("==============> access id " + result.getBody().get("accessId"));
        System.out.println("==============> secret key " + result.getBody().get("secretKey"));
    }

    private String getResponseBody(HttpInputMessage message)
            throws
            IOException {

        InputStreamReader reader = new InputStreamReader(message.getBody(),
                                                         "UTF-8");
        try (StringWriter writer = new StringWriter()) {
            char[] c = new char[1024];
            int n;
            while ((n = reader.read(c)) != -1) {
                writer.write(c,
                             0,
                             n);
            }

            return writer.toString();
        }

    }

    private ResponseErrorHandler defaultErrorHandler() {

        return new ResponseErrorHandler() {

            @Override
            public boolean hasError(ClientHttpResponse response)
                    throws
                    IOException {

                return response.getStatusCode() != HttpStatus.OK;
            }

            @Override
            public void handleError(ClientHttpResponse response)
                    throws
                    IOException {

                System.out.println("===========> status:" + response.getStatusCode());
                System.out.println("===========> status text:" + response.getStatusText());
            }

        };

    }
}

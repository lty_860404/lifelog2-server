package com.dodopipe.lifelog.core.events.user;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by henryhome on 8/12/14.
 */
public class PasswordUpdatedEvent {

    public boolean success;

    public PasswordUpdatedEvent(boolean success) {

        this.success = success;
    }

    public boolean isSuccess() {

        return success;
    }

    public Map<String, Boolean> toMap() {

        Map<String, Boolean> map = new HashMap<>();
        map.put("success",
                isSuccess());
        return map;
    }
}

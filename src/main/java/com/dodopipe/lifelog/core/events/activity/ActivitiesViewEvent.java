package com.dodopipe.lifelog.core.events.activity;

/**
 * @author Henry Yan
 */
public class ActivitiesViewEvent {

    private Long userId;
    private Integer pageNum;
    private Integer numPerPage;

    public ActivitiesViewEvent() {

    }

    public ActivitiesViewEvent(Long userId, Integer pageNum, Integer numPerPage) {

        this.userId = userId;
        this.pageNum = pageNum;
        this.numPerPage = numPerPage;
    }

    public Long getUserId() {

        return userId;
    }

    public Integer getPageNum() {

        return pageNum;
    }

    public Integer getNumPerPage() {

        return numPerPage;
    }
}




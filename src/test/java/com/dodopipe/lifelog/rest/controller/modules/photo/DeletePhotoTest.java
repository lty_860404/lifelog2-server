package com.dodopipe.lifelog.rest.controller.modules.photo;

import com.dodopipe.lifelog.core.events.photo.PhotoDeleteEvent;
import com.dodopipe.lifelog.core.events.photo.PhotoDeletedEvent;
import com.dodopipe.lifelog.core.services.PhotoCommandService;
import com.dodopipe.lifelog.rest.controller.PhotoCommandController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Henry Yan
 */
public class DeletePhotoTest {

    MockMvc mockMvc;

    @InjectMocks
    PhotoCommandController controller;

    @Mock
    PhotoCommandService photoCommandService;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                                  .build();

    }

    @Test
    public void DeletePhotoCorrectly()
            throws
            Exception {

        Long photoId = 80l;

        when(photoCommandService.deletePhoto(any(PhotoDeleteEvent.class))).thenReturn(new PhotoDeletedEvent(true));

        mockMvc.perform(delete("/photos/{photo_id}",
                               photoId))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.success").value(true));
    }
}




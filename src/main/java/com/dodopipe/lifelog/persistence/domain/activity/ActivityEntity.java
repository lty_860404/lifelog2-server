package com.dodopipe.lifelog.persistence.domain.activity;

import com.dodopipe.lifelog.persistence.domain.user.UserEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Henry Yan
 */
@Entity(name = "ACTIVITY")
public class ActivityEntity implements Serializable {

    private static final long serialVersionUID = 2343246452645L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID",
            columnDefinition = "BIGINT(25) NOT NULL")
    protected Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "HOST_ID",
                insertable = false,
                updatable = false)
    protected UserEntity host;

    @Column(name = "HOST_ID",
            columnDefinition = "BIGINT(25)",
            nullable = false)
    protected Long hostId;

    @Column(name = "NAME",
            columnDefinition = "VARCHAR(80)")
    protected String name;

    @Column(name = "START_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date startTime;

    @Column(name = "END_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date endTime;

    /**
     * The column is generated automatically by JPA to store the type of the activity to represent inheritance
     */
    @Column(name = "DTYPE")
    protected String dtype;

    @OneToMany(mappedBy = "activity",
               fetch = FetchType.LAZY,
               cascade = CascadeType.ALL)
    protected List<ActivityAttendeeEntity> attendees;

    public ActivityEntity() {

    }

    public ActivityEntity(Long hostId,
                          String name,
                          Date startTime,
                          Date endTime) {

        this.hostId = hostId;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public UserEntity getHost() {

        return host;
    }

    public void setHost(UserEntity host) {

        this.host = host;
    }

    public Long getHostId() {

        return hostId;
    }

    public void setHostId(Long hostId) {

        this.hostId = hostId;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public Date getStartTime() {

        return startTime;
    }

    public void setStartTime(Date startTime) {

        this.startTime = startTime;
    }

    public Date getEndTime() {

        return endTime;
    }

    public void setEndTime(Date endTime) {

        this.endTime = endTime;
    }

    public List<ActivityAttendeeEntity> getAttendees() {

        return attendees;
    }

    public void setAttendees(List<ActivityAttendeeEntity> attendees) {

        this.attendees = attendees;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof ActivityEntity)) {
            return false;
        }

        ActivityEntity that = (ActivityEntity) o;

        if (!id.equals(that.id)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {

        return id.hashCode();
    }
}




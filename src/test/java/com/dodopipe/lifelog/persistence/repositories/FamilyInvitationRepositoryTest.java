package com.dodopipe.lifelog.persistence.repositories;

import com.dodopipe.lifelog.config.DBUnitDataSourceConfiguration;
import com.dodopipe.lifelog.config.DataSourceConfiguration;
import com.dodopipe.lifelog.config.JPAConfiguration;
import com.dodopipe.lifelog.config.LifelogActiveProfilesResolver;
import com.dodopipe.lifelog.persistence.domain.family.FamilyInvitationDetailEntity;
import com.dodopipe.lifelog.persistence.domain.family.FamilyInvitationEntity;
import com.dodopipe.lifelog.persistence.domain.user.UserEntity;
import com.dodopipe.lifelog.persistence.repositories.family.FamilyInvitationRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserRepository;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * @author yongwei
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPAConfiguration.class,
                                 DataSourceConfiguration.class,
                                 DBUnitDataSourceConfiguration.class})
@ActiveProfiles(resolver = LifelogActiveProfilesResolver.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
@DatabaseSetup("DB_SETUP.xml")
public class FamilyInvitationRepositoryTest {

    @Autowired
    private UserRepository userRepo;
    @Autowired
    private FamilyInvitationRepository inviteRepo;

    @Test
    public void shouldInviteAUserCorrectly() {

        UserEntity userEntity = userRepo.findOne(1000L);
        UserEntity member = userRepo.findOne(3000L);
        FamilyInvitationEntity invitation = userEntity.inviteFamilyMember(member,
                                                                          "Friends");
        inviteRepo.saveAndFlush(invitation);

        assertNotNull(invitation.getId());

    }

    @Test
    public void shouldReadAllSentInvitations() {

        List<FamilyInvitationDetailEntity> invitations = inviteRepo.findAllSentInvitation(1000l);

        for (FamilyInvitationDetailEntity invitation : invitations) {
            System.out.println("InviterUsername => " + invitation.getInviterUsername());
            System.out.println("InviterNickname => " + invitation.getInviterNickname());
            System.out.println("InviterPhone => " + invitation.getInviterPhone());
            System.out.println("InviterAvatarFilePath => " + invitation.getInviterAvatarFilePath());
            System.out.println("InviteeUsername => " + invitation.getInviteeUsername());
            System.out.println("InviteeNickname => " + invitation.getInviteeNickname());
            System.out.println("InviteeAvatarFilePath => " + invitation.getInviteeAvatarFilePath());
        }

        assertEquals(3,
                     invitations.size());
        assertEquals(0,
                     invitations.get(0)
                                .getEntity()
                                .getStatus());
        assertEquals(1,
                     invitations.get(invitations.size() - 1)
                                .getEntity()
                                .getStatus());
    }

    @Test
    public void shouldReadAllReceivedInvitations() {

        List<FamilyInvitationDetailEntity> invitations = inviteRepo.findAllReceivedInvitation(2000l);

        for (FamilyInvitationDetailEntity invitation : invitations) {
            System.out.println("InviterUsername => " + invitation.getInviterUsername());
            System.out.println("InviterNickname => " + invitation.getInviterNickname());
            System.out.println("InviterPhone => " + invitation.getInviterPhone());
            System.out.println("InviterAvatarFilePath => " + invitation.getInviterAvatarFilePath());
            System.out.println("InviteeUsername => " + invitation.getInviteeUsername());
            System.out.println("InviteeNickname => " + invitation.getInviteeNickname());
            System.out.println("InviteeAvatarFilePath => " + invitation.getInviteeAvatarFilePath());
        }

        assertEquals(3,
                     invitations.size());
        assertEquals(0,
                     invitations.get(0)
                                .getEntity()
                                .getStatus());
        assertEquals(1,
                     invitations.get(invitations.size() - 1)
                                .getEntity()
                                .getStatus());
    }

    @Test
    public void shouldGetWaitingInvitations() {

        FamilyInvitationEntity invitationWithId = inviteRepo.findPendingInvitationByInviterIdAndInviteeId(1000L,
                                                                                                          3000L);
        FamilyInvitationEntity invitationWithPhone = inviteRepo.findPendingInvitationByInviterIdAndInviteePhone(1000L,
                                                                                                                "+818012344444");
        assertNotNull(invitationWithId);
        assertNull(invitationWithPhone);
    }
}




package com.dodopipe.lifelog.rest.functional;

import com.dodopipe.lifelog.LifelogApplication;
import com.dodopipe.lifelog.config.LifelogProfile;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.jayway.jsonpath.JsonPath;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author yongwei
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {LifelogApplication.class})
@WebAppConfiguration
@IntegrationTest
@ActiveProfiles(LifelogProfile.QACI)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
@DatabaseSetup("DB_SETUP.xml")
@DatabaseTearDown(value = {"DB_SETUP.xml",
                           "DB_CLEAR.xml"},
                  type = DatabaseOperation.DELETE_ALL)
public class AuthenticationExceptionTest {

    private static Logger logger = LoggerFactory.getLogger(AuthenticationExceptionTest.class);

    @Test
    public void testNonValidSignatureException()
            throws
            Exception {

        RestTemplate rest = new RestTemplate();
        rest.setErrorHandler(commonErrorHandler(HttpStatus.UNAUTHORIZED));
        HttpHeaders headers = HMacSignatureUtils.signedHeaders(HttpMethod.GET,
                                                               "/users/photos",
                                                               null);
        //To corrupt the signature
        headers.setDate(System.currentTimeMillis() + 3000);
        HttpEntity<String> entity = new HttpEntity<>(null,
                                                     headers);

        ResponseEntity<String> response = rest.exchange("http://localhost:8080/user/photos",
                                                        HttpMethod.GET,
                                                        entity,
                                                        String.class);
        if (logger.isDebugEnabled()) {

            logger.debug("body:{}",
                         response.getBody());
        }
        String errorMessage = JsonPath.<String>read(response.getBody(),
                                                    "$.message");
        assertEquals("Auth-060",
                     JsonPath.<String>read(errorMessage,
                                           "$.errorCode"));

    }


    public ResponseErrorHandler commonErrorHandler(final HttpStatus status) {

        return new ResponseErrorHandler() {

            @Override
            public boolean hasError(ClientHttpResponse response)
                    throws
                    IOException {

                return response.getStatusCode() != HttpStatus.OK;
            }

            @Override
            public void handleError(ClientHttpResponse response)
                    throws
                    IOException {

                assertTrue(status == response.getStatusCode());
                if (logger.isDebugEnabled()) {
                    logger.debug("Http Response\n" +
                                         "status code:{}\n" +
                                         "status text:{}" +
                                         response.getStatusCode(),
                                 response.getStatusText());
                }
            }

        };
    }

}

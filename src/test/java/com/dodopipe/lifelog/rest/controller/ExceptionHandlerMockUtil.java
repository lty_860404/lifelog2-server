package com.dodopipe.lifelog.rest.controller;

import com.dodopipe.lifelog.rest.advisor.RestResponseEntityExceptionHandler;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.method.annotation.ExceptionHandlerMethodResolver;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author tianyin.luo
 */
public class ExceptionHandlerMockUtil {


    public static List<HandlerExceptionResolver> exceptionResolversInstance() {

        List<HandlerExceptionResolver> handlers = new ArrayList<>();
        ExceptionHandlerExceptionResolver exceptionResolver = new ExceptionHandlerExceptionResolver() {

            protected ServletInvocableHandlerMethod getExceptionHandlerMethod(HandlerMethod handlerMethod,
                                                                              Exception exception) {

                Method method =
                        new ExceptionHandlerMethodResolver(RestResponseEntityExceptionHandler.class).resolveMethod(exception);
                return new ServletInvocableHandlerMethod(new RestResponseEntityExceptionHandler(),
                                                         method);
            }
        };
        exceptionResolver.setMessageConverters(Arrays.asList(new HttpMessageConverter<?>[]{new MappingJackson2HttpMessageConverter()}));
        exceptionResolver.afterPropertiesSet();
        //handlers.add(exceptionResolver);
        handlers.add(new DefaultHandlerExceptionResolver());
        handlers.add(exceptionResolver);
        return handlers;
    }

}

package com.dodopipe.lifelog.database.utils;

import com.dodopipe.lifelog.persistence.domain.activity.ActivityEntity;
import com.dodopipe.lifelog.persistence.domain.photo.PhotoEntity;
import com.dodopipe.lifelog.persistence.domain.user.PhoneVerificationCodeEntity;
import com.dodopipe.lifelog.persistence.repositories.activity.ActivityRepository;
import com.dodopipe.lifelog.persistence.repositories.photo.PhotoRepository;
import com.dodopipe.lifelog.persistence.repositories.user.PhoneVerificationCodeRepository;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * @author Henry Yan
 */
public class DatabaseUtil {


    public static List<PhotoEntity> populatePhotoWithBoundActivitiesNormal(PhotoRepository photoRepo,
                                                                           ActivityRepository activityRepo) {

        PhotoEntity photoA = photoRepo.findById(3000l);
        PhotoEntity photoB = photoRepo.findById(4000l);

        ActivityEntity activityA = activityRepo.findById(1111l);
        ActivityEntity activityB = activityRepo.findById(2222l);
        ActivityEntity activityC = activityRepo.findById(3333l);

        List<ActivityEntity> activities = new ArrayList<>();

        activities.add(activityA);
        activities.add(activityB);
        photoB.setActivities(activities);
        photoRepo.saveAndFlush(photoB);

        activities.add(activityC);
        photoA.setActivities(activities);
        photoRepo.saveAndFlush(photoA);

        List<PhotoEntity> photos = new ArrayList<>();
        photos.add(photoA);
        photos.add(photoB);

        return photos;
    }

    public static List<PhotoEntity> populatePhotoWithBoundActivitiesExtended(PhotoRepository photoRepo,
                                                                             ActivityRepository activityRepo) {

        PhotoEntity photoA = photoRepo.findById(3000l);
        PhotoEntity photoB = photoRepo.findById(4000l);
        PhotoEntity photoC = photoRepo.findById(5000l);

        ActivityEntity activityA = activityRepo.findById(1111l);
        ActivityEntity activityB = activityRepo.findById(2222l);
        ActivityEntity activityC = activityRepo.findById(3333l);
        ActivityEntity activityD = activityRepo.findById(4444l);

        List<ActivityEntity> activities = new ArrayList<>();

        activities.add(activityA);
        activities.add(activityB);
        photoB.setActivities(activities);
        photoRepo.saveAndFlush(photoB);

        activities.add(activityC);
        photoA.setActivities(activities);
        photoRepo.saveAndFlush(photoA);

        List<ActivityEntity> excludeActivities = new ArrayList<>();
        excludeActivities.add(activityD);
        photoC.setActivities(excludeActivities);
        photoRepo.saveAndFlush(photoC);

        List<PhotoEntity> photos = new ArrayList<>();
        photos.add(photoA);
        photos.add(photoB);
        photos.add(photoC);

        return photos;
    }

    public static void createAndSaveVerificationCode(PhoneVerificationCodeRepository repository) {

        PhoneVerificationCodeEntity entity = new PhoneVerificationCodeEntity("77777777",
                                                                             "testcode",
                                                                             GregorianCalendar.getInstance()
                                                                                              .getTime(),
                                                                             null,
                                                                             null);

        repository.save(entity);
    }
}

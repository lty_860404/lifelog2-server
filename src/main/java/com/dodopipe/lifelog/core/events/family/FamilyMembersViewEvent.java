package com.dodopipe.lifelog.core.events.family;

/**
 * @author yongwei
 */
public class FamilyMembersViewEvent {

    private final Long userId;

    public FamilyMembersViewEvent(Long userId) {

        this.userId = userId;
    }

    public Long getUserId() {

        return userId;
    }
}

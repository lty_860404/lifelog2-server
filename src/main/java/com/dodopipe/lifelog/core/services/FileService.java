package com.dodopipe.lifelog.core.services;

import com.dodopipe.lifelog.core.events.file.FileViewEvent;
import com.dodopipe.lifelog.core.events.file.FileViewedEvent;

/**
 * Created by henryhome on 7/15/14.
 */
public interface FileService {

    public FileViewedEvent viewFile(FileViewEvent event);
}




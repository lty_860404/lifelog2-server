package com.dodopipe.lifelog.persistence.repositories.photo;

import com.dodopipe.lifelog.persistence.domain.photo.PhotoCommentDetailEntity;
import com.dodopipe.lifelog.persistence.domain.photo.PhotoCommentEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Henry Yan
 */

@Repository
public interface PhotoCommentRepository extends JpaRepository<PhotoCommentEntity, Long> {

    @Query(value = "SELECT NEW com.dodopipe.lifelog.persistence.domain.photo.PhotoCommentDetailEntity(pc, u.username, u.nickname) " +
                   "FROM PHOTO_COMMENT pc " +
                   "LEFT JOIN USER u " +
                   "ON pc.commentingId = u.id " +
                   "WHERE pc.photoId = ?1")
    List<PhotoCommentDetailEntity> findPhotoCommentDetailsByPhotoId(Long photoId, Pageable pageable);
}




package com.dodopipe.lifelog.core.domain;

/**
 * Created by henryhome on 8/14/14.
 */
public class ActivityDetail {

    private Activity activity;
    private Boolean isCurrentUserHost;

    public ActivityDetail(Activity activity,
                          Boolean isCurrentUserHost) {

        this.activity = activity;
        this.isCurrentUserHost = isCurrentUserHost;
    }

    public Activity getActivity() {

        return activity;
    }

    public Boolean isCurrentUserHost() {

        return isCurrentUserHost;
    }
}

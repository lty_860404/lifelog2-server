package com.dodopipe.lifelog.core.events.auth;

/**
 * Created by henryhome on 8/20/14.
 */
public class MessageCallbackCreateEvent {

    public String messageSid;
    public String smsSid;
    public String accountSid;
    public String from;
    public String to;
    public String body;
    public Integer numMedia;
    public String messageStatus;
    public Integer errorCode;

    public MessageCallbackCreateEvent(String messageSid,
                                      String smsSid,
                                      String accountSid,
                                      String from,
                                      String to,
                                      String body,
                                      Integer numMedia,
                                      String messageStatus,
                                      Integer errorCode) {
        this.messageSid = messageSid;
        this.smsSid = smsSid;
        this.accountSid = accountSid;
        this.from = from;
        this.to = to;
        this.body = body;
        this.numMedia = numMedia;
        this.messageStatus = messageStatus;
        this.errorCode = errorCode;
    }

    public String getMessageSid() {

        return messageSid;
    }

    public String getSmsSid() {

        return smsSid;
    }

    public String getAccountSid() {

        return accountSid;
    }

    public String getFrom() {

        return from;
    }

    public String getTo() {

        return to;
    }

    public String getBody() {

        return body;
    }

    public Integer getNumMedia() {

        return numMedia;
    }

    public String getMessageStatus() {

        return messageStatus;
    }

    public Integer getErrorCode() {

        return errorCode;
    }
}




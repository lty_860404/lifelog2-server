package com.dodopipe.lifelog.core.events.photo;

import com.dodopipe.lifelog.core.domain.PhotoMetadata;
import com.dodopipe.lifelog.core.events.CreatedEvent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PhotoCreatedEvent
        extends CreatedEvent {

    private final Long photoId;
    private final String comment;
    private final List<PhotoMetadata> photos;

    public PhotoCreatedEvent(final Long photoId,
                             final String comment,
                             final List<PhotoMetadata> list) {

        this.photoId = photoId;
        this.comment = comment;
        this.photos = list;

    }

    public Long getPhotoId() {

        return photoId;
    }

    public String getComment() {

        return comment;
    }

    public List<PhotoMetadata> getPhotos() {

        return photos;
    }

    public Map<String, Object> toMap() {

        Map<String, Object> map = new HashMap<>();
        map.put("id",
                this.getPhotoId());
        map.put("comment",
                this.getComment());
        map.put("photos",
                this.getPhotos());
        return map;
    }
}

package com.dodopipe.lifelog.core.events.activity;

import com.dodopipe.lifelog.core.domain.User;

import java.util.List;

/**
 * @author Henry Yan
 */
public class ActivityAttendeesInviteEvent {

    private Long userId;
    private Long activityId;
    private List<User> activityAttendees;

    public ActivityAttendeesInviteEvent(Long userId,
                                        Long activityId,
                                        List<User> activityAttendees) {

        this.userId = userId;
        this.activityId = activityId;
        this.activityAttendees = activityAttendees;
    }

    public Long getUserId() {

        return userId;
    }

    public Long getActivityId() {

        return activityId;
    }

    public List<User> getActivityAttendees() {

        return activityAttendees;
    }
}

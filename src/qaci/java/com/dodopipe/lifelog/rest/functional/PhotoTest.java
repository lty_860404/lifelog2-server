package com.dodopipe.lifelog.rest.functional;

import com.dodopipe.lifelog.LifelogApplication;
import com.dodopipe.lifelog.config.LifelogProfile;
import com.dodopipe.lifelog.rest.fixture.SignatureHeaderInterceptor;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import net.minidev.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

import static com.dodopipe.lifelog.rest.fixture.RestData.photosData;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {LifelogApplication.class})
@WebAppConfiguration
@IntegrationTest
@ActiveProfiles(LifelogProfile.QACI)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
@DatabaseSetup("DB_SETUP.xml")
@DatabaseTearDown(value = {"DB_SETUP.xml",
                           "DB_CLEAR.xml"},
                  type = DatabaseOperation.DELETE_ALL)
public class PhotoTest {

    private static Logger logger = LoggerFactory.getLogger(PhotoTest.class);
    private final static String BASE_URL = "http://localhost:8080";

    @Test
    public void photoShouldBeAddedAndQueried()
            throws
            Exception {

        RestTemplate template = new RestTemplate();

        SignatureHeaderInterceptor interceptor = new SignatureHeaderInterceptor();
        template.setInterceptors(Collections.<ClientHttpRequestInterceptor>singletonList(interceptor));
        String path = "/photos";

        template.postForEntity(BASE_URL + path,
                               photosData(),
                               String.class);

        ResponseEntity<JSONArray> response = template.getForEntity(BASE_URL + path +"?p=0&n=3",
                                                                   JSONArray.class);
        assertEquals(3,
                     response.getBody()
                             .size());
        assertEquals(200,
                     response.getStatusCode()
                             .value());

        logger.info("{} results: {}",
                    PhotoTest.class.getSimpleName(),
                    response.getBody());
    }
}

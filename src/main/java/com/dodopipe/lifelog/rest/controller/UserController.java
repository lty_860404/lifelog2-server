package com.dodopipe.lifelog.rest.controller;

import com.dodopipe.lifelog.core.domain.BlobObject;
import com.dodopipe.lifelog.core.domain.User;
import com.dodopipe.lifelog.core.domain.UserEditInfo;
import com.dodopipe.lifelog.core.events.auth.UserSignOutEvent;
import com.dodopipe.lifelog.core.events.auth.UserSignedOutEvent;
import com.dodopipe.lifelog.core.events.user.*;
import com.dodopipe.lifelog.core.services.UserService;
import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByIdException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByPhoneException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByUsernameException;
import com.dodopipe.lifelog.rest.error.IllegalPrincipalException;
import com.dodopipe.lifelog.security.AccountNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.IOException;
import java.security.Principal;
import java.util.Map;

/**
 * @author Henry Yan
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<User> viewUser(Principal principal,
                                         @RequestParam(value = "username",
                                                       required = false) String username,
                                         @RequestParam(value = "phone",
                                                       required = false) String phone)
            throws
            IllegalPrincipalException,
            UserNotFoundByIdException,
            UserNotFoundByUsernameException,
            UserNotFoundByPhoneException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);

        UserViewedEvent event = userService.viewUser(new UserViewEvent(userId,
                                                                       username,
                                                                       phone));

        return new ResponseEntity<>(event.getUser(),
                                    HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Map<String, Boolean>> updateUser(Principal principal,
                                                           @RequestBody UserEditInfo userEditInfo)
            throws
            IllegalPrincipalException,
            UserNotFoundByIdException,
            IllegalArgumentException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);

        UserUpdatedEvent event = userService.updateUser(new UserUpdateEvent(userId,
                                                                            userEditInfo));

        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<Map<String, Boolean>> deleteUser(Principal principal)
            throws
            IllegalPrincipalException,
            UserNotFoundByIdException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);

        UserDeletedEvent event = userService.deleteUser(new UserDeleteEvent(userId));

        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/change_avatar",
                    method = RequestMethod.POST,
                    consumes = "multipart/form-data")
    public ResponseEntity<Map<String, Object>> changeAvatar(Principal principal,
                                                            MultipartHttpServletRequest request)
            throws
            IllegalPrincipalException,
            InternalServerErrorException,
            UserNotFoundByIdException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);

        MultipartFile avatar = request.getFile("avatar");

        BlobObject avatarFileData = null;

        if (!avatar.isEmpty()) {
            try {
                avatarFileData = new BlobObject(avatar.getInputStream(),
                                                avatar.getContentType(),
                                                avatar.getSize());
            } catch (IOException e) {
                throw new InternalServerErrorException(e);
            }
        }

        UserAvatarChangedEvent event = userService.changeAvatar(new UserAvatarChangeEvent(userId,
                                                                                          avatarFileData));

        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/sign_out",
                    method = RequestMethod.POST)
    public ResponseEntity<Map<String, Boolean>> signOut(Principal principal)
            throws
            IllegalPrincipalException,
            AccountNotFoundException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);

        UserSignedOutEvent event = userService.signOut(new UserSignOutEvent(userId));

        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/change_password",
                    method = RequestMethod.POST)
    public ResponseEntity<Map<String, Boolean>> changePassword(Principal principal,
                                                               @RequestParam("password") String password)
            throws
            IllegalPrincipalException,
            AccountNotFoundException,
            InternalServerErrorException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);

        PasswordUpdatedEvent event = userService.changePassword(new PasswordUpdateEvent(userId,
                                                                                        password));

        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }
}






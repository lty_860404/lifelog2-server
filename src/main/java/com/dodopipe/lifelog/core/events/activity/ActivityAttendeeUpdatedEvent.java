package com.dodopipe.lifelog.core.events.activity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by henryhome on 7/29/14.
 */
public class ActivityAttendeeUpdatedEvent {
    public boolean success;

    public ActivityAttendeeUpdatedEvent(boolean success) {

        this.success = success;
    }

    public boolean isSuccess() {

        return success;
    }

    public Map<String, Boolean> toMap() {

        Map<String, Boolean> map = new HashMap<>();
        map.put("success",
                isSuccess());
        return map;
    }
}

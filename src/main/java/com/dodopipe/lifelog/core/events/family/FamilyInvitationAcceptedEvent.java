package com.dodopipe.lifelog.core.events.family;

import com.dodopipe.lifelog.core.events.CreatedEvent;

/**
 * @author yongwei
 */
public class FamilyInvitationAcceptedEvent
        extends CreatedEvent {

    private final boolean success;

    public FamilyInvitationAcceptedEvent(boolean result) {

        this.success = result;
    }

    public boolean isSuccess() {

        return this.success;
    }
}

package com.dodopipe.lifelog.persistence.domain.activity;

import com.dodopipe.lifelog.persistence.domain.user.UserEntity;

import javax.persistence.*;

/**
 * @author Henry Yan
 */
@Entity(name = "ACTIVITY_ATTENDEE")
@IdClass(ActivityAttendeePrimaryKey.class)
public class ActivityAttendeeEntity {

    @Id
    @Column(name = "ATTENDEE_ID",
            columnDefinition = "BIGINT(25)",
            nullable = false)
    private Long attendeeId;

    @Id
    @Column(name = "ACTIVITY_ID",
            columnDefinition = "BIGINT(25)",
            nullable = false)
    private Long activityId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ATTENDEE_ID",
                insertable = false,
                updatable = false)
    private UserEntity attendee;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACTIVITY_ID",
                insertable = false,
                updatable = false)
    private ActivityEntity activity;

    @Column(name = "ATTENDEE_NAME",
            columnDefinition = "VARCHAR(80)")

    private String attendeeName;

    @Column(name = "ATTENDEE_PHONE",
            columnDefinition = "VARCHAR(80)")
    private String attendeePhone;

    @Column(name = "IS_HOST",
            columnDefinition = "BOOLEAN")
    private Boolean isHost;

    @Column(name = "STATUS",
            columnDefinition = "TINYINT DEFAULT 0")
    private int status = 0;

    public ActivityAttendeeEntity() {

    }

    public ActivityAttendeeEntity(Long attendeeId,
                                  Long activityId,
                                  String attendeeName,
                                  String attendeePhone,
                                  Boolean isHost) {

        this.attendeeId = attendeeId;
        this.activityId = activityId;
        this.attendeeName = attendeeName;
        this.attendeePhone = attendeePhone;
        this.isHost = isHost;
    }

    public ActivityAttendeeEntity(Long attendeeId,
                                  Long activityId,
                                  String attendeeName,
                                  String attendeePhone,
                                  Boolean isHost,
                                  int status) {

        this.attendeeId = attendeeId;
        this.activityId = activityId;
        this.attendeeName = attendeeName;
        this.attendeePhone = attendeePhone;
        this.isHost = isHost;
        this.status = status;
    }

    public ActivityAttendeePrimaryKey getPrimaryKey() {

        return new ActivityAttendeePrimaryKey(attendeeId,
                                              activityId);
    }

    public Long getAttendeeId() {

        return attendeeId;
    }

    public void setAttendeeId(Long attendeeId) {

        this.attendeeId = attendeeId;
    }

    public Long getActivityId() {

        return activityId;
    }

    public void setActivityId(Long activityId) {

        this.activityId = activityId;
    }

    public ActivityEntity getActivity() {

        return activity;
    }

    public UserEntity getAttendee() {

        return attendee;
    }

    public String getAttendeeName() {

        return attendeeName;
    }

    public void setAttendeeName(String attendeeName) {

        this.attendeeName = attendeeName;
    }

    public String getAttendeePhone() {

        return attendeePhone;
    }

    public void setAttendeePhone(String attendeePhone) {

        this.attendeePhone = attendeePhone;
    }

    public Boolean isHost() {

        return isHost;
    }

    public void setHost(Boolean isHost) {

        this.isHost = isHost;
    }

    public int getStatus() {

        return status;
    }

    public void setStatus(int status) {

        this.status = status;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof ActivityAttendeeEntity)) {
            return false;
        }

        ActivityAttendeeEntity that = (ActivityAttendeeEntity) o;

        if (!activityId.equals(that.activityId)) {
            return false;
        }
        if (!attendeeId.equals(that.attendeeId)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {

        int result = attendeeId.hashCode();
        result = 31 * result + activityId.hashCode();
        return result;
    }
}

package com.dodopipe.lifelog.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Set;

public class WebAppInitializer
        implements WebApplicationInitializer {

    private static Logger logger = LoggerFactory.getLogger(WebAppInitializer.class);

    @Override
    public void onStartup(ServletContext servletContext)
            throws
            ServletException {

        System.out.println("============= startup");
        WebApplicationContext rootContext = createRootContext(servletContext);
        configureSpringMvc(servletContext,
                           rootContext);

    }

    private WebApplicationContext createRootContext(ServletContext servletContext) {

        servletContext.setInitParameter("defaultHtmlEscape",
                                        "true");
        servletContext.setInitParameter(ContextLoader.CONTEXT_INITIALIZER_CLASSES_PARAM,
                                        ContextProfileInitializer.class.getName());
        servletContext.setInitParameter("spring.profiles.active",
                                        LifelogProfile.INTG);
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();


        servletContext.addListener(new ContextLoaderListener(rootContext));


        return rootContext;
    }

    private void configureSpringMvc(ServletContext servletContext,
                                    WebApplicationContext rootContext) {

        AnnotationConfigWebApplicationContext mvcContext =
                new AnnotationConfigWebApplicationContext();
        mvcContext.register(MVCConfiguration.class,
                            JPAConfiguration.class,
                            CoreConfiguration.class,
                            DataSourceConfiguration.class,
                            RepositoryConfiguration.class);
        mvcContext.setParent(rootContext);
        DispatcherServlet dispatcherServlet = new DispatcherServlet(mvcContext);
        ServletRegistration.Dynamic appServlet = servletContext.addServlet("lifelog",
                                                                           dispatcherServlet);
        appServlet.setLoadOnStartup(1);
        Set<String> mappingConflicts = appServlet.addMapping("/");

        if (!mappingConflicts.isEmpty()) {
            for (String s : mappingConflicts) {
                logger.error("Mapping conflicts: " + s);
            }
            throw new IllegalStateException("'dispatcher' servlet cannot be mapped to '/'");
        }

    }


}

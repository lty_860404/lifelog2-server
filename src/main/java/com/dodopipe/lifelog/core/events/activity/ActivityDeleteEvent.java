package com.dodopipe.lifelog.core.events.activity;

import com.dodopipe.lifelog.core.events.DeletedEvent;

/**
 * Created by henryhome on 7/21/14.
 */
public class ActivityDeleteEvent extends DeletedEvent {

    private Long activityId;

    public ActivityDeleteEvent(Long activityId) {

        this.activityId = activityId;
    }

    public Long getActivityId() {

        return activityId;
    }
}

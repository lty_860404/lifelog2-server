package com.dodopipe.lifelog.persistence.utils;

import com.dodopipe.lifelog.core.domain.PhotoComment;
import com.dodopipe.lifelog.core.domain.PhotoLike;
import com.dodopipe.lifelog.persistence.repositories.user.UserRepository;

import java.util.regex.Pattern;

/**
 * @author Henry Yan
 */
public class PhotoUtil {
    private static UserRepository userRepo;

    public static String escape(String str,
                          String... seps) {

        if (str == null) {
            return str;
        }

        String rst = str;
        for (String sep : seps) {

            rst = rst.replace(sep,
                              "\\" + sep);
        }
        return rst;

    }

    public static String undoEscape(String str,
                            String... seps) {

        if (str == null) {
            return str;
        }
        String rst = str;
        for (String sep : seps) {

            rst = rst.replace("\\" + sep,
                              sep);
        }
        return rst;

    }

    public static String[] splitWithDelimiter(String str,
                                        String delimiter) {

        if (str == null) {
            return new String[]{};
        }

        String regex = "(?<!\\\\)" + Pattern.quote(delimiter);
        return str.split(regex);

    }

    public static boolean isEmptyString(String str) {

        return str == null || str.isEmpty();
    }

    public static String photoCommentToString(PhotoComment photoComment) {

        String username = photoComment.getUsername();
        String nickname = photoComment.getNickname();
        String comment = photoComment.getComment();
        return escape(nickname, "@", "#") + "@" + escape(username, "@", "#")
                + "@" + escape(comment, "@", "#") + "#";
    }

    public static String photoLikeToString(PhotoLike photoLike) {
        String username = photoLike.getUsername();
        String nickname = photoLike.getNickName();
        return escape(nickname, "@", "#") + "@" + escape(username, "@", "#") + "#";
    }

    public static UserRepository getUserRepo() {

        return userRepo;
    }

    public static void setUserRepo(UserRepository userRepo) {

        PhotoUtil.userRepo = userRepo;
    }
}

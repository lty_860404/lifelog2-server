package com.dodopipe.lifelog.persistence.adaptors;

import com.dodopipe.lifelog.config.*;
import com.dodopipe.lifelog.core.events.photo.PhotoViewEvent;
import com.dodopipe.lifelog.core.events.photo.PhotoViewedEvent;
import com.dodopipe.lifelog.core.events.photo.PhotosViewEvent;
import com.dodopipe.lifelog.core.events.photo.PhotosViewedEvent;
import com.dodopipe.lifelog.core.ports.persistence.PhotoQueryEventHandler;
import com.dodopipe.lifelog.database.utils.DatabaseUtil;
import com.dodopipe.lifelog.persistence.repositories.activity.ActivityRepository;
import com.dodopipe.lifelog.persistence.repositories.photo.PhotoCommentRepository;
import com.dodopipe.lifelog.persistence.repositories.photo.PhotoRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserRepository;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static com.dodopipe.lifelog.persistence.adaptors.fixture.EventHandlerDataFixture.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Henry Yan
 */
@RunWith(SpringJUnit4ClassRunner.class)

@ContextConfiguration(classes = {JPAConfiguration.class,
                                 DataSourceConfiguration.class,
                                 RepositoryConfiguration.class,
                                 DBUnitDataSourceConfiguration.class})
@ActiveProfiles(resolver = LifelogActiveProfilesResolver.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)

@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class
                        })
@DatabaseSetup("DB_SETUP.xml")

public class PhotoQueryPersistenceEventHandlerTest {

    @Autowired
    private PhotoRepository photoRepo;

    @Autowired
    private PhotoCommentRepository photoCommentRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private ActivityRepository activityRepo;

    private PhotoQueryEventHandler handler;

    @Before
    public void setUp() {

        handler = new PhotoQueryPersistenceEventHandler(photoRepo,
                                                        photoCommentRepo,
                                                        userRepo);
    }

    @Test
    public void testViewAllPhotos()
            throws
            Exception {

        DatabaseUtil.populatePhotoWithBoundActivitiesNormal(photoRepo,
                                                            activityRepo);

        PhotosViewEvent event = generateEventForViewAllPhotos();

        PhotosViewedEvent result = handler.viewAllPhotos(event);

        assertNotNull(result.getPhotoDetails());
        assertEquals(4000l,
                     result.getPhotoDetails()
                           .get(0)
                           .getPhoto()
                           .getId()
                           .intValue());
        assertEquals(3000l,
                     result.getPhotoDetails()
                           .get(1)
                           .getPhoto()
                           .getId()
                           .intValue());
        assertEquals(2000l,
                     result.getPhotoDetails()
                           .get(2)
                           .getPhoto()
                           .getId()
                           .intValue());
        assertEquals(1000l,
                     result.getPhotoDetails()
                           .get(3)
                           .getPhoto()
                           .getId()
                           .intValue());
    }

    @Test
    public void testViewPhotosByActivity()
            throws
            Exception {

        DatabaseUtil.populatePhotoWithBoundActivitiesNormal(photoRepo,
                                                            activityRepo);
        PhotosViewEvent event = generateEventForViewPhotosByActivity();

        PhotosViewedEvent result = handler.viewPhotosByActivity(event);

        assertNotNull(result.getPhotoDetails());
        assertEquals(4000l,
                     result.getPhotoDetails()
                           .get(0)
                           .getPhoto()
                           .getId()
                           .intValue());
        assertEquals(3000l,
                     result.getPhotoDetails()
                           .get(1)
                           .getPhoto()
                           .getId()
                           .intValue());
    }

    @Test
    public void testViewPhoto()
            throws
            Exception {

        PhotoViewEvent event = generatePhotoViewEvent();
        PhotoViewedEvent result = handler.viewPhoto(event);

        assertNotNull(result.getPhotoDetail());
        assertEquals(result.getPhotoDetail()
                           .getPhoto()
                           .getId()
                           .longValue(),
                     4000l);
    }
}




package com.dodopipe.lifelog.core.domain;

import java.util.Date;

/**
 * @author Henry Yan
 */
public class WeddingActivity extends Activity {

    private String place;
    private String groomName;
    private String brideName;

    public WeddingActivity() {

    }

    public WeddingActivity(Long id,
                           Long hostId,
                           String name,
                           Date startTime,
                           Date endTime,
                           String place,
                           String groomName,
                           String brideName) {

        this.id = id;
        this.hostId = hostId;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.place = place;
        this.groomName = groomName;
        this.brideName = brideName;
    }

    public String getPlace() {

        return place;
    }

    public String getGroomName() {

        return groomName;
    }

    public String getBrideName() {

        return brideName;
    }
}

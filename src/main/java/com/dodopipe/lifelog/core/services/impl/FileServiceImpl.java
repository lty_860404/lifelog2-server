package com.dodopipe.lifelog.core.services.impl;

import com.dodopipe.lifelog.core.events.file.FileViewEvent;
import com.dodopipe.lifelog.core.events.file.FileViewedEvent;
import com.dodopipe.lifelog.core.ports.persistence.FileEventHandler;
import com.dodopipe.lifelog.core.services.FileService;

/**
 * @author Henry Yan
 */
public class FileServiceImpl implements FileService {

    private FileEventHandler handler;

    public FileServiceImpl(FileEventHandler handler) {

        this.handler = handler;
    }

    public FileViewedEvent viewFile(FileViewEvent event) {

        return handler.viewFile(event);
    }
}




package com.dodopipe.lifelog.core.services;

import com.dodopipe.lifelog.core.events.auth.*;
import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;
import com.dodopipe.lifelog.core.services.exception.InvalidVerificationCodeException;
import com.dodopipe.lifelog.core.services.exception.MessageNotDeliveredException;
import com.dodopipe.lifelog.core.services.exception.VerificationCodeExpiredException;
import com.dodopipe.lifelog.persistence.PersistenceException;

public interface UserAccountService {

    public VerificationCodeCreatedEvent createAndSendVerificationCode(VerificationCodeCreateEvent event)
            throws
            InternalServerErrorException;

    public VerificationCodeNotifiedEvent notifyVerificationStatus(VerificationCodeNotifyEvent event)
            throws
            MessageNotDeliveredException,
            InternalServerErrorException;

    public void updateVerificationStatus(MessageCallbackCreateEvent event);

    public UserRegisteredEvent register(UserRegisterEvent event)
            throws
            InvalidVerificationCodeException,
            VerificationCodeExpiredException,
            PersistenceException;

    public UserSignedInEvent signIn(UserSignInEvent event)
            throws
            PersistenceException;
}




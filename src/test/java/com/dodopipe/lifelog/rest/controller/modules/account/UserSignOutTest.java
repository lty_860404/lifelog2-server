package com.dodopipe.lifelog.rest.controller.modules.account;

import com.dodopipe.lifelog.core.events.auth.UserSignOutEvent;
import com.dodopipe.lifelog.core.events.auth.UserSignedOutEvent;
import com.dodopipe.lifelog.core.services.UserService;
import com.dodopipe.lifelog.rest.controller.UserController;
import com.dodopipe.lifelog.rest.controller.fixture.UserAsPrincipal;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by henryhome on 8/4/14.
 */
public class UserSignOutTest {
    MockMvc mockMvc;

    @InjectMocks
    UserController controller;

    @Mock
    UserService service;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                                  .build();

    }

    @Test
    public void testSignOut()
            throws
            Exception {

        when(service.signOut(any(UserSignOutEvent.class))).thenReturn(new UserSignedOutEvent(true));

        mockMvc.perform(post("/user/sign_out").principal(new UserAsPrincipal("3000")))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.success").value(true));
    }
}




package com.dodopipe.lifelog.core.services;

import com.dodopipe.lifelog.config.*;
import com.dodopipe.lifelog.core.events.auth.*;
import com.dodopipe.lifelog.database.utils.DatabaseUtil;
import com.dodopipe.lifelog.persistence.repositories.user.PhoneVerificationCodeRepository;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static com.dodopipe.lifelog.core.services.fixture.ServiceDataFixture.*;
import static org.junit.Assert.assertFalse;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CoreConfiguration.class,
                                 JPAConfiguration.class,
                                 DataSourceConfiguration.class,
                                 RepositoryConfiguration.class,
                                 DBUnitDataSourceConfiguration.class})
@ActiveProfiles(resolver = LifelogActiveProfilesResolver.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
@DatabaseSetup("DB_SETUP.xml")
public class UserAccountServiceTest {

    @Autowired
    UserAccountService service;

    @Autowired
    PhoneVerificationCodeRepository verifyRepo;

    @Test
    public void testUserRegisterCorrectly() {

        DatabaseUtil.createAndSaveVerificationCode(verifyRepo);

        UserRegisterEvent event = generateUserRegisterEvent();

        UserRegisteredEvent result = service.register(event);

        System.out.println("The default username is " + result.getDefaultUsername());
        System.out.println("The access id is " + result.getAccessId());
        System.out.println("The secret key is " + result.getSecretKey());

        assertFalse(result.getDefaultUsername()
                          .isEmpty());
        assertFalse(result.getAccessId()
                          .isEmpty());
        assertFalse(result.getSecretKey()
                          .isEmpty());
    }

    @Test
    public void testUserSignInCorrectly() {

        UserSignInEvent event = generateUserSignInEvent();

        UserSignedInEvent result = service.signIn(event);

        System.out.println("The access id is " + result.getAccessId());
        System.out.println("The secret key is " + result.getSecretKey());

        assertFalse(result.getAccessId()
                          .isEmpty());
        assertFalse(result.getSecretKey()
                          .isEmpty());
    }
}

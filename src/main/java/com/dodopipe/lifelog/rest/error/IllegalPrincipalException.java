package com.dodopipe.lifelog.rest.error;

/**
 * @author tianyin.luo
 */
public class IllegalPrincipalException
        extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public IllegalPrincipalException(String message) {

        super(message);
    }

    public IllegalPrincipalException(String message,
                                     Throwable cause) {

        super(message,
              cause);
    }

    public IllegalPrincipalException(Throwable cause) {

        super(cause);
    }


}

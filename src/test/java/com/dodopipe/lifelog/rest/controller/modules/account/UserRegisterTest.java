package com.dodopipe.lifelog.rest.controller.modules.account;

import com.dodopipe.lifelog.core.events.auth.UserRegisterEvent;
import com.dodopipe.lifelog.core.services.UserAccountService;
import com.dodopipe.lifelog.rest.controller.UserAccountController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;

import static com.dodopipe.lifelog.rest.controller.fixture.RestEventFixture.generateUserRegisteredEvent;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Henry Yan
 */
public class UserRegisterTest {

    MockMvc mockMvc;

    @InjectMocks
    UserAccountController controller;

    @Mock
    UserAccountService service;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                                  .build();

    }

    @Test
    public void register()
            throws
            Exception {

        when(service.register(any(UserRegisterEvent.class))).thenReturn(generateUserRegisteredEvent());

        mockMvc.perform(post("/account/register").param("verification_code",
                                                        "33333333")
                                                 .param("country_code",
                                                        "+12345")
                                                 .param("phone",
                                                        "55555555")
                                                 .param("password",
                                                        "xyzrst")
                                                 .param("lang_code",
                                                        "EN"))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.defaultUsername").value("default_username"))
               .andExpect(jsonPath("$.accessId").value("test_access_id"))
               .andExpect(jsonPath("$.secretKey").value("test_secret_key"));
    }
}




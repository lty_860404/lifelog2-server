package com.dodopipe.lifelog.core.events.activity;

import com.dodopipe.lifelog.core.domain.Activity;
import com.dodopipe.lifelog.core.events.UpdateEvent;

/**
 * @author Henry Yan
 */
public class ActivityUpdateEvent extends UpdateEvent {

    public Long activityId;
    public Activity activity;

    public ActivityUpdateEvent(Long activityId,
                               Activity activity) {

        this.activityId = activityId;
        this.activity = activity;
    }

    public Long getActivityId() {

        return activityId;
    }

    public Activity getActivity() {

        return activity;
    }
}




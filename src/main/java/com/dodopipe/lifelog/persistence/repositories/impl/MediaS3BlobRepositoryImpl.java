package com.dodopipe.lifelog.persistence.repositories.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.dodopipe.lifelog.persistence.domain.MediaObject;
import com.dodopipe.lifelog.persistence.repositories.BlobRepository;

import java.util.UUID;

public class MediaS3BlobRepositoryImpl
        implements BlobRepository<MediaObject, String> {


    private final AmazonS3 s3;
    private String bucket;

    public MediaS3BlobRepositoryImpl(AmazonS3 s3) {

        this.s3 = s3;
    }

    public void setBucket(String bkt) {

        this.bucket = bkt;
    }

    @Override
    public <S extends MediaObject> S save(S entity) {

        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(entity.getContentType());
        metadata.setContentLength(entity.getLength());

        String key = UUID.randomUUID()
                         .toString();
        s3.putObject(bucket,
                     key,
                     entity.getInputStream(),
                     metadata);
        entity.setInputStream(null);
        entity.setKey(key);
        return entity;
    }

    @Override
    public MediaObject findById(String key) {

        S3Object object = s3.getObject(bucket,
                                       key);
        if (object == null) {
            return null;
        }

        ObjectMetadata metadata = object.getObjectMetadata();
        MediaObject blob = new MediaObject(object.getObjectContent(),
                                           metadata.getContentType(),
                                           metadata.getContentLength());

        return blob;

    }
}

package com.dodopipe.lifelog.core.events.family;

/**
 * @author tianyin.luo
 */
public class FamilyInvitationsViewEvent {

    private final Long userId;

    public FamilyInvitationsViewEvent(Long userId) {

        this.userId = userId;
    }

    public Long getUserId() {

        return userId;
    }

}

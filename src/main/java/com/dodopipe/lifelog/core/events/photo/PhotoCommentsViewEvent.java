package com.dodopipe.lifelog.core.events.photo;

/**
 * @author Henry Yan
 */
public class PhotoCommentsViewEvent {

    private Long photoId;
    private Integer pageNum;
    private Integer numPerPage;

    public PhotoCommentsViewEvent(Long photoId, Integer pageNum, Integer numPerPage) {

        this.photoId = photoId;
        this.pageNum = pageNum;
        this.numPerPage = numPerPage;
    }

    public Long getPhotoId() {

        return photoId;
    }

    public Integer getPageNum() {

        return pageNum;
    }

    public Integer getNumPerPage() {

        return numPerPage;
    }
}

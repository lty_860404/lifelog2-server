package com.dodopipe.lifelog.persistence.repositories;

import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

@NoRepositoryBean
public interface BlobRepository<T, ID extends Serializable> {

    <S extends T> S save(S entity);

    T findById(ID id);

}

package com.dodopipe.lifelog.core.events.family;

/**
 * @author yongwei
 */
public class FamilyInvitationAcceptEvent {

    private Long invitationId;
    private String inviteeRelationship;
    private String inviteeLangCode;

    public FamilyInvitationAcceptEvent(Long invitationId,
                                       String inviteeRelationship,
                                       String inviteeLangCode) {

        this.invitationId = invitationId;
        this.inviteeRelationship = inviteeRelationship;
        this.inviteeLangCode = inviteeLangCode;
    }

    public Long getInvitationId() {

        return invitationId;
    }

    public String getInviteeRelationship() {

        return inviteeRelationship;
    }

    public String getInviteeLangCode() {

        return inviteeLangCode;
    }
}

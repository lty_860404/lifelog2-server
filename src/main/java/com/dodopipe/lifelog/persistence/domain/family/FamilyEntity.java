package com.dodopipe.lifelog.persistence.domain.family;

import com.dodopipe.lifelog.core.domain.FamilyMember;
import com.dodopipe.lifelog.core.domain.FamilyRelationshipStatus;
import com.dodopipe.lifelog.persistence.domain.user.UserEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Entity(name = "FAMILY")
public class FamilyEntity
        implements Serializable {

    private final static long serialVersionUID = 1L;

    @Id
    @Column(name = "ID",
            columnDefinition = "BIGINT(25) NOT NULL AUTO_INCREMENT")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NAME",
            columnDefinition = "VARCHAR(80)")
    private String name;

    @OneToMany(fetch = FetchType.LAZY,
               cascade = CascadeType.ALL,
               mappedBy = "family")
    private Collection<FamilyMemberEntity> familyMembers;

    public List<FamilyMember> getAllMembers() {

        ArrayList<FamilyMember> members = new ArrayList<>();
        for (FamilyMemberEntity member : familyMembers) {
            UserEntity userEntity = member.getMember();
            members.add(new FamilyMember(member.getFamilyId(),
                                         member.getMemberId(),
                                         userEntity.getUsername(),
                                         userEntity.getNickname(),
                                         userEntity.getAvatarFilePath(),
                                         member.getRelationship(),
                                         new FamilyRelationshipStatus(
                                                 FamilyRelationshipStatus.FamilyMemberStatusType.values()[member.getStatus()])));
        }
        return Collections.unmodifiableList(members);
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public Collection<FamilyMemberEntity> getFamilyMembers() {

        return Collections.unmodifiableCollection(familyMembers);
    }

    public void addFamilyMember(UserEntity user,
                                String relationship) {

        if (this.familyMembers == null) {
            this.familyMembers = new ArrayList<>();
        }

        FamilyMemberEntity familyMember = new FamilyMemberEntity();
        familyMember.setFamily(this);
        familyMember.setMember(user);
        familyMember.setRelationship(relationship);
        this.familyMembers.add(familyMember);

    }

    public void setFamilyMembers(Collection<FamilyMemberEntity> familyMembers) {

        this.familyMembers = familyMembers;
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FamilyEntity other = (FamilyEntity) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }
}

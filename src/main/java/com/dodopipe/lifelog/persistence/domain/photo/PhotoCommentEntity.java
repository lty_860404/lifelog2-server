package com.dodopipe.lifelog.persistence.domain.photo;

import com.dodopipe.lifelog.core.domain.PhotoComment;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author Henry Yan
 */
@Entity(name = "PHOTO_COMMENT")
public class PhotoCommentEntity implements Serializable {

    private static final long serialVersionUID = 2343246452645L;

    @Id
    @Column(name = "ID",
            columnDefinition = "BIGINT(25) NOT NULL AUTO_INCREMENT")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "PHOTO_ID",
            columnDefinition = "BIGINT(25)",
            nullable = false)
    private Long photoId;

    @Column(name = "COMMENTING_ID",
            columnDefinition = "BIGINT(25)",
            nullable = false)
    private Long commentingId;

    @Column(name = "COMMENT",
            columnDefinition = "VARCHAR(2000)")
    private String comment;

    @Column(name = "COMMENTING_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date commentingTime;

    public PhotoCommentEntity() {

    }

    public PhotoCommentEntity(Long id,
                              Long photoId,
                              Long commentingId,
                              String comment,
                              Date commentingTime) {

        this.id = id;
        this.photoId = photoId;
        this.commentingId = commentingId;
        this.comment = comment;
        this.commentingTime = commentingTime;
    }

    public static PhotoCommentEntity fromPhotoComment(Long photoId,
                                                      Long commentingId,
                                                      PhotoComment photoComment) {

        Calendar cal = GregorianCalendar.getInstance();
        PhotoCommentEntity entity = new PhotoCommentEntity();
        entity.setPhotoId(photoId);
        entity.setCommentingId(commentingId);
        entity.setComment(photoComment.getComment());
        entity.setCommentingTime(cal.getTime());

        return entity;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public Long getPhotoId() {

        return photoId;
    }

    public void setPhotoId(Long photoId) {

        this.photoId = photoId;
    }

    public Long getCommentingId() {

        return commentingId;
    }

    public void setCommentingId(Long commentingId) {

        this.commentingId = commentingId;
    }

    public String getComment() {

        return comment;
    }

    public void setComment(String comment) {

        this.comment = comment;
    }

    public Date getCommentingTime() {

        return commentingTime;
    }

    public void setCommentingTime(Date commentingTime) {

        this.commentingTime = commentingTime;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof PhotoCommentEntity)) {
            return false;
        }

        PhotoCommentEntity entity = (PhotoCommentEntity) o;

        if (!id.equals(entity.id)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {

        return id.hashCode();
    }
}




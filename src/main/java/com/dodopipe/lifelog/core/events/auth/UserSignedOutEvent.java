package com.dodopipe.lifelog.core.events.auth;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Henry Yan
 */
public class UserSignedOutEvent {

    public boolean success;

    public UserSignedOutEvent(boolean success) {

        this.success = success;
    }

    public boolean isSuccess() {

        return success;
    }

    public Map<String, Boolean> toMap() {

        Map<String, Boolean> map = new HashMap<>();
        map.put("success",
                isSuccess());
        return map;
    }
}

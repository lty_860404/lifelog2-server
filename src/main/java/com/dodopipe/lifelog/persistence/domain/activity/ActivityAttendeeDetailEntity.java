package com.dodopipe.lifelog.persistence.domain.activity;

import com.dodopipe.lifelog.core.domain.ActivityAttendee;
import com.dodopipe.lifelog.core.domain.ActivityAttendeeStatus;
import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;

/**
 * @author Henry Yan
 */
public class ActivityAttendeeDetailEntity {

    private ActivityAttendeeEntity activityAttendeeEntity;
    private String attendeeAvatarFilePath;

    public ActivityAttendeeDetailEntity(ActivityAttendeeEntity activityAttendeeEntity,
                                        String attendeeAvatarFilePath) {

        this.activityAttendeeEntity = activityAttendeeEntity;
        this.attendeeAvatarFilePath = attendeeAvatarFilePath;
    }

    public ActivityAttendee toActivityAttendee() throws
                                                 InternalServerErrorException {

        return new ActivityAttendee(activityAttendeeEntity.getAttendeeId(),
                                    activityAttendeeEntity.getAttendeeName(),
                                    activityAttendeeEntity.getAttendeePhone(),
                                    attendeeAvatarFilePath,
                                    activityAttendeeEntity.isHost(),
                                    ActivityAttendeeStatus.fromInt(activityAttendeeEntity.getStatus()));
    }

    public ActivityAttendeeEntity getActivityAttendeeEntity() {

        return activityAttendeeEntity;
    }

    public void setActivityAttendeeEntity(ActivityAttendeeEntity activityAttendeeEntity) {

        this.activityAttendeeEntity = activityAttendeeEntity;
    }

    public String getAttendeeAvatarFilePath() {

        return attendeeAvatarFilePath;
    }

    public void setAttendeeAvatarFilePath(String attendeeAvatarFilePath) {

        this.attendeeAvatarFilePath = attendeeAvatarFilePath;
    }
}




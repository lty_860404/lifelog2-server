package com.dodopipe.lifelog.rest.controller.modules.activity;

import com.dodopipe.lifelog.core.events.activity.ActivitiesViewEvent;
import com.dodopipe.lifelog.core.events.activity.ActivitiesViewedEvent;
import com.dodopipe.lifelog.core.services.ActivityQueryService;
import com.dodopipe.lifelog.rest.controller.ActivityQueryController;
import com.dodopipe.lifelog.rest.controller.fixture.UserAsPrincipal;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;

import static com.dodopipe.lifelog.rest.controller.fixture.RestEventFixture.activitiesViewedEvent;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Henry Yan
 */
public class ViewAllAttendingWeddingActivitiesTest {

    MockMvc mockMvc;

    @InjectMocks
    ActivityQueryController controller;

    @Mock
    ActivityQueryService activityQueryService;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                             .build();

    }

    @Test
    public void viewAllAttendingWeddingActivitiesCorrectly()
            throws
            Exception {

        ActivitiesViewedEvent event = activitiesViewedEvent(5);

        when(activityQueryService.viewAllAttendingWeddingActivities(any(ActivitiesViewEvent.class))).thenReturn(event);

        mockMvc.perform(get("/activities/wedding/attending").param("p",
                                                           "0")
                                                    .param("n",
                                                           "20")
                                                    .principal(new UserAsPrincipal("3000")))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$[0].activity.id").value(1000))
               .andExpect(jsonPath("$[1].activity.id").value(1001))
               .andExpect(jsonPath("$[2].activity.id").value(1002))
               .andExpect(jsonPath("$[3].activity.id").value(1003))
               .andExpect(jsonPath("$[4].activity.id").value(1004));
    }
}




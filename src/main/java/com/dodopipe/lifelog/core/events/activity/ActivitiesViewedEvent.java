package com.dodopipe.lifelog.core.events.activity;

import com.dodopipe.lifelog.core.domain.ActivityDetail;
import com.dodopipe.lifelog.core.events.ViewedEvent;

import java.util.List;

/**
 * @author Henry Yan
 */
public class ActivitiesViewedEvent extends ViewedEvent {

    private List<ActivityDetail> activities;

    public ActivitiesViewedEvent() {

    }

    public ActivitiesViewedEvent(List<ActivityDetail> activities) {

        this.activities = activities;
    }

    public List<ActivityDetail> getActivities() {

        return activities;
    }
}

package com.dodopipe.lifelog.persistence.repositories.impl;

import com.dodopipe.lifelog.persistence.PersistenceException;
import com.dodopipe.lifelog.persistence.domain.MediaObject;
import com.dodopipe.lifelog.persistence.repositories.BlobRepository;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Repository
public class MediaFileBlobRepositoryImpl
        implements BlobRepository<MediaObject, String> {

    private static final String META_CACHE_FILE_NAME = "com.dodopipe.lifelog.meta";

    private File storage;
    private File metaCacheFile;
    private Map<String, String> meta;

    public MediaFileBlobRepositoryImpl() {

        this(System.getProperty("java.io.tmpdir"));
    }

    public MediaFileBlobRepositoryImpl(String fpath) {

        this.storage = new File(fpath);
        this.metaCacheFile = new File(this.storage,
                                      META_CACHE_FILE_NAME);

        if (this.metaCacheFile.exists()) {
            this.meta = loadMeta();
        } else {
            this.meta = new HashMap<String, String>();
        }

    }


    @Override
    public <S extends MediaObject> S save(S entity) {

        String key = UUID.randomUUID()
                         .toString();
        File f = new File(storage,
                          key);
        InputStream in = entity.getInputStream();
        assert (in != null);
        try {
            Files.copy(in,
                       f.toPath());
            meta.put(key,
                     entity.getContentType());
            sinkMeta();
        } catch (IOException e) {

            throw new PersistenceException(e);

        }
        entity.setInputStream(null);
        entity.setKey(key);
        return entity;
    }

    @Override
    public MediaObject findById(String id) {

        File file = new File(storage,
                             id);

        if (!file.exists()) {
            return null;
        }
        FileInputStream is = null;
        String contentType = null;
        try {
            is = new FileInputStream(file);
            contentType = meta.get(id);
        } catch (IOException e) {
            throw new PersistenceException(e);
        }
        MediaObject obj = new MediaObject(is,
                                          contentType,
                                          file.length());
        obj.setKey(id);
        return obj;
    }

    @SuppressWarnings("unchecked")
    private Map<String, String> loadMeta() {

        Map<String, String> rst = null;
        try (ObjectInputStream in = new ObjectInputStream(
                new FileInputStream(metaCacheFile))) {
            rst = (Map<String, String>) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            rst = new HashMap<String, String>();
        }
        return rst;
    }

    private void sinkMeta()
            throws
            IOException {

        try (ObjectOutputStream out = new ObjectOutputStream(
                new FileOutputStream(metaCacheFile))) {
            out.writeObject(this.meta);
        } catch (IOException e) {
            throw e;
        }
    }

}

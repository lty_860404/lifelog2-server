package com.dodopipe.lifelog.core.ports.persistence;

import com.dodopipe.lifelog.core.events.activity.*;
import com.dodopipe.lifelog.core.services.exception.ActivityAttendeeNotFoundException;
import com.dodopipe.lifelog.core.services.exception.ActivityNotFoundException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByIdException;

/**
 * @author Henry Yan
 */
public interface ActivityCommandEventHandler {

    public ActivityCreatedEvent createWeddingActivity(ActivityCreateEvent event)
            throws
            UserNotFoundByIdException;

    public ActivityUpdatedEvent updateWeddingActivity(ActivityUpdateEvent event)
            throws
            ActivityNotFoundException;

    public ActivityDeletedEvent deleteActivity(ActivityDeleteEvent event)
            throws
            ActivityNotFoundException;

    public ActivityAttendeesInvitedEvent inviteAttendees(ActivityAttendeesInviteEvent event)
            throws
            ActivityNotFoundException;

    public void addToExternalInvitation(Long inviterId,
                                        String inviteePhone);

    public ActivityInvitationAcceptedEvent acceptInvitation(ActivityInvitationAcceptEvent event)
            throws
            ActivityAttendeeNotFoundException;

    public ActivityInvitationRejectedEvent rejectInvitation(ActivityInvitationRejectEvent event)
            throws
            ActivityAttendeeNotFoundException;

    public ActivityAttendeeUpdatedEvent updateAttendeeInfo(ActivityAttendeeUpdateEvent event);
}




package com.dodopipe.lifelog.core.events.photo;

import com.dodopipe.lifelog.core.events.DeletedEvent;

/**
 * @author Henry Yan
 */
public class PhotoDeleteEvent extends DeletedEvent {
    private Long photoId;

    public PhotoDeleteEvent(Long photoId) {
        this.photoId = photoId;
    }

    public Long getPhotoId() {

        return photoId;
    }
}




package com.dodopipe.lifelog.persistence.domain.photo;

import java.io.Serializable;

/**
 * @author Henry Yan
 */
public class PhotoLikePrimaryKey implements Serializable {
    private Long photoId;
    private Long likingId;

    public PhotoLikePrimaryKey() {

    }

    public PhotoLikePrimaryKey(Long photoId, Long likingId) {
        this.photoId = photoId;
        this.likingId = likingId;
    }

    public Long getPhotoId() {

        return photoId;
    }

    public void setPhotoId(Long photoId) {

        this.photoId = photoId;
    }

    public Long getLikingId() {

        return likingId;
    }

    public void setLikingId(Long likingId) {

        this.likingId = likingId;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof PhotoLikePrimaryKey)) {
            return false;
        }

        PhotoLikePrimaryKey that = (PhotoLikePrimaryKey) o;

        if (!likingId.equals(that.likingId)) {
            return false;
        }
        if (!photoId.equals(that.photoId)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {

        int result = photoId.hashCode();
        result = 31 * result + likingId.hashCode();
        return result;
    }
}

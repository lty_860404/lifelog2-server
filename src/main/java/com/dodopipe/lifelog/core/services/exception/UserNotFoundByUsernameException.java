package com.dodopipe.lifelog.core.services.exception;

/**
 * @author Henry Yan
 */
public class UserNotFoundByUsernameException extends UserNotFoundException {

    private String username;

    public UserNotFoundByUsernameException(String username) {

        this.username = username;
    }

    public UserNotFoundByUsernameException(String username,
                                           Throwable cause) {

        super(cause);
        this.username = username;
    }

    public String getMessage() {

        return "The user with username = " + username + " cannot be found";
    }
}

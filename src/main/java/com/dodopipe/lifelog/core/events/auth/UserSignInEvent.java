package com.dodopipe.lifelog.core.events.auth;

/**
 * @author Henry Yan
 */
public class UserSignInEvent {

    private String username;
    private String phone;
    private String password;

    public UserSignInEvent(String username,
                           String phone,
                           String password) {

        this.username = username;
        this.phone = phone;
        this.password = password;
    }

    public String getUsername() {

        return username;
    }

    public String getPhone() {

        return phone;
    }

    public String getPassword() {

        return password;
    }
}

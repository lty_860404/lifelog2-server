package com.dodopipe.lifelog.core.services.exception;

/**
 * Created by henryhome on 9/1/14.
 */
public class VerificationCodeExpiredException extends ServiceException {

    public VerificationCodeExpiredException() {

    }

    public VerificationCodeExpiredException(String message) {

        super(message);
    }

    public VerificationCodeExpiredException(String message,
                                            Throwable cause) {

        super(message,
              cause);
    }

    public VerificationCodeExpiredException(Throwable cause) {

        super(cause);
    }

    public VerificationCodeExpiredException(String message,
                                            Throwable cause,
                                            boolean enableSuppression,
                                            boolean writableStackTrace) {

        super(message,
              cause,
              enableSuppression,
              writableStackTrace);
    }
}

package com.dodopipe.lifelog.core.events.activity;

import com.dodopipe.lifelog.core.events.ViewEvent;

/**
 * @author Henry Yan
 */
public class ActivityViewEvent extends ViewEvent {

    private Long activityId;
    private Long userId;

    public ActivityViewEvent(Long activityId, Long userId) {

        this.activityId = activityId;
        this.userId = userId;
    }

    public Long getActivityId() {

        return activityId;
    }

    public Long getUserId() {

        return userId;
    }
}




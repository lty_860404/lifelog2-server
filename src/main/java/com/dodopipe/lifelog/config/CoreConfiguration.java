package com.dodopipe.lifelog.config;


import com.dodopipe.lifelog.core.ports.notification.MessageEventHandler;
import com.dodopipe.lifelog.core.ports.persistence.*;
import com.dodopipe.lifelog.core.services.*;
import com.dodopipe.lifelog.core.services.impl.*;
import com.dodopipe.lifelog.notification.sms.MessageNotificationEventHandler;
import com.dodopipe.lifelog.persistence.adaptors.*;
import com.dodopipe.lifelog.persistence.domain.MediaObject;
import com.dodopipe.lifelog.persistence.repositories.BlobRepository;
import com.dodopipe.lifelog.persistence.repositories.ML10NStringRepository;
import com.dodopipe.lifelog.persistence.repositories.activity.ActivityAttendeeRepository;
import com.dodopipe.lifelog.persistence.repositories.activity.ActivityRepository;
import com.dodopipe.lifelog.persistence.repositories.family.*;
import com.dodopipe.lifelog.persistence.repositories.photo.PhotoCommentRepository;
import com.dodopipe.lifelog.persistence.repositories.photo.PhotoLikeRepository;
import com.dodopipe.lifelog.persistence.repositories.photo.PhotoRepository;
import com.dodopipe.lifelog.persistence.repositories.user.PhoneVerificationCodeRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserAccountRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class CoreConfiguration {

    @Configuration
    @PropertySource("classpath:/verification_intg.properties")
    static class UserAccountConfiguration {

    }

    @Configuration
    @PropertySource("classpath:/twilio_intg.properties")
    static class MessageConfiguration {

    }

    @Autowired
    PhoneVerificationCodeRepository verifyRepo;
    @Autowired
    PhotoRepository photoRepo;
    @Autowired
    PhotoCommentRepository photoCommentRepo;
    @Autowired
    PhotoLikeRepository photoLikeRepo;
    @Autowired
    BlobRepository<MediaObject, String> blobRepo;
    @Autowired
    UserRepository userRepo;
    @Autowired
    UserAccountRepository accountRepo;
    @Autowired
    PhoneVerificationCodeRepository phoneRepo;
    @Autowired
    FamilyRepository familyRepo;
    @Autowired
    ML10NStringRepository mRepo;
    @Autowired
    FamilyInvitationRepository familyInvitationRepo;
    @Autowired
    ExternalInvitationRepository externalInvitationRepo;
    @Autowired
    FamilyRelationshipRepository familyRelationshipRepo;
    @Autowired
    FamilyMemberRepository familyMemberRepo;
    @Autowired
    ActivityRepository activityRepo;
    @Autowired
    ActivityAttendeeRepository activityAttendeeRepo;

    @Autowired
    private Environment env;

    @Bean
    public ActivityQueryService activityQueryService() {

        return new ActivityQueryServiceImpl(activityQueryEventHandler());
    }

    @Bean
    public ActivityCommandService activityCommandService() {

        return new ActivityCommandServiceImpl(activityCommandEventHandler());
    }

    @Bean
    public PhotoQueryService photoQueryService() {

        return new PhotoQueryServiceImpl(photoQueryEventHandler());
    }

    @Bean
    public PhotoCommandService photoCommandService() {

        return new PhotoCommandServiceImpl(photoCommandEventHandler());
    }

    @Bean
    public UserAccountService userAccountService() {

        return new UserAccountServiceImpl(userAccountEventHandler(),
                                          userEventHandler(),
                                          familyCommandEventHandler(),
                                          activityCommandEventHandler(),
                                          messageEventHandler());
    }

    @Bean
    public UserService userService() {

        return new UserServiceImpl(userEventHandler());
    }

    @Bean
    public FamilyCommandService familyCommandService() {

        return new FamilyCommandServiceImpl(familyCommandEventHandler(),
                                            familyQueryEventHandler(),
                                            userEventHandler());
    }

    @Bean
    public FamilyQueryService familyQueryService() {

        return new FamilyQueryServiceImpl(familyQueryEventHandler());
    }

    @Bean
    public FileService fileService() {

        return new FileServiceImpl(fileEventHandler());
    }

    @Bean
    public ActivityQueryEventHandler activityQueryEventHandler() {

        return new ActivityQueryPersistenceEventHandler(activityRepo,
                                                        activityAttendeeRepo,
                                                        userRepo);
    }

    @Bean
    public ActivityCommandEventHandler activityCommandEventHandler() {

        return new ActivityCommandPersistenceEventHandler(activityRepo,
                                                          activityAttendeeRepo,
                                                          userRepo,
                                                          externalInvitationRepo);
    }

    @Bean
    public PhotoQueryEventHandler photoQueryEventHandler() {

        return new PhotoQueryPersistenceEventHandler(photoRepo,
                                                     photoCommentRepo,
                                                     userRepo);
    }

    @Bean
    public PhotoCommandEventHandler photoCommandEventHandler() {

        return new PhotoCommandPersistenceEventHandler(photoRepo,
                                                       photoCommentRepo,
                                                       photoLikeRepo,
                                                       userRepo,
                                                       blobRepo);
    }

    @Bean
    public UserEventHandler userEventHandler() {

        return new UserPersistenceEventHandler(userRepo,
                                               accountRepo,
                                               familyRepo,
                                               blobRepo,
                                               mRepo);
    }

    @Bean
    public UserAccountEventHandler userAccountEventHandler() {

        UserAccountPersistenceEventHandler handler = new UserAccountPersistenceEventHandler(accountRepo,
                                                                                            phoneRepo);

        String length = env.getProperty("verificationCode.length");
        String interval = env.getProperty("verificationCode.validInterval");

        handler.setVerificationCodeLength(length);
        handler.setVerificationCodeValidInterval(interval);

        return handler;
    }

    @Bean
    public MessageEventHandler messageEventHandler() {

        MessageNotificationEventHandler handler = new MessageNotificationEventHandler(verifyRepo);

        String accountSid = env.getProperty("twilio.accountSid");
        String authToken = env.getProperty("twilio.authToken");
        String sourcePhone = env.getProperty("twilio.sourcePhone");

        handler.setAccountSid(accountSid);
        handler.setAuthToken(authToken);
        handler.setSourcePhone(sourcePhone);

        return handler;
    }

    @Bean
    public FamilyCommandEventHandler familyCommandEventHandler() {

        return new FamilyCommandPersistenceEventHandler(userRepo,
                                                        familyRepo,
                                                        externalInvitationRepo,
                                                        familyInvitationRepo,
                                                        familyMemberRepo,
                                                        familyRelationshipRepo);
    }

    @Bean
    public FamilyQueryEventHandler familyQueryEventHandler() {

        return new FamilyQueryPersistenceEventHandler(userRepo,
                                                      familyInvitationRepo,
                                                      familyRelationshipRepo,
                                                      familyMemberRepo);
    }

    @Bean
    public FileEventHandler fileEventHandler() {

        return new FilePersistenceEventEventHandler(blobRepo);
    }
}

package com.dodopipe.lifelog.rest.controller.modules.activity;

import com.dodopipe.lifelog.core.events.activity.ActivityDeleteEvent;
import com.dodopipe.lifelog.core.events.activity.ActivityDeletedEvent;
import com.dodopipe.lifelog.core.services.ActivityCommandService;
import com.dodopipe.lifelog.rest.controller.ActivityCommandController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by henryhome on 7/22/14.
 */
public class DeleteActivityTest {

    MockMvc mockMvc;

    @InjectMocks
    ActivityCommandController controller;

    @Mock
    ActivityCommandService activityCommandService;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                             .build();

    }

    @Test
    public void deleteActivityCorrectly()
            throws
            Exception {

        Long activityId = 1000l;

        ActivityDeletedEvent event = new ActivityDeletedEvent(true);

        when(activityCommandService.deleteActivity(any(ActivityDeleteEvent.class))).thenReturn(event);

        mockMvc.perform(delete("/activities/{activity_id}",
                               activityId))
               .andExpect(status().isOk())
               .andExpect(jsonPath("success").value(true));
    }
}

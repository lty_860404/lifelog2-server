package com.dodopipe.lifelog.core.events.photo;

import com.dodopipe.lifelog.core.events.ViewEvent;

/**
 * @author Henry Yan
 */
public class PhotoViewEvent extends ViewEvent {
    private Long photoId;
    private Long userId;

    public PhotoViewEvent(Long photoId, Long userId) {

        this.photoId = photoId;
        this.userId = userId;
    }

    public Long getPhotoId() {

        return photoId;
    }

    public Long getUserId() {

        return userId;
    }
}

package com.dodopipe.lifelog.core.ports.notification;

import com.dodopipe.lifelog.core.events.auth.MessageCallbackCreateEvent;
import com.dodopipe.lifelog.core.events.auth.VerificationCodeNotifiedEvent;
import com.dodopipe.lifelog.core.events.auth.VerificationCodeNotifyEvent;
import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;
import com.dodopipe.lifelog.core.services.exception.MessageNotDeliveredException;

/**
 * Created by henryhome on 7/28/14.
 */
public interface MessageEventHandler {

    public void sendVerificationCodeNotification(String countryCode,
                                                 String destinationPhone,
                                                 String verificationCode)
            throws
            InternalServerErrorException;

    public VerificationCodeNotifiedEvent notifyVerificationStatus(VerificationCodeNotifyEvent event)
            throws
            MessageNotDeliveredException,
            InternalServerErrorException;

    public void updateVerificationStatus(MessageCallbackCreateEvent event);
}




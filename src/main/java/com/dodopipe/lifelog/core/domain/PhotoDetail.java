package com.dodopipe.lifelog.core.domain;

/**
 * Created by henryhome on 8/14/14.
 */
public class PhotoDetail {

    private Photo photo;
    private String ownerUsername;
    private String ownerNickname;
    private String ownerAvatarFilePath;
    private Boolean isCurrUserLiker;

    public PhotoDetail(Photo photo,
                       String ownerUsername,
                       String ownerNickname,
                       String ownerAvatarFilePath,
                       Boolean isCurrUserLiker) {

        this.photo = photo;
        this.ownerUsername = ownerUsername;
        this.ownerNickname = ownerNickname;
        this.ownerAvatarFilePath = ownerAvatarFilePath;
        this.isCurrUserLiker = isCurrUserLiker;
    }

    public Photo getPhoto() {

        return photo;
    }

    public String getOwnerUsername() {

        return ownerUsername;
    }

    public String getOwnerNickname() {

        return ownerNickname;
    }

    public String getOwnerAvatarFilePath() {

        return ownerAvatarFilePath;
    }

    public Boolean isCurrUserLiker() {

        return isCurrUserLiker;
    }
}

package com.dodopipe.lifelog.core.events.family;

import com.dodopipe.lifelog.core.domain.User;

/**
 * Created by henryhome on 7/30/14.
 */
public class FamilyInvitationViewedEvent {
    private User inviter;
    private User invitee;
    private String relationship;

    public FamilyInvitationViewedEvent(User inviter,
                                       User invitee,
                                       String relationship) {
        this.inviter = inviter;
        this.invitee = invitee;
        this.relationship = relationship;
    }

    public User getInviter() {

        return inviter;
    }

    public User getInvitee() {

        return invitee;
    }

    public String getRelationship() {

        return relationship;
    }
}

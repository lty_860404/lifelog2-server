package com.dodopipe.lifelog.core.ports.persistence;

import com.dodopipe.lifelog.core.domain.FamilyMember;
import com.dodopipe.lifelog.core.events.family.*;
import com.dodopipe.lifelog.core.services.exception.FamilyMemberNotFoundException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByIdException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByUsernameException;

/**
 * Created by henryhome on 8/18/14.
 */
public interface FamilyQueryEventHandler {

    public FamilyMember findFamilyMember(Long familyId,
                                         Long memberId)
            throws
            FamilyMemberNotFoundException;

    public FamilyMembersViewedEvent viewAllFamilyMembers(FamilyMembersViewEvent event);

    public FamilyInvitationsViewedEvent viewAllSentInvitations(FamilyInvitationsViewEvent event);

    public FamilyInvitationsViewedEvent viewAllReceivedInvitations(FamilyInvitationsViewEvent event);

    public FamilyRelationshipsViewedEvent viewAllFamilyRelationships(FamilyRelationshipsViewEvent event);

    public FamilyMemberDetailViewedEvent viewFamilyMemberDetail(FamilyMemberDetailViewEvent event)
            throws
            UserNotFoundByIdException,
            UserNotFoundByUsernameException,
            FamilyMemberNotFoundException;
}

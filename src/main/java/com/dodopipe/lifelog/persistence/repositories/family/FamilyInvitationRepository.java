package com.dodopipe.lifelog.persistence.repositories.family;

import com.dodopipe.lifelog.persistence.domain.family.FamilyInvitationDetailEntity;
import com.dodopipe.lifelog.persistence.domain.family.FamilyInvitationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author yongwei
 */

@Repository
public interface FamilyInvitationRepository
        extends JpaRepository<FamilyInvitationEntity, Long> {

    FamilyInvitationEntity findById(Long invitationId);

    List<FamilyInvitationEntity> findByInviteePhone(String phone);

    @Query(value = "SELECT NEW com.dodopipe.lifelog.persistence.domain.family.FamilyInvitationDetailEntity(" +
            "ua.username, ua.nickname, ua.phone, ua.avatarFilePath, ub.username, ub.nickname, ub.avatarFilePath, fam) " +
            "FROM FAMILY_INVITATION fam " +
            "LEFT JOIN USER ua " +
            "ON fam.inviterId = ua.id " +
            "LEFT JOIN USER ub " +
            "ON fam.inviteeId = ub.id " +
            "WHERE fam.inviterId = ?1")
    List<FamilyInvitationDetailEntity> findAllSentInvitation(Long inviterId);

    @Query(value = "SELECT NEW com.dodopipe.lifelog.persistence.domain.family.FamilyInvitationDetailEntity(" +
            "ua.username, ua.nickname, ua.phone, ua.avatarFilePath, ub.username, ub.nickname, ub.avatarFilePath, fam) " +
            "FROM FAMILY_INVITATION fam " +
            "LEFT JOIN USER ua " +
            "ON fam.inviterId = ua.id " +
            "LEFT JOIN USER ub " +
            "ON fam.inviteeId = ub.id " +
            "WHERE fam.inviteeId = ?1")
    List<FamilyInvitationDetailEntity> findAllReceivedInvitation(Long inviteeId);

    @Query(value = "SELECT i " +
            "FROM FAMILY_INVITATION i " +
            "WHERE i.inviterId = ?1 " +
            "AND i.inviteeId = ?2 " +
            "AND i.status = 0")
    FamilyInvitationEntity findPendingInvitationByInviterIdAndInviteeId(Long inviterId,
                                                                        Long inviteeId);

    @Query(value = "SELECT i " +
            "FROM FAMILY_INVITATION i " +
            "WHERE i.inviterId = ?1 " +
            "AND i.inviteePhone = ?2 " +
            "AND i.status = 0")
    FamilyInvitationEntity findPendingInvitationByInviterIdAndInviteePhone(Long inviterId,
                                                                           String phone);
}




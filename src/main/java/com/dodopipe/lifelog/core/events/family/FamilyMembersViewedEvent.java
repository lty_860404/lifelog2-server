package com.dodopipe.lifelog.core.events.family;

import com.dodopipe.lifelog.core.domain.FamilyMember;
import com.dodopipe.lifelog.core.events.ViewEvent;

import java.util.List;

/**
 * @author yongwei
 */
public class FamilyMembersViewedEvent
        extends ViewEvent {

    private final List<FamilyMember> members;

    public FamilyMembersViewedEvent(List<FamilyMember> members) {

        this.members = members;
    }

    public List<FamilyMember> getMembers() {

        return members;
    }
}

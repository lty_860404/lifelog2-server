package com.dodopipe.lifelog.core.services.impl;

import com.dodopipe.lifelog.core.events.activity.*;
import com.dodopipe.lifelog.core.ports.persistence.ActivityQueryEventHandler;
import com.dodopipe.lifelog.core.services.ActivityQueryService;
import com.dodopipe.lifelog.core.services.exception.ActivityNotFoundException;
import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Henry Yan
 */
@Transactional(readOnly = true)
public class ActivityQueryServiceImpl implements ActivityQueryService {

    private ActivityQueryEventHandler handler;

    public ActivityQueryServiceImpl(ActivityQueryEventHandler handler) {

        this.handler = handler;
    }

    @Override
    public ActivitiesViewedEvent viewAllAttendingWeddingActivities(ActivitiesViewEvent event) {

        return handler.viewAllAttendingWeddingActivities(event);
    }

    @Override
    public ActivitiesViewedEvent viewAllAttendedWeddingActivities(ActivitiesViewEvent event) {

        return handler.viewAllAttendedWeddingActivities(event);
    }

    @Override
    public ActivityViewedEvent viewWeddingActivity(ActivityViewEvent event)
            throws
            ActivityNotFoundException {

        return handler.viewWeddingActivity(event);
    }

    @Override
    public ActivityAttendeesViewedEvent viewWeddingActivityAttendees(ActivityAttendeesViewEvent event)
            throws
            InternalServerErrorException {

        return handler.viewWeddingActivityAttendees(event);
    }
}




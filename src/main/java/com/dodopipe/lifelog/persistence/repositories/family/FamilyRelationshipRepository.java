package com.dodopipe.lifelog.persistence.repositories.family;

import com.dodopipe.lifelog.persistence.domain.family.FamilyRelationshipEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author tianyin.luo
 */
@Repository
public interface FamilyRelationshipRepository
        extends JpaRepository<FamilyRelationshipEntity, Long> {

    FamilyRelationshipEntity findByDisplayName(String displayName);

    @Query(value = "SELECT rs " +
            "FROM FAMILY_RELATIONSHIP rs " +
            "WHERE (rs.userId = ?1 OR rs.userId = null) " +
            "AND rs.langCode = ?2 " +
            "ORDER BY rs.relationshipScope DESC")
    List<FamilyRelationshipEntity> findAllByUserIdAndLangCode(Long userId,
                                                              String langCode);
}




package com.dodopipe.lifelog.persistence.repositories;

import com.dodopipe.lifelog.config.DBUnitDataSourceConfiguration;
import com.dodopipe.lifelog.config.DataSourceConfiguration;
import com.dodopipe.lifelog.config.JPAConfiguration;
import com.dodopipe.lifelog.config.LifelogActiveProfilesResolver;
import com.dodopipe.lifelog.database.utils.DatabaseUtil;
import com.dodopipe.lifelog.persistence.domain.photo.PhotoCommentDetailEntity;
import com.dodopipe.lifelog.persistence.domain.photo.PhotoDetailEntity;
import com.dodopipe.lifelog.persistence.domain.photo.PhotoEntity;
import com.dodopipe.lifelog.persistence.domain.user.UserEntity;
import com.dodopipe.lifelog.persistence.repositories.activity.ActivityRepository;
import com.dodopipe.lifelog.persistence.repositories.photo.PhotoCommentRepository;
import com.dodopipe.lifelog.persistence.repositories.photo.PhotoRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserRepository;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPAConfiguration.class,
                                 DataSourceConfiguration.class,
                                 DBUnitDataSourceConfiguration.class})
@ActiveProfiles(resolver = LifelogActiveProfilesResolver.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class
                        })
@DatabaseSetup("DB_SETUP.xml")
public class PhotoRepositoryTest {

    @Autowired
    private PhotoRepository photoRepo;
    @Autowired
    private PhotoCommentRepository photoCommentRepo;
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private ActivityRepository activityRepo;

    @Test
    public void testCreatePhotos() {

        System.out.println("====================== testCreatePhotos =============");
        PhotoEntity photo = new PhotoEntity();
        photo.setOwnerId(1000l);
        photo.setCaption("comment");
        photo.addPhoto("photoA",
                       "image/jpeg");
        photo.addPhoto("photoB",
                       "image/jpeg");

        photoRepo.save(photo);
        PhotoEntity createdPhotoEntity = photoRepo.findById(photo.getId());
        assertEquals("comment",
                     createdPhotoEntity.getCaption());
        assertEquals("photoA#photoB",
                     createdPhotoEntity.getOriginalFilePaths());
        assertEquals("image/jpeg#image/jpeg",
                     createdPhotoEntity.getContentTypes());
    }

    @Test
    public void testFindPhotoById() {

        System.out.println("====================== testFindPhotoById =============");
        DatabaseUtil.populatePhotoWithBoundActivitiesNormal(photoRepo,
                                                            activityRepo);

        PhotoEntity photo = photoRepo.findById(3000l);
        assertNotNull(photo.getCaption());
        assertEquals(3,
                     photo.getActivities()
                          .size());
    }

    @Test
    public void testFindAllPhotosForUser() {

        TimeZone.setDefault(TimeZone.getTimeZone("Etc/UTC"));

        System.out.println("====================== testFindAllPhotosForUser =============");

        DatabaseUtil.populatePhotoWithBoundActivitiesExtended(photoRepo,
                                                              activityRepo);

        PageRequest pageRequest = new PageRequest(0,
                                                  40,
                                                  new Sort(Sort.Direction.DESC,
                                                           "lastModifiedTime"));

        UserEntity user = userRepo.findById(1000l);
        List<PhotoDetailEntity> photos = photoRepo.findAllPhotosForUser(user.getId(),
                                                                        user.getFamily()
                                                                            .getId(),
                                                                        pageRequest);
        for (PhotoDetailEntity photoDetails : photos) {
            System.out.println("id => " + photoDetails.getPhotoEntity()
                                                      .getId());
            System.out.println("lastModifiedTime => " + photoDetails.getPhotoEntity()
                                                                    .getLastModifiedTime());
            System.out.println("isCurrUserLiker => " + photoDetails.isCurrUserLiker());
        }

        assertEquals(4,
                     photos.size());
    }

    @Test
    public void testFindPhotosByActivity() {

        DatabaseUtil.populatePhotoWithBoundActivitiesNormal(photoRepo,
                                                            activityRepo);

        PageRequest pageRequest = new PageRequest(0,
                                                  40,
                                                  new Sort(Sort.Direction.DESC,
                                                           "lastModifiedTime"));

        List<PhotoDetailEntity> photos = photoRepo.findPhotosByActivity(1000l,
                                                                        1111l,
                                                                        pageRequest);

        for (PhotoDetailEntity photoDetails : photos) {
            System.out.println("id => " + photoDetails.getPhotoEntity()
                                                      .getId());
            System.out.println("lastModifiedTime => " + photoDetails.getPhotoEntity()
                                                                    .getLastModifiedTime());
            System.out.println("currUserLiked => " + photoDetails.isCurrUserLiker());
        }

        assertEquals(2,
                     photos.size());

    }

    @Test
    public void testCascadeType() {

        List<PhotoEntity> populatedPhotos = DatabaseUtil.populatePhotoWithBoundActivitiesNormal(photoRepo,
                                                                                                activityRepo);

        assertNotNull(activityRepo.findById(1111l));
        assertNotNull(activityRepo.findById(2222l));
        assertNotNull(activityRepo.findById(3333l));

        photoRepo.delete(populatedPhotos.get(1));
        photoRepo.flush();

        assertNotNull(activityRepo.findById(1111l));
        assertNotNull(activityRepo.findById(2222l));
        assertNotNull(activityRepo.findById(3333l));
    }

    @Test
    public void testUpdatePhotoCaption() {

        PhotoEntity original = photoRepo.findById(3000l);
        assertEquals("Eating famous seafood",
                     original.getCaption());

        // Need to use JPA cached DAO to update the database
        original.setCaption("Drinking famous beer");

        photoRepo.saveAndFlush(original);

        PhotoEntity changed = photoRepo.findById(3000l);
        assertEquals("Drinking famous beer",
                     changed.getCaption());
    }

    @Test
    public void testUpdatePhotoLike() {

        String likeStr = "Chloe Shannon@likestone#";

        int rows = photoRepo.updatePhotoLike(likeStr,
                                             GregorianCalendar.getInstance()
                                                              .getTime(),
                                             80L);

        assertEquals(rows,
                     1);
        PhotoEntity photo = photoRepo.findById(80L);
        assertEquals(photo.getLatestLikingNames(),
                     "Chloe Shannon@likestone#Roger Frank@rf43567#Margaret Lin@rosebeauty#");
    }

    @Test
    public void testUpdatePhotoComment() {

        String commentStr = "Chloe Shannon@likestone@Her smile is very sweet!#";

        int rows = photoRepo.updatePhotoComment(commentStr,
                                                GregorianCalendar.getInstance()
                                                                 .getTime(),
                                                80L);

        assertEquals(rows,
                     1);

        PhotoEntity photo = photoRepo.findById(80L);

        assertEquals(photo.getLatestComments(),
                     "Chloe Shannon@likestone@Her smile is very sweet!#Roger Frank@rf43567@The wedding dress is so pretty!#" +
                             "Margaret Lin@rosebeauty@I think the bride is really gorgeous!#");
    }

    @Test
    public void testViewAllPhotoComments()
            throws
            Exception {

        Long photoId = 3000l;

        PageRequest pageRequest = new PageRequest(0,
                                                  40,
                                                  new Sort(Sort.Direction.DESC,
                                                           "commentingTime"));

        List<PhotoCommentDetailEntity> entities = photoCommentRepo.findPhotoCommentDetailsByPhotoId(photoId,
                                                                                                    pageRequest);

        assertEquals(5,
                     entities.size());
        assertEquals(5000l,
                     entities.get(0)
                             .getPhotoCommentEntity()
                             .getCommentingId()
                             .intValue());
        assertEquals(3000l,
                     entities.get(1)
                             .getPhotoCommentEntity()
                             .getCommentingId()
                             .intValue());
        assertEquals(2000l,
                     entities.get(2)
                             .getPhotoCommentEntity()
                             .getCommentingId()
                             .intValue());
        assertEquals(1000l,
                     entities.get(3)
                             .getPhotoCommentEntity()
                             .getCommentingId()
                             .intValue());
        assertEquals(4000l,
                     entities.get(4)
                             .getPhotoCommentEntity()
                             .getCommentingId()
                             .intValue());
    }
}




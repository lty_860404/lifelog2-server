package com.dodopipe.lifelog.rest.controller.modules.photo;

import com.dodopipe.lifelog.core.domain.PhotoLike;
import com.dodopipe.lifelog.core.events.photo.PhotoLikeAddEvent;
import com.dodopipe.lifelog.core.events.photo.PhotoUpdatedEvent;
import com.dodopipe.lifelog.core.services.PhotoCommandService;
import com.dodopipe.lifelog.rest.controller.PhotoCommandController;
import com.dodopipe.lifelog.rest.controller.utils.TestUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Henry Yan
 */
public class UpdatePhotoLikeTest {
    MockMvc mockMvc;

    @InjectMocks
    PhotoCommandController controller;

    @Mock
    PhotoCommandService photoCommandService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                             .build();
    }

    @Test
    public void updatePhotoLike() throws Exception {
        PhotoLike photoLike = new PhotoLike("abcde", "that poor student");
        Long photoId = 120L;
        when(photoCommandService.addPhotoLike(any(PhotoLikeAddEvent.class)))
                .thenReturn(new PhotoUpdatedEvent(true));

        mockMvc.perform(post("/photos/{photo_id}/like", photoId)
                                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                .content(TestUtil.convertObjectToJsonBytes(photoLike)))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.success").value(true));

        verify(photoCommandService, times(1)).addPhotoLike(any(PhotoLikeAddEvent.class));
        verifyNoMoreInteractions(photoCommandService);

    }
}

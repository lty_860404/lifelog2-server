package com.dodopipe.lifelog.persistence.repositories;

import com.dodopipe.lifelog.config.LifelogProfile;
import com.dodopipe.lifelog.config.RepositoryConfiguration;
import com.dodopipe.lifelog.persistence.domain.MediaObject;
import com.dodopipe.lifelog.persistence.repositories.impl.MediaS3BlobRepositoryImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.net.URLConnection;
import java.nio.file.Files;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {RepositoryConfiguration.class})
@ActiveProfiles({LifelogProfile.QACI})
public class MediaS3BlobRepositoryTest {

    @Autowired
    BlobRepository<MediaObject, String> repo;

    @Test
    public void testMediaS3BlobRepository() {

        assertTrue(repo instanceof MediaS3BlobRepositoryImpl);

    }

    @Test
    public void testSaveAndRead()
            throws
            Exception {

        ClassLoader cl = this.getClass()
                             .getClassLoader();
        URLConnection conn = cl.getResource("test.jpg")
                               .openConnection();

        MediaObject mo = new MediaObject(conn.getInputStream(),
                                         conn.getContentType(),
                                         conn.getContentLengthLong());

        MediaObject cmo = repo.save(mo);

        MediaObject fmo = repo.findById(cmo.getKey());

        File file = new File(System.getProperty("java.io.tmpdir"),
                             "test.jpg");
        if (file.exists()) {
            file.delete();
        }
        Files.copy(fmo.getInputStream(),
                   file.toPath());

        assertEquals(cmo.getContentType(),
                     fmo.getContentType());
        assertNotNull(fmo.getInputStream());

        assertTrue(cmo.getLength() == fmo.getLength());

    }

}

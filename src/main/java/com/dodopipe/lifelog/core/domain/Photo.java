package com.dodopipe.lifelog.core.domain;

import java.util.Date;
import java.util.List;

/**
 * Created by henryhome on 8/5/14.
 */
public class Photo {

    private final Long id;
    private final String caption;
    private final Date uploadedTime;
    private final Date lastModifiedTime;
    private final Integer numLikes;
    private final List<PhotoLike> latestLikes;
    private final List<PhotoComment> latestComments;
    private final List<PhotoMetadata> photoMetas;


    public Photo(Long id,
                 String caption,
                 Date uploadedTime,
                 Date lastModifiedTime,
                 Integer numLikes,
                 List<PhotoLike> latestLikes,
                 List<PhotoComment> latestComments,
                 List<PhotoMetadata> photoMetas) {

        this.id = id;
        this.caption = caption;
        this.uploadedTime = uploadedTime;
        this.lastModifiedTime = lastModifiedTime;
        this.numLikes = numLikes;
        this.latestLikes = latestLikes;
        this.latestComments = latestComments;
        this.photoMetas = photoMetas;
    }

    public Long getId() {

        return id;
    }

    public String getCaption() {

        return caption;
    }

    public Date getUploadedTime() {

        return uploadedTime;
    }

    public Date getLastModifiedTime() {

        return lastModifiedTime;
    }

    public Integer getNumLikes() {

        return numLikes;
    }

    public List<PhotoLike> getLatestLikes() {

        return latestLikes;
    }

    public List<PhotoComment> getLatestComments() {

        return latestComments;
    }

    public List<PhotoMetadata> getPhotoMetas() {

        return photoMetas;
    }
}

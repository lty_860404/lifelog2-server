package com.dodopipe.lifelog.core.events.family;

import com.dodopipe.lifelog.core.events.CreateEvent;

/**
 * @author yongwei
 */
public class FamilyMemberInviteEvent
        extends CreateEvent {

    private final Long inviterId;
    private final String inviteeUsername;
    private final String inviteeNickname;
    private final String inviteePhone;
    private final String inviterRelationship;
    private final String inviterLangCode;

    public FamilyMemberInviteEvent(Long inviterId,
                                   String inviteeUsername,
                                   String inviteeNickname,
                                   String inviteePhone,
                                   String inviterRelationship,
                                   String inviterLangCode) {

        this.inviterId = inviterId;
        this.inviteeUsername = inviteeUsername;
        this.inviteeNickname = inviteeNickname;
        this.inviteePhone = inviteePhone;
        this.inviterRelationship = inviterRelationship;
        this.inviterLangCode = inviterLangCode;
    }

    public Long getInviterId() {

        return inviterId;
    }

    public String getInviteeUsername() {

        return inviteeUsername;
    }

    public String getInviteeNickname() {

        return inviteeNickname;
    }

    public String getInviteePhone() {

        return inviteePhone;
    }

    public String getInviterRelationship() {

        return inviterRelationship;
    }

    public String getInviterLangCode() {

        return inviterLangCode;
    }
}

package com.dodopipe.lifelog.config;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.weaving.DefaultContextLoadTimeWeaver;
import org.springframework.core.env.Environment;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.instrument.classloading.LoadTimeWeaver;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfiguration {

    @Configuration
    @Profile(LifelogProfile.INTG)
    @PropertySource("classpath:/jdbc_intg.properties")
    static class MysqlQAConfiguration {

    }

    @Configuration
    @Profile(LifelogProfile.QACI)
    @PropertySource("classpath:/jdbc_qaci.properties")
    static class MysqlQaciConfiguration {

    }

    @Configuration
    @Profile(LifelogProfile.PROD)
    @PropertySource("classpath:/jdbc_prod.properties")
    static class MysqlProdConfiguration {

    }


    @Autowired
    Environment env;

    @Bean
    @Profile(LifelogProfile.DEVE)
    public DataSource h2DataSource() {

        SimpleDriverDataSource ds = new SimpleDriverDataSource();
        ds.setDriverClass(org.h2.Driver.class);
        ds.setUrl("jdbc:h2:mem:test;MODE=MySQL;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE");
        ds.setUsername("sa");
        ds.setPassword("");

        return ds;
    }

    @Bean
    @Profile(value = {LifelogProfile.INTG,
                      LifelogProfile.QACI,
                      LifelogProfile.PROD})
    public DataSource mysqlDataSource() {

        BasicDataSource dataSource = new BasicDataSource();
        String driverClassName = env.getProperty("jdbc.driverClassName");
        String dbUrl = env.getProperty("jdbc.url");
        String dbUsername = env.getProperty("jdbc.username");
        String dbPassword = env.getProperty("jdbc.password");
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(dbUsername);
        dataSource.setPassword(dbPassword);
        return dataSource;

    }

    @Bean
    @Profile(value = {LifelogProfile.DEVE,
                      LifelogProfile.INTG,
                      LifelogProfile.QACI,
                      LifelogProfile.PROD})
    public LoadTimeWeaver loadTimeWeaver() {

        return new InstrumentationLoadTimeWeaver();
    }

}

package com.dodopipe.lifelog.security;

import org.eclipse.persistence.oxm.MediaType;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.util.Assert;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author yongwei
 */
public class SignatureAuthenticationEntryPoint
        implements AuthenticationEntryPoint,
                   InitializingBean {

    private final String realmName;

    public SignatureAuthenticationEntryPoint(String realmName) {

        this.realmName = realmName;
    }

    @Override
    public void afterPropertiesSet()
            throws
            Exception {

        Assert.hasText(realmName,
                       "realmName must be specified");

    }

    @Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException authException)
            throws
            IOException,
            ServletException {

        response.setHeader("WWW-Authenticate",
                           "DOP realm=\"" + realmName + "\"");
        handleAuthenticationException(authException,
                                      response);

    }

    public String getRealmName() {

        return realmName;
    }

    private void handleAuthenticationException(AuthenticationException authException,
                                               HttpServletResponse response)
            throws
            IOException,
            ServletException {

        if (authException instanceof BadCredentialsException) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
                               AuthenticationExceptionMessage.BAD_CREDENTIAL.toJson());
            return;
        }

        if (authException instanceof SignatureNotFoundException) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
                               AuthenticationExceptionMessage.SIGNATURE_NOT_FOUND.toJson());
        }

        if (authException instanceof RequestExpiredException) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
                               AuthenticationExceptionMessage.REQUEST_TIMEOUT.toJson());
        }

        if (authException instanceof AccountNotFoundException) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
                               AuthenticationExceptionMessage.ACCOUNT_NOT_FOUND.toJson());
        }

        if (authException instanceof AccountExpiredException) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
                               AuthenticationExceptionMessage.USER_ACCOUNT_EXPIRED.toJson());
        }

        if (authException instanceof CredentialsExpiredException) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
                               AuthenticationExceptionMessage.CREDENTIAL_EXPIRED.toJson());
        }

        sendResponse(response,
                     AuthenticationExceptionMessage.UN_AUTHORIZED);
    }

    private void sendResponse(HttpServletResponse response,
                              AuthenticationExceptionMessage message)
            throws
            IOException {

        response.setContentType(MediaType.APPLICATION_JSON.getMediaType());
        PrintWriter pw = response.getWriter();
        pw.print(message.toJson());

    }


}

package com.dodopipe.lifelog.core.events.auth;

import com.dodopipe.lifelog.core.events.CreateEvent;

public class VerificationCodeCreateEvent
        extends CreateEvent {

    private final String countryCode;
    private final String phone;

    public VerificationCodeCreateEvent(final String countryCode, String phone) {

        this.countryCode = countryCode;
        this.phone = phone;
    }

    public String getPhone() {

        return this.phone;
    }

    public String getCountryCode() {

        return countryCode;
    }
}

package com.dodopipe.lifelog.core.services.impl;

import com.dodopipe.lifelog.core.events.auth.UserSignOutEvent;
import com.dodopipe.lifelog.core.events.auth.UserSignedOutEvent;
import com.dodopipe.lifelog.core.events.user.*;
import com.dodopipe.lifelog.core.ports.persistence.UserEventHandler;
import com.dodopipe.lifelog.core.services.UserService;
import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByIdException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByPhoneException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByUsernameException;
import com.dodopipe.lifelog.security.AccountNotFoundException;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Henry Yan
 */
@Transactional
public class UserServiceImpl implements UserService {

    private UserEventHandler userHandler;

    public UserServiceImpl(UserEventHandler userHandler) {

        this.userHandler = userHandler;
    }

    @Override
    public UserSignedOutEvent signOut(UserSignOutEvent event)
            throws
            AccountNotFoundException {

        return userHandler.signOut(event);
    }

    @Override
    public PasswordUpdatedEvent changePassword(PasswordUpdateEvent event)
            throws
            AccountNotFoundException,
            InternalServerErrorException {

        return userHandler.changePassword(event);
    }

    @Override
    public UserViewedEvent viewUser(UserViewEvent event)
            throws
            UserNotFoundByIdException,
            UserNotFoundByUsernameException,
            UserNotFoundByPhoneException {

        return userHandler.viewUser(event);
    }

    @Override
    public UserUpdatedEvent updateUser(UserUpdateEvent event)
            throws
            UserNotFoundByIdException,
            IllegalArgumentException {

        return userHandler.updateUser(event);
    }

    @Override
    public UserDeletedEvent deleteUser(UserDeleteEvent event)
            throws
            UserNotFoundByIdException {

        return userHandler.deleteUser(event);
    }

    @Override
    public UserAvatarChangedEvent changeAvatar(UserAvatarChangeEvent event)
            throws
            UserNotFoundByIdException {

        return userHandler.changeAvatar(event);
    }
}

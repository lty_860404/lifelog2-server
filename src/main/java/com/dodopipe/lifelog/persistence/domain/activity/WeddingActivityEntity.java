package com.dodopipe.lifelog.persistence.domain.activity;

import com.dodopipe.lifelog.core.domain.Activity;
import com.dodopipe.lifelog.core.domain.WeddingActivity;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Henry Yan
 */
@Entity(name = "WEDDING_ACTIVITY")
public class WeddingActivityEntity extends ActivityEntity implements Serializable {

    @Column(name = "PLACE",
            columnDefinition = "VARCHAR(200)")
    private String place;

    @Column(name = "GROOM_NAME",
            columnDefinition = "VARCHAR(80)")
    private String groomName;

    @Column(name = "BRIDE_NAME",
            columnDefinition = "VARCHAR(80)")
    private String brideName;

    public WeddingActivityEntity() {

    }

    public WeddingActivityEntity(Long hostId,
                                 String name,
                                 Date startTime,
                                 Date endTime,
                                 String place,
                                 String groomName,
                                 String brideName) {

        this.hostId = hostId;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.place = place;
        this.groomName = groomName;
        this.brideName = brideName;
    }

    public Activity toWeddingActivity() {

        return new WeddingActivity(getId(),
                                   getHostId(),
                                   getName(),
                                   getStartTime(),
                                   getEndTime(),
                                   getPlace(),
                                   getGroomName(),
                                   getBrideName());
    }

    public String getPlace() {

        return place;
    }

    public void setPlace(String place) {

        this.place = place;
    }

    public String getGroomName() {

        return groomName;
    }

    public void setGroomName(String groomName) {

        this.groomName = groomName;
    }

    public String getBrideName() {

        return brideName;
    }

    public void setBrideName(String brideName) {

        this.brideName = brideName;
    }
}

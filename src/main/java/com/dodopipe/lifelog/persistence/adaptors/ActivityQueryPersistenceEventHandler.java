package com.dodopipe.lifelog.persistence.adaptors;

import com.dodopipe.lifelog.core.domain.ActivityAttendee;
import com.dodopipe.lifelog.core.domain.ActivityDetail;
import com.dodopipe.lifelog.core.events.activity.*;
import com.dodopipe.lifelog.core.ports.persistence.ActivityQueryEventHandler;
import com.dodopipe.lifelog.core.services.exception.ActivityNotFoundException;
import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;
import com.dodopipe.lifelog.persistence.domain.activity.ActivityAttendeeDetailEntity;
import com.dodopipe.lifelog.persistence.domain.activity.WeddingActivityDetailEntity;
import com.dodopipe.lifelog.persistence.domain.activity.WeddingActivityEntity;
import com.dodopipe.lifelog.persistence.repositories.activity.ActivityAttendeeRepository;
import com.dodopipe.lifelog.persistence.repositories.activity.ActivityRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by henryhome on 7/10/14.
 */
public class ActivityQueryPersistenceEventHandler implements ActivityQueryEventHandler {

    private ActivityRepository activityRepo;
    private ActivityAttendeeRepository activityAttendeeRepo;
    private UserRepository userRepo;

    public ActivityQueryPersistenceEventHandler(ActivityRepository activityRepo,
                                                ActivityAttendeeRepository activityAttendeeRepo,
                                                UserRepository userRepo) {

        this.activityRepo = activityRepo;
        this.activityAttendeeRepo = activityAttendeeRepo;
        this.userRepo = userRepo;
    }

    @Override
    public ActivitiesViewedEvent viewAllAttendingWeddingActivities(ActivitiesViewEvent event) {

        Long userId = event.getUserId();
        Date currDate = GregorianCalendar.getInstance()
                                         .getTime();
        Integer pageNum = event.getPageNum();
        Integer numPerPage = event.getNumPerPage();

        PageRequest pageRequest = new PageRequest(pageNum,
                                                  numPerPage,
                                                  new Sort(Sort.Direction.ASC,
                                                           "endTime"));

        List<WeddingActivityDetailEntity> entities =
                activityRepo.findWeddingActivitiesEndAfterDateForUser(userId,
                                                                      currDate,
                                                                      pageRequest);

        List<ActivityDetail> activityDetails = new ArrayList<>();

        for (WeddingActivityDetailEntity entity : entities) {
            activityDetails.add(entity.toActivityDetails());
        }

        return new ActivitiesViewedEvent(activityDetails);
    }

    @Override
    public ActivitiesViewedEvent viewAllAttendedWeddingActivities(ActivitiesViewEvent event) {

        Long userId = event.getUserId();
        Date currDate = GregorianCalendar.getInstance()
                                         .getTime();
        Integer pageNum = event.getPageNum();
        Integer numPerPage = event.getNumPerPage();

        PageRequest pageRequest = new PageRequest(pageNum,
                                                  numPerPage,
                                                  new Sort(Sort.Direction.DESC,
                                                           "endTime"));

        List<WeddingActivityDetailEntity> entities =
                activityRepo.findWeddingActivitiesEndBeforeDateForUser(userId,
                                                                       currDate,
                                                                       pageRequest);

        List<ActivityDetail> activityDetails = new ArrayList<>();

        for (WeddingActivityDetailEntity entity : entities) {
            activityDetails.add(entity.toActivityDetails());
        }

        return new ActivitiesViewedEvent(activityDetails);
    }

    @Override
    public ActivityViewedEvent viewWeddingActivity(ActivityViewEvent event)
            throws
            ActivityNotFoundException {

        Long activityId = event.getActivityId();
        Long userId = event.getUserId();

        WeddingActivityEntity entity =
                activityRepo.findWeddingActivityByActivityIdAndAttendeeId(activityId,
                                                                          userId);

        if (entity == null) {
            throw new ActivityNotFoundException(activityId);
        }

        return new ActivityViewedEvent(entity.toWeddingActivity());
    }

    @Override
    public ActivityAttendeesViewedEvent viewWeddingActivityAttendees(ActivityAttendeesViewEvent event)
            throws
            InternalServerErrorException {

        Long activityId = event.getActivityId();
        Integer pageNum = event.getPageNum();
        Integer numPerPage = event.getNumPerPage();

        PageRequest pageRequest = new PageRequest(pageNum,
                                                  numPerPage);

        List<ActivityAttendeeDetailEntity> attendeeEntities =
                activityAttendeeRepo.findWeddingActivityAttendeesByActivityId(activityId,
                                                                              pageRequest);

        List<ActivityAttendee> attendees = new ArrayList<>();

        for (ActivityAttendeeDetailEntity attendeeEntity : attendeeEntities) {
            attendees.add(attendeeEntity.toActivityAttendee());
        }

        return new ActivityAttendeesViewedEvent(attendees);
    }
}




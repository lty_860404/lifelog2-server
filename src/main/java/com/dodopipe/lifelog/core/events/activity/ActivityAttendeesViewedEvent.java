package com.dodopipe.lifelog.core.events.activity;

import com.dodopipe.lifelog.core.domain.ActivityAttendee;
import com.dodopipe.lifelog.core.events.ViewedEvent;

import java.util.List;

/**
 * Created by henryhome on 7/21/14.
 */
public class ActivityAttendeesViewedEvent extends ViewedEvent {

    private List<ActivityAttendee> attendees;

    public ActivityAttendeesViewedEvent(List<ActivityAttendee> attendees) {

        this.attendees = attendees;
    }

    public List<ActivityAttendee> getAttendees() {

        return attendees;
    }
}




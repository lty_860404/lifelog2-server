package com.dodopipe.lifelog.persistence.repositories.user;

import com.dodopipe.lifelog.persistence.domain.user.UserAccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccountRepository
        extends JpaRepository<UserAccountEntity, Long> {

    UserAccountEntity findByAccessId(String userAccessId);

    UserAccountEntity findBySecretKey(String secretKey);

    UserAccountEntity findByUserId(Long userId);

    UserAccountEntity findByUserIdAndPassword(Long userId, String password);

    UserAccountEntity findByUsernameAndPassword(String username,
                                                String password);

    UserAccountEntity findByPhoneAndPassword(String phone,
                                             String password);
}

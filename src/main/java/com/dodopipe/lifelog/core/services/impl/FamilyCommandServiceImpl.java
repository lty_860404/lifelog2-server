package com.dodopipe.lifelog.core.services.impl;

import com.dodopipe.lifelog.core.domain.FamilyMember;
import com.dodopipe.lifelog.core.domain.User;
import com.dodopipe.lifelog.core.events.family.*;
import com.dodopipe.lifelog.core.ports.persistence.FamilyCommandEventHandler;
import com.dodopipe.lifelog.core.ports.persistence.FamilyQueryEventHandler;
import com.dodopipe.lifelog.core.ports.persistence.UserEventHandler;
import com.dodopipe.lifelog.core.services.FamilyCommandService;
import com.dodopipe.lifelog.core.services.exception.*;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author yongwei
 */
@Transactional
public class FamilyCommandServiceImpl
        implements FamilyCommandService {

    private FamilyCommandEventHandler familyCommandHandler;
    private FamilyQueryEventHandler familyQueryHandler;
    private UserEventHandler userHandler;

    public FamilyCommandServiceImpl(FamilyCommandEventHandler familyCommandHandler,
                                    FamilyQueryEventHandler familyQueryHandler,
                                    UserEventHandler userHandler) {

        this.familyCommandHandler = familyCommandHandler;
        this.familyQueryHandler = familyQueryHandler;
        this.userHandler = userHandler;
    }

    @Override
    public FamilyMemberInvitedEvent inviteFamilyMember(FamilyMemberInviteEvent event)
            throws
            UserNotFoundByIdException,
            InvalidFamilyInvitationException,
            FamilyMemberNotFoundException,
            InternalServerErrorException {

        Long inviterId = event.getInviterId();
        String inviteeUsername = event.getInviteeUsername();
        String inviteePhone = event.getInviteePhone();
        String inviterRelationship = event.getInviterRelationship();
        String inviterLangCode = event.getInviterLangCode();

        User inviter = userHandler.getUserById(inviterId);

        if (inviter == null) {
            throw new UserNotFoundByIdException(inviterId);
        }

        if (inviteeUsername == null && inviteePhone == null) {
            throw new InvalidFamilyInvitationException("Username and phone from invitee are both null");
        }

        User invitee;
        invitee = userHandler.getUserByUsername(inviteeUsername);

        if (invitee == null) {
            invitee = userHandler.getUserByPhone(inviteePhone);
        }

        if (invitee == null) {
            familyCommandHandler.sendExternalInvitation(inviterId,
                                                        inviteePhone);
            invitee = new User();
            invitee.setPhone(inviteePhone);
            return familyCommandHandler.sendFamilyInvitation(inviter,
                                                             invitee,
                                                             inviterRelationship,
                                                             inviterLangCode);
        }

        FamilyMember inviteeAsFamilyMember = familyQueryHandler.findFamilyMember(inviter.getFamilyId(),
                                                                                 invitee.getId());
        if (inviteeAsFamilyMember == null) {
            return familyCommandHandler.sendFamilyInvitation(inviter,
                                                             invitee,
                                                             inviterRelationship,
                                                             inviterLangCode);
        }

        FamilyMember inviterAsFamilyMember = familyQueryHandler.findFamilyMember(invitee.getFamilyId(),
                                                                                 inviter.getId());
        if (inviterAsFamilyMember == null) {
            throw new InternalServerErrorException("Invitee is in inviter's family but inviter is not in invitee's family");
        }

        FamilyMember handledInvitee = inviteeAsFamilyMember.handleInvitingRequest(inviterAsFamilyMember);

        if (handledInvitee == null) {
            return familyCommandHandler.sendFamilyInvitation(inviter,
                                                             invitee,
                                                             inviterRelationship,
                                                             inviterLangCode);
        } else {
            familyCommandHandler.updateFamilyMemberStatus(inviteeAsFamilyMember);
            familyCommandHandler.updateFamilyMemberStatus(inviterAsFamilyMember);
            return new FamilyMemberInvitedEvent(true,
                                                -1l);
        }
    }

    @Override
    public FamilyInvitationAcceptedEvent acceptFamilyInvitation(FamilyInvitationAcceptEvent event)
            throws
            UserNotFoundByIdException,
            FamilyInvitationNotFoundException,
            FamilyMemberNotFoundException {

        FamilyInvitationViewedEvent extractedEvent = familyCommandHandler.updateFamilyInvitationStatus(event);

        User inviter = extractedEvent.getInviter();
        User invitee = extractedEvent.getInvitee();
        String inviterRelationship = extractedEvent.getRelationship();
        String inviteeRelationship = event.getInviteeRelationship();
        String inviteeLangCode = event.getInviteeLangCode();

        FamilyMember inviterAsFamilyMember = familyQueryHandler.findFamilyMember(invitee.getFamilyId(),
                                                                                 inviter.getId());
        if (inviterAsFamilyMember == null) {
            familyCommandHandler.addFamilyMember(inviter.getId(),
                                                 inviterRelationship,
                                                 invitee.getId(),
                                                 inviteeRelationship,
                                                 inviteeLangCode);
        } else {
            FamilyMember inviteeAsFamilyMember = familyQueryHandler.findFamilyMember(inviter.getFamilyId(),
                                                                                     invitee.getId());
            if (inviteeAsFamilyMember == null) {
                throw new InternalServerErrorException("Invitee is in inviter's family but inviter is not in invitee's family");
            }
            inviterAsFamilyMember.handleAcceptingRequest(inviteeAsFamilyMember);
            familyCommandHandler.updateFamilyMemberStatus(inviterAsFamilyMember);
            familyCommandHandler.updateFamilyMemberStatus(inviteeAsFamilyMember);
        }

        return new FamilyInvitationAcceptedEvent(true);
    }

    @Override
    public FamilyMemberUpdatedEvent updateFamilyMember(FamilyMemberUpdateEvent event)
            throws
            UserNotFoundByIdException,
            UserNotFoundByUsernameException,
            FamilyMemberNotFoundException {

        return familyCommandHandler.updateFamilyMember(event);
    }

    @Override
    public FamilyMemberRemovedEvent deleteFamilyMember(FamilyMemberRemoveEvent event)
            throws
            UserNotFoundByIdException,
            UserNotFoundByUsernameException,
            FamilyMemberNotFoundException,
            InternalServerErrorException {

        User executor = userHandler.getUserById(event.getInviterId());

        if (executor == null) {
            throw new UserNotFoundByIdException(event.getInviterId());
        }

        User recipient = userHandler.getUserByUsername(event.getInviteeUsername());

        if (recipient == null) {
            throw new UserNotFoundByUsernameException(event.getInviteeUsername());
        }

        FamilyMember recipientAsFamilyMember = familyQueryHandler.findFamilyMember(executor.getFamilyId(),
                                                                                   recipient.getId());
        if (recipientAsFamilyMember == null) {
            throw new InternalServerErrorException("The recipient is not in executor's family");
        }

        FamilyMember executorAsFamilyMember = familyQueryHandler.findFamilyMember(recipient.getFamilyId(),
                                                                                  executor.getId());

        if (executorAsFamilyMember == null) {
            throw new InternalServerErrorException("The executor is not in recipient's family");
        }

        recipientAsFamilyMember.handleRemovingRequest(executorAsFamilyMember);

        familyCommandHandler.updateFamilyMemberStatus(executorAsFamilyMember);
        familyCommandHandler.updateFamilyMemberStatus(recipientAsFamilyMember);

        return new FamilyMemberRemovedEvent(true);
    }
}




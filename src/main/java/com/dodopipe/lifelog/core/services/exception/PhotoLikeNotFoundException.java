package com.dodopipe.lifelog.core.services.exception;

/**
 * Created by henryhome on 8/5/14.
 */
public class PhotoLikeNotFoundException extends ServiceException {

    private Long photoId;
    private Long userId;

    public PhotoLikeNotFoundException(Long photoId,
                                      Long userId) {

        this.photoId = photoId;
        this.userId = userId;
    }

    public PhotoLikeNotFoundException(Long photoId,
                                      Long userId,
                                      Throwable cause) {

        super(cause);
        this.photoId = photoId;
        this.userId = userId;
    }

    public String getMessage() {

        return "The photo like with photo id = " + photoId + " and user id = " + userId + " is not found";
    }
}

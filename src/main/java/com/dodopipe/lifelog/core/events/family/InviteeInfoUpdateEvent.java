package com.dodopipe.lifelog.core.events.family;

/**
 * Created by henryhome on 7/29/14.
 */
public class InviteeInfoUpdateEvent {

    private Long inviteeId;

    private String inviteeName;

    private String inviteePhone;

    public InviteeInfoUpdateEvent(Long inviteeId,
                                  String inviteeName,
                                  String inviteePhone) {
        this.inviteeId = inviteeId;
        this.inviteeName = inviteeName;
        this.inviteePhone = inviteePhone;
    }

    public Long getInviteeId() {

        return inviteeId;
    }

    public String getInviteeName() {

        return inviteeName;
    }

    public String getInviteePhone() {

        return inviteePhone;
    }
}




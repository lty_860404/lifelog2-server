package com.dodopipe.lifelog.rest.controller;

import com.dodopipe.lifelog.core.domain.BlobObject;
import com.dodopipe.lifelog.core.events.file.FileViewEvent;
import com.dodopipe.lifelog.core.events.file.FileViewedEvent;
import com.dodopipe.lifelog.core.services.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Henry Yan
 */
@RequestMapping("/files")
public class FileController {

    @Autowired
    private FileService fileService;

    @RequestMapping(value = "/{file_key}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> viewFile(@PathVariable("file_key") String fileKey)
            throws Exception {
        FileViewedEvent event = fileService.viewFile(new FileViewEvent(fileKey));
        BlobObject obj = event.getFile();

        byte[] data = new byte[(int) obj.getLength()];
        obj.getInputStream().read(data);

        return new ResponseEntity<>(data, HttpStatus.OK);
    }
}

package com.dodopipe.lifelog.core.events.user;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by henryhome on 8/6/14.
 */
public class UserAvatarChangedEvent {

    public boolean success;
    public String avatarFilePath;

    public UserAvatarChangedEvent(boolean success,
                                  String avatarFilePath) {

        this.success = success;
        this.avatarFilePath = avatarFilePath;
    }

    public boolean isSuccess() {

        return success;
    }

    public String getAvatarFilePath() {

        return avatarFilePath;
    }

    public Map<String, Object> toMap() {

        Map<String, Object> map = new HashMap<>();
        map.put("success",
                isSuccess());
        map.put("avatarFilePath",
                getAvatarFilePath());
        return map;
    }
}




package com.dodopipe.lifelog.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class UserKeyDetails
        implements UserDetails {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private final String userName;
    private final String secretKey;
    private final boolean isAccountExpired;
    private final boolean isCredentialsExpired;

    private final Collection<? extends GrantedAuthority> authorities;

    public UserKeyDetails(final long userId,
                          final String secretKey,
                          final boolean isAccountExpired,
                          final boolean isCredentialsExpired,
                          final Collection<? extends GrantedAuthority> authorities) {

        this.userName = Long.toString(userId);
        this.secretKey = secretKey;
        this.isAccountExpired = isAccountExpired;
        this.isCredentialsExpired = isCredentialsExpired;
        this.authorities = authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        return this.authorities;
    }

    @Override
    public String getPassword() {

        return this.secretKey;
    }

    @Override
    public String getUsername() {

        return this.userName;
    }

    @Override
    public boolean isAccountNonExpired() {

        return !this.isAccountExpired;
    }

    @Override
    public boolean isAccountNonLocked() {

        return !this.isAccountExpired;
    }

    @Override
    public boolean isCredentialsNonExpired() {

        return !this.isCredentialsExpired;
    }

    @Override
    public boolean isEnabled() {

        return !(isAccountExpired || isCredentialsExpired);
    }

    public String getSecretKey() {

        return this.secretKey;
    }

}

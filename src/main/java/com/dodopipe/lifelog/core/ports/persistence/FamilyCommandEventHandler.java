package com.dodopipe.lifelog.core.ports.persistence;

import com.dodopipe.lifelog.core.domain.FamilyMember;
import com.dodopipe.lifelog.core.domain.User;
import com.dodopipe.lifelog.core.events.family.*;
import com.dodopipe.lifelog.core.services.exception.FamilyInvitationNotFoundException;
import com.dodopipe.lifelog.core.services.exception.FamilyMemberNotFoundException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByIdException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByUsernameException;

/**
 * @author yongwei
 */
public interface FamilyCommandEventHandler {

    public FamilyMemberInvitedEvent sendFamilyInvitation(User inviter,
                                                         User invitee,
                                                         String relationship,
                                                         String langCode);

    public void sendExternalInvitation(Long inviterId,
                                       String inviteePhone);

    public void updateFamilyMemberStatus(FamilyMember familyMember)
            throws
            FamilyMemberNotFoundException;

    public FamilyInvitationViewedEvent updateFamilyInvitationStatus(FamilyInvitationAcceptEvent event)
            throws
            FamilyInvitationNotFoundException,
            UserNotFoundByIdException;

    public ExternalInvitationUpdatedEvent updateExternalInvitationStatus(ExternalInvitationUpdateEvent event);

    public InviteeInfoUpdatedEvent updateFamilyInvitationInviteeInfo(InviteeInfoUpdateEvent event);


    public FamilyInvitationAcceptedEvent addFamilyMember(Long inviterId,
                                                         String invitersRelationship,
                                                         Long inviteeId,
                                                         String inviteesRelationship,
                                                         String inviteeLangCode);

    public FamilyMemberUpdatedEvent updateFamilyMember(FamilyMemberUpdateEvent event)
            throws
            UserNotFoundByIdException,
            UserNotFoundByUsernameException,
            FamilyMemberNotFoundException;
}

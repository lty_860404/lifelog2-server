package com.dodopipe.lifelog.persistence.adaptors;

import com.dodopipe.lifelog.core.domain.FamilyInvitationDetail;
import com.dodopipe.lifelog.core.domain.FamilyMember;
import com.dodopipe.lifelog.core.domain.FamilyRelationship;
import com.dodopipe.lifelog.core.events.family.*;
import com.dodopipe.lifelog.core.ports.persistence.FamilyQueryEventHandler;
import com.dodopipe.lifelog.core.services.exception.FamilyMemberNotFoundException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByIdException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByUsernameException;
import com.dodopipe.lifelog.persistence.domain.family.FamilyInvitationDetailEntity;
import com.dodopipe.lifelog.persistence.domain.family.FamilyMemberEntity;
import com.dodopipe.lifelog.persistence.domain.family.FamilyMemberPrimaryKey;
import com.dodopipe.lifelog.persistence.domain.family.FamilyRelationshipEntity;
import com.dodopipe.lifelog.persistence.domain.user.UserEntity;
import com.dodopipe.lifelog.persistence.repositories.family.*;
import com.dodopipe.lifelog.persistence.repositories.user.UserRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by henryhome on 8/18/14.
 */
public class FamilyQueryPersistenceEventHandler implements FamilyQueryEventHandler {

    private UserRepository userRepo;
    private FamilyInvitationRepository familyInvitationRepo;
    private FamilyRelationshipRepository familyRelationshipRepo;
    private FamilyMemberRepository familyMemberRepo;

    public FamilyQueryPersistenceEventHandler(UserRepository userRepo,
                                              FamilyInvitationRepository familyInvitationRepo,
                                              FamilyRelationshipRepository familyRelationshipRepo,
                                              FamilyMemberRepository familyMemberRepo) {

        this.userRepo = userRepo;
        this.familyInvitationRepo = familyInvitationRepo;
        this.familyRelationshipRepo = familyRelationshipRepo;
        this.familyMemberRepo = familyMemberRepo;
    }

    @Override
    public FamilyMember findFamilyMember(Long familyId,
                                         Long memberId)
            throws
            FamilyMemberNotFoundException {

        FamilyMemberEntity familyMemberEntity = familyMemberRepo.findOne(new FamilyMemberPrimaryKey(familyId,
                                                                                                    memberId));
        if (familyMemberEntity == null) {
            throw new FamilyMemberNotFoundException(familyId,
                                                    memberId);
        }
        return familyMemberEntity.toFamilyMember();
    }

    @Override
    public FamilyMembersViewedEvent viewAllFamilyMembers(FamilyMembersViewEvent event) {

        UserEntity user = userRepo.findOne(event.getUserId());
        if (user == null) {
            return new FamilyMembersViewedEvent(Collections.<FamilyMember>emptyList());
        }

        return new FamilyMembersViewedEvent(user.getFamily()
                                                .getAllMembers());
    }

    @Override
    public FamilyInvitationsViewedEvent viewAllSentInvitations(FamilyInvitationsViewEvent event) {

        List<FamilyInvitationDetailEntity> entities =
                familyInvitationRepo.findAllSentInvitation(event.getUserId());

        List<FamilyInvitationDetail> invitations = new ArrayList<>();

        for (FamilyInvitationDetailEntity entity : entities) {
            invitations.add(entity.toFamilyInvitationDetail());
        }

        return new FamilyInvitationsViewedEvent(invitations);
    }

    @Override
    public FamilyInvitationsViewedEvent viewAllReceivedInvitations(FamilyInvitationsViewEvent event) {

        List<FamilyInvitationDetailEntity> entities =
                familyInvitationRepo.findAllReceivedInvitation(event.getUserId());

        List<FamilyInvitationDetail> invitations = new ArrayList<>();

        for (FamilyInvitationDetailEntity entity : entities) {
            invitations.add(entity.toFamilyInvitationDetail());
        }

        return new FamilyInvitationsViewedEvent(invitations);
    }

    @Override
    public FamilyRelationshipsViewedEvent viewAllFamilyRelationships(FamilyRelationshipsViewEvent event) {

        List<FamilyRelationshipEntity> relationships =
                familyRelationshipRepo.findAllByUserIdAndLangCode(event.getUserId(),
                                                                  event.getLangCode());
        List<FamilyRelationship> results = new ArrayList<>();
        if (relationships != null) {
            for (FamilyRelationshipEntity relationship : relationships) {
                results.add(relationship.toFamilyRelationship());
            }
        }

        return new FamilyRelationshipsViewedEvent(results);
    }

    @Override
    public FamilyMemberDetailViewedEvent viewFamilyMemberDetail(FamilyMemberDetailViewEvent event)
            throws
            UserNotFoundByIdException,
            UserNotFoundByUsernameException,
            FamilyMemberNotFoundException {

        UserEntity inviter = userRepo.findOne(event.getInviterId());
        if (inviter == null) {
            throw new UserNotFoundByIdException(event.getInviterId());
        }

        UserEntity invitee = userRepo.findByUsername(event.getInviteeUsername());

        if (invitee == null) {
            throw new UserNotFoundByUsernameException(event.getInviteeUsername());
        }

        FamilyMemberEntity familyMemberEntity =
                familyMemberRepo.findOne(new FamilyMemberPrimaryKey(inviter.getFamily()
                                                                           .getId(),
                                                                    invitee.getId()));
        if (familyMemberEntity == null) {
            throw new FamilyMemberNotFoundException(inviter.getFamily()
                                                           .getId(),
                                                    invitee.getId());
        }

        return new FamilyMemberDetailViewedEvent(familyMemberEntity.toFamilyMemberDetail());
    }
}





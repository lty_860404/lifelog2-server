package com.dodopipe.lifelog.core.services;

import com.dodopipe.lifelog.core.events.family.*;
import com.dodopipe.lifelog.core.services.exception.*;

/**
 * @author yongwei
 */
public interface FamilyCommandService {

    FamilyMemberInvitedEvent inviteFamilyMember(FamilyMemberInviteEvent event)
            throws
            UserNotFoundByIdException,
            InvalidFamilyInvitationException,
            FamilyMemberNotFoundException,
            InternalServerErrorException;

    FamilyInvitationAcceptedEvent acceptFamilyInvitation(FamilyInvitationAcceptEvent event)
            throws
            UserNotFoundByIdException,
            FamilyInvitationNotFoundException,
            FamilyMemberNotFoundException;

    FamilyMemberUpdatedEvent updateFamilyMember(FamilyMemberUpdateEvent event)
            throws
            UserNotFoundByIdException,
            UserNotFoundByUsernameException,
            FamilyMemberNotFoundException;

    FamilyMemberRemovedEvent deleteFamilyMember(FamilyMemberRemoveEvent event)
            throws
            UserNotFoundByIdException,
            UserNotFoundByUsernameException,
            FamilyMemberNotFoundException,
            InternalServerErrorException;
}




package com.dodopipe.lifelog.core.services.utils;

import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by henryhome on 8/4/14.
 */
public class BlobObjectUtil {
    public static byte[] convertToThumbnail(byte[] imageData) throws IOException {

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ImageTransformer transformer = new ImageTransformer(imageData);
        transformer.rotate();
        transformer.scale(320 * 2);
        ImageIO.write(transformer.getImage(),
                      "jpg",
                      out);

        return out.toByteArray();
    }
}




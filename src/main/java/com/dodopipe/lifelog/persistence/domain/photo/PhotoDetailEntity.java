package com.dodopipe.lifelog.persistence.domain.photo;

import com.dodopipe.lifelog.core.domain.PhotoDetail;

/**
 * @author Henry Yan
 */
public class PhotoDetailEntity {

    private PhotoEntity photoEntity;
    private String ownerUsername;
    private String ownerNickname;
    private String ownerAvatarFilePath;
    private Boolean isCurrUserLiker;

    public PhotoDetailEntity(PhotoEntity photoEntity,
                             String ownerUsername,
                             String ownerNickname,
                             String ownerAvatarFilePath,
                             Boolean isCurrUserLiker) {

        this.photoEntity = photoEntity;
        this.ownerUsername = ownerUsername;
        this.ownerNickname = ownerNickname;
        this.ownerAvatarFilePath = ownerAvatarFilePath;
        this.isCurrUserLiker = isCurrUserLiker;
    }

    public PhotoDetail toPhotoDetail() {

        return new PhotoDetail(photoEntity.toPhoto(),
                                getOwnerUsername(),
                                getOwnerNickname(),
                                getOwnerAvatarFilePath(),
                                isCurrUserLiker());
    }

    public PhotoEntity getPhotoEntity() {

        return photoEntity;
    }

    public void setPhotoEntity(PhotoEntity photoEntity) {

        this.photoEntity = photoEntity;
    }

    public String getOwnerUsername() {

        return ownerUsername;
    }

    public void setOwnerUsername(String ownerUsername) {

        this.ownerUsername = ownerUsername;
    }

    public String getOwnerNickname() {

        return ownerNickname;
    }

    public void setOwnerNickname(String ownerNickname) {

        this.ownerNickname = ownerNickname;
    }

    public String getOwnerAvatarFilePath() {

        return ownerAvatarFilePath;
    }

    public void setOwnerAvatarFilePath(String ownerAvatarFilePath) {

        this.ownerAvatarFilePath = ownerAvatarFilePath;
    }

    public Boolean isCurrUserLiker() {

        return isCurrUserLiker;
    }

    public void setCurrUserLiker(Boolean isCurrUserLiker) {

        this.isCurrUserLiker = isCurrUserLiker;
    }
}

package com.dodopipe.lifelog.rest.controller;

import com.dodopipe.lifelog.core.domain.User;
import com.dodopipe.lifelog.core.domain.WeddingActivity;
import com.dodopipe.lifelog.core.events.activity.*;
import com.dodopipe.lifelog.core.services.ActivityCommandService;
import com.dodopipe.lifelog.core.services.exception.ActivityNotFoundException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByIdException;
import com.dodopipe.lifelog.rest.error.IllegalPrincipalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.List;
import java.util.Map;

/**
 * @author Henry Yan
 */
@Controller
@RequestMapping(value = "/activities")
public class ActivityCommandController {

    @Autowired
    private ActivityCommandService activityCommandService;

    @RequestMapping(value = "/wedding",
                    method = RequestMethod.POST)
    /** Polymorphism is not support in Jackson converter, therefore, you have
     *  to explicitly declare the type of the object we want to convert to here */
    public ResponseEntity<WeddingActivity> createWeddingActivity(@RequestBody WeddingActivity activity,
                                                                 Principal principal)
            throws
            IllegalPrincipalException,
            UserNotFoundByIdException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);

        ActivityCreatedEvent event =
                activityCommandService.createWeddingActivity(new ActivityCreateEvent(userId,
                                                                                     activity));

        return new ResponseEntity<>((WeddingActivity) event.getActivity(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/wedding/{activity_id}",
                    method = RequestMethod.POST)
    public ResponseEntity<Map<String, Boolean>> updateWeddingActivity(@PathVariable("activity_id") Long activityId,
                                                                      @RequestBody WeddingActivity activity)
            throws
            ActivityNotFoundException {

        ActivityUpdatedEvent event = activityCommandService.updateWeddingActivity(new ActivityUpdateEvent(activityId,
                                                                                                          activity));

        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/{activity_id}",
                    method = RequestMethod.DELETE)
    public ResponseEntity<Map<String, Boolean>> deleteActivity(@PathVariable("activity_id") Long activityId)
            throws
            ActivityNotFoundException {

        ActivityDeletedEvent event = activityCommandService.deleteActivity(new ActivityDeleteEvent(activityId));

        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/{activity_id}/invite",
                    method = RequestMethod.POST)
    public ResponseEntity<Map<String, Boolean>> inviteAttendees(@PathVariable("activity_id") Long activityId,
                                                                @RequestBody List<User> attendees,
                                                                Principal principal)
            throws IllegalPrincipalException,
            ActivityNotFoundException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);

        ActivityAttendeesInvitedEvent event =
                activityCommandService.inviteAttendees(new ActivityAttendeesInviteEvent(userId,
                                                                                        activityId,
                                                                                        attendees));

        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/{activity_id}/accept",
                    method = RequestMethod.POST)
    public ResponseEntity<Map<String, Boolean>> acceptInvitation(@PathVariable("activity_id") Long activityId,
                                                                 Principal principal)
            throws IllegalPrincipalException,
            ActivityNotFoundException {

        Long attendeeId = PrincipalUtil.getUIDByPrincipal(principal);
        ActivityInvitationAcceptedEvent event =
                activityCommandService.acceptInvitation(new ActivityInvitationAcceptEvent(activityId,
                                                                                          attendeeId));

        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/{activity_id}/reject",
                    method = RequestMethod.POST)
    public ResponseEntity<Map<String, Boolean>> rejectInvitation(@PathVariable("activity_id") Long activityId,
                                                                 Principal principal)
            throws IllegalPrincipalException,
            ActivityNotFoundException {

        Long attendeeId = PrincipalUtil.getUIDByPrincipal(principal);
        ActivityInvitationRejectedEvent event =
                activityCommandService.rejectInvitation(new ActivityInvitationRejectEvent(activityId,
                                                                                          attendeeId));
        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }
}




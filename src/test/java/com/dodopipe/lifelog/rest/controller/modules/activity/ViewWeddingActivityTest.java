package com.dodopipe.lifelog.rest.controller.modules.activity;

import com.dodopipe.lifelog.core.events.activity.ActivityViewEvent;
import com.dodopipe.lifelog.core.events.activity.ActivityViewedEvent;
import com.dodopipe.lifelog.core.services.ActivityQueryService;
import com.dodopipe.lifelog.rest.controller.ActivityQueryController;
import com.dodopipe.lifelog.rest.controller.fixture.UserAsPrincipal;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;

import static com.dodopipe.lifelog.rest.controller.fixture.RestEventFixture.activityViewedEvent;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by henryhome on 7/15/14.
 */
public class ViewWeddingActivityTest {

    MockMvc mockMvc;

    @InjectMocks
    ActivityQueryController controller;

    @Mock
    ActivityQueryService activityQueryService;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                             .build();

    }

    @Test
    public void viewWeddingActivityCorrectly()
            throws
            Exception {

        Long activityId = 1000l;

        ActivityViewedEvent event = activityViewedEvent();

        when(activityQueryService.viewWeddingActivity(any(ActivityViewEvent.class))).thenReturn(event);

        mockMvc.perform(get("/activities/wedding/{activity_id}",
                            activityId).principal(new UserAsPrincipal("4000")))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.id").value(1000));
    }
}

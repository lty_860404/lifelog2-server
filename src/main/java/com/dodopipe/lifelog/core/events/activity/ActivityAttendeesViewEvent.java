package com.dodopipe.lifelog.core.events.activity;

import com.dodopipe.lifelog.core.events.ViewEvent;

/**
 * Created by henryhome on 7/21/14.
 */
public class ActivityAttendeesViewEvent extends ViewEvent {
    private Long activityId;
    private Integer pageNum;
    private Integer numPerPage;

    public ActivityAttendeesViewEvent(Long activityId,
                                      Integer pageNum,
                                      Integer numPerPage) {
        this.activityId = activityId;
        this.pageNum = pageNum;
        this.numPerPage = numPerPage;
    }

    public Long getActivityId() {

        return activityId;
    }

    public Integer getPageNum() {

        return pageNum;
    }

    public Integer getNumPerPage() {

        return numPerPage;
    }
}




package com.dodopipe.lifelog.core.events.photo;

import com.dodopipe.lifelog.core.domain.PhotoDetail;
import com.dodopipe.lifelog.core.events.ViewedEvent;

import java.util.List;

public class PhotosViewedEvent
        extends ViewedEvent {

    private List<PhotoDetail> photoDetails;

    public PhotosViewedEvent(List<PhotoDetail> photoDetails) {

        this.photoDetails = photoDetails;
    }

    public List<PhotoDetail> getPhotoDetails() {

        return this.photoDetails;
    }
}




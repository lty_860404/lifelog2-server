package com.dodopipe.lifelog.rest.error;

public class ExceptionMessageHelper {

    public static String requiredRequestParameter(String... reqParamNames) {

        StringBuilder sb = new StringBuilder();
        sb.append("The following parameters are required: (");
        for (String paramName : reqParamNames) {
            sb.append(paramName);
            sb.append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append(")");
        return sb.toString();
    }
}

#!/bin/bash
BASE_DIR=`dirname $0`/../
PORT=9000
HEAP_SIZE="-Xms1024m -Xmx1024m"
ENV_NAME=prod
if [ "$1"=='qaci' ]
then
	ENV_NAME="qaci"
        PORT=8090
	HEAP_SIZE="-Xms256m -Xmx256m"
fi
if [ -f "${BASE_DIR}/LIFELOG_${ENV_NAME}.pid" ]
then
	cat ${BASE_DIR}/LIFELOG_${ENV_NAME}.pid | xargs kill -KILL > /dev/null 2>&1
	rm ${BASE_DIR}/LIFELOG_${ENV_NAME}.pid
fi
cat << '__EOF__'
     _______..___________.    ___      .______     .___________.    __       __   _______  _______  __        ______     _______ 
    /       ||           |   /   \     |   _  \    |           |   |  |     |  | |   ____||   ____||  |      /  __  \   /  _____|
   |   (----``---|  |----`  /  ^  \    |  |_)  |   `---|  |----`   |  |     |  | |  |__   |  |__   |  |     |  |  |  | |  |  __  
    \   \        |  |      /  /_\  \   |      /        |  |        |  |     |  | |   __|  |   __|  |  |     |  |  |  | |  | |_ | 
.----)   |       |  |     /  _____  \  |  |\  \----.   |  |        |  `----.|  | |  |     |  |____ |  `----.|  `--'  | |  |__| | 
|_______/        |__|    /__/     \__\ | _| `._____|   |__|        |_______||__| |__|     |_______||_______| \______/   \______| 

=================================================================================================================================
__EOF__

java -server $HEAP_SIZE -Dserver.port=$PORT -Dspring.profiles.active=${ENV_NAME} -javaagent:${BASE_DIR}/lib/spring-instrument-4.0.5.RELEASE.jar -jar ${BASE_DIR}/build/libs/lifelog2-server.jar > start_${ENV_NAME}.log 2>&1 &
PID=9999
while [ $PID -eq 9999 ];do
	PID=`ps -ef | grep "${BASE_DIR}/build/libs/lifelog2-server.jar" | grep $PORT | grep -v grep | awk '{print $2}'`
done
echo $PID > ${BASE_DIR}/LIFELOG_${ENV_NAME}.pid

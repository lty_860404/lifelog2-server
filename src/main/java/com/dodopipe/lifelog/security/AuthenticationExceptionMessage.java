package com.dodopipe.lifelog.security;

/**
 * @author yongwei
 */
public class AuthenticationExceptionMessage {

    public static AuthenticationExceptionMessage SIGNATURE_NOT_FOUND = new AuthenticationExceptionMessage("Auth-010",
                                                                                                          "Signature not found");
    public static AuthenticationExceptionMessage REQUEST_TIMEOUT = new AuthenticationExceptionMessage("Auth-020",
                                                                                                      "Request is expired");
    public static AuthenticationExceptionMessage ACCOUNT_NOT_FOUND = new AuthenticationExceptionMessage("Auth-030",
                                                                                                     "Account not found");
    public static AuthenticationExceptionMessage USER_ACCOUNT_EXPIRED = new AuthenticationExceptionMessage("Auth-040",
                                                                                                           "User account is expired");
    public static AuthenticationExceptionMessage CREDENTIAL_EXPIRED = new AuthenticationExceptionMessage("Auth-050",
                                                                                                         "Credential is expired");
    public static AuthenticationExceptionMessage BAD_CREDENTIAL = new AuthenticationExceptionMessage("Auth-060",
                                                                                                     "Not a valid signature");
    public static AuthenticationExceptionMessage UN_AUTHORIZED = new AuthenticationExceptionMessage("Auth-070",
                                                                                                    "Server internal error");

    private final String errorCode;
    private final String errorMessage;

    private AuthenticationExceptionMessage(String errorCode,
                                           String errorMessage) {

        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {

        return errorCode;
    }

    public String getErrorMessage() {

        return errorMessage;
    }

    public String toJson() {

        StringBuffer sb = new StringBuffer();
        sb.append("{")
          .append("\"errorCode\":\"")
          .append(errorCode)
          .append("\",\"errorMessage\":\"")
          .append(errorMessage)
          .append("\"}");
        return sb.toString();
    }
}

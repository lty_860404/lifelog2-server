package com.dodopipe.lifelog.core.events.auth;

import com.dodopipe.lifelog.core.events.NotifiedEvent;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Henry Yan
 */
public class VerificationCodeNotifiedEvent extends NotifiedEvent {

    public String messageStatus;

    public VerificationCodeNotifiedEvent(String messageStatus) {

        this.messageStatus = messageStatus;
    }

    public String getMessageStatus() {

        return messageStatus;
    }

    public Map<String, String> toMap() {

        Map<String, String> map = new HashMap<>();
        map.put("messageStatus", getMessageStatus());
        return map;
    }
}




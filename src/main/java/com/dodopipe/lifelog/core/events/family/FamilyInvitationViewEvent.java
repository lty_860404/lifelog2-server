package com.dodopipe.lifelog.core.events.family;

/**
 * @author tianyin.luo
 */
public class FamilyInvitationViewEvent {

    private final Long userId;

    public FamilyInvitationViewEvent(Long userId) {

        this.userId = userId;
    }

    public Long getUserId() {

        return this.userId;
    }

}

package com.dodopipe.lifelog.core.services.exception;

/**
 * @author Henry Yan
 */
public class ActivityAttendeeNotFoundException extends ServiceException {

    private Long activityId;
    private Long attendeeId;

    public ActivityAttendeeNotFoundException(Long activityId,
                                             Long attendeeId) {

        this.activityId = activityId;
        this.attendeeId = attendeeId;
    }

    public ActivityAttendeeNotFoundException(Long activityId,
                                             Long attendeeId,
                                             Throwable cause) {

        super(cause);
        this.activityId = activityId;
        this.attendeeId = attendeeId;
    }

    @Override
    public String getMessage() {

        return "The attendee with activity id = " + activityId +
                " and attendee id = " + attendeeId + " cannot be found";
    }
}

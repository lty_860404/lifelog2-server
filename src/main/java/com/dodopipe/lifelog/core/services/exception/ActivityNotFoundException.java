package com.dodopipe.lifelog.core.services.exception;

/**
 * @author Henry Yan
 */
public class ActivityNotFoundException extends ServiceException {

    private Long activityId;

    public ActivityNotFoundException(Long activityId) {

        this.activityId = activityId;

    }

    public ActivityNotFoundException(Long activityId, Throwable cause) {

        super(cause);
        this.activityId = activityId;
    }

    @Override
    public String getMessage() {

        return "The activity with id = " + activityId + " cannot be found";
    }
}

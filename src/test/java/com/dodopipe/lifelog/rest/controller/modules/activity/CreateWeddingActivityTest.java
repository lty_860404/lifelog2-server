package com.dodopipe.lifelog.rest.controller.modules.activity;

import com.dodopipe.lifelog.core.domain.Activity;
import com.dodopipe.lifelog.core.events.activity.ActivityCreateEvent;
import com.dodopipe.lifelog.core.events.activity.ActivityCreatedEvent;
import com.dodopipe.lifelog.core.services.ActivityCommandService;
import com.dodopipe.lifelog.rest.controller.ActivityCommandController;
import com.dodopipe.lifelog.rest.controller.fixture.UserAsPrincipal;
import com.dodopipe.lifelog.rest.controller.utils.TestUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;

import static com.dodopipe.lifelog.rest.controller.fixture.RestDataFixture.generateWeddingActivity;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Henry Yan
 */
public class CreateWeddingActivityTest {

    MockMvc mockMvc;

    @InjectMocks
    ActivityCommandController controller;

    @Mock
    ActivityCommandService activityCommandService;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                             .build();

    }

    @Test
    public void createActivityCorrectly()
            throws
            Exception {

        Activity activity = generateWeddingActivity();

        ActivityCreatedEvent event = new ActivityCreatedEvent(activity);

        when(activityCommandService.createWeddingActivity(any(ActivityCreateEvent.class))).thenReturn(event);

        mockMvc.perform(post("/activities/wedding")
                                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                .content(TestUtil.convertObjectToJsonBytes(activity))
                                .principal(new UserAsPrincipal("3000")))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.id").value(1000));
    }
}




package com.dodopipe.lifelog.rest.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author yongwei
 */
public class HMacSignatureUtils {

    private static Logger logger = LoggerFactory.getLogger(HMacSignatureUtils.class);
    private final static String ACCESS_KEY_ID = "vJbGHEZOJuuYATR6Gx8A";
    private final static String SECRET_KEY = "QvfiyQf1Q9KVZyrA285C8Ze22HxaBCaKzQ2eseev";
    private final static String ALGORITHM = "HmacSHA1";

    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
            "EEE, dd MMM yyyy HH:mm:ss zzz",
            Locale.US);

    static {
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("GMT"));
    }

    public static HttpHeaders signedHeaders(HttpMethod method,
                                            String path,
                                            MediaType mediaType)
            throws
            Exception {

        return signedHeaders(method,
                             path,
                             mediaType,
                             ACCESS_KEY_ID,
                             SECRET_KEY);
    }

    public static HttpHeaders signedHeaders(HttpMethod method,
                                            String path,
                                            MediaType mediaType,
                                            String accessKeyId,
                                            String secretKey)
            throws
            Exception {

        HttpHeaders headers = new HttpHeaders();
        if (mediaType != null) {
            headers.setContentType(mediaType);
        }
        headers.setDate(System.currentTimeMillis());
        String headersToSign = createStrToSign(headers,
                                               method,
                                               path);
        headers.add("Authorization",
                    createAuthHeader(headersToSign,
                                     accessKeyId,
                                     secretKey));
        return headers;

    }

    private static String createAuthHeader(String headersToSign,
                                           String accessKeyId,
                                           String secretKey)
            throws
            Exception {

        String signature = hmac(headersToSign,
                                secretKey);
        if (logger.isDebugEnabled()) {
            logger.debug("Headers to signature(client):\n" +
                                 "{}\n" +
                                 "digest:{}",
                         headersToSign,
                         signature);
        }
        return "DOP " + accessKeyId + ":" + signature;
    }

    private static String hmac(String str,
                               String secretKey)
            throws
            Exception {

        SecretKey key = new SecretKeySpec(Base64.getDecoder()
                                                .decode(secretKey),
                                          ALGORITHM);

        Mac mac = Mac.getInstance(ALGORITHM);
        mac.init(key);
        byte[] rowHmac = mac.doFinal(str.getBytes("UTF-8"));
        return Base64.getEncoder()
                     .encodeToString(rowHmac);
    }

    private static String createStrToSign(HttpHeaders headers,
                                          HttpMethod method,
                                          String path) {

        List<String> parts = new ArrayList<>();
        parts.add(method.toString()
                        .toUpperCase());
        parts.add(null2Empty(headers.get("Content-MD5")));
        parts.add(null2Empty(headers.getContentType()));
        parts.add(DATE_FORMAT.format(new Date(headers.getDate())));
        parts.add(null2Empty(path));

        return String.join("\n",
                           parts);

    }

    private static String null2Empty(String str) {

        return str == null ? "" : str;
    }

    private static String null2Empty(MediaType type) {

        if (type == null) {
            return "";
        }
        return null2Empty(type.toString());
    }

    private static String null2Empty(List<String> list) {

        if (list == null || list.isEmpty()) {
            return "";
        }
        return String.join(",",
                           list);
    }

}

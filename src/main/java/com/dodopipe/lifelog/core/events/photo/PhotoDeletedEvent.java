package com.dodopipe.lifelog.core.events.photo;

import com.dodopipe.lifelog.core.events.DeletedEvent;

import java.util.HashMap;

/**
 * @author Henry Yan
 */
public class PhotoDeletedEvent extends DeletedEvent {
    public boolean success;

    public PhotoDeletedEvent(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public HashMap<String, Boolean> toMap() {

        HashMap<String, Boolean> map = new HashMap<>();
        map.put("success", isSuccess());
        return map;
    }
}

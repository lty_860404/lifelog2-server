package com.dodopipe.lifelog.security;

import org.springframework.security.core.AuthenticationException;

/**
 * @author yongwei
 */
public class AccountExpiredException
        extends AuthenticationException {

    public AccountExpiredException(String message) {

        super(message);
    }
}

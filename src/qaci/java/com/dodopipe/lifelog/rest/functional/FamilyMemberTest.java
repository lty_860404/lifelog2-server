package com.dodopipe.lifelog.rest.functional;


import com.dodopipe.lifelog.LifelogApplication;
import com.dodopipe.lifelog.config.LifelogProfile;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManagerFactory;
import java.util.Map;

import static com.dodopipe.lifelog.rest.fixture.RestData.*;
import static org.junit.Assert.*;

/**
 * @author tianyin.luo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {LifelogApplication.class})
@WebAppConfiguration
@IntegrationTest
@ActiveProfiles(LifelogProfile.QACI)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
@DatabaseSetup("DB_SETUP.xml")
@DatabaseTearDown(value = {"DB_SETUP.xml",
                           "DB_CLEAR.xml"},
                  type = DatabaseOperation.DELETE_ALL)
public class FamilyMemberTest {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    private EntityManagerFactory emf;

    @After
    public void tearDown() {

        emf.getCache()
           .evictAll();
    }

    @Test
    public void shouldRemoveFamilyMemberCorrectly(){

        RestTemplate template = QaciTestFactory.signatureRestTemplateInstance();
        template.setErrorHandler(QaciTestFactory.defaultErrorHandlerInstance());
        template.delete("http://localhost:8080/family/members/{memberusername}","John55555");

        template = QaciTestFactory.signatureRestTemplateInstance();
        template.setErrorHandler(QaciTestFactory.defaultErrorHandlerInstance());

        @SuppressWarnings("rawtypes")
        ResponseEntity<JSONObject> responseEntity =
                template.getForEntity("http://localhost:8080/family/members/John55555",
                                      JSONObject.class);
        logger.info("=============> body:" + responseEntity.getBody());
        assertEquals(true,responseEntity.getBody().get("blocked"));
        assertEquals(true,responseEntity.getBody().get("removed"));

    }

    @Test
    public void shouldUpdateFamilyMemberRelationCorrectly(){

        RestTemplate template = QaciTestFactory.signatureRestTemplateInstance();

        //test invite invitee by username
        template.setErrorHandler(QaciTestFactory.defaultErrorHandlerInstance());

        @SuppressWarnings("rawtypes")
        ResponseEntity<Map> responseEntity = template.postForEntity("http://localhost:8080/family/members/John55555",
                                                                   familyMemberUpdateRequest(),
                                                                   Map.class);

        logger.info("=============> body:" + responseEntity.getBody());

        assertNotNull(responseEntity);
        assertTrue((boolean) responseEntity.getBody()
                                          .get("isSuccess"));
    }

    @Test
    public void shouldShowFamilyMemberDetailCorrectly() {

        RestTemplate template = QaciTestFactory.signatureRestTemplateInstance();

        //test invite invitee by username
        template.setErrorHandler(QaciTestFactory.defaultErrorHandlerInstance());

        @SuppressWarnings("rawtypes")
        ResponseEntity<JSONObject> responseEntity =
                template.getForEntity("http://localhost:8080/family/members/John55555",
                                      JSONObject.class);

        logger.info("=============> body:" + responseEntity.getBody());

        assertNotNull(responseEntity);
        assertEquals("John55555",
                     responseEntity.getBody()
                                   .get("username"));
        assertEquals("HUSBAND",
                     responseEntity.getBody()
                                   .get("relation"));
    }

    @Test
    public void shouldAcceptFamilyMemberInvitationCorrectly() {

        RestTemplate template = QaciTestFactory.signatureRestTemplateInstance();

        //test invite invitee by username
        template.setErrorHandler(QaciTestFactory.defaultErrorHandlerInstance());
        @SuppressWarnings("rawtypes")
        ResponseEntity<Map> acceptedEvent = template.postForEntity("http://localhost:8080/family/accept",
                                                                   acceptInvitationRequest(),
                                                                   Map.class);

        logger.info("==============> body:" + acceptedEvent.getBody());

        assertNotNull(acceptedEvent);
        assertTrue((boolean) acceptedEvent.getBody()
                                          .get("isSuccess"));

    }

    @Test
    public void shouldInviteFamilyMemberCorrectly() {

        RestTemplate template = QaciTestFactory.signatureRestTemplateInstance();

        //test invite invitee by username
        template.setErrorHandler(QaciTestFactory.defaultErrorHandlerInstance());
        @SuppressWarnings("rawtypes")
        ResponseEntity<Map> rst = template.postForEntity("http://localhost:8080/family/invite",
                                                         familyInvitationRequestByUserName(),
                                                         Map.class);

        logger.info("==============> body:" + rst.getBody());

        assertNotNull(rst);
    }

    @Test
    public void shouldShowAllFamilyMemberRelationshipsCorrectly() {

        RestTemplate template = QaciTestFactory.signatureRestTemplateInstance();

        template.setErrorHandler(QaciTestFactory.defaultErrorHandlerInstance());
        @SuppressWarnings("rawtypes")
        ResponseEntity<JSONArray> responseEntity = template.getForEntity("http://localhost:8080/family/relationships?lang_code=en",
                                                                         JSONArray.class);

        logger.info("=============> body:" + responseEntity.getBody());

        assertNotNull(responseEntity);
        assertEquals(7,
                     responseEntity.getBody()
                                   .size());
        assertEquals(200,
                     responseEntity.getStatusCode()
                                   .value());
    }

    @Test
    public void shouldShowAllFamilyMembersCorrectly(){

        RestTemplate template = QaciTestFactory.signatureRestTemplateInstance();

        template.setErrorHandler(QaciTestFactory.defaultErrorHandlerInstance());

        @SuppressWarnings("rawtypes")
        ResponseEntity<JSONArray> responseEntity = template.getForEntity("http://localhost:8080/family/members",
                                                                         JSONArray.class);

        logger.info("=============> body:" + responseEntity.getBody());

        assertNotNull(responseEntity);
        assertEquals(2,
                     responseEntity.getBody()
                                   .size());
        assertEquals(200,
                     responseEntity.getStatusCode()
                                   .value());
    }
}




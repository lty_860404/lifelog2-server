package com.dodopipe.lifelog.persistence.repositories.user;

import com.dodopipe.lifelog.persistence.domain.user.UserEntity;
import com.dodopipe.lifelog.persistence.domain.user.UserTagEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository
        extends JpaRepository<UserEntity, Long> {

    UserEntity findById(Long id);

    UserEntity findByPhone(String phone);

    UserEntity findByUsername(String username);

    @Query("SELECT NEW com.dodopipe.lifelog.persistence.domain.user.UserTagEntity(u.id, u.username, u.nickname)" +
           "FROM USER u WHERE u.id = ?1")
    UserTagEntity findUserTagById(Long id);

    @Query("SELECT NEW com.dodopipe.lifelog.persistence.domain.user.UserTagEntity(u.id, u.username, u.nickname)" +
           "FROM USER u WHERE u.username = ?1")
    UserTagEntity findUserTagByUsername(String username);

    @Query(value = "SELECT u.familyId from USER u where u.id =?1")
    Long findFamilyIdById(Long userId);
}




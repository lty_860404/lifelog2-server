package com.dodopipe.lifelog.core.events.family;

import com.dodopipe.lifelog.core.domain.FamilyMember;

/**
 * @author tianyin.luo
 */
public class FamilyMemberRemovedEvent {

    private final boolean isSuccess;

    public FamilyMemberRemovedEvent(boolean isSuccess) {

        this.isSuccess = isSuccess;
    }

    public boolean isSuccess() {

        return this.isSuccess;
    }

}

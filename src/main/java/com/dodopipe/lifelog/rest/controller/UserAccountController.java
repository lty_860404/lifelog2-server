package com.dodopipe.lifelog.rest.controller;

import com.dodopipe.lifelog.core.events.auth.*;
import com.dodopipe.lifelog.core.services.UserAccountService;
import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;
import com.dodopipe.lifelog.core.services.exception.InvalidVerificationCodeException;
import com.dodopipe.lifelog.core.services.exception.VerificationCodeExpiredException;
import com.dodopipe.lifelog.persistence.PersistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
@RequestMapping(value = "/account")
public class UserAccountController {

    @Autowired
    UserAccountService userAccountService;

    @RequestMapping(value = "/verify",
                    method = RequestMethod.POST)
    public ResponseEntity<String> createAndSendVerificationCode(@RequestParam("country_code") String countryCode,
                                                                @RequestParam("phone") String phone)
            throws
            InternalServerErrorException {

        VerificationCodeCreatedEvent event =
                userAccountService.createAndSendVerificationCode(new VerificationCodeCreateEvent(countryCode,
                                                                                                 phone));

        return new ResponseEntity<>(event.getVerificationCode(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/notify_verification_status",
                    method = RequestMethod.POST)
    public ResponseEntity<Map<String, String>> notifyVerificationStatus(@RequestParam("phone") String phone) {

        VerificationCodeNotifiedEvent event =
                userAccountService.notifyVerificationStatus(new VerificationCodeNotifyEvent(phone));

        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/callback",
                    method = RequestMethod.POST)
    public void updateVerificationStatus(
            @RequestParam("MessageSid") String messageSid,
            @RequestParam("SmsSid") String smsSid,
            @RequestParam("AccountSid") String accountSid,
            @RequestParam("From") String from,
            @RequestParam("To") String to,
            @RequestParam("Body") String body,
            @RequestParam("NumMedia") Integer numMedia,
            @RequestParam("MessageStatus") String messageStatus,
            @RequestParam("ErrorCode") Integer errorCode) {

        userAccountService.updateVerificationStatus(new MessageCallbackCreateEvent(messageSid,
                                                                                   smsSid,
                                                                                   accountSid,
                                                                                   from,
                                                                                   to,
                                                                                   body,
                                                                                   numMedia,
                                                                                   messageStatus,
                                                                                   errorCode));
    }

    @RequestMapping(value = "/register",
                    method = RequestMethod.POST)
    public ResponseEntity<Map<String, String>> register(@RequestParam("verification_code") String verificationCode,
                                                        @RequestParam("country_code") String countryCode,
                                                        @RequestParam("phone") String phone,
                                                        @RequestParam("password") String password,
                                                        @RequestParam("lang_code") String langCode)
            throws
            InvalidVerificationCodeException,
            VerificationCodeExpiredException,
            PersistenceException {

        UserRegisteredEvent event = userAccountService.register(new UserRegisterEvent(verificationCode,
                                                                                      countryCode,
                                                                                      phone,
                                                                                      password,
                                                                                      langCode));

        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/sign_in",
                    method = RequestMethod.POST)
    public ResponseEntity<Map<String, String>> signIn(@RequestParam(value = "username",
                                                                    required = false) String username,
                                                      @RequestParam(value = "phone",
                                                                    required = false) String phone,
                                                      @RequestParam("password") String password)
            throws
            PersistenceException {

        UserSignedInEvent event = userAccountService.signIn(new UserSignInEvent(username,
                                                                                phone,
                                                                                password));

        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }
}

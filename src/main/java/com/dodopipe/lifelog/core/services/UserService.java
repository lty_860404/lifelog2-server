package com.dodopipe.lifelog.core.services;

import com.dodopipe.lifelog.core.events.auth.UserSignOutEvent;
import com.dodopipe.lifelog.core.events.auth.UserSignedOutEvent;
import com.dodopipe.lifelog.core.events.user.*;
import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByIdException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByPhoneException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByUsernameException;
import com.dodopipe.lifelog.security.AccountNotFoundException;

/**
 * Created by henryhome on 8/6/14.
 */
public interface UserService {

    public UserSignedOutEvent signOut(UserSignOutEvent event)
            throws
            AccountNotFoundException;

    public PasswordUpdatedEvent changePassword(PasswordUpdateEvent event)
            throws
            AccountNotFoundException,
            InternalServerErrorException;

    public UserViewedEvent viewUser(UserViewEvent event)
            throws
            UserNotFoundByIdException,
            UserNotFoundByUsernameException,
            UserNotFoundByPhoneException;

    public UserUpdatedEvent updateUser(UserUpdateEvent event)
            throws
            UserNotFoundByIdException,
            IllegalArgumentException;

    public UserDeletedEvent deleteUser(UserDeleteEvent event)
            throws
            UserNotFoundByIdException;

    public UserAvatarChangedEvent changeAvatar(UserAvatarChangeEvent event)
            throws
            UserNotFoundByIdException;
}

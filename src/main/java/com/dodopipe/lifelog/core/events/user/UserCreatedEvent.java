package com.dodopipe.lifelog.core.events.user;

import com.dodopipe.lifelog.core.domain.User;
import com.dodopipe.lifelog.core.events.CreatedEvent;

/**
 * @author Henry Yan
 */
public class UserCreatedEvent extends CreatedEvent {
    public User user;

    public UserCreatedEvent(User user) {
        this.user = user;
    }

    public User getUser() {

        return user;
    }
}

package com.dodopipe.lifelog.persistence.domain.user;

import java.io.Serializable;

public class UserTagEntity
        implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Long id;
    private String username;
    private String nickname;

    public UserTagEntity(Long id,
                         String userName,
                         String nickname) {

        this.id = id;
        this.nickname = nickname;
    }

    public Long getId() {

        return id;
    }

    public String getUsername() {

        return username;
    }

    public String getNickname() {

        return nickname;
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        UserTagEntity other = (UserTagEntity) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }
}

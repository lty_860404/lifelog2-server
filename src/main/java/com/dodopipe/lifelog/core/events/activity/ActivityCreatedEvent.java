package com.dodopipe.lifelog.core.events.activity;

import com.dodopipe.lifelog.core.domain.Activity;
import com.dodopipe.lifelog.core.events.CreatedEvent;

/**
 * @author Henry Yan
 */
public class ActivityCreatedEvent extends CreatedEvent {
    protected Activity activity;

    public ActivityCreatedEvent() {

    }

    public ActivityCreatedEvent(Activity activity) {
        this.activity = activity;
    }

    public Activity getActivity() {

        return activity;
    }
}

package com.dodopipe.lifelog.core.events.family;

/**
 * @author tianyin.luo
 */
public class FamilyRelationshipsViewEvent {

    private final Long userId;

    private final String langCode;

    public FamilyRelationshipsViewEvent(Long userId,
                                        String langCode) {

        this.userId = userId;
        this.langCode = langCode;
    }

    public Long getUserId() {

        return this.userId;
    }

    public String getLangCode() {

        return this.langCode;
    }
}

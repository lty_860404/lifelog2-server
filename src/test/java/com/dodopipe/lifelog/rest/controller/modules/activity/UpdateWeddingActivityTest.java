package com.dodopipe.lifelog.rest.controller.modules.activity;

import com.dodopipe.lifelog.core.domain.Activity;
import com.dodopipe.lifelog.core.events.activity.ActivityUpdateEvent;
import com.dodopipe.lifelog.core.events.activity.ActivityUpdatedEvent;
import com.dodopipe.lifelog.core.services.ActivityCommandService;
import com.dodopipe.lifelog.rest.controller.ActivityCommandController;
import com.dodopipe.lifelog.rest.controller.utils.TestUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;

import static com.dodopipe.lifelog.rest.controller.fixture.RestDataFixture.generateWeddingActivity;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by henryhome on 7/21/14.
 */
public class UpdateWeddingActivityTest {
    MockMvc mockMvc;

    @InjectMocks
    ActivityCommandController controller;

    @Mock
    ActivityCommandService activityCommandService;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                             .build();

    }

    @Test
    public void updateActivityCorrectly() throws Exception {
        Long activityId = 1000l;

        Activity activity = generateWeddingActivity();

        ActivityUpdatedEvent event = new ActivityUpdatedEvent(true);

        when(activityCommandService.updateWeddingActivity(any(ActivityUpdateEvent.class))).thenReturn(event);

        mockMvc.perform(post("/activities/wedding/{activity_id}",
                             activityId)
                                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                .content(TestUtil.convertObjectToJsonBytes(activity)))
               .andExpect(status().isOk())
               .andExpect(jsonPath("success").value(true));
    }
}

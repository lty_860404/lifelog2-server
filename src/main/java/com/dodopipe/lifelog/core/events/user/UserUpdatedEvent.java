package com.dodopipe.lifelog.core.events.user;

import com.dodopipe.lifelog.core.events.UpdatedEvent;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by henryhome on 7/29/14.
 */
public class UserUpdatedEvent extends UpdatedEvent {

    public boolean success;

    public UserUpdatedEvent(boolean success) {

        this.success = success;
    }

    public boolean isSuccess() {

        return success;
    }

    public Map<String, Boolean> toMap() {

        Map<String, Boolean> map = new HashMap<>();
        map.put("success",
                isSuccess());
        return map;
    }
}

package com.dodopipe.lifelog.core.events.user;

import com.dodopipe.lifelog.core.events.CreateEvent;

/**
 * Created by henryhome on 7/29/14.
 */
public class UserCreateEvent extends CreateEvent {

    private String verificationCode;
    private String countryCode;
    private String phone;
    private String langCode;

    public UserCreateEvent(String verificationCode,
                           String countryCode,
                           String phone,
                           String langCode) {

        this.verificationCode = verificationCode;
        this.countryCode = countryCode;
        this.phone = phone;
        this.langCode = langCode;
    }

    public String getVerificationCode() {

        return verificationCode;
    }

    public String getCountryCode() {

        return countryCode;
    }

    public String getPhone() {

        return phone;
    }

    public String getLangCode() {

        return langCode;
    }
}

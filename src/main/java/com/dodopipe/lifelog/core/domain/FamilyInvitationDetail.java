package com.dodopipe.lifelog.core.domain;

/**
 * Created by henryhome on 8/13/14.
 */
public class FamilyInvitationDetail {

    String inviterUsername;
    String inviterNickname;
    String inviterPhone;
    String inviterAvatarFilePath;
    String inviteeUsername;
    String inviteeNickname;
    String inviteeAvatarFilePath;
    FamilyInvitation familyInvitation;

    public FamilyInvitationDetail(String inviterUsername,
                                  String inviterNickname,
                                  String inviterPhone,
                                  String inviterAvatarFilePath,
                                  String inviteeUsername,
                                  String inviteeNickname,
                                  String inviteeAvatarFilePath,
                                  FamilyInvitation familyInvitation) {

        this.inviterUsername = inviterUsername;
        this.inviterNickname = inviterNickname;
        this.inviterPhone = inviterPhone;
        this.inviterAvatarFilePath = inviterAvatarFilePath;
        this.inviteeUsername = inviteeUsername;
        this.inviteeNickname = inviteeNickname;
        this.inviteeAvatarFilePath = inviteeAvatarFilePath;
        this.familyInvitation = familyInvitation;
    }

    public String getInviterUsername() {

        return inviterUsername;
    }

    public String getInviterNickname() {

        return inviterNickname;
    }

    public String getInviterPhone() {

        return inviterPhone;
    }

    public String getInviterAvatarFilePath() {

        return inviterAvatarFilePath;
    }

    public String getInviteeUsername() {

        return inviteeUsername;
    }

    public String getInviteeNickname() {

        return inviteeNickname;
    }

    public String getInviteeAvatarFilePath() {

        return inviteeAvatarFilePath;
    }

    public FamilyInvitation getFamilyInvitation() {

        return familyInvitation;
    }
}

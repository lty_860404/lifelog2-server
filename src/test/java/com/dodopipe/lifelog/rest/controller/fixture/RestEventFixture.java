package com.dodopipe.lifelog.rest.controller.fixture;

import com.dodopipe.lifelog.core.domain.*;
import com.dodopipe.lifelog.core.events.activity.ActivitiesViewedEvent;
import com.dodopipe.lifelog.core.events.activity.ActivityAttendeesViewedEvent;
import com.dodopipe.lifelog.core.events.activity.ActivityViewedEvent;
import com.dodopipe.lifelog.core.events.auth.UserRegisteredEvent;
import com.dodopipe.lifelog.core.events.auth.UserSignedInEvent;
import com.dodopipe.lifelog.core.events.family.*;
import com.dodopipe.lifelog.core.events.file.FileViewedEvent;
import com.dodopipe.lifelog.core.events.photo.PhotoCreateEvent;
import com.dodopipe.lifelog.core.events.photo.PhotoCreatedEvent;
import com.dodopipe.lifelog.core.events.photo.PhotosViewEvent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.dodopipe.lifelog.rest.controller.fixture.RestDataFixture.imageData;

public class RestEventFixture {

    public static PhotoCreateEvent createPhotoWithSingleFile()
            throws
            IOException {

        PhotoFileData detail = new PhotoFileData();
        detail.setOwnerId(1000l);
        detail.setComment("some comment on single image");

        detail.setPhotos(imageData("test.jpg"));

        return new PhotoCreateEvent(detail);
    }

    public static PhotosViewEvent viewPhotoEvent() {

        return new PhotosViewEvent(1000l,
                                   null,
                                   null);
    }

    public static PhotoCreatedEvent createdSinglePhotoEvent() {

        PhotoMetadata data = new PhotoMetadata("original_file_key",
                                               "thumbnail_file_key",
                                               "image/jpeg");

        return new PhotoCreatedEvent(1000l,
                                     "test photo comment",
                                     Arrays.asList(data));
    }

    public static FamilyMembersViewedEvent readAllFamilyMembersEvent() {

        ArrayList<FamilyMember> list = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            list.add(new FamilyMember(i + 1l,
                                      i + 1l,
                                      "username" + i,
                                      "name" + i,
                                      "test_avatar_file_path" + i,
                                      "RELATIONSHIP",
                                      new FamilyRelationshipStatus(FamilyRelationshipStatus.FamilyMemberStatusType.VALID)));
        }
        return new FamilyMembersViewedEvent(list);
    }

    public static FamilyMemberInvitedEvent createFamilyMemberInvitedEvent() {

        return new FamilyMemberInvitedEvent(true,
                                            901l);
    }

    public static FamilyInvitationsViewedEvent familyInvitationsViewedEvent() {

        List<FamilyInvitationDetail> list = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            FamilyInvitation invitation = new FamilyInvitation(1000l,
                                                               1000l,
                                                               2000l,
                                                               "+86111111111",
                                                               "FRIENDS",
                                                               FamilyInvitationStatus.PENDING);

            list.add(new FamilyInvitationDetail("inviter_username " + i,
                                                 "inviter_nickname " + i,
                                                 "inviter_phone " + i,
                                                 "inviter_avatar_file_path " + i,
                                                 "invitee_username " + i,
                                                 "invitee_nickanme " + i,
                                                 "invitee_avatar_file_path " + i,
                                                 invitation));
        }
        return new FamilyInvitationsViewedEvent(list);
    }

    public static FamilyRelationshipsViewedEvent familyRelationshipsViewedEvent() {

        List<FamilyRelationship> list = new ArrayList<>();
        list.add(new FamilyRelationship("朋友",
                                        "ZH-CN",
                                        "System"));
        list.add(new FamilyRelationship("爸爸",
                                        "ZH-CN",
                                        "System"));
        list.add(new FamilyRelationship("妈妈",
                                        "ZH-CN",
                                        "System"));
        return new FamilyRelationshipsViewedEvent(list);
    }

    public static FamilyMemberDetailViewedEvent readFamilyDetailEvent() {

        return new FamilyMemberDetailViewedEvent(new FamilyMemberDetail("test",
                                                                  "+123456789",
                                                                  "nick tester",
                                                                  "F",
                                                                  "lalalilalilali...",
                                                                  "avator.png",
                                                                  "ZH-CN",
                                                                  "MOCKMAN",
                                                                  0));
    }

    public static ActivitiesViewedEvent activitiesViewedEvent(int num) {

        List<ActivityDetail> activities = new ArrayList<>();

        for (int i = 0; i < num; i++) {
            WeddingActivity wed = new WeddingActivity(1000l + i,
                                                      4000l,
                                                      null,
                                                      null,
                                                      null,
                                                      null,
                                                      null,
                                                      null);

            activities.add(new ActivityDetail(wed, false));
        }

        return new ActivitiesViewedEvent(activities);
    }

    public static ActivityViewedEvent activityViewedEvent() {

        Activity activity = new WeddingActivity(1000l,
                                                4000l,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null);

        return new ActivityViewedEvent(activity);
    }

    public static FileViewedEvent fileViewedEvent()
            throws
            Exception {

        BlobObject blobObject = imageData("test.jpg").get(0);
        return new FileViewedEvent(blobObject);
    }

    public static ActivityAttendeesViewedEvent activityAttendeesViewedEvent(int num) {

        List<ActivityAttendee> attendees = new ArrayList<>();

        for (int i = 0; i < num; i++) {
            attendees.add(new ActivityAttendee(1000l + i,
                                               "test attendee_name " + i,
                                               "test_attendee_phone " + i,
                                               "test_attendee_avatar_file_path " + i,
                                               null,
                                               null));
        }

        return new ActivityAttendeesViewedEvent(attendees);
    }

    public static UserRegisteredEvent generateUserRegisteredEvent() {
        return new UserRegisteredEvent("default_username", "test_access_id", "test_secret_key");
    }

    public static UserSignedInEvent generateUserSignedInEvent() {
        return new UserSignedInEvent("test_access_id", "test_secret_key");
    }
}

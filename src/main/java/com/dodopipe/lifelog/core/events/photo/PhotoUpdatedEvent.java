package com.dodopipe.lifelog.core.events.photo;

import com.dodopipe.lifelog.core.events.UpdatedEvent;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Henry Yan
 */
public class PhotoUpdatedEvent
        extends UpdatedEvent {

    public boolean success;

    public PhotoUpdatedEvent(boolean success) {

        this.success = success;
    }

    public boolean isSuccess() {

        return success;
    }

    public Map<String, Boolean> toMap() {

        Map<String, Boolean> map = new HashMap<>();
        map.put("success",
                isSuccess());
        return map;
    }
}

package com.dodopipe.lifelog.core.events.activity;

import com.dodopipe.lifelog.core.domain.Activity;
import com.dodopipe.lifelog.core.events.CreateEvent;

/**
 * @author Henry Yan
 */
public class ActivityCreateEvent extends CreateEvent {

    private Long userId;
    private Activity activity;

    public ActivityCreateEvent() {

    }

    public ActivityCreateEvent(Long userId,
                               Activity activity) {
        this.userId = userId;
        this.activity = activity;
    }

    public Long getUserId() {

        return userId;
    }

    public Activity getActivity() {

        return activity;
    }
}

package com.dodopipe.lifelog.persistence.domain.fixture;

import com.dodopipe.lifelog.persistence.domain.photo.PhotoEntity;
import com.dodopipe.lifelog.persistence.utils.PhotoUtil;

/**
 * @author Henry Yan
 */
public class EntityDataFixture {
    public static void addComment(PhotoEntity photoEntity, String username,
                                  String nickname,
                                  String comment) {

        StringBuffer sb = new StringBuffer();
        sb.append(PhotoUtil.escape(username,
                                   "@",
                                   "#"))
          .append("@")
          .append(PhotoUtil.escape(nickname, "@", "#"))
          .append("@")
          .append(PhotoUtil.escape(comment, "@", "#"))
          .append("#")
          .append(PhotoUtil.isEmptyString(photoEntity.getLatestComments()) ? "" : photoEntity.getLatestComments());
        photoEntity.setLatestComments(sb.toString());
    }

    public static void addLike(PhotoEntity photoEntity, String username, String nickname) {

        StringBuffer sb = new StringBuffer();
        sb.append(PhotoUtil.escape(username, "@", "#"))
          .append("@")
          .append(PhotoUtil.escape(nickname, "@", "#"))
          .append("#")
          .append(PhotoUtil.isEmptyString(photoEntity.getLatestLikingNames()) ? "" : photoEntity.getLatestLikingNames());
        photoEntity.setNumLikes(photoEntity.getNumLikes() + 1);
        photoEntity.setLatestLikingNames(sb.toString());
    }
}




package com.dodopipe.lifelog.core.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PhotoFileData {

    private Long ownerId;
    private String comment;
    private List<BlobObject> photos = Collections.emptyList();

    public Long getOwnerId() {

        return ownerId;
    }

    public void setOwnerId(Long userId) {

        this.ownerId = userId;
    }

    public String getComment() {

        return comment;
    }

    public void setComment(String comment) {

        this.comment = comment;
    }

    public List<BlobObject> getPhotos() {

        return Collections.unmodifiableList(this.photos);
    }

    public void setPhotos(List<BlobObject> photos) {

        if (photos == null) {
            this.photos = Collections.emptyList();
        } else {
            this.photos = photos;
        }
    }

    public void addPhoto(BlobObject blob) {

        if (photos == Collections.EMPTY_LIST) {
            photos = new ArrayList<>();
        }
        photos.add(blob);
    }
}

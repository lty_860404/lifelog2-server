package com.dodopipe.lifelog.core.ports.persistence;

import com.dodopipe.lifelog.core.events.auth.*;
import com.dodopipe.lifelog.core.services.exception.InvalidVerificationCodeException;
import com.dodopipe.lifelog.core.services.exception.VerificationCodeExpiredException;
import com.dodopipe.lifelog.persistence.PersistenceException;

public interface UserAccountEventHandler {

    public VerificationCodeCreatedEvent createVerificationCode(VerificationCodeCreateEvent event);

    public UserRegisteredEvent register(Long userId,
                                        String username,
                                        String phone,
                                        String password)
            throws
            PersistenceException;

    public UserSignedInEvent signIn(UserSignInEvent event)
            throws
            PersistenceException;

    public void verifyCode(String phone,
                           String verificationCode)
            throws
            InvalidVerificationCodeException,
            VerificationCodeExpiredException;
}




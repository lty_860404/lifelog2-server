package com.dodopipe.lifelog.core.events.photo;

import com.dodopipe.lifelog.core.events.UpdatedEvent;

import java.util.List;

/**
 * @author Henry Yan
 */
public class BoundActivitiesChangeEvent extends UpdatedEvent {
    private Long photoId;
    private List<Long> activityIds;

    public BoundActivitiesChangeEvent(Long photoId, List<Long> activityIds) {

        this.photoId = photoId;
        this.activityIds = activityIds;
    }

    public Long getPhotoId() {

        return photoId;
    }

    public List<Long> getActivityIds() {

        return activityIds;
    }
}




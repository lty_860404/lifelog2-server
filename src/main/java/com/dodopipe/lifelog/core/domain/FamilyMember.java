package com.dodopipe.lifelog.core.domain;

import static com.dodopipe.lifelog.core.domain.FamilyRelationshipStatus.RelationshipStatusType;

/**
 * @author yongwei
 */
public class FamilyMember {

    private Long familyId;
    private Long memberId;
    private String username;
    private String name;
    private String avatarFilePath;
    private String relationship;
    private FamilyRelationshipStatus status;

    public FamilyMember() {

    }

    public FamilyMember(Long familyId,
                        Long memberId,
                        String username,
                        String name,
                        String avatarFilePath,
                        String relationship,
                        FamilyRelationshipStatus status) {

        this.familyId = familyId;
        this.memberId = memberId;
        this.username = username;
        this.name = name;
        this.avatarFilePath = avatarFilePath;
        this.relationship = relationship;
        this.status = status;
    }

    public Long getFamilyId() {

        return this.familyId;
    }

    public Long getMemberId() {

        return this.memberId;
    }

    public String getUsername() {

        return this.username;
    }

    public String getName() {

        return this.name;
    }

    public String getAvatarFilePath() {

        return avatarFilePath;
    }

    public String getRelationship() {

        return this.relationship;
    }

    public FamilyRelationshipStatus getStatus() {

        return this.status;
    }

    public FamilyMember handleInvitingRequest(FamilyMember other) {

        RelationshipStatusType currStatus = status.updateStatusForInvitingRequest(other.getStatus()
                                                                                       .getOwnsideStatusType());
        if (currStatus != RelationshipStatusType.RELATION_NOT_CONFIRMED) {
            this.updateRelationshipStatus(this,
                                          other,
                                          currStatus);
            return other;
        }
        return null;
    }

    public FamilyMember handleAcceptingRequest(FamilyMember other) {

        RelationshipStatusType currStatus = status.updateStatusForAcceptingRequest(other.getStatus()
                                                                                        .getOwnsideStatusType());
        this.updateRelationshipStatus(this,
                                      other,
                                      currStatus);
        return other;
    }

    public FamilyMember handleRemovingRequest(FamilyMember other) {

        RelationshipStatusType currStatus = status.updateStatusForRemovingRequest(other.getStatus()
                                                                                       .getOwnsideStatusType());
        this.updateRelationshipStatus(this,
                                      other,
                                      currStatus);
        return other;
    }

    private void updateRelationshipStatus(FamilyMember self,
                                          FamilyMember other,
                                          RelationshipStatusType currStatus) {

        self.getStatus()
            .setOwnsideStatusType(currStatus.getOwnsideStatusType());
        other.getStatus()
             .setOwnsideStatusType(currStatus.getOthersideStatusType());

    }
}

package com.dodopipe.lifelog.rest.controller;

import com.dodopipe.lifelog.core.domain.FamilyInvitationDetail;
import com.dodopipe.lifelog.core.domain.FamilyMember;
import com.dodopipe.lifelog.core.domain.FamilyMemberDetail;
import com.dodopipe.lifelog.core.domain.FamilyRelationship;
import com.dodopipe.lifelog.core.events.family.*;
import com.dodopipe.lifelog.core.services.FamilyQueryService;
import com.dodopipe.lifelog.core.services.exception.FamilyMemberNotFoundException;
import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByIdException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByUsernameException;
import com.dodopipe.lifelog.rest.error.IllegalPrincipalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * @author yongwei
 */
@Controller
@RequestMapping("/family")
public class FamilyQueryController {

    @Autowired
    private FamilyQueryService familyQueryService;

    @RequestMapping(method = RequestMethod.GET,
                    value = "/members")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<FamilyMember> viewAllFamilyMembers(Principal principal)
            throws
            IllegalPrincipalException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);
        FamilyMembersViewedEvent event = familyQueryService.viewAllFamilyMembers(new FamilyMembersViewEvent(userId));
        return event.getMembers();
    }

    @RequestMapping(method = RequestMethod.GET,
                    value = "/sent_invitations")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<FamilyInvitationDetail> viewAllSentInvitations(Principal principal)
            throws
            IllegalPrincipalException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);
        FamilyInvitationsViewedEvent event =
                familyQueryService.viewAllSentInvitations(new FamilyInvitationsViewEvent(userId));
        return event.getInvitations();
    }

    @RequestMapping(method = RequestMethod.GET,
                    value = "/received_invitations")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<FamilyInvitationDetail> viewAllReceivedInvitations(Principal principal)
            throws
            IllegalPrincipalException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);
        FamilyInvitationsViewedEvent event =
                familyQueryService.viewAllReceivedInvitations(new FamilyInvitationsViewEvent(userId));
        return event.getInvitations();
    }

    @RequestMapping(method = RequestMethod.GET,
                    value = "/relationships")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<FamilyRelationship> viewAllFamilyRelationships(Principal principal,
                                                               @RequestParam("lang_code") String langCode)
            throws
            IllegalPrincipalException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);

        FamilyRelationshipsViewedEvent event =
                familyQueryService.viewAllFamilyRelationships(new FamilyRelationshipsViewEvent(userId,
                                                                                               langCode));
        return event.getRelationships();
    }

    @RequestMapping(method = RequestMethod.GET,
                    value = "/members/{member_username}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public FamilyMemberDetail viewFamilyMemberDetail(Principal principal,
                                                     @PathVariable("member_username") String memberUsername)
            throws
            IllegalPrincipalException,
            UserNotFoundByIdException,
            UserNotFoundByUsernameException,
            FamilyMemberNotFoundException,
            InternalServerErrorException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);
        FamilyMemberDetailViewEvent requestEvent = new FamilyMemberDetailViewEvent(userId,
                                                                                   memberUsername);
        FamilyMemberDetailViewedEvent event = familyQueryService.viewFamilyMemberDetail(requestEvent);

        return event.getFamilyMemberDetail();
    }
}





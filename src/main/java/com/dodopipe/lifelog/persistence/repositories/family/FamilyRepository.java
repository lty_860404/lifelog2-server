package com.dodopipe.lifelog.persistence.repositories.family;

import com.dodopipe.lifelog.persistence.domain.family.FamilyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FamilyRepository
        extends JpaRepository<FamilyEntity, Long> {

}

package com.dodopipe.lifelog.core.ports.persistence;

import com.dodopipe.lifelog.core.events.file.FileViewEvent;
import com.dodopipe.lifelog.core.events.file.FileViewedEvent;

/**
 * @author Henry Yan
 */
public interface FileEventHandler {

    public FileViewedEvent viewFile(FileViewEvent event);
}




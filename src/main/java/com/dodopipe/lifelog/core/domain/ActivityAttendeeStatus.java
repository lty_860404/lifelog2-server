package com.dodopipe.lifelog.core.domain;

import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;

/**
 * Created by henryhome on 7/21/14.
 */
public enum ActivityAttendeeStatus {
    PENDING,
    ACCEPTED,
    REJECTED;

    public static ActivityAttendeeStatus[] all = ActivityAttendeeStatus.values();
    public static ActivityAttendeeStatus fromInt(int n) throws InternalServerErrorException {
        if (n < all.length) {
            return all[n];
        }
        else {
            throw new InternalServerErrorException(
                    "Convert from int to Status failed: the value of the integer is beyond the range of the enumeration");
        }
    }
}





package com.dodopipe.lifelog.persistence.adaptors;

import com.dodopipe.lifelog.config.*;
import com.dodopipe.lifelog.core.domain.ActivityAttendee;
import com.dodopipe.lifelog.core.domain.User;
import com.dodopipe.lifelog.core.events.activity.*;
import com.dodopipe.lifelog.core.ports.persistence.ActivityQueryEventHandler;
import com.dodopipe.lifelog.persistence.domain.activity.WeddingActivityDetailEntity;
import com.dodopipe.lifelog.persistence.domain.activity.WeddingActivityEntity;
import com.dodopipe.lifelog.persistence.repositories.activity.ActivityAttendeeRepository;
import com.dodopipe.lifelog.persistence.repositories.activity.ActivityRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserRepository;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.dodopipe.lifelog.persistence.adaptors.fixture.EventHandlerDataFixture.generateActivitiesViewEvent;
import static com.dodopipe.lifelog.persistence.adaptors.fixture.EventHandlerDataFixture.generateActivityAttendeesViewEvent;
import static com.dodopipe.lifelog.persistence.adaptors.fixture.EventHandlerDataFixture.generateActivityViewEvent;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * @author Henry Yan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPAConfiguration.class,
                                 DataSourceConfiguration.class,
                                 RepositoryConfiguration.class,
                                 DBUnitDataSourceConfiguration.class})
@ActiveProfiles(resolver = LifelogActiveProfilesResolver.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class
                        })
@DatabaseSetup("DB_SETUP.xml")
public class ActivityQueryPersistenceEventHandlerTest {

    /**
     * The integration test method
     */
    @Autowired
    private ActivityRepository activityRepo;

    @Autowired
    private ActivityAttendeeRepository activityAttendeeRepo;

    @Autowired
    private UserRepository userRepo;

    private ActivityQueryEventHandler handler;

    /**
     * The mock method
     */
    @Mock
    private ActivityRepository activityRepoMock;

    @Mock
    private UserRepository userRepoMock;

    @InjectMocks
    private ActivityQueryPersistenceEventHandler handlerMock;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
        handler = new ActivityQueryPersistenceEventHandler(activityRepo,
                                                           activityAttendeeRepo,
                                                           userRepo);
    }

    @Test
    public void testViewAllAttendingWeddingActivities() {

        ActivitiesViewEvent event = generateActivitiesViewEvent();

        List<WeddingActivityDetailEntity> mockEntities = new ArrayList<>();
        WeddingActivityEntity weda = (WeddingActivityEntity) activityRepo.findById(5555l);
        WeddingActivityEntity wedb = (WeddingActivityEntity) activityRepo.findById(6666l);
        mockEntities.add(new WeddingActivityDetailEntity(weda,
                                                         true));
        mockEntities.add(new WeddingActivityDetailEntity(wedb,
                                                         false));

        when(activityRepoMock.findWeddingActivitiesEndAfterDateForUser(any(Long.class),
                                                                       any(Date.class),
                                                                       any(Pageable.class))).thenReturn(mockEntities);

        ActivitiesViewedEvent result = handlerMock.viewAllAttendingWeddingActivities(event);

        assertEquals(2,
                     result.getActivities()
                           .size());
        assertEquals(5555,
                     result.getActivities()
                           .get(0)
                           .getActivity()
                           .getId()
                           .intValue());
        assertEquals(6666,
                     result.getActivities()
                           .get(1)
                           .getActivity()
                           .getId()
                           .intValue());

    }

    @Test
    public void testViewAllAttendedWeddingActivities() {

        ActivitiesViewEvent event = generateActivitiesViewEvent();

        List<WeddingActivityDetailEntity> mockEntities = new ArrayList<>();
        WeddingActivityEntity weda = (WeddingActivityEntity) activityRepo.findById(3333l);
        WeddingActivityEntity wedb = (WeddingActivityEntity) activityRepo.findById(4444l);
        mockEntities.add(new WeddingActivityDetailEntity(weda,
                                                         false));
        mockEntities.add(new WeddingActivityDetailEntity(wedb,
                                                         true));

        when(activityRepoMock.findWeddingActivitiesEndBeforeDateForUser(any(Long.class),
                                                                        any(Date.class),
                                                                        any(Pageable.class))).thenReturn(mockEntities);

        ActivitiesViewedEvent result = handlerMock.viewAllAttendedWeddingActivities(event);

        assertEquals(2,
                     result.getActivities()
                           .size());
        assertEquals(3333,
                     result.getActivities()
                           .get(0)
                           .getActivity()
                           .getId()
                           .intValue());
        assertEquals(4444,
                     result.getActivities()
                           .get(1)
                           .getActivity()
                           .getId()
                           .intValue());

    }

    @Test
    public void testViewWeddingActivity()
            throws
            Exception {

        ActivityViewEvent event = generateActivityViewEvent();
        ActivityViewedEvent result = handler.viewWeddingActivity(event);

        assertEquals(3333l,
                     result.getActivity()
                           .getId()
                           .intValue());
    }

    @Test
    public void testViewWeddingActivityAttendees()
            throws
            Exception {

        ActivityAttendeesViewEvent event = generateActivityAttendeesViewEvent();
        ActivityAttendeesViewedEvent result = handler.viewWeddingActivityAttendees(event);

        assertEquals(5,
                     result.getAttendees()
                           .size());
    }
}

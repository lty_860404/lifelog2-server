package com.dodopipe.lifelog.persistence.domain.fixture;

import org.eclipse.persistence.jpa.JpaEntityManager;

import javax.persistence.EntityManager;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

public class JPAAssertions {

    public static void assertTableExist(EntityManager em,
                                        Class<?> entityClasz) {

        ResultCollector rc = new ResultCollector();

        try {
            String tableName = em.unwrap(JpaEntityManager.class)
                                 .getServerSession()
                                 .getDescriptor(entityClasz)
                                 .getTables()
                                 .get(0)
                                 .getName();
            if (tableName != null) {
                rc.found = true;
            }
        } catch (Exception e) {
        }

        if (!rc.found) {
            fail("Entity : " + entityClasz.getCanonicalName() +
                         " 's mapping table is not found");
        }

    }

    public static void assertTableHasColumn(EntityManager em,
                                            Class<?> entityClasz,
                                            String fieldName,
                                            String columnName) {

        ResultCollector rc = new ResultCollector();

        try {

            String cName = em.unwrap(JpaEntityManager.class)
                             .getServerSession()
                             .getDescriptor(entityClasz)
                             .getMappingForAttributeName(fieldName)
                             .getField()
                             .getName();
            if (columnName != null) {
                rc.found = true;
                assertEquals(cName,
                             columnName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!rc.found) {
            fail("Entity : " + entityClasz.getCanonicalName() +
                         " has no field (" + fieldName + ")");
        }

        // String table =
        // em.unwrap(JpaEntityManager.class).getServerSession().getDescriptor(MyClass.class).getTables().get(0).getName();
        // String schema =
        // em.unwrap(JpaEntityManager.class).getServerSession().getDescriptor(MyClass.class).getTables().get(0).getTableQualifier();
        // String column =
        // em.unwrap(JpaEntityManager.class).getServerSession().getDescriptor(MyClass.class).getMappingForAttributeName("myAttribute").getField().getName();

    }

    static class ResultCollector {

        public boolean found = false;
    }
}

package com.dodopipe.lifelog.rest.controller;

import com.dodopipe.lifelog.core.domain.BlobObject;
import com.dodopipe.lifelog.core.domain.PhotoComment;
import com.dodopipe.lifelog.core.domain.PhotoFileData;
import com.dodopipe.lifelog.core.domain.PhotoLike;
import com.dodopipe.lifelog.core.events.photo.*;
import com.dodopipe.lifelog.core.services.PhotoCommandService;
import com.dodopipe.lifelog.core.services.exception.*;
import com.dodopipe.lifelog.rest.error.IllegalPrincipalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.IOException;
import java.security.Principal;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/photos", produces = "application/json;charset=UTF-8")
public class PhotoCommandController {

    @Autowired
    private PhotoCommandService photoCommandService;

    @RequestMapping(method = RequestMethod.POST,
                    consumes = "multipart/form-data")
    public ResponseEntity<Map<String, Object>> uploadPhotos(MultipartHttpServletRequest request,
                                                            Principal principal)
            throws
            IllegalPrincipalException,
            InternalServerErrorException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);

        String comment = request.getParameter("comment");
        PhotoFileData photoFileData = new PhotoFileData();

        for (Iterator<String> fileNames = request.getFileNames(); fileNames.hasNext(); ) {
            MultipartFile file = request.getFile(fileNames.next());

            if (file.isEmpty()) {
                continue;
            }

            try {
                photoFileData.addPhoto(new BlobObject(file.getInputStream(),
                                                      file.getContentType(),
                                                      file.getSize()));
            } catch (IOException e) {
                throw new InternalServerErrorException(e);
            }
        }

        photoFileData.setComment(comment);
        photoFileData.setOwnerId(userId);

        PhotoCreatedEvent event = photoCommandService.uploadPhotos(new PhotoCreateEvent(photoFileData));

        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/{photo_id}/comment",
                    method = RequestMethod.POST)
    public ResponseEntity<Map<String, Boolean>> updatePhotoComment(@PathVariable("photo_id") Long photoId,
                                                                   @RequestBody PhotoComment photoComment)
            throws
            PhotoNotFoundException {

        PhotoUpdatedEvent event = photoCommandService.updatePhotoComment(new PhotoCommentUpdateEvent(photoId,
                                                                                                     photoComment));
        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/{photo_id}/like",
                    method = RequestMethod.POST)
    public ResponseEntity<Map<String, Boolean>> addPhotoLike(@PathVariable("photo_id") Long photoId,
                                                             @RequestBody PhotoLike photoLike)
            throws
            PhotoNotFoundException,
            UserNotFoundByUsernameException,
            InvalidPhotoLikeException {

        PhotoUpdatedEvent event = photoCommandService.addPhotoLike(new PhotoLikeAddEvent(photoId,
                                                                                         photoLike));
        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/{photo_id}/undo_like",
                    method = RequestMethod.POST)
    public ResponseEntity<Map<String, Boolean>> removePhotoLike(@PathVariable("photo_id") Long photoId,
                                                                Principal principal)
            throws
            IllegalPrincipalException,
            PhotoLikeNotFoundException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);
        PhotoUpdatedEvent event = photoCommandService.removePhotoLike(new PhotoLikeRemoveEvent(photoId,
                                                                                               userId));
        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/{photo_id}/caption",
                    method = RequestMethod.POST)
    public ResponseEntity<Map<String, Boolean>> updatePhotoCaption(@PathVariable("photo_id") Long photoId,
                                                                   @RequestParam("caption") String caption)
            throws
            PhotoNotFoundException {

        PhotoUpdatedEvent event = photoCommandService.updatePhotoCaption(new PhotoCaptionUpdateEvent(photoId,
                                                                                                     caption));
        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/{photo_id}/bind",
                    method = RequestMethod.POST)
    public ResponseEntity<Map<String, Boolean>> bindActivities(@PathVariable("photo_id") Long photoId,
                                                               @RequestParam("activity_ids") Long[] activities)
            throws
            PhotoNotFoundException {

        List<Long> activityIds = Arrays.asList(activities);
        PhotoUpdatedEvent event = photoCommandService.bindActivities(new BoundActivitiesChangeEvent(photoId,
                                                                                                    activityIds));

        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/{photo_id}/detach",
                    method = RequestMethod.POST)
    public ResponseEntity<Map<String, Boolean>> detachActivities(@PathVariable("photo_id") Long photoId,
                                                                 @RequestParam("activity_ids") Long[] activities)
            throws
            PhotoNotFoundException {

        List<Long> activityIds = Arrays.asList(activities);
        PhotoUpdatedEvent event = photoCommandService.detachActivities(new BoundActivitiesChangeEvent(photoId,
                                                                                                      activityIds));

        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }

    @RequestMapping(value = "/{photo_id}",
                    method = RequestMethod.DELETE)
    public ResponseEntity<Map<String, Boolean>> deletePhoto(@PathVariable("photo_id") Long photoId)
            throws
            PhotoNotFoundException {

        PhotoDeletedEvent event = photoCommandService.deletePhoto(new PhotoDeleteEvent(photoId));

        return new ResponseEntity<>(event.toMap(),
                                    HttpStatus.OK);
    }
}




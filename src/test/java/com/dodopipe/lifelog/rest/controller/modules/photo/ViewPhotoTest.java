package com.dodopipe.lifelog.rest.controller.modules.photo;

import com.dodopipe.lifelog.core.events.photo.PhotoViewEvent;
import com.dodopipe.lifelog.core.services.PhotoQueryService;
import com.dodopipe.lifelog.rest.controller.PhotoQueryController;
import com.dodopipe.lifelog.rest.controller.fixture.UserAsPrincipal;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;

import static com.dodopipe.lifelog.rest.controller.fixture.RestDataFixture.generatePhoto;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by henryhome on 7/4/14.
 */
public class ViewPhotoTest {

    MockMvc mockMvc;

    @InjectMocks
    PhotoQueryController controller;

    @Mock
    PhotoQueryService photoQueryService;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                                  .alwaysDo(print())
                                                  .build();

    }

    @Test
    public void viewPhotoCorrectly()
            throws
            Exception {

        when(photoQueryService.viewPhoto(any(PhotoViewEvent.class)))
                .thenReturn(generatePhoto());

        Long photoId = 1000l;
        mockMvc.perform(
                get("/photos/{photo_id}",
                    photoId).principal(new UserAsPrincipal("3000")))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.photo.id").value(1000));
    }

    @Test
    public void viewPhotoNotFound()
            throws
            Exception {

    }
}




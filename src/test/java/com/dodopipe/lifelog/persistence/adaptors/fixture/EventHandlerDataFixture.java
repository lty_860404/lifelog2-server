package com.dodopipe.lifelog.persistence.adaptors.fixture;

import com.dodopipe.lifelog.core.domain.*;
import com.dodopipe.lifelog.core.events.activity.*;
import com.dodopipe.lifelog.core.events.photo.*;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EventHandlerDataFixture {

    public static PhotoCreateEvent generatePhotoCreateEvent()
            throws
            Exception {

        PhotoFileData detail = new PhotoFileData();
        detail.setOwnerId(3000L);
        detail.setComment("beautiful photos");
        ClassLoader cl = EventHandlerDataFixture.class.getClassLoader();

        URL resource = cl.getResource("test.jpg");

        URLConnection conn = resource.openConnection();

        BlobObject photo = new BlobObject(conn.getInputStream(),
                                          conn.getContentType(),
                                          conn.getContentLengthLong());
        detail.setPhotos(Arrays.asList(photo));

        return new PhotoCreateEvent(detail);

    }

    public static PhotosViewEvent generateEventForViewAllPhotos() {

        return new PhotosViewEvent(1000l,
                                   0,
                                   40);

    }

    public static PhotosViewEvent generateEventForViewPhotosByActivity() {

        return new PhotosViewEvent(1111l,
                                   5000l,
                                   0,
                                   40);
    }

    public static PhotoViewEvent generatePhotoViewEvent() {

        return new PhotoViewEvent(4000l,
                                  1000l);
    }

    public static PhotoCaptionUpdateEvent generatePhotoCaptionUpdateEvent() {

        Long photoId = 80l;
        String caption = "Photos from our fantastic wedding";
        return new PhotoCaptionUpdateEvent(photoId,
                                           caption);
    }

    public static PhotoCommentUpdateEvent generatePhotoCommentUpdateEvent() {

        Long photoId = 80l;
        PhotoComment comment = new PhotoComment("mz34555",
                                                "Mark Zhao",
                                                "Is this you, my friend?You look really awesome!");
        return new PhotoCommentUpdateEvent(photoId,
                                           comment);
    }

    public static PhotoLikeAddEvent generatePhotoLikeUpdateEvent() {

        Long photoId = 80l;
        PhotoLike like = new PhotoLike("mz34555",
                                       "Mark Zhao");
        return new PhotoLikeAddEvent(photoId,
                                     like);
    }

    public static BoundActivitiesChangeEvent generateBoundActivitiesChangeEvent(Boolean isBound) {

        List<Long> activityIds = new ArrayList<>();

        if (isBound) {
            activityIds.add(1111l);
            activityIds.add(2222l);
        } else {
            activityIds.add(3333l);
        }

        return new BoundActivitiesChangeEvent(4000l,
                                              activityIds);
    }

    public static PhotoDeleteEvent generatePhotoDeleteEvent() {

        return new PhotoDeleteEvent(80l);
    }

    public static ActivitiesViewEvent generateActivitiesViewEvent() {

        Long userId = 3000l;
        Integer pageNum = 0;
        Integer numPerPage = 20;

        return new ActivitiesViewEvent(userId,
                                       pageNum,
                                       numPerPage);
    }

    public static ActivityCreateEvent generateActivityCreateEvent() {

        Long userId = 3000l;

        Activity activity = new WeddingActivity(80l,
                                                4000l,
                                                "Test Wedding Activity",
                                                null,
                                                null,
                                                null,
                                                null,
                                                null);

        return new ActivityCreateEvent(userId,
                                       activity);
    }

    public static ActivityUpdateEvent generateActivityUpdateEvent() {

        Long activityId = 3333l;

        Activity activity = new WeddingActivity(80l,
                                                4000l,
                                                "Test Wedding Activity",
                                                null,
                                                null,
                                                "empty",
                                                "empty",
                                                "empty");

        return new ActivityUpdateEvent(activityId,
                                       activity);
    }

    public static ActivityDeleteEvent generateActivityDeleteEvent() {

        Long activityId = 3000l;
        return new ActivityDeleteEvent(activityId);
    }

    public static ActivityViewEvent generateActivityViewEvent() {

        Long activityId = 3333l;
        Long userId = 3000l;

        return new ActivityViewEvent(activityId,
                                     userId);
    }

    public static ActivityAttendeesViewEvent generateActivityAttendeesViewEvent() {

        return new ActivityAttendeesViewEvent(5555l,
                                              0,
                                              40);
    }
}

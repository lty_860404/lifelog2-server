package com.dodopipe.lifelog.core.events;

public class ViewEvent {

    protected boolean entityFound = false;

    public ViewEvent() {

    }

    public ViewEvent(boolean f) {

        entityFound = f;
    }

    public void setEntityFound(boolean f) {

        this.entityFound = f;
    }

    public boolean isEntityFound() {

        return entityFound;
    }

}

package com.dodopipe.lifelog.rest.error;

public class InvalidRequestParameterException
        extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public InvalidRequestParameterException(String message) {

        super(message);

    }


}

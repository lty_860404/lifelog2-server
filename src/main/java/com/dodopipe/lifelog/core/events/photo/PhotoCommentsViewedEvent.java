package com.dodopipe.lifelog.core.events.photo;

import com.dodopipe.lifelog.core.domain.PhotoComment;

import java.util.List;

/**
 * @author Henry Yan
 */
public class PhotoCommentsViewedEvent {

    private List<PhotoComment> photoComments;

    public PhotoCommentsViewedEvent(List<PhotoComment> photoComments) {

        this.photoComments = photoComments;
    }

    public List<PhotoComment> getPhotoComments() {

        return photoComments;
    }
}

package com.dodopipe.lifelog.core.ports.persistence;

import com.dodopipe.lifelog.core.events.activity.*;
import com.dodopipe.lifelog.core.services.exception.ActivityNotFoundException;
import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;

/**
 * @author Henry Yan
 */
public interface ActivityQueryEventHandler {

    public ActivitiesViewedEvent viewAllAttendingWeddingActivities(ActivitiesViewEvent event);

    public ActivitiesViewedEvent viewAllAttendedWeddingActivities(ActivitiesViewEvent event);

    public ActivityViewedEvent viewWeddingActivity(ActivityViewEvent event)
            throws
            ActivityNotFoundException;

    public ActivityAttendeesViewedEvent viewWeddingActivityAttendees(ActivityAttendeesViewEvent event)
            throws
            InternalServerErrorException;
}




package com.dodopipe.lifelog.rest.controller.modules.account;

import com.dodopipe.lifelog.core.events.auth.UserSignInEvent;
import com.dodopipe.lifelog.core.services.UserAccountService;
import com.dodopipe.lifelog.rest.controller.UserAccountController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;

import static com.dodopipe.lifelog.rest.controller.fixture.RestEventFixture.generateUserSignedInEvent;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by henryhome on 8/4/14.
 */
public class UserSignInTest {

    MockMvc mockMvc;

    @InjectMocks
    UserAccountController controller;

    @Mock
    UserAccountService service;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                                  .build();

    }

    @Test
    public void testSignIn()
            throws
            Exception {

        when(service.signIn(any(UserSignInEvent.class))).thenReturn(generateUserSignedInEvent());

        mockMvc.perform(post("/account/sign_in").param("username",
                                                      "test_username")
                                               .param("password",
                                                      "test_password"))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.accessId").value("test_access_id"))
               .andExpect(jsonPath("$.secretKey").value("test_secret_key"));
    }
}

package com.dodopipe.lifelog.security;


import org.springframework.security.core.AuthenticationException;

/**
 * @author yongwei
 */
public class SignatureNotFoundException
        extends AuthenticationException {

    public SignatureNotFoundException(String message) {

        super(message);
    }
}

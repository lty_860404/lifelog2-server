package com.dodopipe.lifelog.rest.advisor;

import com.dodopipe.lifelog.core.services.exception.*;
import com.dodopipe.lifelog.rest.error.ErrorMessageBuilder;
import com.dodopipe.lifelog.rest.error.ErrorMessageBuilder.ErrorCode;
import com.dodopipe.lifelog.rest.error.IllegalPrincipalException;
import com.dodopipe.lifelog.rest.error.RestErrorMessage;
import com.dodopipe.lifelog.security.AccountNotFoundException;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler
        extends
        ResponseEntityExceptionHandler {

    public RestResponseEntityExceptionHandler() {

        super();
    }

    @ExceptionHandler({AuthenticationServiceException.class})
    public ResponseEntity<Object> handleNonceExpiredException(final AuthenticationServiceException nonceExcep,
                                                              final WebRequest request) {

        return handleExceptionInternal(nonceExcep,
                                       "{error:nonce is expired}",
                                       new HttpHeaders(),
                                       HttpStatus.UNAUTHORIZED,
                                       request);
    }

    @ExceptionHandler({TransactionSystemException.class})
    public ResponseEntity<Object> handleDuplication(
            final TransactionSystemException e,
            final WebRequest request) {

        Throwable rootException = e.getRootCause();

        if (rootException == null) {
            return handleExceptionInternal(e,
                                           null,
                                           new HttpHeaders(),
                                           HttpStatus.INTERNAL_SERVER_ERROR,
                                           request);
        }

        if (rootException instanceof MySQLIntegrityConstraintViolationException) {

            return handleIntegrityConstraintViolation(
                    (MySQLIntegrityConstraintViolationException) rootException,
                    request);

        }

        return handleExceptionInternal(e,
                                       null,
                                       new HttpHeaders(),
                                       HttpStatus.INTERNAL_SERVER_ERROR,
                                       request);

    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> handleIllegalArgumentException(
            final IllegalArgumentException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.IllegalArgument,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.NOT_ACCEPTABLE,
                                       request);
    }


    @ExceptionHandler(MySQLIntegrityConstraintViolationException.class)
    private ResponseEntity<Object> handleIntegrityConstraintViolation(
            MySQLIntegrityConstraintViolationException e,
            WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.DataIntegrityConstraintViolation,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.CONFLICT,
                                       request);
    }

    @ExceptionHandler(IllegalPrincipalException.class)
    public ResponseEntity<Object> handleIllegalPrincipalException(
            final IllegalPrincipalException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.IllegalPrincipal,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.NETWORK_AUTHENTICATION_REQUIRED,
                                       request);
    }

    @ExceptionHandler(VerificationCodeNotFoundException.class)
    public ResponseEntity<Object> handleVerificationCodeNotFoundException(
            final VerificationCodeNotFoundException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.VerificationCodeNotFound,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.NOT_FOUND,
                                       request);
    }

    @ExceptionHandler(InvalidVerificationCodeException.class)
    public ResponseEntity<Object> handleInvalidVerificationCodeException(
            final InvalidVerificationCodeException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.InvalidVerificationCode,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.NOT_ACCEPTABLE,
                                       request);
    }

    @ExceptionHandler(VerificationCodeExpiredException.class)
    public ResponseEntity<Object> handleVerificationCodeExpiredException(
            final VerificationCodeExpiredException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.VerificationCodeExpired,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.NOT_ACCEPTABLE,
                                       request);
    }

    @ExceptionHandler(AccountNotFoundException.class)
    public ResponseEntity<Object> handleAccountNotFoundException(
            final AccountNotFoundException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.AccountNotFound,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.NOT_FOUND,
                                       request);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<Object> handleUserNotFondException(
            final UserNotFoundException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.UserNotFound,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.NOT_FOUND,
                                       request);
    }

    @ExceptionHandler(UserNotFoundByIdException.class)
    public ResponseEntity<Object> handleUserNotFoundByIdException(
            final UserNotFoundByIdException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.UserNotFound,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.NOT_FOUND,
                                       request);
    }

    @ExceptionHandler(UserNotFoundByUsernameException.class)
    public ResponseEntity<Object> handleUserNotFoundByUsernameException(
            final UserNotFoundByUsernameException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.UserNotFound,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.NOT_FOUND,
                                       request);
    }

    @ExceptionHandler(UserNotFoundByPhoneException.class)
    public ResponseEntity<Object> handleUserNotFoundByPhoneException(
            final UserNotFoundByPhoneException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.UserNotFound,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.NOT_FOUND,
                                       request);
    }

    @ExceptionHandler(FamilyInvitationNotFoundException.class)
    public ResponseEntity<Object> handleFamilyInvitationNotFoundException(
            final FamilyInvitationNotFoundException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.FamilyInvitationNotFound,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.NOT_FOUND,
                                       request);
    }

    @ExceptionHandler(FamilyMemberNotFoundException.class)
    public ResponseEntity<Object> handleFamilyMemberNotFoundException(
            final FamilyMemberNotFoundException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.FamilyMemberNotFound,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.NOT_FOUND,
                                       request);
    }

    @ExceptionHandler(InvalidFamilyInvitationException.class)
    public ResponseEntity<Object> handleInvalidFamilyInvitationException(
            final InvalidFamilyInvitationException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.InvalidFamilyInvitation,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.NOT_ACCEPTABLE,
                                       request);
    }

    @ExceptionHandler(PhotoNotFoundException.class)
    public ResponseEntity<Object> handlePhotoNotFoundException(
            final PhotoNotFoundException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.PhotoNotFound,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.NOT_FOUND,
                                       request);
    }

    @ExceptionHandler(PhotoLikeNotFoundException.class)
    public ResponseEntity<Object> handlePhotoLikeNotFoundException(
            final PhotoLikeNotFoundException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.PhotoLikeNotFound,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.NOT_FOUND,
                                       request);
    }

    @ExceptionHandler(ActivityNotFoundException.class)
    public ResponseEntity<Object> handleActivityNotFoundException(
            final ActivityNotFoundException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.ActivityNotFound,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.NOT_FOUND,
                                       request);
    }

    @ExceptionHandler(ActivityAttendeeNotFoundException.class)
    public ResponseEntity<Object> handleActivityAttendeeNotFoundException(
            final ActivityAttendeeNotFoundException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.ActivityAttendeeNotFound,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.NOT_FOUND,
                                       request);
    }

    @ExceptionHandler(MessageNotDeliveredException.class)
    public ResponseEntity<Object> handleMessageNotDeliveredException(
            final MessageNotDeliveredException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.MessageNotDelivered,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.NOT_ACCEPTABLE,
                                       request);
    }

    @ExceptionHandler(InternalServerErrorException.class)
    public ResponseEntity<Object> handleInternalServerErrorException(
            final InternalServerErrorException e,
            final WebRequest request) {

        RestErrorMessage errorMessage = ErrorMessageBuilder
                .getInstance()
                .build(ErrorCode.InternalServerError,
                       e.getMessage());

        return handleExceptionInternal(e,
                                       errorMessage,
                                       new HttpHeaders(),
                                       HttpStatus.INTERNAL_SERVER_ERROR,
                                       request);
    }
}




package com.dodopipe.lifelog.rest.fixture;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Arrays;

public class RestData {

    public static HttpEntity<MultiValueMap<String, Object>> photosData()
            throws
            Exception {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));


        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
        parts.add("comment",
                  "post photos from qaci test");
        Resource pic = new ClassPathResource("test.jpg");
        parts.add("pictures",
                  pic);
        return new HttpEntity<>(parts,
                                headers);
    }

    public static HttpEntity<MultiValueMap<String, Object>> userVerifyPhoneRequest() {

        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
        parts.add("country_code", "+33333333");
        parts.add("phone",
                  "55555555");
        return new HttpEntity<>(parts,
                                newHttpHeaders(MediaType.APPLICATION_FORM_URLENCODED));
    }

    public static HttpEntity<MultiValueMap<String, Object>> userRegisterRequest() {

        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
        parts.add("verification_code",
                  "abcdef");
        parts.add("country_code",
                  "+86");
        parts.add("phone",
                  "55555555");
        parts.add("password",
                  "test_password");
        parts.add("lang_code",
                  "en");
        return new HttpEntity<>(parts,
                                newHttpHeaders(MediaType.APPLICATION_FORM_URLENCODED));
    }

    private static HttpHeaders newHttpHeaders(MediaType contentsType) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(contentsType);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return headers;
    }

    public static HttpEntity<MultiValueMap<String, Object>> acceptInvitationRequest() {

        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
        parts.add("invitation_id",
                  "3000");
        parts.add("invitee_relationship",
                  "Friend");
        parts.add("invitee_lang_code",
                  "en");
        return new HttpEntity<>(parts,
                                newHttpHeaders(MediaType.APPLICATION_FORM_URLENCODED));
    }

    public static HttpEntity<MultiValueMap<String, Object>> familyInvitationRequestByUserName() {

        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
        parts.add("invitee_username",
                  "Jack55555");
        parts.add("invitee_nickname",
                  "Jack");
        parts.add("inviter_relationship",
                  "friend");
        parts.add("inviter_lang_code",
                  "en");
        return new HttpEntity<>(parts,
                                newHttpHeaders(MediaType.APPLICATION_FORM_URLENCODED));
    }


    public static HttpEntity<MultiValueMap<String, Object>> familyInvitationRequestByPhone() {

        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
        parts.add("invitee_phone",
                  "+818012342222");
        parts.add("invitee_nickname",
                  "test_nickname");
        parts.add("invitee_relationship",
                  "friend");
        return new HttpEntity<>(parts,
                                newHttpHeaders(MediaType.APPLICATION_FORM_URLENCODED));
    }

    public static HttpEntity<MultiValueMap<String, Object>> familyMemberUpdateRequest() {

        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
        parts.add("relationship",
                  "BOY FRIEND");
        parts.add("lang_code",
                  "en");
        return new HttpEntity<>(parts,
                                newHttpHeaders(MediaType.APPLICATION_FORM_URLENCODED));
    }
}




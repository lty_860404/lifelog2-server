package com.dodopipe.lifelog.core.domain;


public class PhotoComment {

    private String username;
    private String nickname;
    private String comment;

    public PhotoComment() {

    }

    public PhotoComment(String username,
                        String nickname,
                        String comment) {

        this.username = username;
        this.nickname = nickname;
        this.comment = comment;
    }

    public String getNickname() {

        return nickname;
    }

    public String getUsername() {

        return username;
    }

    public String getComment() {

        return comment;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof PhotoComment)) {
            return false;
        }

        PhotoComment that = (PhotoComment) o;

        if (comment != null ? !comment.equals(that.comment) : that.comment != null) {
            return false;
        }
        if (nickname != null ? !nickname.equals(that.nickname) : that.nickname != null) {
            return false;
        }
        if (username != null ? !username.equals(that.username) : that.username != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {

        int result = username != null ? username.hashCode() : 0;
        result = 31 * result + (nickname != null ? nickname.hashCode() : 0);
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {

        final StringBuffer sb = new StringBuffer("PhotoComment{");
        sb.append("username='")
          .append(username)
          .append('\'');
        sb.append(", nickname='")
          .append(nickname)
          .append('\'');
        sb.append(", comment='")
          .append(comment)
          .append('\'');
        sb.append('}');
        return sb.toString();
    }
}




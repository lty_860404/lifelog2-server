package com.dodopipe.lifelog.core.events.family;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by henryhome on 7/29/14.
 */
public class ExternalInvitationUpdatedEvent {
    public boolean success;

    public ExternalInvitationUpdatedEvent(boolean success) {

        this.success = success;
    }

    public boolean isSuccess() {

        return success;
    }

    public Map<String, Boolean> toMap() {

        Map<String, Boolean> map = new HashMap<>();
        map.put("success",
                isSuccess());
        return map;
    }
}




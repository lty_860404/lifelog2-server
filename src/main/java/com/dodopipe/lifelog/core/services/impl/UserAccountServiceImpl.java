package com.dodopipe.lifelog.core.services.impl;

import com.dodopipe.lifelog.core.domain.User;
import com.dodopipe.lifelog.core.events.activity.ActivityAttendeeUpdateEvent;
import com.dodopipe.lifelog.core.events.auth.*;
import com.dodopipe.lifelog.core.events.family.ExternalInvitationUpdateEvent;
import com.dodopipe.lifelog.core.events.family.InviteeInfoUpdateEvent;
import com.dodopipe.lifelog.core.events.user.UserCreateEvent;
import com.dodopipe.lifelog.core.events.user.UserCreatedEvent;
import com.dodopipe.lifelog.core.ports.notification.MessageEventHandler;
import com.dodopipe.lifelog.core.ports.persistence.ActivityCommandEventHandler;
import com.dodopipe.lifelog.core.ports.persistence.FamilyCommandEventHandler;
import com.dodopipe.lifelog.core.ports.persistence.UserAccountEventHandler;
import com.dodopipe.lifelog.core.ports.persistence.UserEventHandler;
import com.dodopipe.lifelog.core.services.UserAccountService;
import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;
import com.dodopipe.lifelog.core.services.exception.InvalidVerificationCodeException;
import com.dodopipe.lifelog.core.services.exception.MessageNotDeliveredException;
import com.dodopipe.lifelog.core.services.exception.VerificationCodeExpiredException;
import com.dodopipe.lifelog.persistence.PersistenceException;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class UserAccountServiceImpl
        implements UserAccountService {

    private UserAccountEventHandler accountHandler;

    private UserEventHandler userHandler;

    private FamilyCommandEventHandler familyHandler;

    private ActivityCommandEventHandler activityHandler;

    private MessageEventHandler messageHandler;

    public UserAccountServiceImpl(UserAccountEventHandler accountHandler,
                                  UserEventHandler userHandler,
                                  FamilyCommandEventHandler familyHandler,
                                  ActivityCommandEventHandler activityHandler,
                                  MessageEventHandler messageHandler) {

        this.accountHandler = accountHandler;
        this.userHandler = userHandler;
        this.familyHandler = familyHandler;
        this.activityHandler = activityHandler;
        this.messageHandler = messageHandler;
    }

    @Override
    public VerificationCodeCreatedEvent createAndSendVerificationCode(VerificationCodeCreateEvent event)
            throws
            InternalServerErrorException {

        VerificationCodeCreatedEvent createdEvent = accountHandler.createVerificationCode(event);

        String countryCode = event.getCountryCode();
        String destinationPhone = event.getPhone();
        String verificationCode = createdEvent.getVerificationCode();

        messageHandler.sendVerificationCodeNotification(countryCode,
                                                        destinationPhone,
                                                        verificationCode);

        return createdEvent;
    }

    @Override
    public VerificationCodeNotifiedEvent notifyVerificationStatus(VerificationCodeNotifyEvent event)
            throws
            MessageNotDeliveredException,
            InternalServerErrorException {

        return messageHandler.notifyVerificationStatus(event);
    }

    @Override
    public void updateVerificationStatus(MessageCallbackCreateEvent event) {

        messageHandler.updateVerificationStatus(event);
    }

    @Override
    public UserRegisteredEvent register(UserRegisterEvent event)
            throws
            InvalidVerificationCodeException,
            VerificationCodeExpiredException,
            PersistenceException {

        String verificationCode = event.getVerificationCode();
        String countryCode = event.getCountryCode();
        String phone = event.getPhone();
        String password = event.getPassword();
        String langCode = event.getLangCode();

        accountHandler.verifyCode(phone,
                                  verificationCode);

        UserCreatedEvent createdEvent = userHandler.createUser(new UserCreateEvent(verificationCode,
                                                                                   countryCode,
                                                                                   phone,
                                                                                   langCode));
        User createdUser = createdEvent.getUser();
        Long userId = createdUser.getId();
        String username = createdUser.getUsername();
        String userPhone = createdUser.getPhone();

        familyHandler.updateFamilyInvitationInviteeInfo(new InviteeInfoUpdateEvent(userId,
                                                                                   username,
                                                                                   userPhone));

        familyHandler.updateExternalInvitationStatus(new ExternalInvitationUpdateEvent(userPhone));

        activityHandler.updateAttendeeInfo(new ActivityAttendeeUpdateEvent(userId,
                                                                           username,
                                                                           userPhone));

        return accountHandler.register(userId,
                                       username,
                                       phone,
                                       password);
    }

    @Override
    public UserSignedInEvent signIn(UserSignInEvent event)
            throws
            PersistenceException {

        return accountHandler.signIn(event);
    }
}




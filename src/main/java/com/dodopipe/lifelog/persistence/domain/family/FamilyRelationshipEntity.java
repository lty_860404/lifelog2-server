package com.dodopipe.lifelog.persistence.domain.family;

import com.dodopipe.lifelog.core.domain.FamilyRelationship;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author tianyin.luo
 */
@Entity(name = "FAMILY_RELATIONSHIP")
public class FamilyRelationshipEntity
        implements Serializable {

    private final static long serialVersionUID = 1434534634532455L;

    @Id
    @Column(name = "ID",
            columnDefinition = "BIGINT(25) NOT NULL AUTO_INCREMENT")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "DISPLAY_NAME",
            columnDefinition = "VARCHAR(80)")
    private String displayName;

    @Column(name = "LANG_CODE",
            columnDefinition = "VARCHAR(20)")
    private String langCode;

    @Column(name = "USER_ID",
            columnDefinition = "BIGINT(25)")
    private Long userId;

    @Column(name = "RELATIONSHIP_SCOPE",
            columnDefinition = "VARCHAR(80)")
    @Enumerated(EnumType.STRING)
    private RelationshipScope relationshipScope;

    public enum RelationshipScope {

        System,
        Customized
    }

    public FamilyRelationshipEntity() {

    }

    public FamilyRelationshipEntity(String displayName,
                                    String langCode,
                                    Long userId,
                                    RelationshipScope relationshipScope) {

        this.displayName = displayName;
        this.langCode = langCode;
        this.userId = userId;
        this.relationshipScope = relationshipScope;
    }

    public FamilyRelationship toFamilyRelationship() {

        return new FamilyRelationship(displayName,
                                      langCode,
                                      relationshipScope.toString());
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public String getDisplayName() {

        return displayName;
    }

    public void setDisplayName(String displayName) {

        this.displayName = displayName;
    }

    public String getLangCode() {

        return langCode;
    }

    public void setLangCode(String langCode) {

        this.langCode = langCode;
    }

    public Long getUserId() {

        return userId;
    }

    public void setUserId(Long userId) {

        this.userId = userId;
    }

    public RelationshipScope getRelationshipScope() {

        return relationshipScope;
    }

    public void setRelationshipScope(RelationshipScope relationshipScope) {

        this.relationshipScope = relationshipScope;
    }

    @Override
    public int hashCode() {

        return id != null ? id.hashCode() : 0;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof FamilyRelationshipEntity)) {
            return false;
        }

        FamilyRelationshipEntity that = (FamilyRelationshipEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }

        return true;
    }
}

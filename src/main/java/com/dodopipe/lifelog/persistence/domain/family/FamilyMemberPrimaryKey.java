package com.dodopipe.lifelog.persistence.domain.family;

import java.io.Serializable;

/**
 * @author tianyin.luo
 */
public class FamilyMemberPrimaryKey
        implements Serializable {

    private final static long serialVersionUID = 1l;

    private Long familyId;

    private Long memberId;

    public FamilyMemberPrimaryKey() {

    }

    public FamilyMemberPrimaryKey(Long familyId,
                                  Long memberId) {

        this.familyId = familyId;
        this.memberId = memberId;
    }

    public Long getFamilyId() {

        return familyId;
    }

    public void setFamilyId(Long familyId) {

        this.familyId = familyId;
    }

    public Long getMemberId() {

        return memberId;
    }

    public void setMemberId(Long memberId) {

        this.memberId = memberId;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FamilyMemberPrimaryKey that = (FamilyMemberPrimaryKey) o;

        if (familyId != null ? !familyId.equals(that.familyId) : that.familyId != null) {
            return false;
        }
        if (memberId != null ? !memberId.equals(that.memberId) : that.memberId != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {

        int result = familyId != null ? familyId.hashCode() : 0;
        result = 31 * result + (memberId != null ? memberId.hashCode() : 0);
        return result;
    }
}

package com.dodopipe.lifelog.config;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.dodopipe.lifelog.persistence.repositories.BlobRepository;
import com.dodopipe.lifelog.persistence.repositories.impl.MediaFileBlobRepositoryImpl;
import com.dodopipe.lifelog.persistence.repositories.impl.MediaS3BlobRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
public class RepositoryConfiguration {

    @Configuration
    @Profile({LifelogProfile.QACI,
              LifelogProfile.PROD})
    @PropertySource("classpath:/amazon_intg.properties")
    static class S3BucketIntg {

    }

    @Autowired
    private Environment env;

    @SuppressWarnings("rawtypes")
    @Bean
    @Profile({LifelogProfile.INTG,
              LifelogProfile.DEVE})
    public BlobRepository mediaFileBlobRepository() {

        return new MediaFileBlobRepositoryImpl();
    }


    @SuppressWarnings("rawtypes")
    @Bean
    @Profile(value = {LifelogProfile.QACI,
                      LifelogProfile.PROD})
    public BlobRepository mediaS3BlobRepository() {

        String accessKey = env.getProperty("amazon.accessKey");
        String accessSecret = env.getProperty("amazon.accessSecret");
        String bucket = env.getProperty("amazon.bucket");

        BasicAWSCredentials credential = new BasicAWSCredentials(accessKey,
                                                                 accessSecret);
        AmazonS3 s3 = new AmazonS3Client(credential);

        MediaS3BlobRepositoryImpl s3Repository = new MediaS3BlobRepositoryImpl(s3);
        s3Repository.setBucket(bucket);
        return s3Repository;
    }


}

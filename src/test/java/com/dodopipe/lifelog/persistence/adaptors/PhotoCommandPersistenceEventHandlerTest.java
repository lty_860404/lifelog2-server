package com.dodopipe.lifelog.persistence.adaptors;

import com.dodopipe.lifelog.config.*;
import com.dodopipe.lifelog.core.events.photo.*;
import com.dodopipe.lifelog.core.ports.persistence.PhotoCommandEventHandler;
import com.dodopipe.lifelog.database.utils.DatabaseUtil;
import com.dodopipe.lifelog.persistence.domain.MediaObject;
import com.dodopipe.lifelog.persistence.repositories.BlobRepository;
import com.dodopipe.lifelog.persistence.repositories.activity.ActivityRepository;
import com.dodopipe.lifelog.persistence.repositories.photo.PhotoCommentRepository;
import com.dodopipe.lifelog.persistence.repositories.photo.PhotoLikeRepository;
import com.dodopipe.lifelog.persistence.repositories.photo.PhotoRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserRepository;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static com.dodopipe.lifelog.persistence.adaptors.fixture.EventHandlerDataFixture.*;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPAConfiguration.class,
                                 DataSourceConfiguration.class,
                                 RepositoryConfiguration.class,
                                 DBUnitDataSourceConfiguration.class})
@ActiveProfiles(resolver = LifelogActiveProfilesResolver.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class
                        })
@DatabaseSetup("DB_SETUP.xml")
public class PhotoCommandPersistenceEventHandlerTest {

    @Autowired
    private PhotoRepository photoRepo;

    @Autowired
    private PhotoCommentRepository photoCommentRepo;

    @Autowired
    private PhotoLikeRepository photoLikeRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private BlobRepository<MediaObject, String> blobRepo;

    @Autowired
    private ActivityRepository activityRepo;

    private PhotoCommandEventHandler handler;

    @Before
    public void setUp() {

        handler = new PhotoCommandPersistenceEventHandler(photoRepo,
                                                          photoCommentRepo,
                                                          photoLikeRepo,
                                                          userRepo,
                                                          blobRepo);

    }

    @Test
    public void testSave()
            throws
            Exception {

        PhotoCreateEvent event = generatePhotoCreateEvent();
        PhotoCreatedEvent created = handler.uploadPhotos(event);
        assertNotNull(created.getPhotoId());
        assertEquals(created.getComment(),
                     event.getPhotoFileData()
                          .getComment());
        assertEquals(created.getPhotos()
                            .size(),
                     1);

        assertNotNull(created.getPhotos()
                             .get(0)
                             .getOriginalFileKey());

        assertNotNull(created.getPhotos()
                             .get(0)
                             .getThumbnailKey());
    }

    @Test
    public void testUpdatePhoto()
            throws
            Exception {

        PhotoCaptionUpdateEvent event = generatePhotoCaptionUpdateEvent();
        PhotoUpdatedEvent result = handler.updatePhotoCaption(event);
        assertTrue(result.isSuccess());
    }

    @Test
    public void testUpdatePhotoComment()
            throws
            Exception {

        PhotoCommentUpdateEvent event = generatePhotoCommentUpdateEvent();
        PhotoUpdatedEvent result = handler.updatePhotoComment(event);
        assertTrue(result.isSuccess());
    }

    @Test
    public void testUpdatePhotoLike()
            throws
            Exception {

        PhotoLikeAddEvent event = generatePhotoLikeUpdateEvent();
        PhotoUpdatedEvent result = handler.addPhotoLike(event);
        assertTrue(result.isSuccess());
    }

    @Test
    public void testBindActivitiesToPhoto() throws Exception {
        DatabaseUtil.populatePhotoWithBoundActivitiesNormal(photoRepo, activityRepo);

        BoundActivitiesChangeEvent event = generateBoundActivitiesChangeEvent(true);

        PhotoUpdatedEvent result = handler.bindActivities(event);

        assertTrue(result.isSuccess());
    }

    @Test
    public void testDetachActivitiesFromPhoto() throws Exception {
        DatabaseUtil.populatePhotoWithBoundActivitiesNormal(photoRepo, activityRepo);

        BoundActivitiesChangeEvent event = generateBoundActivitiesChangeEvent(false);
        PhotoUpdatedEvent result = handler.detachActivities(event);
        assertTrue(result.isSuccess());
    }

    @Test
    public void testDeletePhoto() throws Exception {
        PhotoDeleteEvent event = generatePhotoDeleteEvent();
        PhotoDeletedEvent result = handler.deletePhoto(event);
        assertTrue(result.isSuccess());
    }
}




package com.dodopipe.lifelog.rest.error;

public class ErrorMessageBuilder {

    private static ErrorMessageBuilder instance = new ErrorMessageBuilder();

    private ErrorMessageBuilder() {

    }

    public static ErrorMessageBuilder getInstance() {

        return instance;
    }

    public RestErrorMessage build(ErrorCode code,
                                  Throwable th) {

        return new RestErrorMessage(code.getErrorCode(),
                                    th.getMessage());

    }

    public RestErrorMessage build(ErrorCode code,
                                  String message) {

        return new RestErrorMessage(code.getErrorCode(),
                                    message);
    }

    public enum ErrorCode {
        IllegalArgument("EX000"),
        DataIntegrityConstraintViolation("EX005"),
        IllegalPrincipal("EX010"),
        VerificationCodeNotFound("EX015"),
        InvalidVerificationCode("EX020"),
        VerificationCodeExpired("EX025"),
        AccountNotFound("EX030"),
        UserNotFound("EX035"),
        FamilyInvitationNotFound("EX040"),
        FamilyMemberNotFound("EX045"),
        InvalidFamilyInvitation("EX050"),
        PhotoNotFound("EX055"),
        PhotoLikeNotFound("EX060"),
        ActivityNotFound("EX065"),
        ActivityAttendeeNotFound("EX070"),
        MessageNotDelivered("EX075"),
        InternalServerError("EX080");

        private String code;

        private ErrorCode(String code) {

            this.code = code;
        }

        public String getErrorCode() {

            return code;
        }
    }
}








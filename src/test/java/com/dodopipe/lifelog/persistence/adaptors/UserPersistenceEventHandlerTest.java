package com.dodopipe.lifelog.persistence.adaptors;

import com.dodopipe.lifelog.config.*;
import com.dodopipe.lifelog.core.events.auth.UserSignOutEvent;
import com.dodopipe.lifelog.core.events.auth.UserSignedOutEvent;
import com.dodopipe.lifelog.core.events.user.PasswordUpdateEvent;
import com.dodopipe.lifelog.core.events.user.PasswordUpdatedEvent;
import com.dodopipe.lifelog.core.ports.persistence.UserEventHandler;
import com.dodopipe.lifelog.persistence.domain.MediaObject;
import com.dodopipe.lifelog.persistence.repositories.BlobRepository;
import com.dodopipe.lifelog.persistence.repositories.ML10NStringRepository;
import com.dodopipe.lifelog.persistence.repositories.family.FamilyRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserAccountRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserRepository;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static com.dodopipe.lifelog.core.services.fixture.ServiceDataFixture.generatePasswordUpdateEvent;
import static com.dodopipe.lifelog.core.services.fixture.ServiceDataFixture.generateUserSignOutEvent;
import static org.junit.Assert.assertTrue;

/**
 * @author Henry Yan
 */
@RunWith(SpringJUnit4ClassRunner.class)

@ContextConfiguration(classes = {JPAConfiguration.class,
                                 DataSourceConfiguration.class,
                                 RepositoryConfiguration.class,
                                 DBUnitDataSourceConfiguration.class})
@ActiveProfiles(resolver = LifelogActiveProfilesResolver.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)

@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class
                        })
@DatabaseSetup("DB_SETUP.xml")
public class UserPersistenceEventHandlerTest {

    @Autowired
    public UserRepository userRepo;

    @Autowired
    public UserAccountRepository authRepo;

    @Autowired
    public FamilyRepository familyRepo;

    @Autowired
    public BlobRepository<MediaObject, String> blobRepo;

    @Autowired
    public ML10NStringRepository mRepo;

    public UserEventHandler handler;

    @Before
    public void setUp() {
        handler = new UserPersistenceEventHandler(userRepo, authRepo, familyRepo, blobRepo, mRepo);
    }

    @Test
    public void testUserSignOutCorrectly() {
        UserSignOutEvent event = generateUserSignOutEvent();
        UserSignedOutEvent result = handler.signOut(event);

        assertTrue(result.isSuccess());
    }

    @Test
    public void testChangPasswordCorrectly() {

        PasswordUpdateEvent event = generatePasswordUpdateEvent();
        PasswordUpdatedEvent result = handler.changePassword(event);

        assertTrue(result.isSuccess());
    }
}

package com.dodopipe.lifelog.persistence.domain.activity;

/**
 * Created by henryhome on 8/14/14.
 */
public class ActivityDetailEntity {

    private ActivityEntity activity;
    private Boolean isCurrUserHost;

    public ActivityDetailEntity(ActivityEntity activity,
                                Boolean isCurrUserHost) {

        this.activity = activity;
        this.isCurrUserHost = isCurrUserHost;
    }

    public ActivityEntity getActivity() {

        return activity;
    }

    public void setActivity(ActivityEntity activity) {

        this.activity = activity;
    }

    public Boolean isCurrUserHost() {

        return isCurrUserHost;
    }

    public void setCurrUserHost(Boolean isCurrUserHost) {

        this.isCurrUserHost = isCurrUserHost;
    }
}

package com.dodopipe.lifelog.rest.controller.modules.photo;

import com.dodopipe.lifelog.core.events.photo.PhotoCaptionUpdateEvent;
import com.dodopipe.lifelog.core.events.photo.PhotoUpdatedEvent;
import com.dodopipe.lifelog.core.services.PhotoCommandService;
import com.dodopipe.lifelog.rest.controller.PhotoCommandController;
import com.dodopipe.lifelog.rest.controller.utils.TestUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Henry Yan
 */
public class UpdatePhotoCaptionTest {
    MockMvc mockMvc;

    @InjectMocks
    PhotoCommandController controller;

    @Mock
    PhotoCommandService photoCommandService;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                                  .build();

    }

    @Test
    public void updatePhotoCaptionCorrectly() throws Exception {

        Long photoId = 3000l;

        String caption = "Photos from our fantastic wedding";

        when(photoCommandService.updatePhotoCaption(any(PhotoCaptionUpdateEvent.class))).thenReturn(new PhotoUpdatedEvent(true));

        mockMvc.perform(post("/photos/{photo_id}/caption",
                             photoId).param("caption", caption)
                                .contentType(TestUtil.APPLICATION_JSON_UTF8))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.success").value(true));
    }
}



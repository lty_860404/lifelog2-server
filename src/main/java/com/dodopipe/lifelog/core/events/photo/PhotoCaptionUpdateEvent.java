package com.dodopipe.lifelog.core.events.photo;

import com.dodopipe.lifelog.core.events.UpdateEvent;

/**
 * @author Henry Yan
 */
public class PhotoCaptionUpdateEvent
        extends UpdateEvent {

    private Long photoId;
    private String caption;

    public PhotoCaptionUpdateEvent(Long photoId,
                                   String caption) {

        this.photoId = photoId;
        this.caption = caption;
    }

    public Long getPhotoId() {

        return photoId;
    }

    public String getCaption() {

        return caption;
    }
}




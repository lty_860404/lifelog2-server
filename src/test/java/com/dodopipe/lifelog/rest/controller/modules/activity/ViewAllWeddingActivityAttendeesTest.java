package com.dodopipe.lifelog.rest.controller.modules.activity;

import com.dodopipe.lifelog.core.events.activity.ActivityAttendeesViewEvent;
import com.dodopipe.lifelog.core.events.activity.ActivityAttendeesViewedEvent;
import com.dodopipe.lifelog.core.services.ActivityQueryService;
import com.dodopipe.lifelog.rest.controller.ActivityQueryController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;

import static com.dodopipe.lifelog.rest.controller.fixture.RestEventFixture.activityAttendeesViewedEvent;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by henryhome on 7/21/14.
 */
public class ViewAllWeddingActivityAttendeesTest {

    MockMvc mockMvc;

    @InjectMocks
    ActivityQueryController controller;

    @Mock
    ActivityQueryService activityQueryService;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                             .build();

    }

    @Test
    public void viewAllWeddingActivityAttendeesCorrectly()
            throws
            Exception {

        Long activityId = 1000l;

        ActivityAttendeesViewedEvent event = activityAttendeesViewedEvent(5);

        when(activityQueryService.viewWeddingActivityAttendees(any(ActivityAttendeesViewEvent.class))).thenReturn(event);

        mockMvc.perform(get("/activities/wedding/{activity_id}/attendees",
                            activityId).param("p",
                                              "0")
                                       .param("n",
                                              "20"))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$[0].attendeeId").value(1000))
               .andExpect(jsonPath("$[1].attendeeId").value(1001))
               .andExpect(jsonPath("$[2].attendeeId").value(1002))
               .andExpect(jsonPath("$[3].attendeeId").value(1003))
               .andExpect(jsonPath("$[4].attendeeId").value(1004));

    }
}

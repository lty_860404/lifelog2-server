package com.dodopipe.lifelog.core.events.auth;

public class VerificationCodeCreatedEvent {

    public String verificationCode;

    public VerificationCodeCreatedEvent(String verificationCode) {

        this.verificationCode = verificationCode;
    }

    public String getVerificationCode() {

        return verificationCode;
    }
}




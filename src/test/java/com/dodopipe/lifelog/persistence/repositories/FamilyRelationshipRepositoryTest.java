package com.dodopipe.lifelog.persistence.repositories;

import com.dodopipe.lifelog.config.DBUnitDataSourceConfiguration;
import com.dodopipe.lifelog.config.DataSourceConfiguration;
import com.dodopipe.lifelog.config.JPAConfiguration;
import com.dodopipe.lifelog.config.LifelogActiveProfilesResolver;
import com.dodopipe.lifelog.persistence.domain.family.FamilyRelationshipEntity;
import com.dodopipe.lifelog.persistence.repositories.family.FamilyRelationshipRepository;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * tianyin.luo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPAConfiguration.class,
                                 DataSourceConfiguration.class,
                                 DBUnitDataSourceConfiguration.class})
@ActiveProfiles(resolver = LifelogActiveProfilesResolver.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class
                        })
@DatabaseSetup("DB_SETUP.xml")
public class FamilyRelationshipRepositoryTest {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    FamilyRelationshipRepository relationshipRepo;

    @Test
    public void testFamilyMemberRelationship() {

        FamilyRelationshipEntity relationship = relationshipRepo.findOne(1000l);
        assertEquals(Long.valueOf(1000l),
                     relationship.getId());
    }

    @Test
    public void testFindRelationShipByUserIdAndLangCode() {

        List<FamilyRelationshipEntity> relationshipList = relationshipRepo.findAllByUserIdAndLangCode(1000l,
                                                                                                      "ZH-CN");

        for (FamilyRelationshipEntity relationshipEntity : relationshipList) {
            System.out.println("relationship => " + relationshipEntity.getDisplayName());
            System.out.println("relationship scope => " + relationshipEntity.getRelationshipScope().toString());
        }

        assertEquals(7,
                     relationshipList.size());
        assertEquals("System",
                     relationshipList.get(0).getRelationshipScope().toString());
        assertEquals("Customized",
                     relationshipList.get(6).getRelationshipScope().toString());
    }
}

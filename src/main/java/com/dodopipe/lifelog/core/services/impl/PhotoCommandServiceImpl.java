package com.dodopipe.lifelog.core.services.impl;

import com.dodopipe.lifelog.core.events.photo.*;
import com.dodopipe.lifelog.core.ports.persistence.PhotoCommandEventHandler;
import com.dodopipe.lifelog.core.services.PhotoCommandService;
import com.dodopipe.lifelog.core.services.exception.*;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Henry Yan
 */
@Transactional
public class PhotoCommandServiceImpl implements PhotoCommandService {

    private PhotoCommandEventHandler handler;

    public PhotoCommandServiceImpl(PhotoCommandEventHandler handler) {

        this.handler = handler;
    }

    @Override
    public PhotoCreatedEvent uploadPhotos(PhotoCreateEvent event)
            throws
            InternalServerErrorException {

        return handler.uploadPhotos(event);
    }

    @Override
    public PhotoUpdatedEvent updatePhotoCaption(PhotoCaptionUpdateEvent event)
            throws
            PhotoNotFoundException {

        return handler.updatePhotoCaption(event);
    }

    @Override
    public PhotoUpdatedEvent updatePhotoComment(PhotoCommentUpdateEvent event)
            throws
            PhotoNotFoundException {

        return handler.updatePhotoComment(event);
    }

    @Override
    public PhotoUpdatedEvent addPhotoLike(PhotoLikeAddEvent event)
            throws
            PhotoNotFoundException,
            UserNotFoundByUsernameException,
            InvalidPhotoLikeException {

        return handler.addPhotoLike(event);
    }

    @Override
    public PhotoUpdatedEvent removePhotoLike(PhotoLikeRemoveEvent event)
            throws
            PhotoLikeNotFoundException {

        return handler.removePhotoLike(event);
    }

    @Override
    public PhotoUpdatedEvent bindActivities(BoundActivitiesChangeEvent event)
            throws
            PhotoNotFoundException {

        return handler.bindActivities(event);
    }

    @Override
    public PhotoUpdatedEvent detachActivities(BoundActivitiesChangeEvent event)
            throws
            PhotoNotFoundException {

        return handler.detachActivities(event);
    }

    @Override
    public PhotoDeletedEvent deletePhoto(PhotoDeleteEvent event)
            throws
            PhotoNotFoundException {

        return handler.deletePhoto(event);
    }
}





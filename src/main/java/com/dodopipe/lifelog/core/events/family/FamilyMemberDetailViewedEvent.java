package com.dodopipe.lifelog.core.events.family;

import com.dodopipe.lifelog.core.domain.FamilyMemberDetail;

/**
 * @author tianyin.luo
 */
public class FamilyMemberDetailViewedEvent {

    private final FamilyMemberDetail familyMemberDetail;

    public FamilyMemberDetailViewedEvent(FamilyMemberDetail familyMemberDetail) {

        this.familyMemberDetail = familyMemberDetail;
    }

    public FamilyMemberDetail getFamilyMemberDetail() {

        return this.familyMemberDetail;
    }

}

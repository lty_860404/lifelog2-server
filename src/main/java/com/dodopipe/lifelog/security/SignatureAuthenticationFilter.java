package com.dodopipe.lifelog.security;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.util.Assert;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class SignatureAuthenticationFilter
        extends GenericFilterBean {

    private final static String HEADER_AUTHORIZATION = "Authorization";
    private final static String HEADER_CONTENT_MD5 = "Content-MD5";
    private final static String HEADER_DATE = "Date";
    private final static String AUTHORIZATION_PREFIX = "DOP ";
    private final static String PARAM_NAME_ACCESS_KEY_ID = "__dopaccesskey";
    private final static String PARAM_NAME_SIGNATURE = "__signature";
    private final static String PARAM_NAME_EXPIRE = "__expire";

    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz",
                                                                             Locale.US);
    private static final TimeZone GMT_ZONE = TimeZone.getTimeZone("GMT");
    private static long REQUEST_EXPIRE_PERIOD = 15 * 60 * 1000l;

    private UserDetailsService userService;
    private UserCache userCache;
    private AuthenticationEntryPoint entryPoint;

    static {
        DATE_FORMAT.setTimeZone(GMT_ZONE);
    }

    public SignatureAuthenticationFilter(UserDetailsService service,
                                         UserCache userCache,
                                         AuthenticationEntryPoint entryPoint) {

        this.userService = service;
        this.userCache = userCache;
        this.entryPoint = entryPoint;
    }

    @Override
    public void afterPropertiesSet() {

        Assert.notNull(userService,
                       "A UserDetailService is required");
        Assert.notNull(userCache,
                       "A UserCache is required");
        Assert.notNull(entryPoint,
                       "A AuthenticationEntryPoint is required");
    }

    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain chain)
            throws
            IOException,
            ServletException {

        if (!(request instanceof HttpServletRequest)) {
            chain.doFilter(request,
                           response);
            return;
        }

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;


        SignData signData = getSignData(httpRequest);

        if (signData == null) {
            chain.doFilter(request,
                           response);
            return;
        }

        if (isExpired(signData.getExpireTime())) {
            entryPoint.commence(httpRequest,
                                httpResponse,
                                new RequestExpiredException("Request is expired"));
            return;
        }

        UserDetails user;
        try {
            user = getCurrentUser(signData.getAccessKey());
        } catch (UsernameNotFoundException nf) {
            entryPoint.commence(httpRequest,
                                httpResponse,
                                new AccountNotFoundException("Account is not found"));
            return;
        }

        if (!user.isAccountNonExpired()) {
            entryPoint.commence(httpRequest,
                                httpResponse,
                                new AccountExpiredException("User account is expired"));
            return;
        }

        if (!user.isCredentialsNonExpired()) {
            entryPoint.commence(httpRequest,
                                httpResponse,
                                new CredentialsExpiredException("User credential is expired"));
            return;
        }


        if (!validateSignature(httpRequest,
                               signData,
                               user)) {
            entryPoint.commence(httpRequest,
                                httpResponse,
                                new BadCredentialsException("Not a valid signature"));
            return;
        }
        SecurityContextHolder.getContext()
                             .setAuthentication(createSuccessfulAuthentication(user));
        chain.doFilter(request,
                       response);
    }

    private boolean validateSignature(HttpServletRequest httpRequest,
                                      SignData signData,
                                      UserDetails user)
            throws
            IOException,
            ServletException {

        try {
            if (HmacSignatureVerifier.validate(extractSignedString(httpRequest),
                                               signData.getSignature(),
                                               user.getPassword())) {

                return true;
            }
        } catch (Exception e) {
            throw new AuthenticationServiceException("Service internal error",
                                                     e);
        }
        return false;
    }

    private SignData getSignData(HttpServletRequest httpRequest)
            throws
            IOException,
            ServletException {

        String auth = httpRequest.getHeader(HEADER_AUTHORIZATION);
        SignData signData = extractSignData(auth);
        long expireTime = httpRequest.getDateHeader(HEADER_DATE) + REQUEST_EXPIRE_PERIOD;
        if (signData == null) {
            signData = extractSignData(httpRequest);
            if (signData == null) {
                return null;
            }
            try {
                expireTime = Long.parseLong(httpRequest.getParameter(PARAM_NAME_EXPIRE));
            } catch (NumberFormatException e) {
                expireTime = 0;
            }
        }
        signData.setExpireTime(expireTime);
        return signData;
    }

    static class SignData {

        private final String accessKey;
        private final String signature;
        private long expireTime;

        SignData(final String accessKey,
                 final String signature) {

            this.accessKey = accessKey;
            this.signature = signature;
        }

        String getAccessKey() {

            return this.accessKey;
        }

        String getSignature() {

            return this.signature;
        }

        public long getExpireTime() {

            return expireTime;
        }

        public void setExpireTime(long expireTime) {

            this.expireTime = expireTime;
        }
    }

    private boolean isExpired(long expireTime) {

        long now = new Date().getTime();
        return expireTime <= now;
    }

    private Authentication createSuccessfulAuthentication(UserDetails user) {

        UsernamePasswordAuthenticationToken authRequest;
        authRequest = new UsernamePasswordAuthenticationToken(user,
                                                              user.getPassword(),
                                                              user.getAuthorities());

        return authRequest;
    }

    private UserDetails getCurrentUser(String accessKey)
            throws
            AuthenticationServiceException {

        UserDetails user = userCache.getUserFromCache(accessKey);
        if (user == null) {
            user = userService.loadUserByUsername(accessKey);
        }
        if (user == null) {
            throw new AuthenticationServiceException("UserService returned null, which is an interface contract violation");
        }
        userCache.putUserInCache(user);
        return user;
    }

    private SignData extractSignData(String authorization) {

        if (authorization == null || !authorization.startsWith(AUTHORIZATION_PREFIX)) {
            return null;
        }
        authorization = authorization.substring(AUTHORIZATION_PREFIX.length());
        String[] signature = authorization.split(":");
        if (signature.length != 2) {
            return null;
        }
        return new SignData(signature[0],
                            signature[1]);
    }

    private SignData extractSignData(HttpServletRequest request) {

        String accessKey = request.getParameter(PARAM_NAME_ACCESS_KEY_ID);
        String signature = request.getParameter(PARAM_NAME_SIGNATURE);
        if (accessKey == null || signature == null) {
            return null;
        }
        return new SignData(accessKey,
                            signature);
    }

    private String extractSignedString(HttpServletRequest request) {

        List<String> parts = new ArrayList<>();
        parts.add(null2Empty(request.getMethod()
                                    .toUpperCase()));
        parts.add(null2Empty(request.getHeader(HEADER_CONTENT_MD5)));
        parts.add(removeParametersFromContentType(null2Empty(request.getContentType())));
        long date = request.getDateHeader(HEADER_DATE);
        parts.add(DATE_FORMAT.format(new Date(date)));
        parts.add(null2Empty(request.getServletPath()));

        return String.join("\n",
                           parts);

    }

    private String removeParametersFromContentType(String contentType) {

        String[] contentTypes = contentType.split(";");
        if (contentTypes.length == 0) {
            return "";
        }
        return contentTypes[0].trim();
    }

    private String null2Empty(String str) {

        return str == null ? "" : str;
    }

}

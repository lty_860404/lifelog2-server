package com.dodopipe.lifelog.persistence.repositories;

import com.dodopipe.lifelog.config.DBUnitDataSourceConfiguration;
import com.dodopipe.lifelog.config.DataSourceConfiguration;
import com.dodopipe.lifelog.config.JPAConfiguration;
import com.dodopipe.lifelog.config.LifelogActiveProfilesResolver;
import com.dodopipe.lifelog.persistence.domain.family.FamilyEntity;
import com.dodopipe.lifelog.persistence.domain.family.FamilyMemberEntity;
import com.dodopipe.lifelog.persistence.domain.user.UserEntity;
import com.dodopipe.lifelog.persistence.repositories.family.FamilyRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserRepository;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPAConfiguration.class,
                                 DataSourceConfiguration.class,
                                 DBUnitDataSourceConfiguration.class})
@ActiveProfiles(resolver = LifelogActiveProfilesResolver.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
@DatabaseSetup("DB_SETUP.xml")
public class FamilyRepositoryTest {

    private static Logger logger = LoggerFactory.getLogger(FamilyRepositoryTest.class);

    @Autowired
    FamilyRepository familyRepo;
    @Autowired
    UserRepository userRepo;

    @Test
    public void testCreateFamily() {

        FamilyEntity familyEntity = new FamilyEntity();
        familyEntity.setName("TestFamily");
        familyRepo.save(familyEntity);

        UserEntity userEntity = userRepo.findOne(1000l);
        familyEntity.addFamilyMember(userEntity,
                                     "SELF");
        familyRepo.saveAndFlush(familyEntity);
        FamilyEntity family = familyRepo.getOne(familyEntity.getId());

        assertEquals(1,
                     family.getFamilyMembers()
                              .size());
        assertEquals("TestFamily",
                     family.getName());
        assertNotNull(family.getId());

    }

    @Test
    public void testReadFamily() {

        FamilyEntity familyEntity = familyRepo.findOne(1000l);
        assertEquals("Laura's family",
                     familyEntity.getName());
        Collection<FamilyMemberEntity> members = familyEntity.getFamilyMembers();
        logFamilyMember(members);
        assertEquals(2,
                     familyEntity.getFamilyMembers()
                           .size());
    }

    @Test
    public void testAddFamilyMember() {

        FamilyEntity family = familyRepo.getOne(1000L);

        UserEntity user = new UserEntity();
        user.setUsername("test user");
        user.setFamily(family);
        userRepo.save(user);

        family.addFamilyMember(user,
                               "friends");
        familyRepo.saveAndFlush(family);

        family = familyRepo.getOne(1000L);
        logFamilyMember(family.getFamilyMembers());
        assertEquals(3,
                     family.getFamilyMembers()
                           .size());
    }

    private void logFamilyMember(Collection<FamilyMemberEntity> members) {

        if (logger.isDebugEnabled()) {
            for (FamilyMemberEntity member : members) {
                logger.debug("\nname: {}\n" +
                                     "relationship:{}",
                             member.getMember()
                                   .getUsername(),
                             member.getRelationship());

            }
        }
    }
}




package com.dodopipe.lifelog.rest.controller.modules.photo;

import com.dodopipe.lifelog.core.events.photo.PhotosViewEvent;
import com.dodopipe.lifelog.core.services.PhotoQueryService;
import com.dodopipe.lifelog.rest.controller.PhotoQueryController;
import com.dodopipe.lifelog.rest.controller.fixture.UserAsPrincipal;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;

import static com.dodopipe.lifelog.rest.controller.fixture.RestDataFixture.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class ViewAllPhotosTest {

    MockMvc mockMvc;

    @InjectMocks
    PhotoQueryController controller;

    @Mock
    PhotoQueryService photoQueryService;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                                  .build();

    }

    @Test
    public void readAllPhotosUsesHttpOk()
            throws
            Exception {

        when(photoQueryService.viewAllPhotos(any(PhotosViewEvent.class)))
                .thenReturn(generatePhotos());
        this.mockMvc.perform(
                get("/photos").param("p",
                                     "5")
                              .param("n",
                                     "40")
                              .accept(MediaType.APPLICATION_JSON)
                              .principal(new UserAsPrincipal("1000")))
                    .andExpect(status().isOk());

    }

    @Test
    public void readAllPhotosCorrectly()
            throws
            Exception {

        when(photoQueryService.viewAllPhotos(any(PhotosViewEvent.class)))
                .thenReturn(generatePhotos());

        this.mockMvc.perform(
                get("/photos").param("p",
                                     "5")
                              .param("n",
                                     "40")
                              .accept(
                                      MediaType.APPLICATION_JSON)
                              .principal(new UserAsPrincipal("1000")))
                    .andExpect(jsonPath("$[0].photo.id").value(1000))
                    .andExpect(jsonPath("$[1].photo.id").value(1001))
                    .andDo(print());
    }

    @Test
    public void readAllPhotosWithEmptyResponse()
            throws
            Exception {

        when(photoQueryService.viewAllPhotos(any(PhotosViewEvent.class)))
                .thenReturn(generateEmptyPhotos());

        this.mockMvc.perform(
                get("/photos").param("p",
                                     "5")
                              .param("n",
                                     "40")
                              .accept(
                                      MediaType.APPLICATION_JSON)
                              .principal(new UserAsPrincipal("1000")))
                    .andExpect(status().isOk())
                    .andDo(print());
    }

    @Test
    public void viewPhotosByActivityCorrectly()
            throws
            Exception {

    }

}




package com.dodopipe.lifelog.core.events.family;

/**
 * Created by henryhome on 7/29/14.
 */
public class ExternalInvitationUpdateEvent {

    private String phone;

    public ExternalInvitationUpdateEvent(String phone) {

        this.phone = phone;
    }

    public String getPhone() {

        return phone;
    }
}

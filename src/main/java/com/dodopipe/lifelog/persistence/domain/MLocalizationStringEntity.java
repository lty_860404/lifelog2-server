package com.dodopipe.lifelog.persistence.domain;

import javax.persistence.*;

@Entity(name = "M_LOCALIZATION_STRING")
public class MLocalizationStringEntity {

    @Id
    @Column(name = "ID",
            columnDefinition = "BIGINT(25) NOT NULL AUTO_INCREMENT")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "TYPE_CODE",
            columnDefinition = "INT(10)")
    private int typeCode;

    @Column(name = "LANG_CODE",
            columnDefinition = "VARCHAR(20)")
    private String languageCode;

    @Column(name = "PROP_NAME",
            columnDefinition = "VARCHAR(80)")
    private String propName;

    @Column(name = "PROP_VALUE",
            columnDefinition = "VARCHAR(80)")
    private String propValue;


    public enum TypeCode {

        FAMILY_NAME,
        RELATIONSHIP
    }

    public enum StringKey {

        FATHER("Father"),
        MOTHER("Mother"),
        WIFE("Wife"),
        HUSBAND("Husband"),
        SELF("Self"),
        FRIEND("Friend"),
        DEFAULT_FAMILY_NAME("Default family name");

        private String name;

        private StringKey(String name) {

            this.name = name;
        }

        @Override
        public String toString() {

            return name;
        }
    }

    public Integer getId() {

        return id;
    }

    public void setId(Integer id) {

        this.id = id;
    }

    public int getTypeCode() {

        return typeCode;
    }

    public void setTypeCode(int typeCode) {

        this.typeCode = typeCode;
    }

    public String getLanguageCode() {

        return languageCode;
    }

    public void setLanguageCode(String languageCode) {

        this.languageCode = languageCode;
    }

    public String getPropName() {

        return propName;
    }

    public void setPropName(String keyName) {

        this.propName = keyName;
    }

    public String getPropValue() {

        return propValue;
    }

    public void setPropValue(String propValue) {

        this.propValue = propValue;
    }
}





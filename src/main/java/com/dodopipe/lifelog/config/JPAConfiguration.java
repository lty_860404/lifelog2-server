package com.dodopipe.lifelog.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.instrument.classloading.LoadTimeWeaver;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaDialect;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableJpaRepositories(basePackages = "com.dodopipe.lifelog.persistence.repositories")
@EnableTransactionManagement
public abstract class JPAConfiguration {

    @Autowired
    DataSource dataSource;
    @Autowired
    LoadTimeWeaver loadTimeWeaver;

    @Bean
    public EntityManagerFactory entityManagerFactory()
            throws
            SQLException {

        EclipseLinkJpaVendorAdapter adaptor = new EclipseLinkJpaVendorAdapter();
        adaptor.setGenerateDdl(true);
        Map<String, String> property = new HashMap<String, String>();
        property.put("eclipselink.weaving",
                     "true");
        property.put("eclipselink.logging.level",
                     "FINE");

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(adaptor);
        factory.setLoadTimeWeaver(loadTimeWeaver);

        factory.setJpaPropertyMap(property);
        factory.setPackagesToScan("com.dodopipe.lifelog.persistence.domain");
        factory.setDataSource(dataSource);
        factory.afterPropertiesSet();
        return factory.getObject();
    }

    @Bean
    public EntityManager entityManager(EntityManagerFactory entityManagerFactory) {

        return entityManagerFactory.createEntityManager();
    }

    @Bean
    public PlatformTransactionManager transactionManager()
            throws
            SQLException {

        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactory());
        return txManager;
    }


    @Bean
    public EclipseLinkJpaDialect eclipseLinkJpaDialect() {

        return new EclipseLinkJpaDialect();
    }
}

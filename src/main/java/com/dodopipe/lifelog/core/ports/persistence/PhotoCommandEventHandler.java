package com.dodopipe.lifelog.core.ports.persistence;

import com.dodopipe.lifelog.core.events.photo.*;
import com.dodopipe.lifelog.core.services.exception.*;

/**
 * @author Henry Yan
 */
public interface PhotoCommandEventHandler {

    public PhotoCreatedEvent uploadPhotos(PhotoCreateEvent event)
            throws
            InternalServerErrorException;

    public PhotoUpdatedEvent updatePhotoCaption(PhotoCaptionUpdateEvent event)
            throws
            PhotoNotFoundException;

    public PhotoUpdatedEvent updatePhotoComment(PhotoCommentUpdateEvent event)
            throws
            PhotoNotFoundException;

    public PhotoUpdatedEvent addPhotoLike(PhotoLikeAddEvent event)
            throws
            PhotoNotFoundException,
            UserNotFoundByUsernameException,
            InvalidPhotoLikeException;

    public PhotoUpdatedEvent removePhotoLike(PhotoLikeRemoveEvent event)
            throws
            PhotoLikeNotFoundException;

    public PhotoUpdatedEvent bindActivities(BoundActivitiesChangeEvent event)
            throws
            PhotoNotFoundException;

    public PhotoUpdatedEvent detachActivities(BoundActivitiesChangeEvent event)
            throws
            PhotoNotFoundException;

    public PhotoDeletedEvent deletePhoto(PhotoDeleteEvent event)
            throws
            PhotoNotFoundException;
}

package com.dodopipe.lifelog.core.events.activity;

import com.dodopipe.lifelog.core.domain.Activity;
import com.dodopipe.lifelog.core.events.ViewedEvent;

/**
 * @author Henry Yan
 */
public class ActivityViewedEvent extends ViewedEvent {

    private Activity activity;

    public ActivityViewedEvent(Activity activity) {

        this.activity = activity;
    }

    public Activity getActivity() {

        return activity;
    }
}




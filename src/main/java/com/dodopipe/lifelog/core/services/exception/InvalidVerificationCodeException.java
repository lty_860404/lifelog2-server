package com.dodopipe.lifelog.core.services.exception;

/**
 * Created by henryhome on 9/1/14.
 */
public class InvalidVerificationCodeException extends ServiceException {

    public InvalidVerificationCodeException() {

    }

    public InvalidVerificationCodeException(String message) {

        super(message);
    }

    public InvalidVerificationCodeException(String message,
                                            Throwable cause) {

        super(message,
              cause);
    }

    public InvalidVerificationCodeException(Throwable cause) {

        super(cause);
    }

    public InvalidVerificationCodeException(String message,
                                            Throwable cause,
                                            boolean enableSuppression,
                                            boolean writableStackTrace) {

        super(message,
              cause,
              enableSuppression,
              writableStackTrace);
    }
}




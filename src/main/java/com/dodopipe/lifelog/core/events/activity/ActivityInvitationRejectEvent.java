package com.dodopipe.lifelog.core.events.activity;

/**
 * Created by henryhome on 7/22/14.
 */
public class ActivityInvitationRejectEvent {
    private Long activityId;
    private Long attendeeId;

    public ActivityInvitationRejectEvent(Long activityId, Long attendeeId) {
        this.activityId = activityId;
        this.attendeeId = attendeeId;
    }

    public Long getActivityId() {

        return activityId;
    }

    public Long getAttendeeId() {

        return attendeeId;
    }
}

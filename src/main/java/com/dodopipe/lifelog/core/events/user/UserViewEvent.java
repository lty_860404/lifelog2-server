package com.dodopipe.lifelog.core.events.user;

import com.dodopipe.lifelog.core.events.ViewEvent;

/**
 * Created by henryhome on 8/6/14.
 */
public class UserViewEvent extends ViewEvent {

    private Long userId;
    private String username;
    private String phone;

    public UserViewEvent(Long userId,
                         String username,
                         String phone) {

        this.userId = userId;
        this.username = username;
        this.phone = phone;
    }

    public Long getUserId() {

        return userId;
    }

    public String getUsername() {

        return username;
    }

    public String getPhone() {

        return phone;
    }
}

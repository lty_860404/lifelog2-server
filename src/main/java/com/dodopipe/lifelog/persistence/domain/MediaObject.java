package com.dodopipe.lifelog.persistence.domain;

import java.io.InputStream;

public class MediaObject {

    private String key;

    private transient InputStream in = null;

    private String contentType = null;

    private long length = -1;

    public MediaObject() {

    }

    /**
     * @param in
     * @param contentType
     * @param length
     */
    public MediaObject(InputStream in,
                       String contentType,
                       long length) {

        this.in = in;
        this.contentType = contentType;
        this.length = length;
    }

    public InputStream getInputStream() {

        return in;
    }

    public void setInputStream(InputStream in) {

        this.in = in;
    }

    public String getContentType() {

        return contentType;
    }

    public void setContentType(String contentType) {

        this.contentType = contentType;
    }

    public long getLength() {

        return length;
    }

    public void setLength(long length) {

        this.length = length;
    }

    public String getKey() {

        return key;
    }

    public void setKey(String key) {

        this.key = key;
    }


}

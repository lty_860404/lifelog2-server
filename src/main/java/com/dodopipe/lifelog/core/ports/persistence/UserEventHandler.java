package com.dodopipe.lifelog.core.ports.persistence;

import com.dodopipe.lifelog.core.domain.User;
import com.dodopipe.lifelog.core.events.auth.UserSignOutEvent;
import com.dodopipe.lifelog.core.events.auth.UserSignedOutEvent;
import com.dodopipe.lifelog.core.events.user.*;
import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByIdException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByPhoneException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByUsernameException;
import com.dodopipe.lifelog.security.AccountNotFoundException;

/**
 * @author Henry Yan
 */
public interface UserEventHandler {

    public User getUserById(Long userId);

    public User getUserByUsername(String username);

    public User getUserByPhone(String phone);

    public UserSignedOutEvent signOut(UserSignOutEvent event)
            throws
            AccountNotFoundException;

    public PasswordUpdatedEvent changePassword(PasswordUpdateEvent event)
            throws
            AccountNotFoundException,
            InternalServerErrorException;

    public UserCreatedEvent createUser(UserCreateEvent event);

    public UserViewedEvent viewUser(UserViewEvent event)
            throws
            UserNotFoundByIdException,
            UserNotFoundByUsernameException,
            UserNotFoundByPhoneException;

    public UserUpdatedEvent updateUser(UserUpdateEvent event)
            throws
            UserNotFoundByIdException,
            IllegalArgumentException;

    public UserDeletedEvent deleteUser(UserDeleteEvent event)
            throws
            UserNotFoundByIdException;

    public UserAvatarChangedEvent changeAvatar(UserAvatarChangeEvent event)
            throws
            UserNotFoundByIdException;
}

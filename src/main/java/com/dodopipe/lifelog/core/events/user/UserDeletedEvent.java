package com.dodopipe.lifelog.core.events.user;

import com.dodopipe.lifelog.core.events.DeletedEvent;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by henryhome on 8/6/14.
 */
public class UserDeletedEvent extends DeletedEvent {
    public boolean success;

    public UserDeletedEvent(boolean success) {

        this.success = success;
    }

    public boolean isSuccess() {

        return success;
    }

    public Map<String, Boolean> toMap() {

        Map<String, Boolean> map = new HashMap<>();
        map.put("success",
                isSuccess());
        return map;
    }
}

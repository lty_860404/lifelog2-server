package com.dodopipe.lifelog.core.services.exception;

/**
 * @author yongwei
 */
public class FamilyInvitationNotFoundException
        extends ServiceException {

    public FamilyInvitationNotFoundException(String message) {

        super(message);
    }

    public FamilyInvitationNotFoundException(String message,
                                             Throwable cause) {

        super(message,
              cause);
    }

    public FamilyInvitationNotFoundException(Throwable cause) {

        super(cause);
    }

    public FamilyInvitationNotFoundException(String message,
                                             Throwable cause,
                                             boolean enableSuppression,
                                             boolean writableStackTrace) {
        super(message,
              cause,
              enableSuppression,
              writableStackTrace);
    }
}

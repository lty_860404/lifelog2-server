package com.dodopipe.lifelog.core.events.family;

/**
 * @author tianyin.luo
 */
public class FamilyMemberUpdateEvent {

    private final Long inviterId;
    private final String inviteeUsername;
    private final String relationship;
    private final String langCode;

    public FamilyMemberUpdateEvent(Long inviterId,
                                   String inviteeUsername,
                                   String relationship, String langCode) {

        this.inviterId = inviterId;
        this.inviteeUsername = inviteeUsername;
        this.relationship = relationship;
        this.langCode = langCode;
    }

    public Long getInviterId() {

        return this.inviterId;
    }

    public String getInviteeUsername() {

        return this.inviteeUsername;
    }

    public String getRelationship() {

        return this.relationship;
    }

    public String getLangCode() {

        return langCode;
    }
}

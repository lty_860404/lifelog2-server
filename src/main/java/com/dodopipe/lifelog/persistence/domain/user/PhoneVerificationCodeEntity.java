package com.dodopipe.lifelog.persistence.domain.user;

import org.eclipse.persistence.annotations.Index;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

@Entity(name = "PHONE_VERIFICATION_CODE")
public class PhoneVerificationCodeEntity
        implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID",
            columnDefinition = "BIGINT(25) NOT NULL AUTO_INCREMENT")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Index(unique = true,
           name = "PHONE_VERIFICATION_CODE_PHONE")
    @Column(name = "PHONE",
            columnDefinition = "VARCHAR(80)")
    private String phone;

    @Column(name = "VERIFICATION_CODE",
            columnDefinition = "VARCHAR(80)")
    private String verificationCode;

    @Column(name = "CREATED_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;

    @Column(name = "MESSAGE_STATUS",
            columnDefinition = "VARCHAR(80)")
    private String messageStatus;

    @Column(name = "MESSAGE_ERROR_CODE",
            columnDefinition = "INT(10)")
    private Integer messageErrorCode;

    public PhoneVerificationCodeEntity() {

    }

    public PhoneVerificationCodeEntity(String phone,
                                       String verificationCode,
                                       Date createdTime,
                                       String messageStatus,
                                       Integer messageErrorCode) {

        this.phone = phone;
        this.verificationCode = verificationCode;
        this.createdTime = createdTime;
        this.messageStatus = messageStatus;
        this.messageErrorCode = messageErrorCode;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public String getPhone() {

        return phone;
    }

    public void setPhone(String phone) {

        this.phone = phone;
    }

    public String getVerificationCode() {

        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {

        this.verificationCode = verificationCode;
    }

    public Date getCreatedTime() {

        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {

        this.createdTime = createdTime;
    }

    public String getMessageStatus() {

        return messageStatus;
    }

    public void setMessageStatus(String messageStatus) {

        this.messageStatus = messageStatus;
    }

    public Integer getMessageErrorCode() {

        return messageErrorCode;
    }

    public void setMessageErrorCode(Integer messageErrorCode) {

        this.messageErrorCode = messageErrorCode;
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PhoneVerificationCodeEntity other = (PhoneVerificationCodeEntity) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

    public static PhoneVerificationCodeEntity getInstance(String phone,
                                                          int verificationCodeLength) {

        return new PhoneVerificationCodeEntity(phone,
                                               generateVerificationCode(verificationCodeLength),
                                               GregorianCalendar.getInstance()
                                                                .getTime(),
                                               null,
                                               null);

    }

    private static String generateVerificationCode(int verificationCodeLength) {

        StringBuilder verificationCode = new StringBuilder();

        Random rand = new Random();

        for (int i = 0; i < verificationCodeLength; i++) {
            verificationCode.append(rand.nextInt(10));

        }

        return verificationCode.toString();
    }
}

package com.dodopipe.lifelog.persistence.domain.user;

import org.apache.commons.codec.binary.Base64;
import org.eclipse.persistence.annotations.Index;

import javax.crypto.KeyGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Entity(name = "USER_ACCOUNT")
public class UserAccountEntity
        implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final String ALGORITHM = "HmacSHA1";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID",
            columnDefinition = "BIGINT(25) NOT NULL AUTO_INCREMENT")
    private Long id;

    @Index(unique = true,
           name = "ACCESS_ID")
    @Column(name = "ACCESS_ID",
            columnDefinition = "VARCHAR(80)",
            nullable = false)
    private String accessId;

    @Column(name = "SECRET_KEY",
            columnDefinition = "VARCHAR(80)",
            nullable = false)
    private String secretKey;

    @Column(name = "USER_ID",
            columnDefinition = "BIGINT(25)",
            nullable = false)
    private Long userId;

    @Column(name = "USERNAME",
            columnDefinition = "VARCHAR(80)")
    private String username;

    @Column(name = "PHONE",
            columnDefinition = "VARCHAR(80)")
    private String phone;

    @Column(name = "PASSWORD",
            columnDefinition = "VARCHAR(80)")
    private String password;

    @Column(name = "ACCOUNT_EXPIRED",
            columnDefinition = "BOOLEAN")
    private Boolean accountExpired;

    @Column(name = "CREDENTIALS_EXPIRED",
            columnDefinition = "BOOLEAN")
    private Boolean credentialsExpired;

    public static long getSerialVersionUID() {

        return serialVersionUID;
    }

    public static String getAlgorithm() {

        return ALGORITHM;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public String getAccessId() {

        return accessId;
    }

    public void setAccessId(String accessId) {

        this.accessId = accessId;
    }

    public String getSecretKey() {

        return secretKey;
    }

    public void setSecretKey(String secretKey) {

        this.secretKey = secretKey;
    }

    public Long getUserId() {

        return userId;
    }

    public void setUserId(Long userId) {

        this.userId = userId;
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {

        this.username = username;
    }

    public String getPhone() {

        return phone;
    }

    public void setPhone(String phone) {

        this.phone = phone;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    public Boolean isAccountExpired() {

        return accountExpired;
    }

    public void setAccountExpired(Boolean accountExpired) {

        this.accountExpired = accountExpired;
    }

    public Boolean isCredentialsExpired() {

        return credentialsExpired;
    }

    public void setCredentialsExpired(Boolean credentialsExpired) {

        this.credentialsExpired = credentialsExpired;
    }

    public static String generateBase64EncodedRandomProperty(int length)
            throws
            NoSuchAlgorithmException {

        KeyGenerator keyGen = KeyGenerator.getInstance(ALGORITHM);
        keyGen.init(length);
        byte[] property = keyGen.generateKey()
                                .getEncoded();
        return Base64.encodeBase64URLSafeString(property);
    }

    public static String MD5Encode(byte[] password)
            throws
            NoSuchAlgorithmException {

        StringBuilder sb = new StringBuilder();

        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] encoded = md.digest(password);
        for (byte b : encoded) {
            sb.append(Integer.toHexString(0xFF & b));
        }

        return sb.toString();
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        UserAccountEntity other = (UserAccountEntity) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }
}




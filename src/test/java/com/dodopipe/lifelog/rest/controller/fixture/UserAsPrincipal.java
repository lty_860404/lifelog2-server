package com.dodopipe.lifelog.rest.controller.fixture;

import java.security.Principal;

public class UserAsPrincipal
        implements Principal {

    private final String name;

    public UserAsPrincipal(final String name) {

        this.name = name;
    }

    @Override
    public String getName() {

        return name;
    }

}

package com.dodopipe.lifelog.rest.controller;

import com.dodopipe.lifelog.rest.error.IllegalPrincipalException;

import java.security.Principal;

/**
 * @author tianyin.luo
 */
public class PrincipalUtil {

    public static Long getUIDByPrincipal(Principal principal)
            throws
            IllegalPrincipalException {

        Long uid;
        try {
            if (principal == null) {
                throw new IllegalPrincipalException("principal is null");
            }
            uid = Long.parseLong(principal.getName());
        } catch (NumberFormatException nfe) {
            throw new IllegalPrincipalException(nfe);
        }
        return uid;
    }

}

package com.dodopipe.lifelog.rest.controller.modules.photo;

import com.dodopipe.lifelog.core.events.photo.BoundActivitiesChangeEvent;
import com.dodopipe.lifelog.core.events.photo.PhotoUpdatedEvent;
import com.dodopipe.lifelog.core.services.PhotoCommandService;
import com.dodopipe.lifelog.rest.controller.PhotoCommandController;
import com.dodopipe.lifelog.rest.controller.utils.TestUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static com.dodopipe.lifelog.rest.controller.fixture.RestDataFixture.generateActivityIds;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Henry Yan
 */
public class DetachActivitiesTest {

    MockMvc mockMvc;

    @InjectMocks
    PhotoCommandController controller;

    @Mock
    PhotoCommandService photoCommandService;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                                  .build();

    }

    @Test
    public void DetachActivitiesCorrectly()
            throws
            Exception {

        Long photoId = 80l;
        Long[] activityIds = generateActivityIds(5);

        when(photoCommandService.detachActivities(any(BoundActivitiesChangeEvent.class))).thenReturn(new PhotoUpdatedEvent(true));

        MockHttpServletRequestBuilder builder = post("/photos/{photo_id}/detach",
                                                     photoId);

        for (Long activityId : activityIds) {
            builder.param("activity_ids", String.valueOf(activityId));
        }

        builder.contentType(TestUtil.APPLICATION_JSON_UTF8);

        mockMvc.perform(builder)
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.success").value(true));
    }
}




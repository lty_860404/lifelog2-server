package com.dodopipe.lifelog.core.domain;

import static com.dodopipe.lifelog.core.domain.FamilyRelationshipStatus.FamilyMemberStatusType.*;

/**
 * @author tianyin.luo
 */
public class FamilyRelationshipStatus {

    public enum FamilyMemberStatusType {
        VALID,
        BE_BLOCKED,
        REMOVED;

        public boolean isBlocked(){
            switch (this){
                case VALID:
                    return false;
                case BE_BLOCKED:
                    return true;
                case REMOVED:
                    return true;
                default:
                    throw new IllegalStateException("Illegal state for FamilyMemberStatusType");
            }
        }

        public boolean isRemoved(){
            switch (this){
                case VALID:
                    return false;
                case BE_BLOCKED:
                    return false;
                case REMOVED:
                    return true;
                default:
                    throw new IllegalStateException("Illegal state for FamilyMemberStatusType");
            }
        }
    }

    public enum RelationshipStatusType {

        RELATION_VALID(VALID,
                       VALID),
        RELATION_BE_BLOCKED(BE_BLOCKED,
                            REMOVED),
        RELATION_REMOVED(REMOVED,
                         BE_BLOCKED),
        RELATION_REMOVED_BOTH(REMOVED,
                              REMOVED),
        RELATION_NOT_CONFIRMED(null,
                               null);

        private FamilyMemberStatusType ownsideStatusType;
        private FamilyMemberStatusType othersideStatusType;

        RelationshipStatusType(FamilyMemberStatusType ownsideStatusType,
                               FamilyMemberStatusType othersideStatusType) {

            this.ownsideStatusType = ownsideStatusType;
            this.othersideStatusType = othersideStatusType;
        }

        public FamilyMemberStatusType getOwnsideStatusType() {

            return this.ownsideStatusType;
        }

        public FamilyMemberStatusType getOthersideStatusType() {

            return this.othersideStatusType;
        }
    }

    private FamilyMemberStatusType ownsideStatusType;

    public FamilyRelationshipStatus(FamilyMemberStatusType ownsideStatusType) {

        this.ownsideStatusType = ownsideStatusType;
    }

    public RelationshipStatusType updateStatusForRemovingRequest(FamilyMemberStatusType othersideStatusType) {

        switch (this.ownsideStatusType) {
            case VALID:
                return RelationshipStatusType.RELATION_REMOVED;
            case BE_BLOCKED:
                return RelationshipStatusType.RELATION_REMOVED_BOTH;
            default:
                throw new IllegalStateException();
        }
    }

    public RelationshipStatusType updateStatusForInvitingRequest(FamilyMemberStatusType othersideStatusType) {

        switch (this.ownsideStatusType) {
            case VALID:
                throw new IllegalStateException();
            case REMOVED:
                if (othersideStatusType == BE_BLOCKED) {
                    return RelationshipStatusType.RELATION_VALID;
                }
            default:
                return RelationshipStatusType.RELATION_NOT_CONFIRMED;
        }
    }

    public RelationshipStatusType updateStatusForAcceptingRequest(FamilyMemberStatusType othersideStatusType) {

        return RelationshipStatusType.RELATION_VALID;
    }

    public FamilyMemberStatusType getOwnsideStatusType() {

        return ownsideStatusType;
    }

    public void setOwnsideStatusType(FamilyMemberStatusType ownsideStatusType) {

        this.ownsideStatusType = ownsideStatusType;
    }
}

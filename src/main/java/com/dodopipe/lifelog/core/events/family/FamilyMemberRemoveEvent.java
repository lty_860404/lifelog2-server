package com.dodopipe.lifelog.core.events.family;

/**
 * @author tianyin.luo
 */
public class FamilyMemberRemoveEvent {

    private final Long inviterId;

    private final String inviteeUsername;

    public FamilyMemberRemoveEvent(Long inviterId,
                                   String inviteeUsername) {

        this.inviterId = inviterId;
        this.inviteeUsername = inviteeUsername;
    }

    public Long getInviterId() {

        return this.inviterId;
    }

    public String getInviteeUsername() {

        return this.inviteeUsername;
    }

}

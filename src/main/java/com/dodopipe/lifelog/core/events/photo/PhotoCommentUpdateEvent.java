package com.dodopipe.lifelog.core.events.photo;

import com.dodopipe.lifelog.core.domain.PhotoComment;
import com.dodopipe.lifelog.core.events.UpdateEvent;

/**
 * @author Henry Yan
 */
public class PhotoCommentUpdateEvent extends UpdateEvent {

    public final Long photoId;
    public final PhotoComment photoComment;

    public PhotoCommentUpdateEvent(Long photoId,
                                   PhotoComment photoComment) {

        this.photoId = photoId;
        this.photoComment = photoComment;
    }

    public Long getPhotoId() {

        return photoId;
    }

    public PhotoComment getPhotoComment() {

        return photoComment;
    }
}

package com.dodopipe.lifelog.persistence.domain.photo;

import com.dodopipe.lifelog.core.domain.*;
import com.dodopipe.lifelog.persistence.domain.activity.ActivityEntity;
import com.dodopipe.lifelog.persistence.domain.user.UserEntity;
import com.dodopipe.lifelog.persistence.utils.PhotoUtil;

import javax.persistence.*;
import java.util.*;

/**
 * @author Henry Yan
 */
@Entity(name = "PHOTO")
public class PhotoEntity {

    private static final long serialVersionUID = 2343246452645L;

    @Id
    @Column(name = "ID",
            columnDefinition = "BIGINT(25) NOT NULL")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "OWNER_ID",
            columnDefinition = "BIGINT(25)",
            nullable = false)
    private Long ownerId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OWNER_ID",
                insertable = false,
                updatable = false)
    private UserEntity owner;

    @ManyToMany(fetch = FetchType.LAZY,
                cascade = {CascadeType.PERSIST,
                           CascadeType.MERGE})
    @JoinTable(name = "ACTIVITY_PHOTO_STREAM")
    private Collection<ActivityEntity> activities;

    @Column(name = "LAST_MODIFIED_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedTime;

    @Column(name = "UPLOADED_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date uploadedTime;

    @Column(name = "NUM_LIKES",
            columnDefinition = "INT(10)")
    private Integer numLikes;

    @Column(name = "LATEST_LIKING_NAMES",
            columnDefinition = "VARCHAR(2000)")
    private String latestLikingNames;

    @Column(name = "LATEST_COMMENTS",
            columnDefinition = "VARCHAR(2000)")
    private String latestComments;

    @Column(name = "CAPTION",
            columnDefinition = "VARCHAR(500)")
    private String caption;

    @Column(name = "CONTENT_TYPES",
            columnDefinition = "VARCHAR(500)")
    private String contentTypes;

    @Column(name = "ORIGINAL_FILE_PATHS",
            columnDefinition = "VARCHAR(500)")
    private String originalFilePaths;

    @Column(name = "THUMBNAIL_FILE_PATHS",
            columnDefinition = "VARCHAR(500)")
    private String thumbnailFilePaths;

    public PhotoEntity() {

    }

    public PhotoEntity(Long ownerId,
                       String caption,
                       Integer numLikes,
                       String latestLikingNames,
                       String latestComments,
                       Date uploadedTime,
                       Date lastModifiedTime,
                       String contentTypes,
                       String originalFilePaths,
                       String thumbnailFilePaths,
                       Collection<ActivityEntity> activities) {

        this.ownerId = ownerId;
        this.caption = caption;
        this.numLikes = numLikes;
        this.latestLikingNames = latestLikingNames;
        this.latestComments = latestComments;
        this.uploadedTime = uploadedTime;
        this.lastModifiedTime = lastModifiedTime;
        this.contentTypes = contentTypes;
        this.originalFilePaths = originalFilePaths;
        this.thumbnailFilePaths = thumbnailFilePaths;
        this.activities = activities;
    }

    public List<PhotoLike> getLatestPhotoLikesAsList() {

        List<PhotoLike> likes = new ArrayList<>();
        String[] likeStrs = PhotoUtil.splitWithDelimiter(this.latestLikingNames,
                                                         "#");
        for (String likeStr : likeStrs) {
            likeStr = PhotoUtil.undoEscape(likeStr,
                                           "@",
                                           "#");
            String[] photoLike = PhotoUtil.splitWithDelimiter(likeStr,
                                                              "@");

            photoLike[0] = PhotoUtil.undoEscape(photoLike[0],
                                                "@",
                                                "#");
            photoLike[1] = PhotoUtil.undoEscape(photoLike[1],
                                                "@",
                                                "#");

            likes.add(new PhotoLike(photoLike[0],
                                    photoLike[1]));
        }
        return Collections.unmodifiableList(likes);
    }

    public void addPhoto(String path,
                         String contentType) {

        StringBuffer sb = new StringBuffer();
        sb.append(PhotoUtil.isEmptyString(this.originalFilePaths) ? "" : this.originalFilePaths)
          .append(PhotoUtil.isEmptyString(this.originalFilePaths) ? "" : "#")
          .append(PhotoUtil.escape(path,
                                   "#"));

        this.originalFilePaths = sb.toString();

        sb = new StringBuffer();
        sb.append(PhotoUtil.isEmptyString(this.contentTypes) ? "" : this.contentTypes)
          .append(PhotoUtil.isEmptyString(this.contentTypes) ? "" : "#")
          .append(PhotoUtil.escape(contentType,
                                   "#"));

        this.contentTypes = sb.toString();
    }

    public void addThumbnail(String path) {

        StringBuffer sb = new StringBuffer();
        sb.append(PhotoUtil.isEmptyString(this.thumbnailFilePaths) ? "" : this.thumbnailFilePaths)
          .append(PhotoUtil.isEmptyString(this.thumbnailFilePaths) ? "" : "#")
          .append(PhotoUtil.escape(path,
                                   "#"));

        this.thumbnailFilePaths = sb.toString();
    }

    public List<PhotoMetadata> getPhotoMetadataAsList() {

        List<PhotoMetadata> images = new ArrayList<>();
        String[] originalFilePaths = PhotoUtil.splitWithDelimiter(this.originalFilePaths,
                                                                  "#");
        String[] thumbnailFilePaths = PhotoUtil.splitWithDelimiter(this.thumbnailFilePaths,
                                                                   "#");
        String[] contentTypes = PhotoUtil.splitWithDelimiter(this.contentTypes,
                                                             "#");
        int len = Math.min(originalFilePaths.length,
                           thumbnailFilePaths.length);

        if (len == 0) {
            return Collections.emptyList();
        }

        for (int i = 0; i < len; i++) {
            images.add(new PhotoMetadata(originalFilePaths[i],
                                         thumbnailFilePaths[i],
                                         contentTypes[i]));
        }

        return Collections.unmodifiableList(images);
    }

    public List<PhotoComment> getLatestCommentsAsList() {

        List<PhotoComment> comments = new ArrayList<>();
        String[] commentStrs = PhotoUtil.splitWithDelimiter(this.latestComments,
                                                            "#");
        for (String commentStr : commentStrs) {
            String[] photoComment = PhotoUtil.splitWithDelimiter(commentStr,
                                                                 "@");

            for (int i = 0; i < photoComment.length; i++) {
                photoComment[i] = PhotoUtil.undoEscape(photoComment[i],
                                                       "@",
                                                       "#");
            }

            comments.add(new PhotoComment(photoComment[0],
                                          photoComment[1],
                                          photoComment[2]));
        }

        return Collections.unmodifiableList(comments);
    }

    public Photo toPhoto() {

        return new Photo(getId(),
                         getCaption(),
                         getUploadedTime(),
                         getLastModifiedTime(),
                         getNumLikes(),
                         getLatestPhotoLikesAsList(),
                         getLatestCommentsAsList(),
                         getPhotoMetadataAsList());
    }

    public static PhotoEntity fromPhotoFileData(PhotoFileData photoFileData) {

        PhotoEntity photoEntity = new PhotoEntity();
        photoEntity.setOwnerId(photoFileData.getOwnerId());
        photoEntity.setCaption(photoFileData.getComment());
        return photoEntity;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public Long getOwnerId() {

        return ownerId;
    }

    public void setOwnerId(Long ownerId) {

        this.ownerId = ownerId;
    }

    public UserEntity getOwner() {

        return owner;
    }

    public void setOwner(UserEntity owner) {

        this.owner = owner;
    }

    public Date getLastModifiedTime() {

        return lastModifiedTime;
    }

    public void setLastModifiedTime(Date lastModifiedTime) {

        this.lastModifiedTime = lastModifiedTime;
    }

    public Date getUploadedTime() {

        return uploadedTime;
    }

    public void setUploadedTime(Date uploadedTime) {

        this.uploadedTime = uploadedTime;
    }

    public Integer getNumLikes() {

        return numLikes;
    }

    public void setNumLikes(Integer numLikes) {

        this.numLikes = numLikes;
    }

    public String getLatestLikingNames() {

        return latestLikingNames;
    }

    public void setLatestLikingNames(String latestLikingNames) {

        this.latestLikingNames = latestLikingNames;
    }

    public String getLatestComments() {

        return latestComments;
    }

    public void setLatestComments(String latestComments) {

        this.latestComments = latestComments;
    }

    public String getCaption() {

        return caption;
    }

    public void setCaption(String caption) {

        this.caption = caption;
    }

    public String getContentTypes() {

        return contentTypes;
    }

    public void setContentTypes(String contentTypes) {

        this.contentTypes = contentTypes;
    }

    public String getOriginalFilePaths() {

        return originalFilePaths;
    }

    public void setOriginalFilePaths(String originalFilePaths) {

        this.originalFilePaths = originalFilePaths;
    }

    public String getThumbnailFilePaths() {

        return thumbnailFilePaths;
    }

    public void setThumbnailFilePaths(String thumbnailFilePaths) {

        this.thumbnailFilePaths = thumbnailFilePaths;
    }

    public Collection<ActivityEntity> getActivities() {

        return activities;
    }

    public void setActivities(Collection<ActivityEntity> activities) {

        this.activities = activities;
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PhotoEntity other = (PhotoEntity) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }
}

package com.dodopipe.lifelog.core.events.user;

import com.dodopipe.lifelog.core.domain.UserEditInfo;
import com.dodopipe.lifelog.core.events.UpdateEvent;

/**
 * Created by henryhome on 7/28/14.
 */
public class UserUpdateEvent extends UpdateEvent {

    private Long userId;
    private UserEditInfo userEditInfo;

    public UserUpdateEvent(Long userId, UserEditInfo userEditInfo) {

        this.userId = userId;
        this.userEditInfo = userEditInfo;
    }

    public Long getUserId() {

        return userId;
    }

    public UserEditInfo getUserEditInfo() {

        return userEditInfo;
    }
}




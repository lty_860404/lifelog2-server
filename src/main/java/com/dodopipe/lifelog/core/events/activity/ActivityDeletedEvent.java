package com.dodopipe.lifelog.core.events.activity;

import com.dodopipe.lifelog.core.events.DeletedEvent;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by henryhome on 7/21/14.
 */
public class ActivityDeletedEvent extends DeletedEvent {
    public boolean success;

    public ActivityDeletedEvent(boolean success) {

        this.success = success;
    }

    public boolean isSuccess() {

        return success;
    }

    public Map<String, Boolean> toMap() {

        Map<String, Boolean> map = new HashMap<>();
        map.put("success",
                isSuccess());
        return map;
    }
}

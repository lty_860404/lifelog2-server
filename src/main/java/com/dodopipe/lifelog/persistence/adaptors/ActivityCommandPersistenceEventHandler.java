package com.dodopipe.lifelog.persistence.adaptors;

import com.dodopipe.lifelog.core.domain.ActivityAttendeeStatus;
import com.dodopipe.lifelog.core.domain.User;
import com.dodopipe.lifelog.core.domain.WeddingActivity;
import com.dodopipe.lifelog.core.events.activity.*;
import com.dodopipe.lifelog.core.ports.persistence.ActivityCommandEventHandler;
import com.dodopipe.lifelog.core.services.exception.ActivityAttendeeNotFoundException;
import com.dodopipe.lifelog.core.services.exception.ActivityNotFoundException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByIdException;
import com.dodopipe.lifelog.persistence.domain.activity.ActivityAttendeeEntity;
import com.dodopipe.lifelog.persistence.domain.activity.ActivityAttendeePrimaryKey;
import com.dodopipe.lifelog.persistence.domain.activity.ActivityEntity;
import com.dodopipe.lifelog.persistence.domain.activity.WeddingActivityEntity;
import com.dodopipe.lifelog.persistence.domain.family.ExternalInvitationEntity;
import com.dodopipe.lifelog.persistence.domain.user.UserEntity;
import com.dodopipe.lifelog.persistence.repositories.activity.ActivityAttendeeRepository;
import com.dodopipe.lifelog.persistence.repositories.activity.ActivityRepository;
import com.dodopipe.lifelog.persistence.repositories.family.ExternalInvitationRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserRepository;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Henry Yan
 */
public class ActivityCommandPersistenceEventHandler implements ActivityCommandEventHandler {

    private ActivityRepository activityRepo;
    private ActivityAttendeeRepository activityAttendeeRepo;
    private UserRepository userRepo;
    private ExternalInvitationRepository externalInvitationRepo;

    public ActivityCommandPersistenceEventHandler(ActivityRepository activityRepo,
                                                  ActivityAttendeeRepository activityAttendeeRepo,
                                                  UserRepository userRepo,
                                                  ExternalInvitationRepository externalInvitationRepo) {

        this.activityRepo = activityRepo;
        this.activityAttendeeRepo = activityAttendeeRepo;
        this.userRepo = userRepo;
        this.externalInvitationRepo = externalInvitationRepo;
    }

    @Override
    public ActivityCreatedEvent createWeddingActivity(ActivityCreateEvent event)
            throws
            UserNotFoundByIdException {

        Long userId = event.getUserId();
        WeddingActivity activity = (WeddingActivity) event.getActivity();

        UserEntity host = userRepo.findById(userId);

        if (host == null) {
            throw new UserNotFoundByIdException(userId);
        }

        WeddingActivityEntity entity = new WeddingActivityEntity(userId,
                                                                 activity.getName(),
                                                                 activity.getStartTime(),
                                                                 activity.getEndTime(),
                                                                 activity.getPlace(),
                                                                 activity.getGroomName(),
                                                                 activity.getBrideName());

        WeddingActivityEntity savedEntity = activityRepo.save(entity);

        List<ActivityAttendeeEntity> attendees = new ArrayList<>();
        attendees.add(new ActivityAttendeeEntity(userId,
                                                 savedEntity.getId(),
                                                 host.getUsername(),
                                                 host.getPhone(),
                                                 true,
                                                 ActivityAttendeeStatus.ACCEPTED.ordinal()));

        entity.setAttendees(attendees);

        activityRepo.save(entity);

        return new ActivityCreatedEvent(entity.toWeddingActivity());
    }

    @Override
    public ActivityUpdatedEvent updateWeddingActivity(ActivityUpdateEvent event)
            throws
            ActivityNotFoundException {

        Long activityId = event.getActivityId();
        WeddingActivity activity = (WeddingActivity) event.getActivity();

        WeddingActivityEntity entity = activityRepo.findWeddingActivityById(activityId);

        if (entity == null) {
            throw new ActivityNotFoundException(activityId);
        }

        if (activity.getName() != null) {
            entity.setName(activity.getName());
        }

        if (activity.getStartTime() != null) {
            entity.setStartTime(activity.getStartTime());
        }

        if (activity.getEndTime() != null) {
            entity.setEndTime(activity.getEndTime());
        }

        if (activity.getPlace() != null) {
            entity.setPlace(activity.getPlace());
        }

        if (activity.getGroomName() != null) {
            entity.setGroomName(activity.getGroomName());
        }

        if (activity.getBrideName() != null) {
            entity.setBrideName(activity.getBrideName());
        }

        activityRepo.save(entity);

        return new ActivityUpdatedEvent(true);
    }

    @Override
    public ActivityDeletedEvent deleteActivity(ActivityDeleteEvent event)
            throws
            ActivityNotFoundException {

        try {
            activityRepo.delete(event.getActivityId());
        } catch (EmptyResultDataAccessException e) {
            throw new ActivityNotFoundException(event.getActivityId(),
                                                e);
        }

        return new ActivityDeletedEvent(true);
    }

    @Override
    public ActivityAttendeesInvitedEvent inviteAttendees(ActivityAttendeesInviteEvent event)
            throws
            ActivityNotFoundException {

        Long inviterId = event.getUserId();
        Long activityId = event.getActivityId();
        List<User> attendees = event.getActivityAttendees();

        ActivityEntity activity = activityRepo.findById(activityId);

        if (activity == null) {
            throw new ActivityNotFoundException(activityId);
        }

        List<ActivityAttendeeEntity> attendeeEntities = activity.getAttendees();

        for (User attendee : attendees) {
            UserEntity entity = userRepo.findByUsername(attendee.getUsername());

            Long attendeeId = null;
            String attendeeName = null;
            String attendeePhone = null;

            if (entity == null) {
                addToExternalInvitation(inviterId,
                                        attendee.getPhone());
            } else {
                attendeeId = entity.getId();
                attendeeName = entity.getNickname();
                attendeePhone = entity.getPhone();
            }

            attendeeEntities.add(new ActivityAttendeeEntity(attendeeId,
                                                            activityId,
                                                            attendeeName,
                                                            attendeePhone,
                                                            false,
                                                            ActivityAttendeeStatus.PENDING.ordinal()));
        }

        activity.setAttendees(attendeeEntities);

        activityRepo.save(activity);

        return new ActivityAttendeesInvitedEvent(true);
    }

    @Override
    public void addToExternalInvitation(Long inviterId,
                                        String inviteePhone) {

        UserEntity user = userRepo.findOne(inviterId);
        ExternalInvitationEntity externalUserInvitation = user.inviteExternalUser(inviteePhone);
        externalInvitationRepo.save(externalUserInvitation);
    }

    @Override
    public ActivityAttendeeUpdatedEvent updateAttendeeInfo(ActivityAttendeeUpdateEvent event) {

        List<ActivityAttendeeEntity> entities = activityAttendeeRepo.findByAttendeePhone(event.getAttendeePhone());

        if (!entities.isEmpty()) {
            for (ActivityAttendeeEntity entity : entities) {
                if (event.getAttendeeId() != null) {
                    entity.setAttendeeId(event.getAttendeeId());
                }

                if (!event.getAttendeeName()
                          .isEmpty()) {
                    entity.setAttendeeName(event.getAttendeeName());
                }

                activityAttendeeRepo.save(entity);
            }
        }

        return new ActivityAttendeeUpdatedEvent(true);
    }

    @Override
    public ActivityInvitationAcceptedEvent acceptInvitation(ActivityInvitationAcceptEvent event)
            throws
            ActivityAttendeeNotFoundException {

        Long activityId = event.getActivityId();
        Long attendeeId = event.getAttendeeId();

        ActivityAttendeeEntity attendee = activityAttendeeRepo.findOne(new ActivityAttendeePrimaryKey(activityId,
                                                                                                      attendeeId));

        if (attendee == null) {
            throw new ActivityAttendeeNotFoundException(activityId,
                                                        attendeeId);
        }

        attendee.setStatus(ActivityAttendeeStatus.ACCEPTED.ordinal());

        activityAttendeeRepo.save(attendee);

        return new ActivityInvitationAcceptedEvent(true);
    }

    @Override
    public ActivityInvitationRejectedEvent rejectInvitation(ActivityInvitationRejectEvent event)
            throws
            ActivityAttendeeNotFoundException {

        Long activityId = event.getActivityId();
        Long attendeeId = event.getAttendeeId();

        ActivityAttendeeEntity attendee = activityAttendeeRepo.findOne(new ActivityAttendeePrimaryKey(activityId,
                                                                                                      attendeeId));

        if (attendee == null) {
            throw new ActivityAttendeeNotFoundException(activityId,
                                                        attendeeId);
        }

        attendee.setStatus(ActivityAttendeeStatus.REJECTED.ordinal());

        activityAttendeeRepo.save(attendee);

        return new ActivityInvitationRejectedEvent(true);
    }
}




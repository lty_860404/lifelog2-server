package com.dodopipe.lifelog.notification.sms;

import com.dodopipe.lifelog.core.events.auth.MessageCallbackCreateEvent;
import com.dodopipe.lifelog.core.events.auth.VerificationCodeNotifiedEvent;
import com.dodopipe.lifelog.core.events.auth.VerificationCodeNotifyEvent;
import com.dodopipe.lifelog.core.ports.notification.MessageEventHandler;
import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;
import com.dodopipe.lifelog.core.services.exception.MessageNotDeliveredException;
import com.dodopipe.lifelog.core.services.exception.VerificationCodeNotFoundException;
import com.dodopipe.lifelog.persistence.domain.user.PhoneVerificationCodeEntity;
import com.dodopipe.lifelog.persistence.repositories.user.PhoneVerificationCodeRepository;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by henryhome on 7/28/14.
 */
public class MessageNotificationEventHandler
        implements MessageEventHandler {

    private String accountSid;
    private String authToken;
    private String sourcePhone;

    private PhoneVerificationCodeRepository verifyRepo;

    public MessageNotificationEventHandler(PhoneVerificationCodeRepository verifyRepo) {

        this.verifyRepo = verifyRepo;
    }

    @Override
    public void sendVerificationCodeNotification(String countryCode,
                                                 String destinationPhone,
                                                 String verificationCode)
            throws
            InternalServerErrorException {

        String accountSid = getAccountSid();
        String authToken = getAuthToken();
        String sourcePhone = getSourcePhone();

        TwilioRestClient client = new TwilioRestClient(accountSid,
                                                       authToken);

        List<NameValuePair> params = new ArrayList<>();

        String MESSAGE = "Your verification code for your LIFELOG account is "
                + verificationCode
                + ", please type it into the form to complete your registration!";

        params.add(new BasicNameValuePair("Body",
                                          MESSAGE));
        params.add(new BasicNameValuePair("To",
                                          countryCode + destinationPhone));
        params.add(new BasicNameValuePair("From",
                                          sourcePhone));
        params.add(new BasicNameValuePair("statusCallback",
                                          "/account/callback"));

        MessageFactory messageFactory = client.getAccount()
                                              .getMessageFactory();
        try {
            messageFactory.create(params);
        } catch (TwilioRestException e) {
            throw new InternalServerErrorException(e);
        }
    }

    @Override
    public VerificationCodeNotifiedEvent notifyVerificationStatus(VerificationCodeNotifyEvent event)
            throws
            VerificationCodeNotFoundException,
            MessageNotDeliveredException,
            InternalServerErrorException {

        String phone = event.getPhone();

        PhoneVerificationCodeEntity entity = verifyRepo.findByPhone(phone);

        if (entity == null) {
            throw new VerificationCodeNotFoundException(phone);
        }

        String messageStatus = entity.getMessageStatus();
        Integer messageErrorCode = entity.getMessageErrorCode();

        if ("failed".equals(messageStatus) || "undelivered".equals(messageStatus)) {
            switch (messageErrorCode) {
                case 30001:
                    throw new MessageNotDeliveredException("The message queue is overflowed, please try to send your message again later");
                case 30002:
                    throw new InternalServerErrorException("The server was suspended between the time of message send and delivery");
                case 30003:
                    throw new MessageNotDeliveredException("The mobile phone you are trying to reach is switched off or otherwise unavailable");
                case 30004:
                    throw new MessageNotDeliveredException("The phone you are trying to reach is blocked from receiving this message");
                case 30005:
                    throw new MessageNotDeliveredException("The phone you are trying to reach is unknown and may no longer exist");
                case 30006:
                    throw new MessageNotDeliveredException("The phone you are trying to reach is unable to receive this message");
                case 30007:
                    throw new MessageNotDeliveredException("Your message content is going against carrier guidelines");
                default:
                    throw new InternalServerErrorException("Unknown error");
            }
        }

        if ("queued".equals(messageStatus)) {
            return new VerificationCodeNotifiedEvent("queued");
        }

        if ("sending".equals(messageStatus)) {
            return new VerificationCodeNotifiedEvent("sending");
        }

        if ("sent".equals(messageStatus)) {
            return new VerificationCodeNotifiedEvent("sent");
        }

        return new VerificationCodeNotifiedEvent("delivered");
    }

    @Override
    public void updateVerificationStatus(MessageCallbackCreateEvent event) {

        String destinationPhone = event.getTo();
        String messageStatus = event.getMessageStatus();
        Integer errorCode = event.getErrorCode();


        PhoneVerificationCodeEntity entity = verifyRepo.findByPhone(destinationPhone);

        if (entity != null) {
            entity.setMessageStatus(messageStatus);
            entity.setMessageErrorCode(errorCode);
        }

        verifyRepo.save(entity);
    }

    public String getAccountSid() {

        return accountSid;
    }

    public void setAccountSid(String accountSid) {

        this.accountSid = accountSid;
    }

    public String getAuthToken() {

        return authToken;
    }

    public void setAuthToken(String authToken) {

        this.authToken = authToken;
    }

    public String getSourcePhone() {

        return sourcePhone;
    }

    public void setSourcePhone(String sourcePhone) {

        this.sourcePhone = sourcePhone;
    }
}




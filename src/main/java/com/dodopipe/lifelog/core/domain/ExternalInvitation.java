package com.dodopipe.lifelog.core.domain;

/**
 * Created by henryhome on 7/29/14.
 */
public class ExternalInvitation {

    private Long id;

    private Long inviterId;

    private String inviteePhone;

    private ExternalInvitationStatus status;

    public ExternalInvitation() {

    }

    public ExternalInvitation(Long id,
                              Long inviterId,
                              String inviteePhone,
                              ExternalInvitationStatus status) {

        this.id = id;
        this.inviterId = inviterId;
        this.inviteePhone = inviteePhone;
        this.status = status;
    }

    public Long getId() {

        return id;
    }

    public Long getInviterId() {

        return inviterId;
    }

    public String getInviteePhone() {

        return inviteePhone;
    }

    public ExternalInvitationStatus getStatus() {

        return status;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof ExternalInvitation)) {
            return false;
        }

        ExternalInvitation that = (ExternalInvitation) o;

        if (!id.equals(that.id)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {

        return id.hashCode();
    }
}

package com.dodopipe.lifelog.core.services.exception;

/**
 * @author Henry Yan
 */
public class PhotoNotFoundException extends ServiceException {

    private Long photoId;

    public PhotoNotFoundException(Long photoId) {

        this.photoId = photoId;

    }

    public PhotoNotFoundException(Long photoId,
                                  Throwable cause) {

        super(cause);
        this.photoId = photoId;
    }

    @Override
    public String getMessage() {

        return "The photo with id = " + photoId + " cannot be found";
    }
}

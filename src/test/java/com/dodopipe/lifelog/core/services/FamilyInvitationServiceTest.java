package com.dodopipe.lifelog.core.services;

import com.dodopipe.lifelog.config.*;
import com.dodopipe.lifelog.core.domain.FamilyInvitationStatus;
import com.dodopipe.lifelog.core.domain.FamilyRelationshipStatus.FamilyMemberStatusType;
import com.dodopipe.lifelog.core.events.family.*;
import com.dodopipe.lifelog.persistence.domain.family.FamilyMemberEntity;
import com.dodopipe.lifelog.persistence.domain.user.UserEntity;
import com.dodopipe.lifelog.persistence.repositories.family.FamilyInvitationRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserRepository;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

import static org.junit.Assert.*;

/**
 * @author yongwei
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CoreConfiguration.class,
                                 JPAConfiguration.class,
                                 DataSourceConfiguration.class,
                                 RepositoryConfiguration.class,
                                 DBUnitDataSourceConfiguration.class})
@ActiveProfiles(resolver = LifelogActiveProfilesResolver.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
@DatabaseSetup("DB_SETUP.xml")
public class FamilyInvitationServiceTest {

    private static Logger logger = LoggerFactory.getLogger(FamilyInvitationServiceTest.class);
    @Autowired
    FamilyCommandService familyCommandService;

    @Autowired
    FamilyQueryService familyQueryService;

    @Autowired
    UserAccountService uaService;

    @Autowired
    FamilyInvitationRepository repo;

    @Autowired
    UserRepository userRepo;

    @Test
    public void shouldReadAllRelationships() {

        FamilyRelationshipsViewEvent event = new FamilyRelationshipsViewEvent(1000l,
                                                                              "ZH-CN");
        FamilyRelationshipsViewedEvent success = familyQueryService.viewAllFamilyRelationships(event);

        assertEquals(7,
                     success.getRelationships()
                            .size());
        assertEquals("System",
                     success.getRelationships()
                            .get(0)
                            .getRelationshipScope());
        assertEquals("Customized",
                     success.getRelationships()
                            .get(6)
                            .getRelationshipScope());
    }

    @Test
    public void shouldReadAllReceivedInvitations() {

        FamilyInvitationsViewEvent event = new FamilyInvitationsViewEvent(2000l);
        FamilyInvitationsViewedEvent success = familyQueryService.viewAllReceivedInvitations(event);

        assertEquals(3,
                     success.getInvitations()
                            .size());
        assertEquals("+818012342222",
                     success.getInvitations()
                            .get(0)
                            .getFamilyInvitation()
                            .getInviteePhone());
    }

    @Test
    public void shouldReadAllSentInvitations() {

        FamilyInvitationsViewEvent event = new FamilyInvitationsViewEvent(1000L);
        FamilyInvitationsViewedEvent success = familyQueryService.viewAllSentInvitations(event);
        assertEquals(3,
                     success.getInvitations()
                            .size());
        assertEquals(FamilyInvitationStatus.PENDING,
                     success.getInvitations()
                            .get(0)
                            .getFamilyInvitation()
                            .getStatus());
        assertEquals(FamilyInvitationStatus.ACCEPT,
                     success.getInvitations()
                            .get(success.getInvitations()
                                        .size() - 1)
                            .getFamilyInvitation()
                            .getStatus());
    }

    @Test
    public void shouldInviteAnExistedUserByPhone()
            throws
            Exception {

        //add invitation by account without add external invitation
        FamilyMemberInviteEvent event = new FamilyMemberInviteEvent(2000L,
                                                                    null,
                                                                    "testUser",
                                                                    "+818012341111",
                                                                    "Friends",
                                                                    "en");
        FamilyMemberInvitedEvent success = familyCommandService.inviteFamilyMember(event);
        userRepo.flush();
        assertNotNull(success);
        assertTrue(success.isSuccess());
        assertTrue((success.getInvitationId() > 0));
    }

    @Test
    public void shouldInviteANewUserByPhone()
            throws
            Exception {
        //add invitation by account with add external invitation
        FamilyMemberInviteEvent event = new FamilyMemberInviteEvent(1000L,
                                                                    null,
                                                                    "testUser",
                                                                    "+818012343333",
                                                                    "Friends",
                                                                    "en");
        FamilyMemberInvitedEvent result = familyCommandService.inviteFamilyMember(event);
        userRepo.flush();
        assertTrue(result.isSuccess());
    }

    @Test
    public void shouldInviteAnExistedUserByUserName()
            throws
            Exception {
        //add invitation by username
        FamilyMemberInviteEvent event = new FamilyMemberInviteEvent(2000L,
                                                                    "Laura55555",
                                                                    "invitee Laura",
                                                                    null,
                                                                    "Friends",
                                                                    "en");
        FamilyMemberInvitedEvent success = familyCommandService.inviteFamilyMember(event);
        userRepo.flush();
        assertNotNull(success);
        assertTrue(success.isSuccess());
        assertTrue(success.getInvitationId() > 0);
    }

    @Test
    public void shouldInviteAnBlockedFamilyMemberByPhone()
            throws
            Exception {
        //add invitation by account without add external invitation
        FamilyMemberInviteEvent event = new FamilyMemberInviteEvent(1000L,
                                                                    null,
                                                                    "testUser",
                                                                    "+818012343333",
                                                                    "Friends",
                                                                    "en");
        FamilyMemberInvitedEvent success = familyCommandService.inviteFamilyMember(event);
        userRepo.flush();
        assertNotNull(success);
        assertTrue(success.isSuccess());
        assertEquals(Long.valueOf(-1l),
                     success.getInvitationId());
        assertTrue((success.getInvitationId() == -1));
    }

    @Test
    public void shouldInviteAnRemovedFamilyMemberByPhone()
            throws
            Exception {
        //add invitation by account without add external invitation
        FamilyMemberInviteEvent event = new FamilyMemberInviteEvent(3000L,
                                                                    null,
                                                                    "testUser",
                                                                    "+818012341111",
                                                                    "Friends",
                                                                    "en");
        FamilyMemberInvitedEvent success = familyCommandService.inviteFamilyMember(event);
        userRepo.flush();
        assertNotNull(success);
        assertTrue(success.isSuccess());
        assertTrue((success.getInvitationId() > 0));
    }

    @Test
    public void shouldReturnAnExistInvitation()
            throws
            Exception {
        //add invitation by account without add external invitation
        FamilyMemberInviteEvent event = new FamilyMemberInviteEvent(3000L,
                                                                    "Laura55555",
                                                                    "testUser",
                                                                    "+818012342222",
                                                                    "Friends",
                                                                    "en");
        FamilyMemberInvitedEvent success = familyCommandService.inviteFamilyMember(event);
        userRepo.flush();
        assertNotNull(success);
        assertTrue(success.isSuccess());
        assertTrue((success.getInvitationId() > 0));
    }

    @Test
    public void shouldAcceptANewFamilyInvitation() {

        FamilyInvitationAcceptEvent event = new FamilyInvitationAcceptEvent(3000L,
                                                                            "BEST FRIENDS",
                                                                            "en");
        FamilyInvitationAcceptedEvent success = familyCommandService.acceptFamilyInvitation(event);

        UserEntity invitee = userRepo.findOne(3000L);
        Collection<FamilyMemberEntity> members = invitee.getFamily()
                                                        .getFamilyMembers();
        if (logger.isDebugEnabled()) {
            for (FamilyMemberEntity member : members) {
                logger.debug("\nname:{}\n" +
                                     "relation:{}",
                             member.getMember()
                                   .getUsername(),
                             member.getRelationship());
                if (member.getMemberId() == 2000l) {
                    assertEquals(FamilyMemberStatusType.VALID.ordinal(),
                                 member.getStatus());
                } else {
                    assertEquals(FamilyMemberStatusType.BE_BLOCKED.ordinal(),
                                 member.getStatus());
                }
            }

        }
        assertEquals(1,
                     members.size());
        assertTrue(success.isSuccess());

    }

    @Test
    public void shouldAcceptAnBlockedFamilyInvitation() {

        FamilyInvitationAcceptEvent event = new FamilyInvitationAcceptEvent(1000L,
                                                                            "BEST FRIENDS",
                                                                            "en");
        FamilyInvitationAcceptedEvent success = familyCommandService.acceptFamilyInvitation(event);

        UserEntity invitee = userRepo.findOne(2000L);
        Collection<FamilyMemberEntity> members = invitee.getFamily()
                                                        .getFamilyMembers();
        if (logger.isDebugEnabled()) {
            for (FamilyMemberEntity member : members) {
                logger.debug("\nname:{}\n" +
                                     "relation:{}",
                             member.getMember()
                                   .getUsername(),
                             member.getRelationship());
                assertEquals(FamilyMemberStatusType.VALID.ordinal(),
                             member.getStatus());
            }

        }
        assertEquals(3,
                     members.size());
        assertTrue(success.isSuccess());

    }
}

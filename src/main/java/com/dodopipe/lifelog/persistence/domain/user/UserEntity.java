package com.dodopipe.lifelog.persistence.domain.user;

import com.dodopipe.lifelog.core.domain.FamilyInvitationStatus;
import com.dodopipe.lifelog.core.domain.User;
import com.dodopipe.lifelog.persistence.domain.family.ExternalInvitationEntity;
import com.dodopipe.lifelog.persistence.domain.family.FamilyEntity;
import com.dodopipe.lifelog.persistence.domain.family.FamilyInvitationEntity;
import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;


@Entity(name = "USER")
public class UserEntity
        implements Serializable {

    private static final long serialVersionUID = 1L;

    private static SecureRandom random = new SecureRandom();

    public enum Gender {
        Male,
        Female
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID",
            columnDefinition = "BIGINT(25) NOT NULL AUTO_INCREMENT")
    private Long id;

    @Column(name = "USERNAME",
            columnDefinition = "VARCHAR(80)",
            unique = true)
    private String username;

    @Column(name = "NICKNAME",
            columnDefinition = "VARCHAR(80)")
    private String nickname;

    @Column(name = "FAMILY_ID",
            columnDefinition = "BIGINT(25)",
            nullable = false,
            insertable = false,
            updatable = false)
    private Long familyId;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FAMILY_ID",
                referencedColumnName = "ID",
                nullable = false)
    @JoinFetch(JoinFetchType.INNER)
    private FamilyEntity family;

    @Column(name = "COUNTRY_CODE",
            columnDefinition = "VARCHAR(20)")
    private String countryCode;

    @Column(name = "PHONE",
            columnDefinition = "VARCHAR(80)",
            unique = true)
    private String phone;

    @Column(name = "GENDER",
            columnDefinition = "VARCHAR(80)")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "SIGNATURE",
            columnDefinition = "VARCHAR(500)")
    private String signature;

    @Column(name = "AVATAR_FILE_PATH",
            columnDefinition = "VARCHAR(200)")
    private String avatarFilePath;

    @Column(name = "LANG_CODE",
            columnDefinition = "VARCHAR(20)")
    private String langCode;

    @Column(name = "REGISTER_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registerTime;

    public UserEntity() {

    }

    public UserEntity(long id) {

        this.id = id;
    }

    public static String generateDefaultUsername() {
        //TODO
        return new BigInteger(40,
                              random).toString(32);
    }

    public FamilyInvitationEntity inviteFamilyMember(UserEntity invitee,
                                                     String relationship) {

        return new FamilyInvitationEntity(getId(),
                                          invitee.getId(),
                                          invitee.getPhone(),
                                          relationship,
                                          FamilyInvitationStatus.PENDING.ordinal());
    }

    public ExternalInvitationEntity inviteExternalUser(String phoneNumber) {

        ExternalInvitationEntity externalUserInvitation = new ExternalInvitationEntity();
        externalUserInvitation.setInviterId(getId());
        externalUserInvitation.setInviteePhone(phoneNumber);
        return externalUserInvitation;
    }

    public User toUser() {

        String userGender = null;

        if (getGender() != null) {
            userGender = getGender().toString();
        }

        return new User(getId(),
                        getUsername(),
                        getNickname(),
                        getFamily().getId(),
                        getCountryCode(),
                        getPhone(),
                        userGender,
                        getSignature(),
                        getAvatarFilePath(),
                        getLangCode(),
                        getRegisterTime());
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {

        this.username = username;
    }

    public String getNickname() {

        return nickname;
    }

    public void setNickname(String nickname) {

        this.nickname = nickname;
    }

    public Long getFamilyId() {

        return familyId;
    }

    public void setFamilyId(Long familyId) {

        this.familyId = familyId;
    }

    public FamilyEntity getFamily() {

        return family;
    }

    public void setFamily(FamilyEntity family) {

        this.family = family;
    }

    public String getCountryCode() {

        return countryCode;
    }

    public void setCountryCode(String countryCode) {

        this.countryCode = countryCode;
    }

    public String getPhone() {

        return phone;
    }

    public void setPhone(String phone) {

        this.phone = phone;
    }

    public Gender getGender() {

        return gender;
    }

    public void setGender(Gender gender) {

        this.gender = gender;
    }

    public String getSignature() {

        return signature;
    }

    public void setSignature(String signature) {

        this.signature = signature;
    }

    public String getAvatarFilePath() {

        return avatarFilePath;
    }

    public void setAvatarFilePath(String avatarFilePath) {

        this.avatarFilePath = avatarFilePath;
    }

    public String getLangCode() {

        return langCode;
    }

    public void setLangCode(String langCode) {

        this.langCode = langCode;
    }

    public Date getRegisterTime() {

        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {

        this.registerTime = registerTime;
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        UserEntity other = (UserEntity) obj;
        if (id != other.id) {
            return false;
        }
        return true;
    }
}

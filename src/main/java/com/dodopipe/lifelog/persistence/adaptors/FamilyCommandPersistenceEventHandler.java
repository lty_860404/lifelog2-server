package com.dodopipe.lifelog.persistence.adaptors;

import com.dodopipe.lifelog.core.domain.*;
import com.dodopipe.lifelog.core.events.family.*;
import com.dodopipe.lifelog.core.ports.persistence.FamilyCommandEventHandler;
import com.dodopipe.lifelog.core.services.exception.FamilyInvitationNotFoundException;
import com.dodopipe.lifelog.core.services.exception.FamilyMemberNotFoundException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByIdException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByUsernameException;
import com.dodopipe.lifelog.persistence.domain.family.*;
import com.dodopipe.lifelog.persistence.domain.user.UserEntity;
import com.dodopipe.lifelog.persistence.repositories.family.*;
import com.dodopipe.lifelog.persistence.repositories.user.UserRepository;

import java.util.List;

/**
 * @author yongwei
 */
public class FamilyCommandPersistenceEventHandler
        implements FamilyCommandEventHandler {

    private UserRepository userRepo;
    private FamilyRepository familyRepo;
    private ExternalInvitationRepository externalInvitationRepo;
    private FamilyInvitationRepository familyInvitationRepo;
    private FamilyMemberRepository familyMemberRepo;
    private FamilyRelationshipRepository familyRelationshipRepo;

    public FamilyCommandPersistenceEventHandler(UserRepository userRepo,
                                                FamilyRepository familyRepo,
                                                ExternalInvitationRepository externalInvitationRepo,
                                                FamilyInvitationRepository familyInvitationRepo,
                                                FamilyMemberRepository familyMemberRepo,
                                                FamilyRelationshipRepository familyRelationshipRepo) {

        this.userRepo = userRepo;
        this.familyRepo = familyRepo;
        this.externalInvitationRepo = externalInvitationRepo;
        this.familyInvitationRepo = familyInvitationRepo;
        this.familyMemberRepo = familyMemberRepo;
        this.familyRelationshipRepo = familyRelationshipRepo;
    }

    @Override
    public FamilyMemberInvitedEvent sendFamilyInvitation(User inviter,
                                                         User invitee,
                                                         String relationship,
                                                         String langCode) {

        FamilyInvitationEntity pendingInv =
                familyInvitationRepo.findPendingInvitationByInviterIdAndInviteeId(inviter.getId(),
                                                                                  invitee.getId());

        if (pendingInv == null) {
            pendingInv = familyInvitationRepo.findPendingInvitationByInviterIdAndInviteePhone(inviter.getId(),
                                                                                              invitee.getPhone());
        }

        if (pendingInv != null) {
            return new FamilyMemberInvitedEvent(true,
                                                pendingInv.getId());
        }

        FamilyInvitationEntity invitation = new FamilyInvitationEntity(inviter.getId(),
                                                                       invitee.getId(),
                                                                       invitee.getPhone(),
                                                                       relationship,
                                                                       FamilyInvitationStatus.PENDING.ordinal());
        familyInvitationRepo.save(invitation);

        FamilyRelationshipEntity familyRelationshipEntity = familyRelationshipRepo.findByDisplayName(relationship);

        if (familyRelationshipEntity == null) {
            FamilyRelationshipEntity storedEntity = new FamilyRelationshipEntity(relationship,
                                                                                 langCode,
                                                                                 inviter.getId(),
                                                                                 FamilyRelationshipEntity.RelationshipScope.Customized);

            familyRelationshipRepo.save(storedEntity);
        }

        return new FamilyMemberInvitedEvent(true,
                                            invitation.getId());
    }

    @Override
    public void sendExternalInvitation(Long inviterId,
                                       String inviteePhone) {

        UserEntity user = userRepo.findOne(inviterId);
        ExternalInvitationEntity externalUserInvitation = user.inviteExternalUser(inviteePhone);
        externalInvitationRepo.save(externalUserInvitation);
    }

    @Override
    public void updateFamilyMemberStatus(FamilyMember familyMember)
            throws
            FamilyMemberNotFoundException {

        FamilyMemberEntity familyMemberEntity =
                familyMemberRepo.findOne(new FamilyMemberPrimaryKey(familyMember.getFamilyId(),
                                                                    familyMember.getMemberId()));

        if (familyMemberEntity == null) {
            throw new FamilyMemberNotFoundException(familyMember.getFamilyId(),
                                                    familyMember.getMemberId());
        }

        familyMemberEntity.setStatus(familyMember.getStatus()
                                                 .getOwnsideStatusType()
                                                 .ordinal());
        familyMemberRepo.save(familyMemberEntity);
    }

    @Override
    public FamilyInvitationViewedEvent updateFamilyInvitationStatus(FamilyInvitationAcceptEvent event)
            throws
            FamilyInvitationNotFoundException,
            UserNotFoundByIdException {

        Long invitationId = event.getInvitationId();

        FamilyInvitationEntity fmInvitation = familyInvitationRepo.findById(invitationId);

        if (fmInvitation == null) {
            throw new FamilyInvitationNotFoundException("The family Invitation with id = " + invitationId + " is not found");
        }

        Long inviterId = fmInvitation.getInviterId();
        Long inviteeId = fmInvitation.getInviteeId();

        UserEntity inviter = userRepo.findById(inviterId);

        if (inviter == null) {
            throw new UserNotFoundByIdException(inviterId);
        }

        UserEntity invitee = userRepo.findById(inviteeId);

        if (invitee == null) {
            throw new UserNotFoundByIdException(inviteeId);
        }

        fmInvitation.setStatus(FamilyInvitationStatus.ACCEPT.ordinal());
        familyInvitationRepo.save(fmInvitation);

        return new FamilyInvitationViewedEvent(inviter.toUser(),
                                               invitee.toUser(),
                                               fmInvitation.getRelationship());
    }

    @Override
    public ExternalInvitationUpdatedEvent updateExternalInvitationStatus(ExternalInvitationUpdateEvent event) {

        List<ExternalInvitationEntity> entities = externalInvitationRepo.findByInviteePhone(event.getPhone());

        if (!entities.isEmpty()) {
            for (ExternalInvitationEntity entity : entities) {
                entity.setStatus(ExternalInvitationStatus.REGISTERED.ordinal());
            }
        }

        return new ExternalInvitationUpdatedEvent(true);
    }

    @Override
    public InviteeInfoUpdatedEvent updateFamilyInvitationInviteeInfo(InviteeInfoUpdateEvent event) {

        List<FamilyInvitationEntity> entities = familyInvitationRepo.findByInviteePhone(event.getInviteePhone());

        if (!entities.isEmpty()) {
            for (FamilyInvitationEntity entity : entities) {
                if (event.getInviteeId() != null) {
                    entity.setInviteeId(event.getInviteeId());
                }

                familyInvitationRepo.save(entity);
            }
        }

        return new InviteeInfoUpdatedEvent(true);
    }

    @Override
    public FamilyInvitationAcceptedEvent addFamilyMember(Long inviterId,
                                                         String inviterRelationship,
                                                         Long inviteeId,
                                                         String inviteeRelationship,
                                                         String inviteeLangCode) {

        UserEntity inviter = userRepo.findById(inviterId);
        UserEntity invitee = userRepo.findById(inviteeId);

        FamilyEntity inviterFamily = inviter.getFamily();
        FamilyEntity inviteeFamily = invitee.getFamily();

        inviterFamily.addFamilyMember(invitee,
                                      inviteeRelationship);
        inviteeFamily.addFamilyMember(inviter,
                                      inviterRelationship);
        familyRepo.save(inviterFamily);
        familyRepo.save(inviteeFamily);

        FamilyRelationshipEntity familyRelationshipEntity =
                familyRelationshipRepo.findByDisplayName(inviteeRelationship);

        if (familyRelationshipEntity == null) {
            FamilyRelationshipEntity storedEntity = new FamilyRelationshipEntity(inviteeRelationship,
                                                                                 inviteeLangCode,
                                                                                 inviteeId,
                                                                                 FamilyRelationshipEntity.RelationshipScope.Customized);

            familyRelationshipRepo.save(storedEntity);
        }

        return new FamilyInvitationAcceptedEvent(true);
    }

    @Override
    public FamilyMemberUpdatedEvent updateFamilyMember(FamilyMemberUpdateEvent event)
            throws
            UserNotFoundByIdException,
            UserNotFoundByUsernameException,
            FamilyMemberNotFoundException {

        String relationship = event.getRelationship();
        String langCode = event.getLangCode();

        UserEntity inviter = userRepo.findById(event.getInviterId());
        if (inviter == null) {
            throw new UserNotFoundByIdException(event.getInviterId());
        }

        UserEntity invitee = userRepo.findByUsername(event.getInviteeUsername());

        if (invitee == null) {
            throw new UserNotFoundByUsernameException(event.getInviteeUsername());
        }

        FamilyMemberEntity familyMemberEntity =
                familyMemberRepo.findOne(new FamilyMemberPrimaryKey(inviter.getFamily()
                                                                           .getId(),
                                                                    invitee.getId()));
        if (familyMemberEntity == null) {
            throw new FamilyMemberNotFoundException(inviter.getFamily()
                                                           .getId(),
                                                    invitee.getId());
        }

        familyMemberEntity.setRelationship(event.getRelationship());
        familyMemberRepo.save(familyMemberEntity);

        FamilyRelationshipEntity familyRelationshipEntity = familyRelationshipRepo.findByDisplayName(relationship);

        if (familyRelationshipEntity == null) {
            FamilyRelationshipEntity storedEntity = new FamilyRelationshipEntity(relationship,
                                                                                 langCode,
                                                                                 inviter.getId(),
                                                                                 FamilyRelationshipEntity.RelationshipScope.Customized);

            familyRelationshipRepo.save(storedEntity);
        }

        return new FamilyMemberUpdatedEvent(true);
    }
}

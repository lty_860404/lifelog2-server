package com.dodopipe.lifelog.core.events.photo;

import com.dodopipe.lifelog.core.domain.PhotoLike;
import com.dodopipe.lifelog.core.events.UpdateEvent;

/**
 * @author Henry Yan
 */
public class PhotoLikeAddEvent
        extends UpdateEvent {

    public Long photoId;
    public PhotoLike photoLike;

    public PhotoLikeAddEvent() {

    }

    public PhotoLikeAddEvent(Long photoId,
                             PhotoLike photoLike) {

        this.photoId = photoId;
        this.photoLike = photoLike;
    }

    public Long getPhotoId() {

        return photoId;
    }

    public PhotoLike getPhotoLike() {

        return photoLike;
    }
}

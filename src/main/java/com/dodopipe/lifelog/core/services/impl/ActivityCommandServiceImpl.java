package com.dodopipe.lifelog.core.services.impl;

import com.dodopipe.lifelog.core.events.activity.*;
import com.dodopipe.lifelog.core.ports.persistence.ActivityCommandEventHandler;
import com.dodopipe.lifelog.core.services.ActivityCommandService;
import com.dodopipe.lifelog.core.services.exception.ActivityNotFoundException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByIdException;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Henry Yan
 */
@Transactional
public class ActivityCommandServiceImpl implements ActivityCommandService {


    private ActivityCommandEventHandler handler;

    public ActivityCommandServiceImpl(ActivityCommandEventHandler handler) {

        this.handler = handler;
    }

    public ActivityCreatedEvent createWeddingActivity(ActivityCreateEvent event)
            throws
            UserNotFoundByIdException {

        return handler.createWeddingActivity(event);
    }

    public ActivityUpdatedEvent updateWeddingActivity(ActivityUpdateEvent event)
            throws
            ActivityNotFoundException {

        return handler.updateWeddingActivity(event);
    }

    public ActivityDeletedEvent deleteActivity(ActivityDeleteEvent event)
            throws
            ActivityNotFoundException {

        return handler.deleteActivity(event);
    }

    public ActivityAttendeesInvitedEvent inviteAttendees(ActivityAttendeesInviteEvent event)
            throws
            ActivityNotFoundException {

        return handler.inviteAttendees(event);
    }

    public ActivityInvitationAcceptedEvent acceptInvitation(ActivityInvitationAcceptEvent event)
            throws
            ActivityNotFoundException {

        return handler.acceptInvitation(event);
    }

    public ActivityInvitationRejectedEvent rejectInvitation(ActivityInvitationRejectEvent event)
            throws
            ActivityNotFoundException {

        return handler.rejectInvitation(event);
    }
}

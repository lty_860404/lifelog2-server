package com.dodopipe.lifelog.core.domain;

import java.util.Date;

/**
 * @author HenryYan
 */
public abstract class Activity {

    protected Long id;
    protected Long hostId;
    protected String name;
    protected Date startTime;
    protected Date endTime;

    public Long getId() {

        return id;
    }

    public Long getHostId() {

        return hostId;
    }

    public String getName() {

        return name;
    }

    public Date getStartTime() {

        return startTime;
    }

    public Date getEndTime() {

        return endTime;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof Activity)) {
            return false;
        }

        Activity activity = (Activity) o;

        if (!id.equals(activity.id)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {

        return id.hashCode();
    }
}

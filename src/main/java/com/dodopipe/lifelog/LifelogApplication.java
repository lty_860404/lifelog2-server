package com.dodopipe.lifelog;

import com.dodopipe.lifelog.config.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.TimeZone;

@Configuration
@EnableAutoConfiguration(exclude = {})
@ComponentScan
@Import({CoreConfiguration.class,
         MVCConfiguration.class,
         RestSecurityConfiguration.class,
         DataSourceConfiguration.class,
         JPAConfiguration.class,
         RepositoryConfiguration.class
        })
public class LifelogApplication {

    public static void main(String[] args) {

        TimeZone.setDefault(TimeZone.getTimeZone("Etc/UTC"));
        SpringApplication.run(LifelogApplication.class,
                              args);
    }


}

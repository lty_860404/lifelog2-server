package com.dodopipe.lifelog.core.events.photo;

import com.dodopipe.lifelog.core.domain.PhotoDetail;
import com.dodopipe.lifelog.core.events.ViewedEvent;

/**
 * @author Henry Yan
 */
public class PhotoViewedEvent extends ViewedEvent {
    private PhotoDetail photoDetail;

    public PhotoViewedEvent(PhotoDetail photoDetail) {

        this.photoDetail = photoDetail;
    }

    public PhotoDetail getPhotoDetail() {

        return photoDetail;
    }
}




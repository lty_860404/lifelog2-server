package com.dodopipe.lifelog.persistence.adaptors;

import com.dodopipe.lifelog.core.domain.BlobObject;
import com.dodopipe.lifelog.core.events.file.FileViewEvent;
import com.dodopipe.lifelog.core.events.file.FileViewedEvent;
import com.dodopipe.lifelog.core.ports.persistence.FileEventHandler;
import com.dodopipe.lifelog.persistence.domain.MediaObject;
import com.dodopipe.lifelog.persistence.repositories.BlobRepository;

/**
 * @author Henry Yan
 */
public class FilePersistenceEventEventHandler
        implements FileEventHandler {

    private BlobRepository<MediaObject, String> blobRepo;

    public FilePersistenceEventEventHandler(BlobRepository<MediaObject, String> blobRepo) {

        this.blobRepo = blobRepo;
    }

    public FileViewedEvent viewFile(FileViewEvent event) {

        String fileKey = event.getFileKey();

        MediaObject entity = blobRepo.findById(fileKey);

        BlobObject file = new BlobObject(entity.getInputStream(),
                                         entity.getContentType(),
                                         entity.getLength());

        return new FileViewedEvent(file);
    }
}

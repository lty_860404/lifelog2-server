package com.dodopipe.lifelog.core.services.impl;

import com.dodopipe.lifelog.core.events.family.*;
import com.dodopipe.lifelog.core.ports.persistence.FamilyQueryEventHandler;
import com.dodopipe.lifelog.core.services.FamilyQueryService;
import com.dodopipe.lifelog.core.services.exception.FamilyMemberNotFoundException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByIdException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByUsernameException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by henryhome on 8/18/14.
 */
@Transactional(readOnly = true)
public class FamilyQueryServiceImpl implements FamilyQueryService {

    private FamilyQueryEventHandler familyQueryHandler;

    public FamilyQueryServiceImpl(FamilyQueryEventHandler familyQueryHandler) {

        this.familyQueryHandler = familyQueryHandler;
    }

    @Override
    public FamilyMembersViewedEvent viewAllFamilyMembers(FamilyMembersViewEvent event) {

        return familyQueryHandler.viewAllFamilyMembers(event);
    }

    @Override
    public FamilyInvitationsViewedEvent viewAllSentInvitations(FamilyInvitationsViewEvent event) {

        return familyQueryHandler.viewAllSentInvitations(event);
    }

    @Override
    public FamilyInvitationsViewedEvent viewAllReceivedInvitations(FamilyInvitationsViewEvent event) {

        return familyQueryHandler.viewAllReceivedInvitations(event);
    }

    @Override
    public FamilyRelationshipsViewedEvent viewAllFamilyRelationships(FamilyRelationshipsViewEvent event) {

        return familyQueryHandler.viewAllFamilyRelationships(event);
    }

    @Override
    public FamilyMemberDetailViewedEvent viewFamilyMemberDetail(FamilyMemberDetailViewEvent event)
            throws
            UserNotFoundByIdException,
            UserNotFoundByUsernameException,
            FamilyMemberNotFoundException {

        return familyQueryHandler.viewFamilyMemberDetail(event);
    }
}

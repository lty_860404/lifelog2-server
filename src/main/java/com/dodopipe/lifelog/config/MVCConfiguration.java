package com.dodopipe.lifelog.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.dodopipe.lifelog.rest.advisor",
                               "com.dodopipe.lifelog.rest.controller"})
public class MVCConfiguration {

}

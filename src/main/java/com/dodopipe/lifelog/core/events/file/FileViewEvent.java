package com.dodopipe.lifelog.core.events.file;

import com.dodopipe.lifelog.core.events.ViewEvent;

/**
 * Created by henryhome on 7/15/14.
 */
public class FileViewEvent extends ViewEvent {
    private String fileKey;

    public FileViewEvent(String fileKey) {
        this.fileKey = fileKey;
    }

    public String getFileKey() {

        return fileKey;
    }
}




package com.dodopipe.lifelog.persistence.domain.family;

import com.dodopipe.lifelog.core.domain.FamilyMember;
import com.dodopipe.lifelog.core.domain.FamilyMemberDetail;
import com.dodopipe.lifelog.core.domain.FamilyRelationshipStatus;
import com.dodopipe.lifelog.core.domain.FamilyRelationshipStatus.FamilyMemberStatusType;
import com.dodopipe.lifelog.persistence.domain.user.UserEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author yongwei
 */
@Entity(name = "FAMILY_MEMBER")
@IdClass(FamilyMemberPrimaryKey.class)
public class FamilyMemberEntity
        implements Serializable {

    private final static long serialVersionUID = 1L;

    @Id
    @Column(name = "FAMILY_ID",
            columnDefinition = "BIGINT(25)",
            nullable = false)
    public Long familyId;

    @Id
    @Column(name = "MEMBER_ID",
            columnDefinition = "BIGINT(25)",
            nullable = false)
    public Long memberId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FAMILY_ID",
                insertable = false,
                updatable = false)
    private FamilyEntity family;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MEMBER_ID",
                insertable = false,
                updatable = false)
    private UserEntity member;

    @Column(name = "STATUS",
            columnDefinition = "TINYINT DEFAULT 0")
    private int status;

    @Column(name = "RELATIONSHIP",
            columnDefinition = "VARCHAR(80)")
    private String relationship;

    public FamilyMemberPrimaryKey getPK() {

        return new FamilyMemberPrimaryKey(this.familyId,
                                          this.memberId);
    }

    public Long getFamilyId() {

        return familyId;
    }

    public void setFamilyId(Long familyId) {

        this.familyId = familyId;
    }

    public Long getMemberId() {

        return memberId;
    }

    public void setMemberId(Long memberId) {

        this.memberId = memberId;
    }

    public FamilyEntity getFamily() {

        return this.family;
    }

    public void setFamily(FamilyEntity family) {

        this.family = family;
        if (family != null) {
            this.familyId = family.getId();
        }
    }

    public UserEntity getMember() {

        return member;
    }

    public void setMember(UserEntity member) {

        this.member = member;
        if (member != null) {
            this.memberId = member.getId();
        }
    }

    public int getStatus() {

        return this.status;
    }

    public void setStatus(int status) {

        this.status = status;
    }

    public String getRelationship() {

        return relationship;
    }

    public void setRelationship(String relationship) {

        this.relationship = relationship;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FamilyMemberEntity that = (FamilyMemberEntity) o;

        if (familyId != null ? !familyId.equals(that.familyId) : that.familyId != null) {
            return false;
        }
        if (memberId != null ? !memberId.equals(that.memberId) : that.memberId != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {

        int result = familyId != null ? familyId.hashCode() : 0;
        result = 31 * result + (memberId != null ? memberId.hashCode() : 0);
        return result;
    }

    public FamilyMemberDetail toFamilyMemberDetail() {

        String userGender = null;

        if (getMember().getGender() != null) {
            userGender = getMember().getGender()
                                    .toString();
        }

        return new FamilyMemberDetail(getMember().getUsername(),
                                      getMember().getPhone(),
                                      getMember().getNickname(),
                                      userGender,
                                      getMember().getSignature(),
                                      getMember().getAvatarFilePath(),
                                      getMember().getLangCode(),
                                      getRelationship(),
                                      getStatus());
    }

    public FamilyMember toFamilyMember() {

        return new FamilyMember(this.familyId,
                                this.memberId,
                                this.getMember()
                                    .getUsername(),
                                this.getMember()
                                    .getNickname(),
                                this.getMember()
                                    .getAvatarFilePath(),
                                this.getRelationship(),
                                new FamilyRelationshipStatus(FamilyMemberStatusType.values()[this.status]));
    }
}

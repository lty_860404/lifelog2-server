package com.dodopipe.lifelog.core.events.auth;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Henry Yan
 */
public class UserSignedInEvent {

    private String accessId;
    private String secretKey;

    public UserSignedInEvent(String accessId,
                             String secretKey) {

        this.accessId = accessId;
        this.secretKey = secretKey;
    }

    public String getAccessId() {

        return accessId;
    }

    public String getSecretKey() {

        return secretKey;
    }

    public Map<String, String> toMap() {

        Map<String, String> map = new HashMap<>();
        map.put("accessId", accessId);
        map.put("secretKey", secretKey);
        return map;
    }
}

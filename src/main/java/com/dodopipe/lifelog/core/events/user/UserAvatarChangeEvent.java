package com.dodopipe.lifelog.core.events.user;

import com.dodopipe.lifelog.core.domain.BlobObject;

/**
 * Created by henryhome on 8/6/14.
 */
public class UserAvatarChangeEvent {

    private Long userId;
    private BlobObject avatarFileData;

    public UserAvatarChangeEvent(Long userId, BlobObject avatarFileData) {
        this.userId = userId;
        this.avatarFileData = avatarFileData;
    }

    public Long getUserId() {

        return userId;
    }

    public BlobObject getAvatarFileData() {

        return avatarFileData;
    }
}

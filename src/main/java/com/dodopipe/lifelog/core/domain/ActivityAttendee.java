package com.dodopipe.lifelog.core.domain;

/**
 * Created by henryhome on 7/21/14.
 */
public class ActivityAttendee {

    private Long attendeeId;
    private String attendeeName;
    private String attendeePhone;
    private String attendeeAvatarFileKey;
    private Boolean host;
    private ActivityAttendeeStatus status;

    public ActivityAttendee() {

    }

    public ActivityAttendee(Long attendeeId,
                            String attendeeName,
                            String attendeePhone,
                            String attendeeAvatarFileKey,
                            Boolean host,
                            ActivityAttendeeStatus status) {

        this.attendeeId = attendeeId;
        this.attendeeName = attendeeName;
        this.attendeePhone = attendeePhone;
        this.attendeeAvatarFileKey = attendeeAvatarFileKey;
        this.host = host;
        this.status = status;
    }

    public Long getAttendeeId() {

        return attendeeId;
    }

    public String getAttendeeName() {

        return attendeeName;
    }

    public String getAttendeePhone() {

        return attendeePhone;
    }

    public String getAttendeeAvatarFileKey() {

        return attendeeAvatarFileKey;
    }

    public Boolean getHost() {

        return host;
    }

    public ActivityAttendeeStatus getStatus() {

        return status;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof ActivityAttendee)) {
            return false;
        }

        ActivityAttendee that = (ActivityAttendee) o;

        if (!attendeeId.equals(that.attendeeId)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {

        return attendeeId.hashCode();
    }
}




package com.dodopipe.lifelog.persistence.adaptors;

import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.dodopipe.lifelog.core.domain.BlobObject;
import com.dodopipe.lifelog.core.domain.PhotoComment;
import com.dodopipe.lifelog.core.domain.PhotoFileData;
import com.dodopipe.lifelog.core.domain.PhotoLike;
import com.dodopipe.lifelog.core.events.photo.*;
import com.dodopipe.lifelog.core.ports.persistence.PhotoCommandEventHandler;
import com.dodopipe.lifelog.core.services.exception.*;
import com.dodopipe.lifelog.core.services.utils.BlobObjectUtil;
import com.dodopipe.lifelog.persistence.domain.MediaObject;
import com.dodopipe.lifelog.persistence.domain.activity.ActivityEntity;
import com.dodopipe.lifelog.persistence.domain.photo.PhotoCommentEntity;
import com.dodopipe.lifelog.persistence.domain.photo.PhotoEntity;
import com.dodopipe.lifelog.persistence.domain.photo.PhotoLikeEntity;
import com.dodopipe.lifelog.persistence.domain.photo.PhotoLikePrimaryKey;
import com.dodopipe.lifelog.persistence.domain.user.UserEntity;
import com.dodopipe.lifelog.persistence.domain.user.UserTagEntity;
import com.dodopipe.lifelog.persistence.repositories.BlobRepository;
import com.dodopipe.lifelog.persistence.repositories.photo.PhotoCommentRepository;
import com.dodopipe.lifelog.persistence.repositories.photo.PhotoLikeRepository;
import com.dodopipe.lifelog.persistence.repositories.photo.PhotoRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserRepository;
import com.dodopipe.lifelog.persistence.utils.PhotoUtil;
import org.apache.commons.io.IOUtils;
import org.springframework.dao.EmptyResultDataAccessException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.*;

public class PhotoCommandPersistenceEventHandler
        implements PhotoCommandEventHandler {

    private final PhotoRepository photoRepo;
    private final PhotoCommentRepository photoCommentRepo;
    private final PhotoLikeRepository photoLikeRepo;
    private final BlobRepository<MediaObject, String> blobRepo;
    private final UserRepository userRepo;

    public PhotoCommandPersistenceEventHandler(PhotoRepository photoRepo,
                                               PhotoCommentRepository photoCommentRepo,
                                               PhotoLikeRepository photoLikeRepo,
                                               UserRepository userRepo,
                                               BlobRepository<MediaObject, String> blobRepo) {

        this.photoRepo = photoRepo;
        this.photoCommentRepo = photoCommentRepo;
        this.photoLikeRepo = photoLikeRepo;
        this.blobRepo = blobRepo;
        this.userRepo = userRepo;
    }

    @Override
    public PhotoCreatedEvent uploadPhotos(PhotoCreateEvent event)
            throws
            InternalServerErrorException {

        PhotoFileData photoFileData = event.getPhotoFileData();
        PhotoEntity photoEntity = PhotoEntity.fromPhotoFileData(photoFileData);

        for (BlobObject blob : photoFileData.getPhotos()) {

            List<MediaObject> mediaObjects;
            try {
                mediaObjects = storePhotoAndThumbnail(blob,
                                                      blobRepo);
            } catch (IOException e) {
                throw new InternalServerErrorException(e);
            }

            photoEntity.addPhoto(mediaObjects.get(0)
                                             .getKey(),
                                 mediaObjects.get(0)
                                             .getContentType());


            photoEntity.addThumbnail(mediaObjects.get(1)
                                                 .getKey());
        }

        Calendar cal = GregorianCalendar.getInstance();
        Date currTime = cal.getTime();

        photoEntity.setUploadedTime(currTime);
        photoEntity.setLastModifiedTime(currTime);

        photoRepo.save(photoEntity);

        return new PhotoCreatedEvent(photoEntity.getId(),
                                     photoEntity.getCaption(),
                                     photoEntity.getPhotoMetadataAsList());
    }

    @Override
    public PhotoUpdatedEvent updatePhotoCaption(PhotoCaptionUpdateEvent event)
            throws
            PhotoNotFoundException {

        Long photoId = event.getPhotoId();
        String caption = event.getCaption();

        PhotoEntity photoEntity = photoRepo.findById(photoId);

        if (photoEntity == null) {
            throw new PhotoNotFoundException(photoId);
        }

        photoEntity.setCaption(caption);

        photoEntity.setLastModifiedTime(GregorianCalendar.getInstance()
                                                         .getTime());
        photoRepo.save(photoEntity);

        return new PhotoUpdatedEvent(true);
    }

    @Override
    public PhotoUpdatedEvent updatePhotoComment(PhotoCommentUpdateEvent event)
            throws
            PhotoNotFoundException {

        Long photoId = event.getPhotoId();
        PhotoComment photoComment = event.getPhotoComment();

        if (!photoRepo.exists(photoId)) {
            throw new PhotoNotFoundException(photoId);
        }

        UserTagEntity commentedUser = userRepo.findUserTagByUsername(photoComment.getUsername());

        if (commentedUser == null) {
            throw new UserNotFoundByUsernameException(photoComment.getUsername());
        }

        PhotoCommentEntity commentEntity = PhotoCommentEntity.fromPhotoComment(photoId,
                                                                               commentedUser.getId(),
                                                                               photoComment);
        photoCommentRepo.save(commentEntity);

        String commentInfoStr = PhotoUtil.photoCommentToString(photoComment);
        photoRepo.updatePhotoComment(commentInfoStr,
                                     GregorianCalendar.getInstance()
                                                      .getTime(),
                                     photoId);

        return new PhotoUpdatedEvent(true);
    }

    @Override
    public PhotoUpdatedEvent addPhotoLike(PhotoLikeAddEvent event)
            throws
            PhotoNotFoundException,
            UserNotFoundByUsernameException,
            InvalidPhotoLikeException {

        Long photoId = event.getPhotoId();
        PhotoLike photoLike = event.getPhotoLike();

        if (!photoRepo.exists(photoId)) {
            throw new PhotoNotFoundException(photoId);
        }

        UserEntity likedUser = userRepo.findByUsername(photoLike.getUsername());

        if (likedUser == null) {
            throw new UserNotFoundByUsernameException(photoLike.getUsername());
        }

        PhotoLikeEntity storedEntity = photoLikeRepo.findOne(new PhotoLikePrimaryKey(photoId,
                                                                                     likedUser.getId()));

        if (storedEntity != null) {
            throw new InvalidPhotoLikeException("The user with username = " + photoLike.getUsername()
                                                        + " has already liked the photo with id = "
                                                        + photoId);
        }

        PhotoLikeEntity insertedEntity = PhotoLikeEntity.fromPhotoLike(photoId,
                                                                       likedUser.getId(),
                                                                       true);
        photoLikeRepo.save(insertedEntity);

        String likeInfoStr = PhotoUtil.photoLikeToString(photoLike);
        photoRepo.updatePhotoLike(likeInfoStr,
                                  GregorianCalendar.getInstance()
                                                   .getTime(),
                                  photoId);

        return new PhotoUpdatedEvent(true);
    }

    @Override
    public PhotoUpdatedEvent removePhotoLike(PhotoLikeRemoveEvent event)
            throws
            PhotoLikeNotFoundException {

        Long photoId = event.getPhotoId();
        Long userId = event.getUserId();

        try {
            photoLikeRepo.delete(new PhotoLikePrimaryKey(photoId,
                                                         userId));
        } catch (EmptyResultDataAccessException e) {
            throw new PhotoLikeNotFoundException(photoId,
                                                 userId,
                                                 e);
        }

        return new PhotoUpdatedEvent(true);
    }

    @Override
    public PhotoUpdatedEvent bindActivities(BoundActivitiesChangeEvent event)
            throws
            PhotoNotFoundException {

        Long photoId = event.getPhotoId();
        PhotoEntity photoEntity = photoRepo.findById(photoId);

        if (photoEntity == null) {
            throw new PhotoNotFoundException(photoId);
        }

        List<Long> activityIds = event.getActivityIds();

        Collection<ActivityEntity> currActivities = photoEntity.getActivities();

        for (Long activityId : activityIds) {
            ActivityEntity activity = new ActivityEntity();
            activity.setId(activityId);
            currActivities.add(activity);
        }

        photoEntity.setActivities(currActivities);

        photoRepo.save(photoEntity);

        return new PhotoUpdatedEvent(true);
    }

    @Override
    public PhotoUpdatedEvent detachActivities(BoundActivitiesChangeEvent event)
            throws
            PhotoNotFoundException {

        Long photoId = event.getPhotoId();
        PhotoEntity photoEntity = photoRepo.findById(photoId);

        if (photoEntity == null) {
            throw new PhotoNotFoundException(photoId);
        }

        List<Long> activityIds = event.getActivityIds();

        Collection<ActivityEntity> currActivities = photoEntity.getActivities();

        for (ActivityEntity activity : currActivities) {
            if (activityIds.contains(activity.getId())) {
                currActivities.remove(activity);
            }
        }

        photoEntity.setActivities(currActivities);

        photoRepo.save(photoEntity);

        return new PhotoUpdatedEvent(true);
    }

    @Override
    public PhotoDeletedEvent deletePhoto(PhotoDeleteEvent event)
            throws
            PhotoNotFoundException {

        try {
            photoRepo.delete(event.getPhotoId());
        } catch (EmptyResultDataAccessException e) {
            throw new PhotoNotFoundException(event.getPhotoId(),
                                             e);
        }

        return new PhotoDeletedEvent(true);
    }

    private List<MediaObject> storePhotoAndThumbnail(BlobObject blob,
                                                     BlobRepository<MediaObject, String> blobRepo)
            throws
            IOException,
            AmazonS3Exception {

        byte[] photoData;

        try {
            photoData = IOUtils.toByteArray(blob.getInputStream());
        } finally {
            blob.getInputStream()
                .close();
            blob.setInputStream(null);
        }

        byte[] thumbnailData = BlobObjectUtil.convertToThumbnail(photoData);

        MediaObject photo = blobRepo.save(new MediaObject(new ByteArrayInputStream(photoData),
                                                          blob.getContentType(),
                                                          blob.getLength()));

        MediaObject thumbnail = blobRepo.save(new MediaObject(new ByteArrayInputStream(thumbnailData),
                                                              blob.getContentType(),
                                                              thumbnailData.length));
        List<MediaObject> mediaObjects = new ArrayList<>();

        mediaObjects.add(photo);
        mediaObjects.add(thumbnail);

        return mediaObjects;
    }
}




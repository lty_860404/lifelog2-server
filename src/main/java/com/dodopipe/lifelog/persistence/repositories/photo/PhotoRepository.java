package com.dodopipe.lifelog.persistence.repositories.photo;

import com.dodopipe.lifelog.persistence.domain.photo.PhotoDetailEntity;
import com.dodopipe.lifelog.persistence.domain.photo.PhotoEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

/**
 * @author Henry Yan
 */
public interface PhotoRepository extends JpaRepository<PhotoEntity, Long> {

    PhotoEntity findById(Long photoId);

    @Query(value = "SELECT DISTINCT NEW com.dodopipe.lifelog.persistence.domain.photo.PhotoDetailEntity(p, " +
            "o.username, o.nickname, o.avatarFilePath, pl.liked) " +
            "FROM PHOTO p " +
            "LEFT JOIN p.owner o " +
            "LEFT JOIN p.activities act " +
            "LEFT JOIN act.attendees att " +
            "LEFT JOIN FAMILY_MEMBER fm " +
            "ON p.ownerId = fm.memberId " +
            "LEFT JOIN PHOTO_LIKE pl " +
            "ON p.id = pl.photoId " +
            "AND pl.likingId = ?1 " +
            "WHERE fm.familyId = ?2 " +
            "OR att.attendeeId = ?1")
    List<PhotoDetailEntity> findAllPhotosForUser(Long userId, Long familyId, Pageable pageable);

    @Query(value = "SELECT DISTINCT NEW com.dodopipe.lifelog.persistence.domain.photo.PhotoDetailEntity(p, " +
            "o.username, o.nickname, o.avatarFilePath, pl.liked) " +
            "FROM PHOTO p " +
            "LEFT JOIN p.owner o " +
            "LEFT JOIN p.activities act " +
            "LEFT JOIN act.attendees att " +
            "LEFT JOIN FAMILY_MEMBER fm " +
            "ON p.ownerId = fm.memberId " +
            "LEFT JOIN PHOTO_LIKE pl " +
            "ON p.id = pl.photoId " +
            "AND pl.likingId = ?1 " +
            "WHERE fm.familyId = ?2 " +
            "OR att.attendeeId = ?1")
    List<PhotoDetailEntity> findAllPhotosForUser(Long userId, Long familyId, Sort sort);

    @Query(value = "SELECT DISTINCT NEW com.dodopipe.lifelog.persistence.domain.photo.PhotoDetailEntity(p, " +
            "o.username, o.nickname, o.avatarFilePath, pl.liked) " +
            "FROM PHOTO p " +
            "LEFT JOIN p.owner o " +
            "LEFT JOIN p.activities act " +
            "LEFT JOIN PHOTO_LIKE pl " +
            "ON pl.photoId = p.id " +
            "AND pl.likingId = ?1 " +
            "WHERE act.id = ?2")
    List<PhotoDetailEntity> findPhotosByActivity(Long userId, Long activityId, Pageable pageable);

    @Query(value = "SELECT DISTINCT NEW com.dodopipe.lifelog.persistence.domain.photo.PhotoDetailEntity(p, " +
            "o.username, o.nickname, o.avatarFilePath, pl.liked) " +
            "FROM PHOTO p " +
            "LEFT JOIN p.owner o " +
            "LEFT JOIN p.activities act " +
            "LEFT JOIN PHOTO_LIKE pl " +
            "ON pl.photoId = p.id " +
            "AND pl.likingId = ?1 " +
            "WHERE act.id = ?2")
    List<PhotoDetailEntity> findPhotosByActivity(Long userId, Long activityId, Sort sort);

    @Query(value = "SELECT DISTINCT NEW com.dodopipe.lifelog.persistence.domain.photo.PhotoDetailEntity(p, " +
            "o.username, o.nickname, o.avatarFilePath, pl.liked) " +
            "FROM PHOTO p " +
            "LEFT JOIN p.owner o " +
            "LEFT JOIN PHOTO_LIKE pl " +
            "ON pl.photoId = p.id " +
            "AND pl.likingId = ?1 " +
            "WHERE p.id = ?2")
    PhotoDetailEntity findPhotoById(Long userId, Long photoId);

    @Modifying
    @Query(value = "update PHOTO p set p.latestComments = concat(?1, p.latestComments), p.lastModifiedTime = ?2 WHERE p.id = ?3")
    int updatePhotoComment(String photoCommentStr, Date lastModifiedTime, Long photoId);

    @Modifying
    @Query(value = "update PHOTO p set p.latestLikingNames = concat(?1, p.latestLikingNames), p.lastModifiedTime = ?2 WHERE p.id = ?3")
    int updatePhotoLike(String photoLikeStr, Date lastModifiedTime, Long photoId);
}




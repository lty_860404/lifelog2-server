package com.dodopipe.lifelog.persistence.adaptors;

import com.dodopipe.lifelog.config.*;
import com.dodopipe.lifelog.core.events.activity.*;
import com.dodopipe.lifelog.core.ports.persistence.ActivityCommandEventHandler;
import com.dodopipe.lifelog.persistence.repositories.activity.ActivityAttendeeRepository;
import com.dodopipe.lifelog.persistence.repositories.activity.ActivityRepository;
import com.dodopipe.lifelog.persistence.repositories.family.ExternalInvitationRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserRepository;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static com.dodopipe.lifelog.persistence.adaptors.fixture.EventHandlerDataFixture.*;
import static org.junit.Assert.*;

/**
 * @author Henry Yan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPAConfiguration.class,
                                 DataSourceConfiguration.class,
                                 RepositoryConfiguration.class,
                                 DBUnitDataSourceConfiguration.class})
@ActiveProfiles(resolver = LifelogActiveProfilesResolver.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class
                        })
@DatabaseSetup("DB_SETUP.xml")
public class ActivityCommandPersistenceEventHandlerTest {

    @Autowired
    private ActivityRepository activityRepo;

    @Autowired
    private ActivityAttendeeRepository activityAttendeeRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private ExternalInvitationRepository externalInvitationRepo;

    private ActivityCommandEventHandler handler;

    @Before
    public void setUp() {

        handler = new ActivityCommandPersistenceEventHandler(activityRepo,
                                                             activityAttendeeRepo,
                                                             userRepo,
                                                             externalInvitationRepo);
    }

    @Test
    public void testCreateWeddingActivityCorrectly()
            throws
            Exception {

        ActivityCreateEvent event = generateActivityCreateEvent();
        ActivityCreatedEvent result = handler.createWeddingActivity(event);
        assertNotNull(result.getActivity());
    }

    @Test
    public void testUpdateWeddingActivityCorrectly()
            throws
            Exception {

        ActivityUpdateEvent event = generateActivityUpdateEvent();
        ActivityUpdatedEvent result = handler.updateWeddingActivity(event);
        assertTrue(result.isSuccess());
    }

    @Test
    public void testDeleteWeddingActivityCorrectly() {

        ActivityDeleteEvent event = generateActivityDeleteEvent();
        ActivityDeletedEvent result = handler.deleteActivity(event);
        assertTrue(result.isSuccess());
    }
}

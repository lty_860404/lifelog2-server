package com.dodopipe.lifelog.core.services.impl;

import com.dodopipe.lifelog.core.events.photo.*;
import com.dodopipe.lifelog.core.ports.persistence.PhotoQueryEventHandler;
import com.dodopipe.lifelog.core.services.PhotoQueryService;
import com.dodopipe.lifelog.core.services.exception.PhotoNotFoundException;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public class PhotoQueryServiceImpl
        implements PhotoQueryService {

    private PhotoQueryEventHandler handler;

    public PhotoQueryServiceImpl(PhotoQueryEventHandler handler) {

        this.handler = handler;
    }

    @Override
    public PhotosViewedEvent viewAllPhotos(PhotosViewEvent event) {

        return handler.viewAllPhotos(event);
    }

    @Override
    public PhotosViewedEvent viewPhotosByActivity(PhotosViewEvent event) {

        return handler.viewPhotosByActivity(event);
    }

    @Override
    public PhotoViewedEvent viewPhoto(PhotoViewEvent event)
            throws
            PhotoNotFoundException {

        return handler.viewPhoto(event);
    }

    @Override
    public PhotoCommentsViewedEvent viewAllPhotoComments(PhotoCommentsViewEvent event) {

        return handler.viewAllPhotoComments(event);
    }
}

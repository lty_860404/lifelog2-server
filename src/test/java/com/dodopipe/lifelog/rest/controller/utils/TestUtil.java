package com.dodopipe.lifelog.rest.controller.utils;

import com.dodopipe.lifelog.rest.advisor.RestResponseEntityExceptionHandler;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.method.annotation.ExceptionHandlerMethodResolver;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by henryhome on 6/27/14.
 */
public class TestUtil {
    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
                                                                        MediaType.APPLICATION_JSON.getSubtype(),
                                                                        Charset.forName("utf8"));

    public static byte[] convertObjectToJsonBytes(Object object) throws
                                                                 IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }

    public static List<HandlerExceptionResolver> createExceptionResolver() {

        List<HandlerExceptionResolver> handlers = new ArrayList<>();
        ExceptionHandlerExceptionResolver exceptionResolver = new ExceptionHandlerExceptionResolver() {

            protected ServletInvocableHandlerMethod getExceptionHandlerMethod(HandlerMethod handlerMethod,
                                                                              Exception exception) {

                Method method =
                        new ExceptionHandlerMethodResolver(RestResponseEntityExceptionHandler.class).resolveMethod(exception);
                return new ServletInvocableHandlerMethod(new RestResponseEntityExceptionHandler(),
                                                         method);
            }
        };

        exceptionResolver.setMessageConverters(Arrays.asList(new HttpMessageConverter<?>[]{new MappingJackson2HttpMessageConverter()}));
        exceptionResolver.afterPropertiesSet();
        //handlers.add(exceptionResolver);
        handlers.add(new DefaultHandlerExceptionResolver());
        handlers.add(exceptionResolver);
        return handlers;
    }
}

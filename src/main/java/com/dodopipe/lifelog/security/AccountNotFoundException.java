package com.dodopipe.lifelog.security;

import org.springframework.security.core.AuthenticationException;

/**
 * Created by henryhome on 7/28/14.
 */
public class AccountNotFoundException extends AuthenticationException {

    public AccountNotFoundException(String message) {

        super(message);
    }
}

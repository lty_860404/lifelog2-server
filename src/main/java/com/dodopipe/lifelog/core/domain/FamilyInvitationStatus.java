package com.dodopipe.lifelog.core.domain;

import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;

/**
 * @author Henry Yan
 */
public enum FamilyInvitationStatus {
    PENDING,
    ACCEPT,
    DENY;

    public static FamilyInvitationStatus[] all = FamilyInvitationStatus.values();
    public static FamilyInvitationStatus fromInt(int n) throws InternalServerErrorException {
        if (n < all.length) {
            return all[n];
        }
        else {
            throw new InternalServerErrorException(
                    "Convert from int to Status failed: the value of the integer is beyond the range of the enumeration");
        }
    }
}

package com.dodopipe.lifelog.rest.controller.modules.photo;

import com.dodopipe.lifelog.core.domain.PhotoComment;
import com.dodopipe.lifelog.core.events.photo.PhotoCommentUpdateEvent;
import com.dodopipe.lifelog.core.events.photo.PhotoUpdatedEvent;
import com.dodopipe.lifelog.core.services.PhotoCommandService;
import com.dodopipe.lifelog.rest.controller.PhotoCommandController;
import com.dodopipe.lifelog.rest.controller.utils.TestUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Henry Yan
 */
public class UpdatePhotoCommentTest {
    MockMvc mockMvc;

    @InjectMocks
    PhotoCommandController controller;

    @Mock
    PhotoCommandService photoCommandService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                                  .build();
    }

    @Test
    public void updatePhotoComment() throws Exception {
        PhotoComment photoComment = new PhotoComment("abcde", "that poor student", "Do what you want");

        Long photoId = 80L;
        when(photoCommandService.updatePhotoComment(any(PhotoCommentUpdateEvent.class)))
                         .thenReturn(new PhotoUpdatedEvent(true));

        mockMvc.perform(post("/photos/{photo_id}/comment", photoId)
                        .contentType(TestUtil.APPLICATION_JSON_UTF8)
                        .content(TestUtil.convertObjectToJsonBytes(photoComment)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value(true));

        verify(photoCommandService, times(1)).updatePhotoComment(any(PhotoCommentUpdateEvent.class));
        verifyNoMoreInteractions(photoCommandService);

    }
}

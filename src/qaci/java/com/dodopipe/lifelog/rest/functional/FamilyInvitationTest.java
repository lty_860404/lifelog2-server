package com.dodopipe.lifelog.rest.functional;

import com.dodopipe.lifelog.LifelogApplication;
import com.dodopipe.lifelog.config.LifelogProfile;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import net.minidev.json.JSONArray;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManagerFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author tianyin.luo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {LifelogApplication.class})
@WebAppConfiguration
@IntegrationTest
@ActiveProfiles(LifelogProfile.QACI)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
@DatabaseSetup("DB_SETUP.xml")
@DatabaseTearDown(value = {"DB_SETUP.xml",
                           "DB_CLEAR.xml"},
                  type = DatabaseOperation.DELETE_ALL)
public class FamilyInvitationTest {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EntityManagerFactory emf;

    @After
    public void tearDown() {

        emf.getCache()
           .evictAll();
    }

    @Test
    public void shouldShowAllSentInvitationsCorrectly() {

        RestTemplate template = QaciTestFactory.signatureRestTemplateInstance();

        //test invite invitee by username
        template.setErrorHandler(QaciTestFactory.defaultErrorHandlerInstance());
        @SuppressWarnings("rawtypes")
        ResponseEntity<JSONArray> responseEntity =
                template.getForEntity("http://localhost:8080/family/sent_invitations",
                                      JSONArray.class);

        logger.info("==============> body:" + responseEntity.getBody());

        assertNotNull(responseEntity);
        assertEquals(2,
                     responseEntity.getBody()
                                   .size());
        assertEquals(200,
                     responseEntity.getStatusCode()
                                   .value());
    }

    @Test
    public void shouldShowAllReceivedInvitationsCorrectly() {

        RestTemplate template = QaciTestFactory.signatureRestTemplateInstance();

        //test invite invitee by username
        template.setErrorHandler(QaciTestFactory.defaultErrorHandlerInstance());
        @SuppressWarnings("rawtypes")
        ResponseEntity<JSONArray> responseEntity =
                template.getForEntity("http://localhost:8080/family/received_invitations",
                                      JSONArray.class);

        logger.info("==============> body:" + responseEntity.getBody());

        assertNotNull(responseEntity);
        assertEquals(1,
                     responseEntity.getBody()
                                   .size());
        assertEquals(200,
                     responseEntity.getStatusCode()
                                   .value());

    }

}

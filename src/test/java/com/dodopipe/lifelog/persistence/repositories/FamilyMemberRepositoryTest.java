package com.dodopipe.lifelog.persistence.repositories;

import com.dodopipe.lifelog.config.DBUnitDataSourceConfiguration;
import com.dodopipe.lifelog.config.DataSourceConfiguration;
import com.dodopipe.lifelog.config.JPAConfiguration;
import com.dodopipe.lifelog.config.LifelogActiveProfilesResolver;
import com.dodopipe.lifelog.persistence.domain.family.FamilyMemberEntity;
import com.dodopipe.lifelog.persistence.domain.family.FamilyMemberPrimaryKey;
import com.dodopipe.lifelog.persistence.repositories.family.FamilyMemberRepository;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

/**
 * @author tianyin.luo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPAConfiguration.class,
                                 DataSourceConfiguration.class,
                                 DBUnitDataSourceConfiguration.class})
@ActiveProfiles(resolver = LifelogActiveProfilesResolver.class)
@Transactional
@TransactionConfiguration(defaultRollback = true)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
@DatabaseSetup("DB_SETUP.xml")
public class FamilyMemberRepositoryTest {

    @Autowired
    private FamilyMemberRepository familyMemberRepository;

    @Test
    public void shouldfindOne() {

        FamilyMemberPrimaryKey pk = new FamilyMemberPrimaryKey(1000l,
                                               2000l);
        FamilyMemberEntity familyMember = familyMemberRepository.findOne(pk);
        assertEquals(Long.valueOf(2000l),
                     familyMember.getMember()
                                       .getId());
    }

    @Test
    public void shouleReadFamilyMemberByFamilyIdAndMemberUsername() {

        FamilyMemberEntity familyMember = familyMemberRepository.findOne(new FamilyMemberPrimaryKey(1000l,
                                                                                                    2000l));

        assertEquals(Long.valueOf(1000l),
                     familyMember.getFamily()
                                 .getId());
        assertEquals(2000,
                     familyMember.getMember()
                                 .getId()
                                 .intValue());
    }

    @Test
    public void shouldUpdateFamilyMember() {

        FamilyMemberPrimaryKey pk = new FamilyMemberPrimaryKey(1000l,
                                               2000l);
        FamilyMemberEntity familyMember = familyMemberRepository.findOne(pk);

        familyMember.setRelationship("BEST FRIEND");

        familyMemberRepository.saveAndFlush(familyMember);

        familyMember = familyMemberRepository.findOne(pk);

        assertEquals("BEST FRIEND",
                     familyMember.getRelationship());
    }
}

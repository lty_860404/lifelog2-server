package com.dodopipe.lifelog.rest.controller.modules.file;

import com.dodopipe.lifelog.core.events.file.FileViewEvent;
import com.dodopipe.lifelog.core.events.file.FileViewedEvent;
import com.dodopipe.lifelog.core.services.FileService;
import com.dodopipe.lifelog.rest.controller.FileController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;

import static com.dodopipe.lifelog.rest.controller.fixture.RestEventFixture.fileViewedEvent;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author Henry Yan
 */
public class ViewFileTest {
    MockMvc mockMvc;

    @InjectMocks
    FileController controller;

    @Mock
    FileService fileService;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                             .build();

    }

    @Test
    public void viewFileCorrectly()
            throws
            Exception {
        String fileKey = "test";

        FileViewedEvent event = fileViewedEvent();

        when(fileService.viewFile(any(FileViewEvent.class))).thenReturn(event);

        mockMvc.perform(get("/files/{file_key}", fileKey))
               .andExpect(status().isOk());
    }
}




package com.dodopipe.lifelog.core.events.family;

import com.dodopipe.lifelog.core.domain.FamilyRelationship;

import java.util.List;

/**
 * @author tianyin.luo
 */
public class FamilyRelationshipsViewedEvent {

    private final List<FamilyRelationship> relationships;

    public FamilyRelationshipsViewedEvent(List<FamilyRelationship> relationships) {

        this.relationships = relationships;
    }

    public List<FamilyRelationship> getRelationships() {

        return this.relationships;
    }

}

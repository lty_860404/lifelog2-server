package com.dodopipe.lifelog.security;

import org.springframework.security.core.AuthenticationException;

/**
 * @author yongwei
 */
public class RequestExpiredException
        extends AuthenticationException {

    public RequestExpiredException(String message) {

        super(message);
    }
}

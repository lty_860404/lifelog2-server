package com.dodopipe.lifelog.rest.controller;

import com.dodopipe.lifelog.core.services.FamilyCommandService;
import com.dodopipe.lifelog.rest.controller.fixture.UserAsPrincipal;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author tianyin.luo
 */
public class IllegalPrincipalExceptionTest {

    private MockMvc mockMvc;

    @InjectMocks
    private FamilyCommandController familyController;

    @Mock
    private FamilyCommandService familyCommandService;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);
        mockMvc = standaloneSetup(familyController).setMessageConverters(new MappingJackson2HttpMessageConverter())
                                                   .setHandlerExceptionResolvers(ExceptionHandlerMockUtil.exceptionResolversInstance())
                                                   .build();
    }

    @Test
    public void testInviteFamilyWithIllegalPrincipalException()
            throws
            Exception {

        this.mockMvc
                .perform(
                        MockMvcRequestBuilders.post("/family/invite")
                                              .param("invitee_username",
                                                     "John55555")
                                              .param("invitee_phone",
                                                     "+818012342222")
                                              .param("inviter_relationship",
                                                     "Friend")
                                              .param("inviter_lang_code",
                                                     "en")
                                              .principal(new UserAsPrincipal("abcde"))
                        )
                .andDo(print())
                .andExpect(status().isNetworkAuthenticationRequired());

    }
}

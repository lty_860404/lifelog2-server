package com.dodopipe.lifelog.rest.controller.modules.family;

import com.dodopipe.lifelog.core.events.family.FamilyInvitationsViewEvent;
import com.dodopipe.lifelog.core.events.family.FamilyMemberDetailViewEvent;
import com.dodopipe.lifelog.core.events.family.FamilyMembersViewEvent;
import com.dodopipe.lifelog.core.events.family.FamilyRelationshipsViewEvent;
import com.dodopipe.lifelog.core.services.FamilyQueryService;
import com.dodopipe.lifelog.rest.controller.FamilyQueryController;
import com.dodopipe.lifelog.rest.controller.fixture.UserAsPrincipal;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.dodopipe.lifelog.rest.controller.fixture.RestEventFixture.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author yongwei
 */
public class ViewFamilyMembersTest {

    MockMvc mockMvc;

    @InjectMocks
    FamilyQueryController controller;

    @Mock
    FamilyQueryService service;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);
        this.mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                                  .build();
    }

    @Test
    public void viewFamilyMember()
            throws
            Exception {

        when(service.viewAllFamilyMembers(any(FamilyMembersViewEvent.class)))
                .thenReturn(readAllFamilyMembersEvent());

        this.mockMvc
                .perform(
                        MockMvcRequestBuilders.get("/family/members")
                                              .principal(new UserAsPrincipal("1000"))
                        )
                .andExpect(status().isOk());

    }

    @Test
    public void viewAllSentInvitations()
            throws
            Exception {

        when(service.viewAllSentInvitations(any(FamilyInvitationsViewEvent.class))).thenReturn(familyInvitationsViewedEvent());

        this.mockMvc
                .perform(
                        MockMvcRequestBuilders.get("/family/sent_invitations")
                                              .principal(new UserAsPrincipal("1000"))
                        )
                .andDo(print())
                .andExpect(status().isOk());

    }


    @Test
    public void viewAllReceivedInvitations()
            throws
            Exception {

        when(service.viewAllReceivedInvitations(any(FamilyInvitationsViewEvent.class))).thenReturn(familyInvitationsViewedEvent());

        this.mockMvc
                .perform(
                        MockMvcRequestBuilders.get("/family/received_invitations")
                                              .principal(new UserAsPrincipal("1000"))
                        )
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void viewAllFamilyRelationships()
            throws
            Exception {

        when(service.viewAllFamilyRelationships(any(FamilyRelationshipsViewEvent.class))).thenReturn(familyRelationshipsViewedEvent());

        this.mockMvc.perform(MockMvcRequestBuilders.get("/family/relationships")
                                                   .param("lang_code",
                                                          "cn")
                                                   .principal(new UserAsPrincipal("1000")))
                    .andDo(print())
                    .andExpect(status().isOk());

    }

    @Test
    public void viewFamilyMemberDetail()
            throws
            Exception {

        when(service.viewFamilyMemberDetail(any(FamilyMemberDetailViewEvent.class))).thenReturn(readFamilyDetailEvent());

        this.mockMvc.perform(MockMvcRequestBuilders.get("/family/members/{memberUsername}",
                                                        "test")
                                                   .principal(new UserAsPrincipal("1000")))
                    .andDo(print())
                    .andExpect(status().isOk());
    }
}




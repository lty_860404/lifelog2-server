package com.dodopipe.lifelog.persistence.repositories.family;

import com.dodopipe.lifelog.persistence.domain.family.ExternalInvitationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author yongwei
 */
@Repository
public interface ExternalInvitationRepository
        extends JpaRepository<ExternalInvitationEntity, Long> {

    public List<ExternalInvitationEntity> findByInviteePhone(String phone);
}




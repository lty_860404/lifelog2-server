package com.dodopipe.lifelog.rest.controller.modules.family;

import com.dodopipe.lifelog.core.events.family.*;
import com.dodopipe.lifelog.core.services.FamilyCommandService;
import com.dodopipe.lifelog.rest.controller.FamilyCommandController;
import com.dodopipe.lifelog.rest.controller.fixture.UserAsPrincipal;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


import static com.dodopipe.lifelog.rest.controller.fixture.RestEventFixture.createFamilyMemberInvitedEvent;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * @author tianyin.luo
 */
public class PostFamilyMembersTest {

    MockMvc mockMvc;

    @InjectMocks
    FamilyCommandController controller;

    @Mock
    FamilyCommandService familyCommandService;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller).setMessageConverters(
                new MappingJackson2HttpMessageConverter())
                                                  .build();

    }

    @Test
    public void postFamilyInviteController()
            throws
            Exception {

        when(familyCommandService.inviteFamilyMember(any(FamilyMemberInviteEvent.class)))
                .thenReturn(createFamilyMemberInvitedEvent());

        this.mockMvc
                .perform(
                        MockMvcRequestBuilders.post("/family/invite")
                                              .param("invitee_username",
                                                     "John01")
                                              .param("invitee_phone",
                                                     "+818012342222")
                                              .param("inviter_relationship",
                                                     "Friend")
                                              .param("inviter_lang_code",
                                                     "en")
                                              .principal(new UserAsPrincipal("1000"))
                        )
                .andExpect(status().isOk());

    }

    @Test
    public void postAcceptFamilyInvitationController()
            throws
            Exception {

        when(familyCommandService.acceptFamilyInvitation(any(FamilyInvitationAcceptEvent.class)))
                .thenReturn(new FamilyInvitationAcceptedEvent(true));

        this.mockMvc.perform(
                MockMvcRequestBuilders.post("/family/accept")
                                      .param("invitation_id",
                                             "1000")
                                      .param("invitee_relationship",
                                             "BEST FRIEND")
                                      .param("invitee_lang_code",
                                             "en"))
                    .andExpect(status().isOk());

    }

    @Test
    public void postUpdateFamilyMemberController()
            throws
            Exception {

        when(familyCommandService.updateFamilyMember(any(FamilyMemberUpdateEvent.class)))
                .thenReturn(new FamilyMemberUpdatedEvent(true));

        this.mockMvc.perform(MockMvcRequestBuilders.post("/family/members/{member_username}",
                                                         "tester")
                                                   .param("relationship",
                                                          "student")
                                                   .param("lang_code",
                                                          "en")
                                                   .principal(new UserAsPrincipal("1000")))
                    .andDo(print())
                    .andExpect(status().isOk());

    }

    @Test
    public void deleteFamilyMemberContoller()
            throws
            Exception {

        when(familyCommandService.deleteFamilyMember(any(FamilyMemberRemoveEvent.class)))
                .thenReturn(new FamilyMemberRemovedEvent(true));

        this.mockMvc.perform(MockMvcRequestBuilders.delete("/family/members/{member_username}",
                                                           "test")
                                                   .principal(new UserAsPrincipal("1000")))
                    .andDo(print())
                    .andExpect(status().isOk());

    }
}




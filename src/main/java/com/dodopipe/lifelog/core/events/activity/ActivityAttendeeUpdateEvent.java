package com.dodopipe.lifelog.core.events.activity;

/**
 * Created by henryhome on 7/29/14.
 */
public class ActivityAttendeeUpdateEvent {

    private Long attendeeId;
    private String attendeeName;
    private String attendeePhone;

    public ActivityAttendeeUpdateEvent(Long attendeeId,
                                       String attendeeName,
                                       String attendeePhone) {

        this.attendeeId = attendeeId;
        this.attendeeName = attendeeName;
        this.attendeePhone = attendeePhone;
    }

    public Long getAttendeeId() {

        return attendeeId;
    }

    public String getAttendeeName() {

        return attendeeName;
    }

    public String getAttendeePhone() {

        return attendeePhone;
    }
}

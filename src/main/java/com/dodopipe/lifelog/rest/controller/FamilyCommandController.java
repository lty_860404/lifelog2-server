package com.dodopipe.lifelog.rest.controller;

import com.dodopipe.lifelog.core.events.family.*;
import com.dodopipe.lifelog.core.services.FamilyCommandService;
import com.dodopipe.lifelog.core.services.exception.*;
import com.dodopipe.lifelog.rest.error.IllegalPrincipalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tianyin.luo
 */
@Controller
@RequestMapping("/family")
public class FamilyCommandController {

    @Autowired
    private FamilyCommandService familyCommandService;

    @RequestMapping(method = RequestMethod.POST,
                    value = "/invite")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Map<String, Object> inviteFamilyMember(Principal principal,
                                                  @RequestParam(value = "invitee_username",
                                                                required = false) String inviteeUserName,
                                                  @RequestParam(value = "invitee_phone",
                                                                required = false) String inviteePhone,
                                                  @RequestParam(value = "invitee_nickname",
                                                                required = false) String inviteeNickName,
                                                  @RequestParam("inviter_relationship") String inviterRelationship,
                                                  @RequestParam("inviter_lang_code") String inviterLangCode)
            throws
            IllegalPrincipalException,
            UserNotFoundByIdException,
            InvalidFamilyInvitationException,
            FamilyMemberNotFoundException,
            InternalServerErrorException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);

        FamilyMemberInvitedEvent invitedEvent =
                familyCommandService.inviteFamilyMember(new FamilyMemberInviteEvent(userId,
                                                                                    inviteeUserName,
                                                                                    inviteeNickName,
                                                                                    inviteePhone,
                                                                                    inviterRelationship,
                                                                                    inviterLangCode));

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("isSuccess",
                      invitedEvent.isSuccess());

        if (invitedEvent.getInvitationId() > 0) {
            resultMap.put("invitationId",
                          invitedEvent.getInvitationId());
        }

        return resultMap;
    }

    @RequestMapping(method = RequestMethod.POST,
                    value = "/accept")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Map<String, Object> acceptFamilyInvitation(@RequestParam(value = "invitation_id") Long invitationId,
                                                      @RequestParam(value = "invitee_relationship")
                                                      String inviteeRelationship,
                                                      @RequestParam(value = "invitee_lang_code")
                                                      String inviteeLangCode)
            throws
            UserNotFoundByIdException,
            FamilyInvitationNotFoundException,
            FamilyMemberNotFoundException {

        FamilyInvitationAcceptedEvent acceptedEvent =
                familyCommandService.acceptFamilyInvitation(new FamilyInvitationAcceptEvent(invitationId,
                                                                                            inviteeRelationship,
                                                                                            inviteeLangCode));

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("isSuccess",
                      acceptedEvent.isSuccess());
        return resultMap;
    }

    @RequestMapping(method = RequestMethod.POST,
                    value = "/members/{member_username}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Map<String, Object> updateFamilyMember(Principal principal,
                                                  @PathVariable("member_username") String memberUsername,
                                                  @RequestParam("relationship") String relationship,
                                                  @RequestParam("lang_code") String langCode)
            throws
            IllegalPrincipalException,
            UserNotFoundByIdException,
            UserNotFoundByUsernameException,
            FamilyMemberNotFoundException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);

        FamilyMemberUpdatedEvent updatedEvent =
                familyCommandService.updateFamilyMember(new FamilyMemberUpdateEvent(userId,
                                                                                    memberUsername,
                                                                                    relationship,
                                                                                    langCode));

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("isSuccess",
                      updatedEvent.isSuccess());
        return resultMap;
    }

    @RequestMapping(method = RequestMethod.DELETE,
                    value = "/members/{member_username}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Map<String, Object> deleteFamilyMember(Principal principal,
                                                  @PathVariable("member_username") String memberUsername)
            throws
            IllegalPrincipalException {

        Long userId = PrincipalUtil.getUIDByPrincipal(principal);

        FamilyMemberRemovedEvent removedEvent =
                familyCommandService.deleteFamilyMember(new FamilyMemberRemoveEvent(userId,
                                                                                    memberUsername));

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("isSuccess",
                      removedEvent.isSuccess());
        return resultMap;
    }
}




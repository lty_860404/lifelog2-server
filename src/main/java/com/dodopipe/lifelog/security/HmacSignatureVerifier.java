package com.dodopipe.lifelog.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

public class HmacSignatureVerifier {

    private static Logger logger = LoggerFactory.getLogger(HmacSignatureVerifier.class);
    private final static String ALGORITHM = "HmacSHA1";
    private final static String CHARSET_NAME = "UTF-8";

    public static boolean validate(String strToVerify,
                                   String digest,
                                   String secretKey)
            throws
            Exception {

        String sign = hmacDigest(strToVerify,
                                 secretKey);

        if (logger.isDebugEnabled()) {
            logger.debug("Headers to signature:\n" +
                                 "{} \n" +
                                 "digest from client       :{} \n" +
                                 "digest computed in server:{}",
                         strToVerify,
                         digest,
                         sign);
        }

        return sign.equals(digest);
    }

    private static String hmacDigest(String str,
                                     String secretKey)
            throws
            Exception {

        SecretKey key = new SecretKeySpec(base64Decode(secretKey),
                                          ALGORITHM);
        Mac mac = Mac.getInstance(ALGORITHM);
        mac.init(key);
        byte[] rowHmac = mac.doFinal(str.getBytes(CHARSET_NAME));

        return base64Encode(rowHmac);

    }

    private static byte[] base64Decode(String secretKey) {

        return Base64.getDecoder()
                     .decode(secretKey);
    }

    private static String base64Encode(byte[] data) {

        return Base64.getEncoder()
                     .encodeToString(data);
    }

}

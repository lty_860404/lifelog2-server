package com.dodopipe.lifelog.core.events.auth;

/**
 * @author Henry Yan
 */
public class UserSignOutEvent {

    private Long userId;

    public UserSignOutEvent(Long userId) {

        this.userId = userId;
    }

    public Long getUserId() {

        return userId;
    }
}

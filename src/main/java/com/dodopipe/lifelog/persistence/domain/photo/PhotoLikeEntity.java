package com.dodopipe.lifelog.persistence.domain.photo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author Henry Yan
 */
@Entity(name = "PHOTO_LIKE")
@IdClass(PhotoLikePrimaryKey.class)
public class PhotoLikeEntity implements Serializable {

    private static final long serialVersionUID = 2343246452645L;

    @Id
    @Column(name = "PHOTO_ID",
            columnDefinition = "BIGINT(25)",
            nullable = false)
    private Long photoId;

    @Id
    @Column(name = "LIKING_ID",
            columnDefinition = "BIGINT(25)",
            nullable = false)
    private Long likingId;

    @Column(name = "LIKED_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date likedTime;

    @Column(name = "IS_LIKED",
            columnDefinition = "BOOLEAN")
    private Boolean liked;

    public PhotoLikeEntity() {

    }

    public PhotoLikeEntity(Long photoId,
                           Long likingId,
                           Date likedTime,
                           Boolean liked) {

        this.photoId = photoId;
        this.likingId = likingId;
        this.likedTime = likedTime;
        this.liked = liked;
    }

    public PhotoLikePrimaryKey getPrimaryKey() {

        return new PhotoLikePrimaryKey(this.photoId,
                                       this.likingId);
    }

    public static PhotoLikeEntity fromPhotoLike(Long photoId,
                                                Long likingId,
                                                Boolean liked) {

        Calendar cal = GregorianCalendar.getInstance();
        PhotoLikeEntity entity = new PhotoLikeEntity();
        entity.setPhotoId(photoId);
        entity.setLikingId(likingId);
        entity.setLikedTime(cal.getTime());
        entity.setLiked(liked);
        return entity;
    }

    public Long getPhotoId() {

        return photoId;
    }

    public void setPhotoId(Long photoId) {

        this.photoId = photoId;
    }

    public Long getLikingId() {

        return likingId;
    }

    public void setLikingId(Long likingId) {

        this.likingId = likingId;
    }

    public Date getLikedTime() {

        return likedTime;
    }

    public void setLikedTime(Date likedTime) {

        this.likedTime = likedTime;
    }

    public Boolean isLiked() {

        return liked;
    }

    public void setLiked(Boolean liked) {

        this.liked = liked;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof PhotoLikeEntity)) {
            return false;
        }

        PhotoLikeEntity that = (PhotoLikeEntity) o;

        if (!likingId.equals(that.likingId)) {
            return false;
        }
        if (!photoId.equals(that.photoId)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {

        int result = photoId.hashCode();
        result = 31 * result + likingId.hashCode();
        return result;
    }
}




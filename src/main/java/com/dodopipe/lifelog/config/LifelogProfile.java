package com.dodopipe.lifelog.config;

public interface LifelogProfile {

    String DEVE = "deve";
    String INTG = "intg";
    String QACI = "qaci";
    String PROD = "prod";

}

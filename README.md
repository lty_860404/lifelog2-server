Restful Service
===============

The projects introduced an implementation of restful service architecture based on spring4, inspired by the spring tutorial
[ Designing and implementing RESTful Web Services with Spring ](https://spring.io/guides/tutorials/rest/).

The core concepts of the implementation are

- [Separation of Concerns](http://weblogs.asp.net/arturtrosin/separation-of-concern-vs-single-responsibility-principle-soc-vs-srp)
- [Hexagnal Architecture](http://alistair.cockburn.us/Hexagonal+architecture)
- [Command Query Responsibility Segregation](http://martinfowler.com/bliki/CQRS.html)
- [Test Driven Development](http://en.wikipedia.org/wiki/Test-driven_development)

How to run and check the project
================================

The project is based on spring boot and build system named gradle.

Test the project
--------------------------------

**Unit test**
    
    ./gradlew test

Unit test will run all logic test with a in-memory h2 database, these tests will run fast and quickly.


**Integration test**
    
    ./gradlew intg
    
These tests will check whether system is run correctly integrated together, usually integrated with an 
database or some other modules and systems.
    
**Quality Assurance Continues Integration**

    ./gradlew qaci
    
These tests will run on a staging environment, to check whether the system will complete the user story
correctly.

Build the package
-----------------
    ./gradlew build
    
The above command, will run test and package the system to a single jar.
    
Run the application in command
------------------------------

    ./gradlew bootRun

The aboved command will run an embedded Tomcat Server and listen on localhost:8080.
 
Run the application as a separated jar
-------------------------------------

    java -server -Xms1024m -Xmx1024m -Dspring.profiles.active=prod -javaagent:lib/spring-instrument-4.0.5.RELEASE.jar -jar build/libs/lifelog2-server.jar
    


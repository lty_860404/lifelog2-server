package com.dodopipe.lifelog.core.domain;

import org.junit.Before;
import org.junit.Test;
import com.dodopipe.lifelog.core.domain.FamilyRelationshipStatus.RelationshipStatusType;
import static com.dodopipe.lifelog.core.domain.FamilyRelationshipStatus.FamilyMemberStatusType.*;
import static org.junit.Assert.*;

/**
 * @author tianyin.luo
 */
public class FamilyMemberStatusTypeTest {

    private FamilyRelationshipStatus validStatus;
    private FamilyRelationshipStatus removedStatus;
    private FamilyRelationshipStatus blockedStatus;

    @Before
    public void setUp(){
        validStatus = new FamilyRelationshipStatus(VALID);
        blockedStatus = new FamilyRelationshipStatus(BE_BLOCKED);
        removedStatus = new FamilyRelationshipStatus(REMOVED);
    }

    @Test
    public void testRemoveFamilyMember(){

        RelationshipStatusType relationshipStatusType =
                validStatus.updateStatusForRemovingRequest(validStatus.getOwnsideStatusType());
        assertEquals(RelationshipStatusType.RELATION_REMOVED,
                     relationshipStatusType);

        relationshipStatusType = blockedStatus.updateStatusForRemovingRequest(removedStatus.getOwnsideStatusType());
        assertEquals(RelationshipStatusType.RELATION_REMOVED_BOTH,
                     relationshipStatusType);
    }

    @Test
    public void testInviteFamilyMember(){

        RelationshipStatusType relationshipStatusType =
                removedStatus.updateStatusForInvitingRequest(blockedStatus.getOwnsideStatusType());
        assertEquals(RelationshipStatusType.RELATION_VALID,
                     relationshipStatusType);

        relationshipStatusType = blockedStatus.updateStatusForInvitingRequest(removedStatus.getOwnsideStatusType());
        assertEquals(RelationshipStatusType.RELATION_NOT_CONFIRMED,
                     relationshipStatusType);

        relationshipStatusType = removedStatus.updateStatusForInvitingRequest(removedStatus.getOwnsideStatusType());
        assertEquals(RelationshipStatusType.RELATION_NOT_CONFIRMED,
                     relationshipStatusType);
    }

    @Test
    public void testAcceptFamilyMember(){

        RelationshipStatusType relationshipStatusType =
                removedStatus.updateStatusForAcceptingRequest(blockedStatus.getOwnsideStatusType());
        assertEquals(RelationshipStatusType.RELATION_VALID,
                     relationshipStatusType);

        relationshipStatusType = removedStatus.updateStatusForAcceptingRequest(removedStatus.getOwnsideStatusType());
        assertEquals(RelationshipStatusType.RELATION_VALID,
                     relationshipStatusType);

    }
}




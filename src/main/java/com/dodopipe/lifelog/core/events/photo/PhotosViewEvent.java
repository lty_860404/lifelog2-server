package com.dodopipe.lifelog.core.events.photo;

import com.sun.glass.events.ViewEvent;

public class PhotosViewEvent
        extends ViewEvent {

    private Long activityId = null;
    private Long userId = null;
    private Integer pageNum = null;
    private Integer numPerPage = null;

    public PhotosViewEvent(Long userId,
                           Integer pageNum,
                           Integer numPerPage) {

        this.userId = userId;
        this.pageNum = pageNum;
        this.numPerPage = numPerPage;
    }

    public PhotosViewEvent(Long activityId,
                           Long userId,
                           Integer pageNum,
                           Integer numPerPage) {
        this.activityId = activityId;
        this.userId = userId;
        this.pageNum = pageNum;
        this.numPerPage = numPerPage;
    }

    public Long getActivityId() {

        return activityId;
    }

    public Long getUserId() {

        return userId;
    }

    public Integer getNumPerPage() {

        return numPerPage;
    }

    public Integer getPageNum() {

        return pageNum;
    }
}


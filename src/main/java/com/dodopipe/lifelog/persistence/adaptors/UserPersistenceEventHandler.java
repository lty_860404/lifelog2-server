package com.dodopipe.lifelog.persistence.adaptors;

import com.dodopipe.lifelog.core.domain.BlobObject;
import com.dodopipe.lifelog.core.domain.User;
import com.dodopipe.lifelog.core.domain.UserEditInfo;
import com.dodopipe.lifelog.core.events.auth.UserSignOutEvent;
import com.dodopipe.lifelog.core.events.auth.UserSignedOutEvent;
import com.dodopipe.lifelog.core.events.user.*;
import com.dodopipe.lifelog.core.ports.persistence.UserEventHandler;
import com.dodopipe.lifelog.core.services.exception.InternalServerErrorException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByIdException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByPhoneException;
import com.dodopipe.lifelog.core.services.exception.UserNotFoundByUsernameException;
import com.dodopipe.lifelog.persistence.domain.MLocalizationStringEntity;
import com.dodopipe.lifelog.persistence.domain.MediaObject;
import com.dodopipe.lifelog.persistence.domain.family.FamilyEntity;
import com.dodopipe.lifelog.persistence.domain.user.UserAccountEntity;
import com.dodopipe.lifelog.persistence.domain.user.UserEntity;
import com.dodopipe.lifelog.persistence.repositories.BlobRepository;
import com.dodopipe.lifelog.persistence.repositories.ML10NStringRepository;
import com.dodopipe.lifelog.persistence.repositories.family.FamilyRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserAccountRepository;
import com.dodopipe.lifelog.persistence.repositories.user.UserRepository;
import com.dodopipe.lifelog.security.AccountNotFoundException;
import org.springframework.dao.EmptyResultDataAccessException;

import java.security.NoSuchAlgorithmException;

/**
 * Created by henryhome on 7/29/14.
 */
public class UserPersistenceEventHandler implements UserEventHandler {

    private UserRepository userRepo;
    private UserAccountRepository authRepo;
    private FamilyRepository familyRepo;
    private BlobRepository<MediaObject, String> blobRepo;
    private ML10NStringRepository mRepo;

    public UserPersistenceEventHandler(UserRepository userRepo,
                                       UserAccountRepository authRepo,
                                       FamilyRepository familyRepo,
                                       BlobRepository<MediaObject, String> blobRepo,
                                       ML10NStringRepository mRepo) {

        this.userRepo = userRepo;
        this.authRepo = authRepo;
        this.familyRepo = familyRepo;
        this.blobRepo = blobRepo;
        this.mRepo = mRepo;
    }

    @Override
    public User getUserById(Long userId) {

        UserEntity userEntity = userRepo.findOne(userId);
        if (userEntity != null) {
            return userEntity.toUser();
        }

        return null;
    }

    @Override
    public User getUserByUsername(String username) {

        UserEntity userEntity;
        userEntity = userRepo.findByUsername(username);
        if (userEntity != null) {
            return userEntity.toUser();
        }

        return null;
    }

    @Override
    public User getUserByPhone(String phone) {

        UserEntity userEntity;
        userEntity = userRepo.findByPhone(phone);
        if (userEntity != null) {
            return userEntity.toUser();
        }

        return null;
    }

    @Override
    public UserSignedOutEvent signOut(UserSignOutEvent event)
            throws
            AccountNotFoundException {

        Long userId = event.getUserId();
        UserAccountEntity ua = authRepo.findByUserId(userId);

        if (ua == null) {
            throw new AccountNotFoundException("The account with user id = " + userId + " is not found");
        }

        ua.setAccountExpired(true);

        authRepo.save(ua);

        return new UserSignedOutEvent(true);
    }

    @Override
    public PasswordUpdatedEvent changePassword(PasswordUpdateEvent event)
            throws
            AccountNotFoundException,
            InternalServerErrorException {

        Long userId = event.getUserId();
        String password = event.getPassword();
        UserAccountEntity ua = authRepo.findByUserIdAndPassword(userId,
                                                                password);

        if (ua == null) {
            throw new AccountNotFoundException("The account with user id = " + userId
                                                       + " and password = " + password + " is not found");
        }

        try {
            ua.setPassword(UserAccountEntity.MD5Encode(password.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            throw new InternalServerErrorException(e);
        }

        authRepo.save(ua);

        return new PasswordUpdatedEvent(true);
    }

    @Override
    public UserCreatedEvent createUser(UserCreateEvent event) {

        UserEntity userEntity = new UserEntity();
        String defaultUsername = null;

        Boolean isUnique = false;

        while (!isUnique) {
            defaultUsername = UserEntity.generateDefaultUsername();
            UserEntity dupeUsername = userRepo.findByUsername(defaultUsername);

            if (dupeUsername != null) {
                continue;
            }

            isUnique = true;
        }

        userEntity.setUsername(defaultUsername);
        userEntity.setCountryCode(event.getCountryCode());
        userEntity.setPhone(event.getPhone());

        // Save the user so that we can get the id of this user, which will be used when saving self to family
        UserEntity savedEntity = userRepo.save(userEntity);

        FamilyEntity family = createDefaultFamily(userEntity.getUsername(),
                                                  event.getLangCode());
        family.addFamilyMember(savedEntity,
                               "me");

        savedEntity.setFamily(family);
        userRepo.save(savedEntity);

        return new UserCreatedEvent(savedEntity.toUser());
    }

    @Override
    public UserViewedEvent viewUser(UserViewEvent event)
            throws
            UserNotFoundByIdException,
            UserNotFoundByUsernameException,
            UserNotFoundByPhoneException {

        Long userId = event.getUserId();
        String username = event.getUsername();
        String phone = event.getPhone();

        UserEntity userEntity;

        if (username != null) {
            userEntity = userRepo.findByUsername(username);

            if (userEntity == null) {
                throw new UserNotFoundByUsernameException(username);
            }

            return new UserViewedEvent(userEntity.toUser());
        }

        if (phone != null) {
            userEntity = userRepo.findByPhone(phone);

            if (userEntity == null) {
                throw new UserNotFoundByPhoneException(phone);
            }

            return new UserViewedEvent(userEntity.toUser());
        }

        userEntity = userRepo.findById(userId);

        if (userEntity == null) {
            throw new UserNotFoundByIdException(userId);
        }

        return new UserViewedEvent(userEntity.toUser());
    }

    @Override
    public UserUpdatedEvent updateUser(UserUpdateEvent event)
            throws
            UserNotFoundByIdException,
            IllegalArgumentException {

        Long userId = event.getUserId();
        UserEditInfo userEditInfo = event.getUserEditInfo();

        UserEntity userEntity = userRepo.findById(userId);

        // If a text field received is null, it means the client does not want to update this field, but
        // if it is an empty string, it will be accepted
        if (userEntity == null) {
            throw new UserNotFoundByIdException(userId);
        }

        if (userEditInfo.getUsername() != null) {
            userEntity.setUsername(userEditInfo.getUsername());
        }

        if (userEditInfo.getNickname() != null) {
            userEntity.setNickname(userEditInfo.getNickname());
        }

        if (userEditInfo.getPhone() != null) {
            userEntity.setPhone(userEditInfo.getPhone());
        }

        if (userEditInfo.getGender() != null) {
            if ("Male".equalsIgnoreCase(userEditInfo.getGender())) {
                userEntity.setGender(UserEntity.Gender.Male);
            } else if ("Female".equalsIgnoreCase(userEditInfo.getGender())) {
                userEntity.setGender(UserEntity.Gender.Female);
            } else {
                throw new IllegalArgumentException("The gender can only be male or female");
            }
        }

        if (userEditInfo.getSignature() != null) {
            userEntity.setSignature(userEditInfo.getSignature());
        }

        if (userEditInfo.getAvatarFilePath() != null) {
            userEntity.setAvatarFilePath(userEditInfo.getAvatarFilePath());
        }

        if (userEditInfo.getLangCode() != null) {
            userEntity.setLangCode(userEditInfo.getLangCode());
        }

        userRepo.save(userEntity);

        return new UserUpdatedEvent(true);
    }

    @Override
    public UserDeletedEvent deleteUser(UserDeleteEvent event)
            throws
            UserNotFoundByIdException {

        try {
            userRepo.delete(event.getUserId());
        } catch (EmptyResultDataAccessException e) {
            throw new UserNotFoundByIdException(event.getUserId(),
                                                e);
        }

        return new UserDeletedEvent(true);
    }

    @Override
    public UserAvatarChangedEvent changeAvatar(UserAvatarChangeEvent event)
            throws
            UserNotFoundByIdException {

        Long userId = event.getUserId();
        BlobObject avatarFileData = event.getAvatarFileData();

        UserEntity userEntity = userRepo.findById(userId);

        if (userEntity == null) {
            throw new UserNotFoundByIdException(userId);
        }

        if (avatarFileData == null) {
            return new UserAvatarChangedEvent(true,
                                              null);
        }

        MediaObject avatarFileEntity = blobRepo.save(new MediaObject(avatarFileData.getInputStream(),
                                                                     avatarFileData.getContentType(),
                                                                     avatarFileData.getLength()));

        userEntity.setAvatarFilePath(avatarFileEntity.getKey());

        userRepo.save(userEntity);

        return new UserAvatarChangedEvent(true,
                                          avatarFileEntity.getKey());
    }

    private FamilyEntity createDefaultFamily(String username,
                                             String langCode) {

        String defaultName = mRepo.findByTypeAndKey(MLocalizationStringEntity.TypeCode.FAMILY_NAME.ordinal(),
                                                    MLocalizationStringEntity.StringKey.DEFAULT_FAMILY_NAME.toString(),
                                                    langCode);
        FamilyEntity family = new FamilyEntity();
        family.setName(username + defaultName);
        familyRepo.save(family);

        return family;
    }
}




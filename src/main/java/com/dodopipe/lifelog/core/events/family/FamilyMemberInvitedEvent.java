package com.dodopipe.lifelog.core.events.family;

import java.util.HashMap;

/**
 * @author yongwei
 */
public class FamilyMemberInvitedEvent {

    private final boolean isSuccess;

    private final Long invitationId;

    public FamilyMemberInvitedEvent(boolean isSuccess,Long invitationId) {

        this.isSuccess = isSuccess;
        this.invitationId = invitationId;
    }

    public boolean isSuccess() {

        return this.isSuccess;
    }

    public Long getInvitationId(){

        return this.invitationId;
    }
}

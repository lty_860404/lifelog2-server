package com.dodopipe.lifelog.rest.functional;

import com.dodopipe.lifelog.rest.fixture.SignatureHeaderInterceptor;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Collections;

/**
 * @author tianyin.luo
 */
public class QaciTestFactory {

    /**
     * return a restTemplate with signatureHeadInterceptor
     */
    public static RestTemplate signatureRestTemplateInstance() {

        RestTemplate template = new RestTemplate();
        SignatureHeaderInterceptor interceptor = new SignatureHeaderInterceptor();
        template.setInterceptors(Collections.<ClientHttpRequestInterceptor>singletonList(interceptor));

        return template;
    }

    public static ResponseErrorHandler defaultErrorHandlerInstance() {

        return new ResponseErrorHandler() {

            @Override
            public boolean hasError(ClientHttpResponse response)
                    throws
                    IOException {

                return response.getStatusCode() != HttpStatus.OK;
            }

            @Override
            public void handleError(ClientHttpResponse response)
                    throws
                    IOException {

                System.out.println("===========> status:" + response.getStatusCode());
                System.out.println("===========> status text:" + response.getStatusText());

            }

        };

    }

}

package com.dodopipe.lifelog.persistence.domain.family;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author yongwei
 */
@Entity(name = "EXTERNAL_INVITATION")
public class ExternalInvitationEntity
        implements Serializable {

    private final static long serialVersionUID = 1L;

    @Id
    @Column(name = "ID",
            columnDefinition = "BIGINT(25) NOT NULL AUTO_INCREMENT")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "INVITER_ID",
            columnDefinition = "BIGINT(25)")
    private Long inviterId;

    @Column(name = "INVITEE_PHONE",
            columnDefinition = "VARCHAR(80)")
    private String inviteePhone;

    @Column(name = "STATUS",
            columnDefinition = "TINYINT DEFAULT 0")
    private int status = 0;

    public ExternalInvitationEntity() {

    }

    public ExternalInvitationEntity(Long inviterId,
                                    String inviteePhone) {

        this.inviterId = inviterId;
        this.inviteePhone = inviteePhone;
    }

    public ExternalInvitationEntity(Long inviterId,
                                    String inviteePhone,
                                    int status) {

        this.inviterId = inviterId;
        this.inviteePhone = inviteePhone;
        this.status = status;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public Long getInviterId() {

        return inviterId;
    }

    public void setInviterId(Long inviterId) {

        this.inviterId = inviterId;
    }

    public String getInviteePhone() {

        return inviteePhone;
    }

    public void setInviteePhone(String inviteePhone) {

        this.inviteePhone = inviteePhone;
    }

    public int getStatus() {

        return status;
    }

    public void setStatus(int status) {

        this.status = status;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof ExternalInvitationEntity)) {
            return false;
        }

        ExternalInvitationEntity that = (ExternalInvitationEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {

        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {

        final StringBuffer sb = new StringBuffer("ExternalInvitation{");
        sb.append("id=")
          .append(id);
        sb.append(", inviterId=")
          .append(inviterId);
        sb.append(", inviteePhone='")
          .append(inviteePhone)
          .append('\'');
        sb.append(", status=")
          .append(status);
        sb.append('}');
        return sb.toString();
    }
}




package com.dodopipe.lifelog.persistence.domain;

import com.dodopipe.lifelog.core.domain.PhotoComment;
import com.dodopipe.lifelog.persistence.domain.photo.PhotoEntity;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.dodopipe.lifelog.persistence.domain.fixture.EntityDataFixture.addComment;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PhotoEntityTest {

    private PhotoEntity photoEntity;

    @Before
    public void setupOnce() {

        photoEntity = new PhotoEntity();

    }

    @Test
    public void testGetLatestComments() {

        addComment(photoEntity,
                   "username",
                   "nickName",
                   "some comment");
        List<PhotoComment> comments = photoEntity.getLatestCommentsAsList();

        assertTrue(comments.size() == 1);
        assertEquals(comments.get(0)
                             .getUsername(),
                     "username");
        assertEquals(comments.get(0)
                             .getNickname(),
                     "nickName");

    }

    @Test
    public void testGetLatestCommentsWithSpecialCharacters() {

        String username = "username@";
        String nickName = "nickName@";
        String comment = "some comment";
        addComment(photoEntity,
                   username,
                   nickName,
                   comment);
        List<PhotoComment> comments = photoEntity.getLatestCommentsAsList();
        assertTrue(comments.size() == 1);
        assertEquals(comments.get(0)
                             .getUsername(),
                     username);
        assertEquals(comments.get(0)
                             .getNickname(),
                     nickName);
        assertEquals(comments.get(0)
                             .getComment(),
                     comment);
    }
}

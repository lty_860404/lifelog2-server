package com.dodopipe.lifelog.core.events.family;

import com.dodopipe.lifelog.core.domain.FamilyInvitationDetail;

import java.util.List;

/**
 * @author tianyin.luo
 */
public class FamilyInvitationsViewedEvent {

    private List<FamilyInvitationDetail> invitations;

    public FamilyInvitationsViewedEvent(List<FamilyInvitationDetail> invitations) {

        this.invitations = invitations;
    }

    public List<FamilyInvitationDetail> getInvitations() {

        return invitations;
    }

    public void setInvitations(List<FamilyInvitationDetail> invitations) {

        this.invitations = invitations;
    }
}




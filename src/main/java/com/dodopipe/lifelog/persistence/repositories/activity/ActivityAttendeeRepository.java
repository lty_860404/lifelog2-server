package com.dodopipe.lifelog.persistence.repositories.activity;

import com.dodopipe.lifelog.persistence.domain.activity.ActivityAttendeeDetailEntity;
import com.dodopipe.lifelog.persistence.domain.activity.ActivityAttendeeEntity;
import com.dodopipe.lifelog.persistence.domain.activity.ActivityAttendeePrimaryKey;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by henryhome on 7/21/14.
 */
public interface ActivityAttendeeRepository extends JpaRepository<ActivityAttendeeEntity, ActivityAttendeePrimaryKey> {

    List<ActivityAttendeeEntity> findByAttendeePhone(String phone);

    @Query("SELECT NEW com.dodopipe.lifelog.persistence.domain.activity.ActivityAttendeeDetailEntity(aa, " +
           "u.avatarFilePath) " +
           "from ACTIVITY a " +
           "LEFT JOIN a.attendees aa " +
           "LEFT JOIN USER u " +
           "ON aa.attendeeId = u.id " +
           "WHERE a.dtype = 'WEDDING_ACTIVITY' AND a.id = ?1")
    List<ActivityAttendeeDetailEntity> findWeddingActivityAttendeesByActivityId(Long activityId, Pageable pageable);
}

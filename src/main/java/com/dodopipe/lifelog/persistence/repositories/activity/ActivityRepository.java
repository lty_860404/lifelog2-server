package com.dodopipe.lifelog.persistence.repositories.activity;

import com.dodopipe.lifelog.persistence.domain.activity.WeddingActivityDetailEntity;
import com.dodopipe.lifelog.persistence.domain.activity.ActivityEntity;
import com.dodopipe.lifelog.persistence.domain.activity.WeddingActivityEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @author Henry Yan
 */
@Repository
public interface ActivityRepository
        extends JpaRepository<ActivityEntity, Long> {

    ActivityEntity findById(Long activityId);

    @Query("SELECT NEW com.dodopipe.lifelog.persistence.domain.activity.ActivityDetailEntity(" +
                   "act, att.isHost) " +
                   "FROM ACTIVITY act " +
                   "LEFT JOIN act.attendees att " +
                   "WHERE att.attendeeId = ?1 " +
                   "AND act.endTime <= ?2")
    List<WeddingActivityDetailEntity> findActivitiesEndBeforeDateForUser(Long userId,
                                                                   Date currDate,
                                                                   Pageable pageable);

    @Query("SELECT NEW com.dodopipe.lifelog.persistence.domain.activity.ActivityDetailEntity(" +
                   "act, att.isHost) " +
                   "FROM ACTIVITY act " +
                   "LEFT JOIN act.attendees att " +
                   "WHERE att.attendeeId = ?1 " +
                   "AND act.endTime > ?2")
    List<WeddingActivityDetailEntity> findActivitiesEndAfterDateForUser(Long userId,
                                                                  Date currDate,
                                                                  Pageable pageable);

    @Query("SELECT act FROM ACTIVITY act " +
                   "WHERE act.dtype = 'WEDDING_ACTIVITY' " +
                   "AND act.id = ?1")
    WeddingActivityEntity findWeddingActivityById(Long activityId);

    @Query("SELECT act FROM ACTIVITY act " +
                   "LEFT JOIN act.attendees att " +
                   "WHERE act.dtype='WEDDING_ACTIVITY' " +
                   "AND act.id = ?1 " +
                   "AND att.attendeeId = ?2")
    WeddingActivityEntity findWeddingActivityByActivityIdAndAttendeeId(Long activityId,
                                                                       Long attendeeId);

    @Query("SELECT NEW com.dodopipe.lifelog.persistence.domain.activity.WeddingActivityDetailEntity(" +
                   "act, att.isHost) " +
                   "FROM ACTIVITY act " +
                   "LEFT JOIN act.attendees att " +
                   "WHERE att.attendeeId = ?1 " +
                   "AND act.dtype ='WEDDING_ACTIVITY' " +
                   "AND act.endTime <= ?2")
    List<WeddingActivityDetailEntity> findWeddingActivitiesEndBeforeDateForUser(Long userId,
                                                                          Date currDate,
                                                                          Pageable pageable);

    @Query("SELECT NEW com.dodopipe.lifelog.persistence.domain.activity.WeddingActivityDetailEntity(" +
                   "act, att.isHost) " +
                   "FROM ACTIVITY act " +
                   "LEFT JOIN act.attendees att " +
                   "WHERE att.attendeeId = ?1 " +
                   "AND act.dtype ='WEDDING_ACTIVITY' " +
                   "AND act.endTime > ?2")
    List<WeddingActivityDetailEntity> findWeddingActivitiesEndAfterDateForUser(Long userId,
                                                                         Date currDate,
                                                                         Pageable pageable);
}




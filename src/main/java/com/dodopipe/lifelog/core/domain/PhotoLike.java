package com.dodopipe.lifelog.core.domain;

public class PhotoLike {

    private String username;
    private String nickName;

    public PhotoLike() {

    }

    public PhotoLike(String username,
                     String nickName) {

        this.username = username;
        this.nickName = nickName;
    }

    public String getUsername() {

        return username;
    }

    public String getNickName() {

        return nickName;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof PhotoLike)) {
            return false;
        }

        PhotoLike photoLike = (PhotoLike) o;

        if (nickName != null ? !nickName.equals(photoLike.nickName) : photoLike.nickName != null) {
            return false;
        }
        if (username != null ? !username.equals(photoLike.username) : photoLike.username != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {

        int result = username != null ? username.hashCode() : 0;
        result = 31 * result + (nickName != null ? nickName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {

        final StringBuffer sb = new StringBuffer("PhotoLike{");
        sb.append("username='")
          .append(username)
          .append('\'');
        sb.append(", nickName='")
          .append(nickName)
          .append('\'');
        sb.append('}');
        return sb.toString();
    }
}
